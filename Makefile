# Compile all the VST Plugins

all: build


build:
	# Libs
	$(MAKE) -C libs/juce

	# Plugins
	$(MAKE) -C plugins/jackass
	$(MAKE) -C plugins/bitmangler/Linux
	$(MAKE) -C plugins/highlife/Linux
	$(MAKE) -C plugins/vex/Linux
	$(MAKE) -C plugins/wolpertinger/Linux
	$(MAKE) -C plugins/jive-distressor/Linux
	$(MAKE) -C plugins/jive-gate/Linux
	$(MAKE) -C plugins/jive-xsynth/Linux
	$(MAKE) -C plugins/juce_pitcher/Linux
	$(MAKE) -C plugins/tal-dub-3-juce/Linux
	$(MAKE) -C plugins/tal-elek7ro-juce/Linux
	$(MAKE) -C plugins/tal-filter-juce/Linux
	$(MAKE) -C plugins/tal-filter-2-juce/Linux
	$(MAKE) -C plugins/tal-reverb-juce/Linux
	$(MAKE) -C plugins/tal-reverb-2-juce/Linux
	$(MAKE) -C plugins/tal-reverb-3-juce/Linux
	$(MAKE) -C plugins/tal-vocoder-2-juce/Linux

clean:
	# Libs
	$(MAKE) clean -C libs/juce

	# Plugins
	$(MAKE) clean -C plugins/jackass
	$(MAKE) clean -C plugins/bitmangler/Linux
	$(MAKE) clean -C plugins/highlife/Linux
	$(MAKE) clean -C plugins/vex/Linux
	$(MAKE) clean -C plugins/wolpertinger/Linux
	$(MAKE) clean -C plugins/jive-distressor/Linux
	$(MAKE) clean -C plugins/jive-gate/Linux
	$(MAKE) clean -C plugins/jive-xsynth/Linux
	$(MAKE) clean -C plugins/juce_pitcher/Linux
	$(MAKE) clean -C plugins/tal-dub-3-juce/Linux
	$(MAKE) clean -C plugins/tal-elek7ro-juce/Linux
	$(MAKE) clean -C plugins/tal-filter-juce/Linux
	$(MAKE) clean -C plugins/tal-filter-2-juce/Linux
	$(MAKE) clean -C plugins/tal-reverb-juce/Linux
	$(MAKE) clean -C plugins/tal-reverb-2-juce/Linux
	$(MAKE) clean -C plugins/tal-reverb-3-juce/Linux
	$(MAKE) clean -C plugins/tal-vocoder-2-juce/Linux

	rm -f bin/*.so
