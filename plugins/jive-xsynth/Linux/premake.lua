
project.name = "jive-xsynth"
project.bindir = "../../../bin/"
project.libdir = "../../../bin/"

project.configs = { "Release", "Debug" }

package = newpackage()
package.name = "jive-xsynth"
package.kind = "dll"
package.language = "c++"
package.linkflags = { "static-runtime" }

package.config["Release"].objdir = "intermediate/Release"
package.config["Debug"].objdir   = "intermediate/Debug"

package.config["Release"].target = "jive-xsynth"
package.config["Debug"].target   = "jive-xsynth_debug"

package.config["Release"].defines      = { "LINUX=1", "NDEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1" };
package.config["Release"].buildoptions = { "-O2 -s -fvisibility=hidden -msse -ffast-math -static" }
package.config["Release"].links        = { "freetype", "pthread", "rt", "X11", "GL", "juce" }

package.config["Debug"].defines        = { "LINUX=1", "DEBUG=1", "_DEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1" };
package.config["Debug"].buildoptions   = { "-O0 -ggdb -static" }
package.config["Debug"].links          = { "freetype", "pthread", "rt", "X11", "GL", "juce_debug" }

package.includepaths = {
    "../src",
    "../src/xsynth",
    "../src/xsynth/src",
    "../src/xsynth/extras",
    "/usr/include",
    "/usr/include/freetype2",
    "../../../libs/juce/source",
    "../../../vstsdk2.4",
    "../jive",
    "../lib/",
    "../lib/libdssialsacompat-1.0.8a/"
}

package.libpaths = {
    "/usr/X11R6/lib/",
    "/usr/lib/",
    "../../../libs/"
}

package.files = {
    matchfiles (
        "../src/XSynthJuceFilter.cpp",
        "../src/xsynth/src/minblep_tables.c",
        "../src/xsynth/src/xsynth-dssi.c",
        "../src/xsynth/src/xsynth_data.c",
        "../src/xsynth/src/xsynth_ports.c",
        "../src/xsynth/src/xsynth_synth.c",
        "../src/xsynth/src/xsynth_voice.c",
        "../src/xsynth/src/xsynth_voice_render.c",
        "../lib/BasePlugin.cpp",
        "../lib/DssiPlugin.cpp",
        "../lib/LadspaPlugin.cpp",
        "../lib/libdssialsacompat-1.0.8a/dssi_alsa_compat.c",
        "../../../libs/juce/source/src/audio/plugin_client/VST/juce_VST_Wrapper.cpp"
    )
}
