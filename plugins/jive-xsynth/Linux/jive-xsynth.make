# C++ Shared Library Makefile autogenerated by premake
# Don't edit this file! Instead edit `premake.lua` then rerun `make`

ifndef CONFIG
  CONFIG=Release
endif

# if multiple archs are defined turn off automated dependency generation
DEPFLAGS := $(if $(word 2, $(TARGET_ARCH)), , -MMD)

ifeq ($(CONFIG),Release)
  BINDIR := ../../../bin/
  LIBDIR := ../../../bin/
  OBJDIR := intermediate/Release
  OUTDIR := ../../../bin/
  CPPFLAGS := $(DEPFLAGS) -D "LINUX=1" -D "NDEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -I "../src" -I "../src/xsynth" -I "../src/xsynth/src" -I "../src/xsynth/extras" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../vstsdk2.4" -I "../jive" -I "../lib/" -I "../lib/libdssialsacompat-1.0.8a/"
  CFLAGS += $(CPPFLAGS) $(TARGET_ARCH) -fPIC -O2 -O2 -s -fvisibility=hidden -msse -ffast-math -static
  CXXFLAGS += $(CFLAGS)
  LDFLAGS += -L$(BINDIR) -L$(LIBDIR) -shared -s -L"/usr/X11R6/lib/" -L"/usr/lib/" -L"../../../libs/" -lfreetype -lpthread -lrt -lX11 -lGL -ljuce
  LDDEPS :=
  RESFLAGS := -D "LINUX=1" -D "NDEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -I "../src" -I "../src/xsynth" -I "../src/xsynth/src" -I "../src/xsynth/extras" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../vstsdk2.4" -I "../jive" -I "../lib/" -I "../lib/libdssialsacompat-1.0.8a/"
  TARGET := libjive-xsynth.so
 BLDCMD = $(CXX) -o $(OUTDIR)/$(TARGET) $(OBJECTS) $(LDFLAGS) $(RESOURCES) $(TARGET_ARCH)
endif

ifeq ($(CONFIG),Debug)
  BINDIR := ../../../bin/
  LIBDIR := ../../../bin/
  OBJDIR := intermediate/Debug
  OUTDIR := ../../../bin/
  CPPFLAGS := $(DEPFLAGS) -D "LINUX=1" -D "DEBUG=1" -D "_DEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -I "../src" -I "../src/xsynth" -I "../src/xsynth/src" -I "../src/xsynth/extras" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../vstsdk2.4" -I "../jive" -I "../lib/" -I "../lib/libdssialsacompat-1.0.8a/"
  CFLAGS += $(CPPFLAGS) $(TARGET_ARCH) -fPIC -g -O0 -ggdb -static
  CXXFLAGS += $(CFLAGS)
  LDFLAGS += -L$(BINDIR) -L$(LIBDIR) -shared -L"/usr/X11R6/lib/" -L"/usr/lib/" -L"../../../libs/" -lfreetype -lpthread -lrt -lX11 -lGL -ljuce_debug
  LDDEPS :=
  RESFLAGS := -D "LINUX=1" -D "DEBUG=1" -D "_DEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -I "../src" -I "../src/xsynth" -I "../src/xsynth/src" -I "../src/xsynth/extras" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../vstsdk2.4" -I "../jive" -I "../lib/" -I "../lib/libdssialsacompat-1.0.8a/"
  TARGET := libjive-xsynth_debug.so
 BLDCMD = $(CXX) -o $(OUTDIR)/$(TARGET) $(OBJECTS) $(LDFLAGS) $(RESOURCES) $(TARGET_ARCH)
endif

OBJECTS := \
	$(OBJDIR)/XSynthJuceFilter.o \
	$(OBJDIR)/minblep_tables.o \
	$(OBJDIR)/xsynth-dssi.o \
	$(OBJDIR)/xsynth_data.o \
	$(OBJDIR)/xsynth_ports.o \
	$(OBJDIR)/xsynth_synth.o \
	$(OBJDIR)/xsynth_voice.o \
	$(OBJDIR)/xsynth_voice_render.o \
	$(OBJDIR)/BasePlugin.o \
	$(OBJDIR)/DssiPlugin.o \
	$(OBJDIR)/LadspaPlugin.o \
	$(OBJDIR)/dssi_alsa_compat.o \
	$(OBJDIR)/juce_VST_Wrapper.o \

MKDIR_TYPE := msdos
CMD := $(subst \,\\,$(ComSpec)$(COMSPEC))
ifeq (,$(CMD))
  MKDIR_TYPE := posix
endif
ifeq (/bin,$(findstring /bin,$(SHELL)))
  MKDIR_TYPE := posix
endif
ifeq ($(MKDIR_TYPE),posix)
  CMD_MKBINDIR := mkdir -p $(BINDIR)
  CMD_MKLIBDIR := mkdir -p $(LIBDIR)
  CMD_MKOUTDIR := mkdir -p $(OUTDIR)
  CMD_MKOBJDIR := mkdir -p $(OBJDIR)
else
  CMD_MKBINDIR := $(CMD) /c if not exist $(subst /,\\,$(BINDIR)) mkdir $(subst /,\\,$(BINDIR))
  CMD_MKLIBDIR := $(CMD) /c if not exist $(subst /,\\,$(LIBDIR)) mkdir $(subst /,\\,$(LIBDIR))
  CMD_MKOUTDIR := $(CMD) /c if not exist $(subst /,\\,$(OUTDIR)) mkdir $(subst /,\\,$(OUTDIR))
  CMD_MKOBJDIR := $(CMD) /c if not exist $(subst /,\\,$(OBJDIR)) mkdir $(subst /,\\,$(OBJDIR))
endif

.PHONY: clean

$(OUTDIR)/$(TARGET): $(OBJECTS) $(LDDEPS) $(RESOURCES)
	@echo Linking jive-xsynth
	-@$(CMD_MKBINDIR)
	-@$(CMD_MKLIBDIR)
	-@$(CMD_MKOUTDIR)
	@$(BLDCMD)

clean:
	@echo Cleaning jive-xsynth
ifeq ($(MKDIR_TYPE),posix)
	-@rm -f $(OUTDIR)/$(TARGET)
	-@rm -rf $(OBJDIR)
else
	-@if exist $(subst /,\,$(OUTDIR)/$(TARGET)) del /q $(subst /,\,$(OUTDIR)/$(TARGET))
	-@if exist $(subst /,\,$(OBJDIR)) del /q $(subst /,\,$(OBJDIR))
	-@if exist $(subst /,\,$(OBJDIR)) rmdir /s /q $(subst /,\,$(OBJDIR))
endif

$(OBJDIR)/XSynthJuceFilter.o: ../src/XSynthJuceFilter.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/minblep_tables.o: ../src/xsynth/src/minblep_tables.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/xsynth-dssi.o: ../src/xsynth/src/xsynth-dssi.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/xsynth_data.o: ../src/xsynth/src/xsynth_data.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/xsynth_ports.o: ../src/xsynth/src/xsynth_ports.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/xsynth_synth.o: ../src/xsynth/src/xsynth_synth.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/xsynth_voice.o: ../src/xsynth/src/xsynth_voice.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/xsynth_voice_render.o: ../src/xsynth/src/xsynth_voice_render.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/BasePlugin.o: ../lib/BasePlugin.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/DssiPlugin.o: ../lib/DssiPlugin.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/LadspaPlugin.o: ../lib/LadspaPlugin.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/dssi_alsa_compat.o: ../lib/libdssialsacompat-1.0.8a/dssi_alsa_compat.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/juce_VST_Wrapper.o: ../../../libs/juce/source/src/audio/plugin_client/VST/juce_VST_Wrapper.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

-include $(OBJECTS:%.o=%.d)

