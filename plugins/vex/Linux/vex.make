# C++ Shared Library Makefile autogenerated by premake
# Don't edit this file! Instead edit `premake.lua` then rerun `make`

ifndef CONFIG
  CONFIG=Release
endif

# if multiple archs are defined turn off automated dependency generation
DEPFLAGS := $(if $(word 2, $(TARGET_ARCH)), , -MMD)

ifeq ($(CONFIG),Release)
  BINDIR := ../../../bin/
  LIBDIR := ../../../bin/
  OBJDIR := intermediate/Release
  OUTDIR := ../../../bin/
  CPPFLAGS := $(DEPFLAGS) -D "LINUX=1" -D "NDEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -D "XVEX_VST_PLUGIN=1" -I "../src" -I "../src/soundtouch" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../libs/juce/source/extras/audio plugins" -I "../../../vstsdk2.4"
  CFLAGS += $(CPPFLAGS) $(TARGET_ARCH) -fPIC -O2 -O2 -s -fvisibility=hidden -msse -ffast-math -static
  CXXFLAGS += $(CFLAGS)
  LDFLAGS += -L$(BINDIR) -L$(LIBDIR) -shared -s -L"/usr/X11R6/lib/" -L"/usr/lib/" -L"../../../libs/" -lfreetype -lpthread -lrt -lX11 -lGL -ljuce
  LDDEPS :=
  RESFLAGS := -D "LINUX=1" -D "NDEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -D "XVEX_VST_PLUGIN=1" -I "../src" -I "../src/soundtouch" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../libs/juce/source/extras/audio plugins" -I "../../../vstsdk2.4"
  TARGET := libvex.so
 BLDCMD = $(CXX) -o $(OUTDIR)/$(TARGET) $(OBJECTS) $(LDFLAGS) $(RESOURCES) $(TARGET_ARCH)
endif

ifeq ($(CONFIG),Debug)
  BINDIR := ../../../bin/
  LIBDIR := ../../../bin/
  OBJDIR := intermediate/Debug
  OUTDIR := ../../../bin/
  CPPFLAGS := $(DEPFLAGS) -D "LINUX=1" -D "DEBUG=1" -D "_DEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -D "XVEX_VST_PLUGIN=1" -I "../src" -I "../src/soundtouch" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../libs/juce/source/extras/audio plugins" -I "../../../vstsdk2.4"
  CFLAGS += $(CPPFLAGS) $(TARGET_ARCH) -fPIC -g -O0 -ggdb -static
  CXXFLAGS += $(CFLAGS)
  LDFLAGS += -L$(BINDIR) -L$(LIBDIR) -shared -L"/usr/X11R6/lib/" -L"/usr/lib/" -L"../../../libs/" -lfreetype -lpthread -lrt -lX11 -lGL -ljuce_debug
  LDDEPS :=
  RESFLAGS := -D "LINUX=1" -D "DEBUG=1" -D "_DEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -D "XVEX_VST_PLUGIN=1" -I "../src" -I "../src/soundtouch" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../libs/juce/source/extras/audio plugins" -I "../../../vstsdk2.4"
  TARGET := libvex_debug.so
 BLDCMD = $(CXX) -o $(OUTDIR)/$(TARGET) $(OBJECTS) $(LDFLAGS) $(RESOURCES) $(TARGET_ARCH)
endif

OBJECTS := \
	$(OBJDIR)/Main.o \
	$(OBJDIR)/StandardLibrary.o \
	$(OBJDIR)/VexEditorComponent.o \
	$(OBJDIR)/VexFilter.o \
	$(OBJDIR)/MyLookAndFeel.o \
	$(OBJDIR)/Resources.o \
	$(OBJDIR)/cVoice.o \
	$(OBJDIR)/cWaveRenderer.o \
	$(OBJDIR)/allpass.o \
	$(OBJDIR)/comb.o \
	$(OBJDIR)/revmodel.o \
	$(OBJDIR)/ResourceFile.o \

MKDIR_TYPE := msdos
CMD := $(subst \,\\,$(ComSpec)$(COMSPEC))
ifeq (,$(CMD))
  MKDIR_TYPE := posix
endif
ifeq (/bin,$(findstring /bin,$(SHELL)))
  MKDIR_TYPE := posix
endif
ifeq ($(MKDIR_TYPE),posix)
  CMD_MKBINDIR := mkdir -p $(BINDIR)
  CMD_MKLIBDIR := mkdir -p $(LIBDIR)
  CMD_MKOUTDIR := mkdir -p $(OUTDIR)
  CMD_MKOBJDIR := mkdir -p $(OBJDIR)
else
  CMD_MKBINDIR := $(CMD) /c if not exist $(subst /,\\,$(BINDIR)) mkdir $(subst /,\\,$(BINDIR))
  CMD_MKLIBDIR := $(CMD) /c if not exist $(subst /,\\,$(LIBDIR)) mkdir $(subst /,\\,$(LIBDIR))
  CMD_MKOUTDIR := $(CMD) /c if not exist $(subst /,\\,$(OUTDIR)) mkdir $(subst /,\\,$(OUTDIR))
  CMD_MKOBJDIR := $(CMD) /c if not exist $(subst /,\\,$(OBJDIR)) mkdir $(subst /,\\,$(OBJDIR))
endif

.PHONY: clean

$(OUTDIR)/$(TARGET): $(OBJECTS) $(LDDEPS) $(RESOURCES)
	@echo Linking vex
	-@$(CMD_MKBINDIR)
	-@$(CMD_MKLIBDIR)
	-@$(CMD_MKOUTDIR)
	@$(BLDCMD)

clean:
	@echo Cleaning vex
ifeq ($(MKDIR_TYPE),posix)
	-@rm -f $(OUTDIR)/$(TARGET)
	-@rm -rf $(OBJDIR)
else
	-@if exist $(subst /,\,$(OUTDIR)/$(TARGET)) del /q $(subst /,\,$(OUTDIR)/$(TARGET))
	-@if exist $(subst /,\,$(OBJDIR)) del /q $(subst /,\,$(OBJDIR))
	-@if exist $(subst /,\,$(OBJDIR)) rmdir /s /q $(subst /,\,$(OBJDIR))
endif

$(OBJDIR)/Main.o: ../src/Main.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/StandardLibrary.o: ../src/StandardLibrary.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/VexEditorComponent.o: ../src/VexEditorComponent.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/VexFilter.o: ../src/VexFilter.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/MyLookAndFeel.o: ../src/lookandfeel/MyLookAndFeel.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/Resources.o: ../src/resources/Resources.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/cVoice.o: ../src/synth/cVoice.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/cWaveRenderer.o: ../src/synth/cWaveRenderer.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/allpass.o: ../src/synth/freeverb/allpass.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/comb.o: ../src/synth/freeverb/comb.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/revmodel.o: ../src/synth/freeverb/revmodel.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/ResourceFile.o: ../src/waverom/ResourceFile.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

-include $(OBJECTS:%.o=%.d)

