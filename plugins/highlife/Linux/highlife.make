# C++ Shared Library Makefile autogenerated by premake
# Don't edit this file! Instead edit `premake.lua` then rerun `make`

ifndef CONFIG
  CONFIG=Release
endif

# if multiple archs are defined turn off automated dependency generation
DEPFLAGS := $(if $(word 2, $(TARGET_ARCH)), , -MMD)

ifeq ($(CONFIG),Release)
  BINDIR := ../../../bin/
  LIBDIR := ../../../bin/
  OBJDIR := intermediate/Release
  OUTDIR := ../../../bin/
  CPPFLAGS := $(DEPFLAGS) -D "LINUX=1" -D "NDEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -D "XHIGHLIFE_VST_PLUGIN=1" -I "../src" -I "../src/Highlife" -I "../src/FluidSynth" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../vstsdk2.4"
  CFLAGS += $(CPPFLAGS) $(TARGET_ARCH) -fPIC -O2 -O2 -s -fvisibility=hidden -msse -ffast-math -static
  CXXFLAGS += $(CFLAGS)
  LDFLAGS += -L$(BINDIR) -L$(LIBDIR) -shared -s -L"/usr/X11R6/lib/" -L"/usr/lib/" -L"../../../libs/" -lfreetype -lpthread -lrt -lX11 -lGL -ljuce
  LDDEPS :=
  RESFLAGS := -D "LINUX=1" -D "NDEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -D "XHIGHLIFE_VST_PLUGIN=1" -I "../src" -I "../src/Highlife" -I "../src/FluidSynth" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../vstsdk2.4"
  TARGET := libhighlife.so
 BLDCMD = $(CXX) -o $(OUTDIR)/$(TARGET) $(OBJECTS) $(LDFLAGS) $(RESOURCES) $(TARGET_ARCH)
endif

ifeq ($(CONFIG),Debug)
  BINDIR := ../../../bin/
  LIBDIR := ../../../bin/
  OBJDIR := intermediate/Debug
  OUTDIR := ../../../bin/
  CPPFLAGS := $(DEPFLAGS) -D "LINUX=1" -D "DEBUG=1" -D "_DEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -D "XHIGHLIFE_VST_PLUGIN=1" -I "../src" -I "../src/Highlife" -I "../src/FluidSynth" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../vstsdk2.4"
  CFLAGS += $(CPPFLAGS) $(TARGET_ARCH) -fPIC -g -O0 -ggdb -static
  CXXFLAGS += $(CFLAGS)
  LDFLAGS += -L$(BINDIR) -L$(LIBDIR) -shared -L"/usr/X11R6/lib/" -L"/usr/lib/" -L"../../../libs/" -lfreetype -lpthread -lrt -lX11 -lGL -ljuce_debug
  LDDEPS :=
  RESFLAGS := -D "LINUX=1" -D "DEBUG=1" -D "_DEBUG=1" -D "JUCE_ALSA=0" -D "JUCE_USE_VSTSDK_2_4=1" -D "XHIGHLIFE_VST_PLUGIN=1" -I "../src" -I "../src/Highlife" -I "../src/FluidSynth" -I "/usr/include" -I "/usr/include/freetype2" -I "../../../libs/juce/source" -I "../../../vstsdk2.4"
  TARGET := libhighlife_debug.so
 BLDCMD = $(CXX) -o $(OUTDIR)/$(TARGET) $(OBJECTS) $(LDFLAGS) $(RESOURCES) $(TARGET_ARCH)
endif

OBJECTS := \
	$(OBJDIR)/Main.o \
	$(OBJDIR)/fluid_conv.o \
	$(OBJDIR)/fluid_defsfont.o \
	$(OBJDIR)/fluid_io.o \
	$(OBJDIR)/fluid_list.o \
	$(OBJDIR)/fluid_sys.o \
	$(OBJDIR)/allpass.o \
	$(OBJDIR)/comb.o \
	$(OBJDIR)/revmodel.o \
	$(OBJDIR)/DLS.o \
	$(OBJDIR)/gig.o \
	$(OBJDIR)/RIFF.o \
	$(OBJDIR)/common.o \
	$(OBJDIR)/dct64_i386.o \
	$(OBJDIR)/decode_i386.o \
	$(OBJDIR)/interface.o \
	$(OBJDIR)/layer2.o \
	$(OBJDIR)/layer3.o \
	$(OBJDIR)/tabinit.o \
	$(OBJDIR)/HighLifeLfo.o \
	$(OBJDIR)/Highlife.o \
	$(OBJDIR)/HighLifeAkp.o \
	$(OBJDIR)/HighLifeCriticalSection.o \
	$(OBJDIR)/HighLifeDls.o \
	$(OBJDIR)/HighLifeEditor.o \
	$(OBJDIR)/HighLifeEnvelope.o \
	$(OBJDIR)/HighLifeFilter.o \
	$(OBJDIR)/HighLifeGig.o \
	$(OBJDIR)/HighLifeGui.o \
	$(OBJDIR)/HighLifeGuiMenu.o \
	$(OBJDIR)/HighLifeGuiPaint.o \
	$(OBJDIR)/HighLifeMp3.o \
	$(OBJDIR)/HighLifeOgg.o \
	$(OBJDIR)/HighLifeParameters.o \
	$(OBJDIR)/HighLifePlug.o \
	$(OBJDIR)/HighLifeRaw.o \
	$(OBJDIR)/HighLifeRiffWave.o \
	$(OBJDIR)/HighLifeSampleEdit.o \
	$(OBJDIR)/HighLifeSf2.o \
	$(OBJDIR)/HighLifeSfz.o \
	$(OBJDIR)/HighLifeTool.o \
	$(OBJDIR)/HighLifeVoice.o \
	$(OBJDIR)/HighLifeVstHost.o \
	$(OBJDIR)/HighLifeWav.o \
	$(OBJDIR)/Resources.o \

MKDIR_TYPE := msdos
CMD := $(subst \,\\,$(ComSpec)$(COMSPEC))
ifeq (,$(CMD))
  MKDIR_TYPE := posix
endif
ifeq (/bin,$(findstring /bin,$(SHELL)))
  MKDIR_TYPE := posix
endif
ifeq ($(MKDIR_TYPE),posix)
  CMD_MKBINDIR := mkdir -p $(BINDIR)
  CMD_MKLIBDIR := mkdir -p $(LIBDIR)
  CMD_MKOUTDIR := mkdir -p $(OUTDIR)
  CMD_MKOBJDIR := mkdir -p $(OBJDIR)
else
  CMD_MKBINDIR := $(CMD) /c if not exist $(subst /,\\,$(BINDIR)) mkdir $(subst /,\\,$(BINDIR))
  CMD_MKLIBDIR := $(CMD) /c if not exist $(subst /,\\,$(LIBDIR)) mkdir $(subst /,\\,$(LIBDIR))
  CMD_MKOUTDIR := $(CMD) /c if not exist $(subst /,\\,$(OUTDIR)) mkdir $(subst /,\\,$(OUTDIR))
  CMD_MKOBJDIR := $(CMD) /c if not exist $(subst /,\\,$(OBJDIR)) mkdir $(subst /,\\,$(OBJDIR))
endif

.PHONY: clean

$(OUTDIR)/$(TARGET): $(OBJECTS) $(LDDEPS) $(RESOURCES)
	@echo Linking highlife
	-@$(CMD_MKBINDIR)
	-@$(CMD_MKLIBDIR)
	-@$(CMD_MKOUTDIR)
	@$(BLDCMD)

clean:
	@echo Cleaning highlife
ifeq ($(MKDIR_TYPE),posix)
	-@rm -f $(OUTDIR)/$(TARGET)
	-@rm -rf $(OBJDIR)
else
	-@if exist $(subst /,\,$(OUTDIR)/$(TARGET)) del /q $(subst /,\,$(OUTDIR)/$(TARGET))
	-@if exist $(subst /,\,$(OBJDIR)) del /q $(subst /,\,$(OBJDIR))
	-@if exist $(subst /,\,$(OBJDIR)) rmdir /s /q $(subst /,\,$(OBJDIR))
endif

$(OBJDIR)/Main.o: ../src/Main.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/fluid_conv.o: ../src/FluidSynth/src/fluid_conv.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/fluid_defsfont.o: ../src/FluidSynth/src/fluid_defsfont.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/fluid_io.o: ../src/FluidSynth/src/fluid_io.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/fluid_list.o: ../src/FluidSynth/src/fluid_list.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/fluid_sys.o: ../src/FluidSynth/src/fluid_sys.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/allpass.o: ../src/Freeverb/allpass.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/comb.o: ../src/Freeverb/comb.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/revmodel.o: ../src/Freeverb/revmodel.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/DLS.o: ../src/LibGig/DLS.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/gig.o: ../src/LibGig/gig.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/RIFF.o: ../src/LibGig/RIFF.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/common.o: ../src/Mpglib/common.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/dct64_i386.o: ../src/Mpglib/dct64_i386.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/decode_i386.o: ../src/Mpglib/decode_i386.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/interface.o: ../src/Mpglib/interface.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/layer2.o: ../src/Mpglib/layer2.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/layer3.o: ../src/Mpglib/layer3.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/tabinit.o: ../src/Mpglib/tabinit.c
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CC) $(CFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeLfo.o: ../src/Highlife/HighLifeLfo.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/Highlife.o: ../src/Highlife/Highlife.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeAkp.o: ../src/Highlife/HighLifeAkp.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeCriticalSection.o: ../src/Highlife/HighLifeCriticalSection.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeDls.o: ../src/Highlife/HighLifeDls.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeEditor.o: ../src/Highlife/HighLifeEditor.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeEnvelope.o: ../src/Highlife/HighLifeEnvelope.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeFilter.o: ../src/Highlife/HighLifeFilter.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeGig.o: ../src/Highlife/HighLifeGig.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeGui.o: ../src/Highlife/HighLifeGui.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeGuiMenu.o: ../src/Highlife/HighLifeGuiMenu.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeGuiPaint.o: ../src/Highlife/HighLifeGuiPaint.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeMp3.o: ../src/Highlife/HighLifeMp3.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeOgg.o: ../src/Highlife/HighLifeOgg.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeParameters.o: ../src/Highlife/HighLifeParameters.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifePlug.o: ../src/Highlife/HighLifePlug.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeRaw.o: ../src/Highlife/HighLifeRaw.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeRiffWave.o: ../src/Highlife/HighLifeRiffWave.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeSampleEdit.o: ../src/Highlife/HighLifeSampleEdit.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeSf2.o: ../src/Highlife/HighLifeSf2.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeSfz.o: ../src/Highlife/HighLifeSfz.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeTool.o: ../src/Highlife/HighLifeTool.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeVoice.o: ../src/Highlife/HighLifeVoice.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeVstHost.o: ../src/Highlife/HighLifeVstHost.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/HighLifeWav.o: ../src/Highlife/HighLifeWav.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

$(OBJDIR)/Resources.o: ../src/Resources/Resources.cpp
	-@$(CMD_MKOBJDIR)
	@echo $(notdir $<)
	@$(CXX) $(CXXFLAGS) -o "$@" -c "$<"

-include $(OBJECTS:%.o=%.d)

