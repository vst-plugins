
project.name = "highlife"
project.bindir = "../../../bin/"
project.libdir = "../../../bin/"

project.configs = { "Release", "Debug" }

package = newpackage()
package.name = "highlife"
package.kind = "dll"
package.language = "c++"
package.linkflags = { "static-runtime" }

package.config["Release"].objdir = "intermediate/Release"
package.config["Debug"].objdir   = "intermediate/Debug"

package.config["Release"].target = "highlife"
package.config["Debug"].target   = "highlife_debug"

package.config["Release"].defines      = { "LINUX=1", "NDEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1", "XHIGHLIFE_VST_PLUGIN=1" };
package.config["Release"].buildoptions = { "-O2 -s -fvisibility=hidden -msse -ffast-math -static" }
package.config["Release"].links        = { "freetype", "pthread", "rt", "X11", "GL", "juce" }

package.config["Debug"].defines        = { "LINUX=1", "DEBUG=1", "_DEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1", "XHIGHLIFE_VST_PLUGIN=1" };
package.config["Debug"].buildoptions   = { "-O0 -ggdb -static" }
package.config["Debug"].links          = { "freetype", "pthread", "rt", "X11", "GL", "juce_debug" }

package.includepaths = {
    "../src",
    "../src/Highlife",
    "../src/FluidSynth",
    "/usr/include",
    "/usr/include/freetype2",
    "../../../libs/juce/source",
    "../../../vstsdk2.4"
}

package.libpaths = {
    "/usr/X11R6/lib/",
    "/usr/lib/",
    "../../../libs/"
}

package.files = {
    matchfiles (
        "../src/*.cpp",
        "../src/FluidSynth/src/*.c",
        "../src/Freeverb/*.cpp",
        "../src/LibGig/*.cpp",
        "../src/Mpglib/common.c",
        "../src/Mpglib/dct64_i386.c",
        "../src/Mpglib/decode_i386.c",
        "../src/Mpglib/interface.c",
        "../src/Mpglib/layer2.c",
        "../src/Mpglib/layer3.c",
        "../src/Mpglib/tabinit.c",
     -- "../src/Ogg/src/*.c",
     -- "../src/Vorbis/lib/*.c",
        "../src/Highlife/*.cpp",
        "../src/Resources/*.cpp"
        --"../jive/JiveClipListComponent.cpp"
    )
}
