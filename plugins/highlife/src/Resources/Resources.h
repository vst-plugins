/* (Auto-generated binary data file). */

#pragma once
#ifndef BINARY_RESOURCES_H
#define BINARY_RESOURCES_H

namespace Resources
{
    extern const char*  buttons;
    extern const char*  buttons_ext;
    const int           buttons_size = 395;

    extern const char*  digits;
    extern const char*  digits_ext;
    const int           digits_size = 249;

    extern const char*  fixed_font;
    extern const char*  fixed_font_ext;
    const int           fixed_font_size = 969;

    extern const char*  gui;
    extern const char*  gui_ext;
    const int           gui_size = 71460;

    extern const char*  gui_edit;
    extern const char*  gui_edit_ext;
    const int           gui_edit_size = 42884;

    extern const char*  knob;
    extern const char*  knob_ext;
    const int           knob_size = 69842;

    extern const char*  knob_12;
    extern const char*  knob_12_ext;
    const int           knob_12_size = 25611;

    extern const char*  knob_alpha;
    extern const char*  knob_alpha_ext;
    const int           knob_alpha_size = 21885;

    extern const char*  led;
    extern const char*  led_ext;
    const int           led_size = 421;

    extern const char*  loop;
    extern const char*  loop_ext;
    const int           loop_size = 188;

    extern const char*  pb;
    extern const char*  pb_ext;
    const int           pb_size = 54354;

    extern const char*  keyboard;
    extern const char*  keyboard_ext;
    const int           keyboard_size = 3036;

};

#endif
