
/* Fix for VST includes */
#ifdef __linux__
#define __cdecl
#endif

#include "public.sdk/source/vst2.x/audioeffect.h"
#include "public.sdk/source/vst2.x/audioeffect.cpp"
#include "public.sdk/source/vst2.x/audioeffectx.h"
#include "public.sdk/source/vst2.x/audioeffectx.cpp"
#include "public.sdk/source/vst2.x/vstplugmain.cpp"

#include <jack/jack.h>
#include <jack/midiport.h>

#define MAX_MIDI_BUFFER 1024

// Shared Stuff
struct midi_data_t {
    int status;
    int note;
    int velocity;
    uint32_t time;
};

volatile midi_data_t midi_data[MAX_MIDI_BUFFER];

// Jack Stuff
jack_client_t* jack_client;
jack_port_t* jack_port;

static int process_callback(jack_nframes_t nframes, void *arg)
{
    void* port_buffer = jack_port_get_buffer(jack_port, nframes);
    jack_midi_clear_buffer(port_buffer);
    unsigned char* buffer;

    for (int i=0; i<MAX_MIDI_BUFFER; i++)
    {
        if (midi_data[i].status != 0) {
            buffer = jack_midi_event_reserve(port_buffer, midi_data[i].time, 3);
            buffer[0] = midi_data[i].status;
            buffer[1] = midi_data[i].note;
            buffer[2] = midi_data[i].velocity;
            midi_data[i].status = 0; //event has been processed
        } else {
            break;
        }
    }

    return 0;
}

// VST Stuff
class JackAss : public AudioEffectX
{
public:
    JackAss(audioMasterCallback audioMaster);
    ~JackAss();

    virtual void processReplacing(float** inputs, float** outputs, VstInt32 sampleFrames);
    virtual VstInt32 processEvents(VstEvents* events);

    virtual bool getEffectName(char* name);
    virtual bool getVendorString(char* text);
    virtual bool getProductString(char* text);
    virtual VstInt32 getVendorVersion();
    virtual VstInt32 canDo(char* text);

    virtual VstInt32 getNumMidiInputChannels();
    virtual VstInt32 getNumMidiOutputChannels();

protected:
    int init_jack();
    void close_jack();
    void clearMidiBuffer();
};

AudioEffect* createEffectInstance(audioMasterCallback audioMaster)
{
    return new JackAss(audioMaster);
}

JackAss::JackAss(audioMasterCallback audioMaster) : AudioEffectX(audioMaster, 0, 0)
{
    if (audioMaster)
    {
        isSynth();

        setNumInputs(0);
        setNumOutputs(2); // For Hosts that don't support MIDI plugins
        setUniqueID((long)"JackAss");

        clearMidiBuffer();
        init_jack();
    }
}

JackAss::~JackAss()
{
    close_jack();
}

int JackAss::init_jack()
{
    jack_client = jack_client_open("JackAss", JackNullOption, 0);
    jack_port = jack_port_register(jack_client, "midi_out", JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);
    jack_set_process_callback(jack_client, process_callback, 0);
    return jack_activate(jack_client);
}

void JackAss::close_jack()
{
    jack_deactivate(jack_client);
    jack_client_close(jack_client);
    jack_client = 0;
}

void JackAss::clearMidiBuffer()
{
    for (int i=0; i<MAX_MIDI_BUFFER; i++) {
        midi_data[i].status = 0;
        midi_data[i].note = 0;
        midi_data[i].velocity = 0;
        midi_data[i].time = 0;
    }
}

void JackAss::processReplacing(float** inputs, float** outputs, VstInt32 sampleFrames)
{
    // Silent output
    float* out1 = outputs[0];
    float* out2 = outputs[1];

    while (--sampleFrames >= 0) {
        out1[sampleFrames] = 0;
        out2[sampleFrames] = 0;
    }
}

VstInt32 JackAss::processEvents(VstEvents* ev)
{
    VstInt32 i, j=0, k=0;

    for (i=0; i<MAX_MIDI_BUFFER; i++) {
      if (midi_data[i].status == 0) {
        k = i;
        break;
      }
    }

    for (i=0; i < ev->numEvents && i < MAX_MIDI_BUFFER; i++)
    {
        if ((ev->events[i])->type != kVstMidiType) {
            j++;
            continue;
        }

        VstMidiEvent* event = (VstMidiEvent*)ev->events[i];
        char* midiData = event->midiData;

        midi_data[k+i-j].status = (int)midiData[0];
        midi_data[k+i-j].note = (int)midiData[1];
        midi_data[k+i-j].velocity = (int)midiData[2];
        midi_data[k+i-j].time = event->deltaFrames;

        event++;
    }

    // Stop looking when reaching this pos
    midi_data[k+i-j+1].status = 0;

    return 1;
}

bool JackAss::getEffectName(char* name)
{
    vst_strncpy(name, "JackAss", kVstMaxEffectNameLen);
    return true;
}

bool JackAss::getProductString(char* text)
{
    vst_strncpy(text, "JackAss", kVstMaxProductStrLen);
    return true;
}

bool JackAss::getVendorString(char* text)
{
    vst_strncpy(text, "falkTX", kVstMaxVendorStrLen);
    return true;
}

VstInt32 JackAss::getVendorVersion()
{
    return 1000;
}

VstInt32 JackAss::canDo(char* text)
{
    if (!strcmp(text, "receiveVstEvents"))
        return 1;
    if (!strcmp(text, "receiveVstMidiEvent"))
        return 1;
    return -1;
}

VstInt32 JackAss::getNumMidiInputChannels()
{
    return 1;
}

VstInt32 JackAss::getNumMidiOutputChannels()
{
    return 0;
}
