
project.name = "juce_pitcher"
project.bindir = "../../../bin/"
project.libdir = "../../../bin/"

project.configs = { "Release", "Debug" }

package = newpackage()
package.name = "juce_pitcher"
package.kind = "dll"
package.language = "c++"
package.linkflags = { "static-runtime" }

package.config["Release"].objdir = "intermediate/Release"
package.config["Debug"].objdir   = "intermediate/Debug"

package.config["Release"].target = "juce_pitcher"
package.config["Debug"].target   = "juce_pitcher_debug"

package.config["Release"].defines      = { "LINUX=1", "NDEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1", "JucePlugin_Build_VST=1" };
package.config["Release"].buildoptions = { "-O2 -s -fvisibility=hidden -msse -ffast-math -static" }
package.config["Release"].links        = { "freetype", "pthread", "rt", "X11", "GL", "juce" }

package.config["Debug"].defines        = { "LINUX=1", "DEBUG=1", "_DEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1", "JucePlugin_Build_VST=1" };
package.config["Debug"].buildoptions   = { "-O0 -ggdb -static" }
package.config["Debug"].links          = { "freetype", "pthread", "rt", "X11", "GL", "juce_debug" }

package.includepaths = {
    "../src",
    "../src/soundtouch",
    "/usr/include",
    "/usr/include/freetype2",
    "../../../libs/juce/source",
    "../../../vstsdk2.4"
}

package.libpaths = {
    "/usr/X11R6/lib/",
    "/usr/lib/",
    "../../../libs/"
}

package.files = {
    matchfiles (
        "../src/*.cpp",
        "../src/soundtouch/AAFilter.cpp",
        "../src/soundtouch/FIFOSampleBuffer.cpp",
        "../src/soundtouch/FIRFilter.cpp",
        "../src/soundtouch/RateTransposer.cpp",
        "../src/soundtouch/SoundTouch.cpp",
        "../src/soundtouch/TDStretch.cpp",
        "../src/soundtouch/cpu_detect_x86_gcc.cpp",
        "../src/soundtouch/mmx_optimized.cpp",
        "../src/soundtouch/sse_optimized.cpp",
        "../../../libs/juce/source/src/audio/plugin_client/VST/juce_VST_Wrapper.cpp"
    )
}
