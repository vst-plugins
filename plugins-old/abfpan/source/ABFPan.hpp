/*-----------------------------------------------------------------------------

(C) 2002 Dave Malham, Dogma Design

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

Contact with regard to this code: dave@dmalham.freeserve.co.uk

Copyright notice;

<a href="http://www.computermusic.ch/">The Swiss Center for Computer Music</a> and the
<a href="http://www.hmtzh.ch/">Hochschule fur Musik Winterthur Zurich</a>
who sponsored the writing of the plugin `BPan', written by Dave Malham,
grant permission for the code to be release into the public domain, under the terms of the Gnu GPL.

Gerald Bennett, 28 February 2003
http://www.computermusic.ch/sccm_txt.html#anchor98180



-----------------------------------------------------------------------------*/
#ifndef __ABFPan_H
#define __ABFPan_H

#if MAC

#if __ide_target("BPan_m")
#define _SPACIAL_
#endif
#if __ide_target("BPan_m_gui")
#define _SPACIAL_
#endif

#endif

#include <audioeffectx.h>
#include <cstring>
#include <cmath>

//#include "vstcontrols.h"

enum mirrorType
{
    kNoPan = 0,
    kNoMirror = 1,
    kYMirror = 2,
    kXMirror = 3,
    kXYMirror = 4
};

enum
{

#ifdef _SPACIAL_	// only need this for the spacializer effect version
    kLRPanParam = 0,
    kFBPanParam = 1,
    kTypeOfMirror,
    kPanAngle,

#endif
    kAzimuthLeft,
    kElevationLeft,
    kCentreLeft,
    kVolumeLeft,
    kDistanceLeft,
    kDistanceFactorLeft,
    kZerothLeft,
    kFirstLeft,

    kAzimuthRight,
    kElevationRight,
    kVolumeRight,
    kDistanceRight,
    kDistanceFactorRight,
    kCentreRight,
    kZerothRight,
    kFirstRight,

    kNumParams
};

const int	kNumInputs = 2;
const int	kNumOutputs = 4;

double computePanAngle(double LR, double FB);

const double kAntiDenorm = 1e-20;

#ifndef kPI
#define kPI    3.14159265358979323846
#endif

#ifndef k2PI
#define k2PI    2*3.14159265358979323846
#endif

const double kDtoR = kPI/180;
const double kRtoD = 180/kPI;

#if MAC
const double kLog2 = 0.3010299957;		// deals with some kind of Codewarrior problem
#else
const double kLog2 = log(2);
#endif

const double kdBFactor = ((-6.0)/20.0);	// saves later maths operations in converting fDistanceFactorLeft into
// dB's in range 0 to -maxloss

const float k6dBPoint = float (1 - (0.5/(20/6)));	// allows for left+right without overload
// this is a -6dB amount if gainlaw is set for
// 20dB drop over first part of scale

const double kAvCoef1 = 0.999;			// coefficients for averaging
const double kAvCoef2 = 1 - kAvCoef1;	// used to smooth controls


class ABFPan;

class ABFPanProgram
{
    friend class ABFPan;
public:
    ABFPanProgram();
    ~ABFPanProgram() {}


private:
    double dALeft;
    double dELeft;
    double dWLeft;
    double dXLeft;
    double dYLeft;
    double dZLeft;
    double dWLeftOld;
    double dXLeftOld;
    double dYLeftOld;
    double dZLeftOld;

    double dARight;
    double dERight;
    double dWRight;
    double dXRight;
    double dYRight;
    double dZRight;
    double dWRightOld;
    double dXRightOld;
    double dYRightOld;
    double dZRightOld;

    double dOldPanAngle;

    float fAzimuthLeft;
    float fElevationLeft;
    float fZerothLeft;
    float fFirstLeft;
    float fVolumeLeft;
    float fDistanceLeft;
    float fDistanceFactorLeft;
    float fCentreLeft;
    float fAzimuthRight;
    float fElevationRight;
    float fZerothRight;
    float fFirstRight;
    float fVolumeRight;
    float fDistanceRight;
    float fDistanceFactorRight;
    float fCentreRight;

#ifdef _SPACIAL_	// only need this for the spacializer effect version
    float fLRPanParam;
    float fFBPanParam;
    float fTypeOfMirror;
#endif


    char name[24];
};



class ABFPan : public AudioEffectX
{
public:
    ABFPan(audioMasterCallback audioMaster);
    ~ABFPan();

    virtual void process(float **inputs, float **outputs, long sampleFrames);
    virtual void processReplacing(float **inputs, float **outputs, long sampleFrames);
    virtual void setProgram(long program);
    virtual void setProgramName(char *name);
    virtual void getProgramName(char *name);
    virtual void setParameter(long index, float value);
    virtual float getParameter(long index);
    virtual void getParameterLabel(long index, char *label);
    virtual void getParameterDisplay(long index, char *text);
    virtual void getParameterName(long index, char *text);
    virtual double distancingLeft (double dBFactor);
    virtual double distancingRight (double dBFactor);
    virtual bool getVendorString (char* text);
    virtual VstPlugCategory getPlugCategory();
    virtual double gainLaw (double gain);
    virtual bool getOutputProperties (long index, VstPinProperties* properties);
    virtual bool getInputProperties (long index, VstPinProperties* properties);

private:

    ABFPanProgram *programs;


    double dALeft;
    double dELeft;
    double dWLeft;
    double dXLeft;
    double dYLeft;
    double dZLeft;
    double dWLeftOld;
    double dXLeftOld;
    double dYLeftOld;
    double dZLeftOld;

    double dOldPanAngle;

    double dARight;
    double dERight;
    double dWRight;
    double dXRight;
    double dYRight;
    double dZRight;
    double dWRightOld;
    double dXRightOld;
    double dYRightOld;
    double dZRightOld;

    float fAzimuthLeft;
    float fElevationLeft;
    float fZerothLeft;
    float fFirstLeft;
    float fVolumeLeft;
    float fDistanceLeft;
    float fDistanceFactorLeft;
    float fCentreLeft;
    float fAzimuthRight;
    float fElevationRight;
    float fZerothRight;
    float fFirstRight;
    float fVolumeRight;
    float fDistanceRight;
    float fDistanceFactorRight;
    float fCentreRight;

#ifdef _SPACIAL_	// only need this for the spacializer effect version
    float fLRPanParam;
    float fFBPanParam;
    float fTypeOfMirror;
#endif

    char programName[24];
};


#endif
