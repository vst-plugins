/*-----------------------------------------------------------------------------

(C) 2002 Dave Malham, Dogma Design

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

Contact with regard to this code: dave@dmalham.freeserve.co.uk

Copyright notice;

<a href="http://www.computermusic.ch/">The Swiss Center for Computer Music</a> and the
<a href="http://www.hmtzh.ch/">Hochschule fur Musik Winterthur Zurich</a>
who sponsored the writing of the plugin `BPan', written by Dave Malham,
grant permission for the code to be release into the public domain, under the terms of the Gnu GPL.

Gerald Bennett, 28 February 2003
http://www.computermusic.ch/sccm_txt.html#anchor98180



-----------------------------------------------------------------------------*/
#include <cstdio>
#include <cmath>
#include "ABFPan.hpp"

//-----------------------------------------------------------------------------
ABFPanProgram::ABFPanProgram ()
{
	fAzimuthLeft	= float (1.0);		// due front
	fElevationLeft	= float (0.5);		// due front
	fZerothLeft 	= float (0.707);
	fFirstLeft 		= float (1.0);
	fDistanceLeft 	= float (0.1);
	fCentreLeft		= float (0.1);		// notional centre area
	fDistanceFactorLeft = float (0.75);	// -4.5 dB for each doubling of distance (-6 * 0.25)
	fVolumeLeft 	= k6dBPoint;// allows for left+right without overload
	fAzimuthRight	= float (1.0);		// due front
	fElevationRight	= float (0.5);		// due front
	fZerothRight 	= float (0.707);
	fFirstRight 	= float (1.0);
	fDistanceRight	= float (0.1);
	fCentreRight 	= float (0.1);		// notional centre area
	fDistanceFactorRight = float (0.75);// -4.5 dB for each doubling of distance (-6 * 0.25)
	fVolumeRight 	= k6dBPoint;// allows for left+right without overload


	dALeft		= 0.0;
	dELeft		= 0.0;
	dWLeft		= 0.0;
	dXLeft		= 0.0;
	dYLeft		= 0.0;
	dZLeft		= 0.0;
	dWLeftOld	= 0.0;
	dXLeftOld	= 0.0;
	dYLeftOld	= 0.0;
	dZLeftOld	= 0.0;

	dOldPanAngle = 0.0;

	dARight		= 0.0;
	dERight		= 0.0;
	dWRight		= 0.0;
	dXRight		= 0.0;
	dYRight		= 0.0;
	dZRight		= 0.0;
	dWRightOld	= 0.0;
	dXRightOld	= 0.0;
	dYRightOld	= 0.0;
	dZRightOld	= 0.0;

#ifdef _SPACIAL_	// only need this for the master effect version
	fLRPanParam		= 0;
	fFBPanParam		= 0;
	fTypeOfMirror = 0.2;
#endif
	strcpy (name, "Ambisonic pan control");
}

//-----------------------------------------------------------------------------

ABFPan::ABFPan(audioMasterCallback audioMaster)
	: AudioEffectX(audioMaster, 32, kNumParams)
{
	programs = new ABFPanProgram[numPrograms];

	if (programs)
		setProgram (0);

	setNumInputs(kNumInputs);		// two in
	setNumOutputs(kNumOutputs);		// four out

#ifdef _SPACIAL_
	setUniqueID((long)"SBps");	// identify as standalone spacialiser version
#else
	setUniqueID((long)"SBpe");	// identify as standalone effect version
#endif


	canMono();		// makes sense to feed both inputs with the same signal
	canProcessReplacing();	// supports both accumulating and replacing output
	strcpy(programName, "Default");	// default program name



}

//-----------------------------------------------------------------------------------------
ABFPan::~ABFPan()
{
	if (programs)
		delete[] programs;


}

//-----------------------------------------------------------------------------------------
void ABFPan::setProgramName(char *name)
{
	strcpy(programs[curProgram].name, name);
}
//-----------------------------------------------------------------------------

bool ABFPan:: getVendorString (char* text)
{
	strcpy (text, "Swiss Center");
	return true;
}
//------------------------------------------------------------------------

VstPlugCategory ABFPan::getPlugCategory()
{
#ifdef _SPACIAL_
	return kPlugCategSpacializer;
#else
	return kPlugCategEffect;
#endif
}

//------------------------------------------------------------------------
void ABFPan::getProgramName (char *name)
{
	if (!strcmp (programs[curProgram].name, "Init"))
		sprintf (name, "%s %d", programs[curProgram].name, (int)(curProgram + 1));
	else
		strcpy (name, programs[curProgram].name);
}
//------------------------------------------------------------------------
double ABFPan::distancingLeft (double dBFactor)
{

	return  pow(10, (fDistanceFactorLeft * dBFactor * log(fDistanceLeft / fCentreLeft)) / kLog2);
}
//------------------------------------------------------------------------
double ABFPan::distancingRight (double dBFactor)
{

	return pow(10, (fDistanceFactorRight * dBFactor * log(fDistanceRight / fCentreRight)) / kLog2);
}

//------------------------------------------------------------------------
double ABFPan::gainLaw (double gain)
{
double vol;
	if (gain >= 0.5) {
	vol = pow(10, ((gain-1) * 2.0));	// generate log gain, based on (gain - 1) * 2 * 20/20
										// giving a 20 dB range	to half way down
	} else if (gain >= 0.2) {
	vol = pow(10, ((gain-0.625)*8.0));	// generate log gain, based on (gain - factor based on end  of previous range)* 2 * 40/20))
	} else {							// giving a 40 dB range over the next 0.3 range, total = 60dB
	vol = pow(10, ((gain-0.483333333333)*12.0));
}
	return vol;
}


//------------------------------------------------------------------------
void ABFPan::setProgram (long program)
{
	ABFPanProgram * ap = &programs[program];

	curProgram = program;
	setParameter (kAzimuthLeft, ap->fAzimuthLeft);
	setParameter (kElevationLeft,ap->fElevationLeft);
	setParameter (kVolumeLeft, ap->fVolumeLeft);
	setParameter (kDistanceLeft, ap->fDistanceLeft);
	setParameter (kCentreLeft, ap->fCentreLeft);
	setParameter (kDistanceFactorLeft, ap->fDistanceFactorLeft);
	setParameter (kZerothLeft, ap->fZerothLeft);
	setParameter (kFirstLeft, fFirstLeft = ap->fFirstLeft);

	setParameter (kAzimuthRight, fAzimuthRight = ap->fAzimuthRight);
	setParameter (kElevationRight, fElevationRight = ap->fElevationRight);
	setParameter (kVolumeRight, fVolumeRight = ap->fVolumeRight);
	setParameter (kDistanceRight, fDistanceRight = ap->fDistanceRight);
	setParameter (kCentreRight, ap->fCentreRight);
	setParameter (kDistanceFactorRight, ap->fDistanceFactorRight);
	setParameter (kZerothRight, fZerothRight = ap->fZerothRight);
	setParameter (kFirstRight, fFirstRight = ap->fFirstRight);

#ifdef _SPACIAL_	// only need this for the master effect version
	setParameter (kLRPanParam, fLRPanParam = ap->fLRPanParam);
	setParameter (kLRPanParam, fLRPanParam = ap->fLRPanParam);
	setParameter (kTypeOfMirror, fTypeOfMirror = ap->fTypeOfMirror);
	setParameter (kPanAngle, float(computePanAngle(fLRPanParam,fFBPanParam)));
#endif
}
//-----------------------------------------------------------------------------------------
void ABFPan::setParameter(long index, float value)
{
	ABFPanProgram * ap = &programs[curProgram];

	switch (index)
	{
		case kAzimuthLeft	: fAzimuthLeft	= ap->fAzimuthLeft = value; break;
		case kElevationLeft : fElevationLeft = ap->fElevationLeft = value; break;
		case kVolumeLeft 	: fVolumeLeft		= ap->fVolumeLeft = value; break;
		case kDistanceLeft	: 	if (value < 0.00001) {
									fDistanceLeft = float(0.00001);
								}else {
									fDistanceLeft = ap->fDistanceLeft = value;
									}
								break;
		case kDistanceFactorLeft : fDistanceFactorLeft = ap->fDistanceFactorLeft = value; break;
		case kCentreLeft	: 	if (value < 0.001) {
									fCentreLeft = float(0.001);
								}else {
									fCentreLeft = ap->fCentreLeft = value;
									}
								break;
		case kZerothLeft	: fZerothLeft = ap->fZerothLeft = value; break;
		case kFirstLeft		: fFirstLeft = ap->fFirstLeft = value; break;



		case kAzimuthRight	: fAzimuthRight	= ap->fAzimuthRight = value; break;
		case kElevationRight : fElevationRight = ap->fElevationRight = value; break;
		case kVolumeRight 	: fVolumeRight = ap->fVolumeRight = value; break;
		case kDistanceRight	: 	if (value < 0.00001) {
									fDistanceRight = float(0.00001);
								}else {
									fDistanceRight = ap->fDistanceRight = value;
									}
								break;
		case kDistanceFactorRight : fDistanceFactorRight = ap->fDistanceFactorRight = value; break;
		case kCentreRight	: 	if (value < 0.001) {
									fCentreRight = float(0.001);
								}else {
									fCentreRight = ap->fCentreRight = value;
									}
								break;
		case kZerothRight	: fZerothRight = ap->fZerothRight = value; break;
		case kFirstRight	: fFirstRight = ap->fFirstRight = value; break;

#ifdef _SPACIAL_	// only need this for the master effect version
		case kLRPanParam	: fLRPanParam = ap->fLRPanParam = value; break;
		case kFBPanParam	: fFBPanParam = ap->fFBPanParam = value; break;
		case kTypeOfMirror	: fTypeOfMirror = ap->fTypeOfMirror = value; break;
		case kPanAngle		: break;
#endif

	}
	if (editor)
		editor->postUpdate ();

	dALeft = (-fAzimuthLeft) * k2PI;	// change 0.0 - 1.0 range to radians
	dELeft = (fElevationLeft - 0.5) * kPI;	// change 0 -> 1.0 range to +- half radian
	dARight = (-fAzimuthRight) * k2PI;
	dERight = (fElevationRight - 0.5)  * kPI;


}


//-----------------------------------------------------------------------------------------


double computePanAngle(double dLRPan, double dFBPan)
{
	double alpha = atan2 (dLRPan - 0.5, 0.5 - dFBPan);
	if (alpha < 0.f)
		alpha += (float)k2PI;
	return ( -alpha);
}


//-----------------------------------------------------------------------------------------
double computePanDistance(double, double);

double computePanDistance(double dLRPan, double dFBPan)
{
	double dist = hypot (dLRPan - 0.5, dFBPan - 0.5);

	return (dist);
}



//-----------------------------------------------------------------------------------------
float ABFPan::getParameter(long index)
{
		float v = 0.0;

	switch (index)
	{
		case kAzimuthLeft	: v = fAzimuthLeft; break;
		case kElevationLeft : v = fElevationLeft; break;
		case kVolumeLeft 	: v = fVolumeLeft; break;
		case kDistanceLeft	: v = fDistanceLeft; break;
		case kDistanceFactorLeft : v = fDistanceFactorLeft; break;
		case kCentreLeft	: v = fCentreLeft; break;
		case kZerothLeft	: v = fZerothLeft; break;
		case kFirstLeft		: v = fFirstLeft; break;



		case kAzimuthRight	: v = fAzimuthRight; break;
		case kElevationRight : v = fElevationRight; break;
		case kVolumeRight 	: v = fVolumeRight; break;
		case kDistanceRight	: v = fDistanceRight; break;
		case kDistanceFactorRight : v = fDistanceFactorRight; break;
		case kCentreRight	: v = fCentreRight; break;
		case kZerothRight	: v = fZerothRight; break;
		case kFirstRight	: v = fFirstRight; break;

#ifdef _SPACIAL_	// only need this for the master effect version
		case kLRPanParam	: v = fLRPanParam; break;
		case kFBPanParam	: v = fFBPanParam; break;
		case kPanAngle		:	break;
		case kTypeOfMirror	: v = fTypeOfMirror; break;
#endif
	}
	return v;
}

//-----------------------------------------------------------------------------------------
void ABFPan::getParameterName(long index, char *label)
{
	switch (index)
	{
		case kAzimuthLeft	: strcpy (label, "Left Az"); break;
		case kElevationLeft : strcpy (label, "Left El"); break;
		case kVolumeLeft 	: strcpy (label, "Left Vol"); break;
		case kDistanceLeft	: strcpy (label, "Left Dist"); break;
		case kDistanceFactorLeft : strcpy (label, "Left DFact"); break;
		case kCentreLeft	: strcpy (label, "Left Centre"); break;
		case kZerothLeft	: strcpy (label, "Left Zero"); break;
		case kFirstLeft		: strcpy (label, "Left First"); break;



		case kAzimuthRight	: strcpy (label, "Rht Az"); break;
		case kElevationRight : strcpy (label, "Rht El"); break;
		case kVolumeRight 	: strcpy (label, "Rht Vol"); break;
		case kDistanceRight	: strcpy (label, "Rht Dist"); break;
		case kDistanceFactorRight : strcpy (label, "Rht DFact"); break;
		case kCentreRight	: strcpy (label, "Rht Centre"); break;
		case kZerothRight	: strcpy (label, "Rht Zero"); break;
		case kFirstRight	: strcpy (label, "Rht First"); break;

#ifdef _SPACIAL_	// only need this for the master effect version
		case kLRPanParam	: strcpy (label, "L_R Pan"); break;
		case kFBPanParam	: strcpy (label, "F_B Pan"); break;
		case kPanAngle		: strcpy (label, "PanAngle"); break;
		case kTypeOfMirror	: strcpy (label, "Mirror Type"); break;
#endif
		}
}

//-----------------------------------------------------------------------------------------
void ABFPan::getParameterDisplay (long index, char *text)
{
	switch (index)
	{
		case kAzimuthLeft	: float2string (float(360 - ((1.0 - fAzimuthLeft) *360)), text); break; // convert to degrees before sending back
		case kElevationLeft : float2string (float((fElevationLeft - 0.5) * 180), text); break; // convert to degrees before sending back
		case kVolumeLeft	: dB2string (fVolumeLeft, text); break;
		case kDistanceLeft	: float2string (fDistanceLeft * 10, text); break;
		case kDistanceFactorLeft : float2string (float((fDistanceFactorLeft * (-6.0))), text); break;
		case kCentreLeft	: float2string (fCentreLeft * 10, text); break;
		case kZerothLeft	: float2string (fZerothLeft * 100, text); break;
		case kFirstLeft		: float2string (fFirstLeft * 100, text); break;


		case kAzimuthRight	: float2string  (float(360 - ((1.0 - fAzimuthRight) * 360)), text); break;
		case kElevationRight : float2string (float((fElevationRight - 0.5) * 180), text); break;
		case kVolumeRight	: dB2string (fVolumeRight, text); break;
		case kDistanceRight	: float2string (fDistanceRight * 10, text); break;
		case kDistanceFactorRight : float2string (float((fDistanceFactorRight * (-6.0))), text); break;
		case kCentreRight	: float2string (fCentreRight * 10, text); break;
		case kZerothRight	: float2string (fZerothRight * 100, text); break;
		case kFirstRight	: float2string (fFirstRight * 100, text); break;

#ifdef _SPACIAL_	// only need this for the master effect version
		case kLRPanParam	:	float2string (fLRPanParam, text); break;

		case kFBPanParam	:	float2string (fFBPanParam, text); break;

		case kTypeOfMirror	: long2string (long ((fTypeOfMirror * 5) + 0.51f) , text); break;
		case kPanAngle		: float2string (float(360 + ((computePanAngle(fLRPanParam,fFBPanParam)) * kRtoD)),text); break;
#endif
	}
}

//-----------------------------------------------------------------------------------------
void ABFPan::getParameterLabel(long index, char *label)
{
	switch (index)
	{
		case kAzimuthLeft	: strcpy (label, "degrees"); break;
		case kElevationLeft : strcpy (label, "degrees"); break;
		case kVolumeLeft 	: strcpy (label, " dB "); break;
		case kDistanceLeft	: strcpy (label, "    "); break;
		case kDistanceFactorLeft	: strcpy (label, " dB "); break;
		case kCentreLeft	: strcpy (label, "    "); break;
		case kZerothLeft	: strcpy (label, "%"); break;
		case kFirstLeft		: strcpy (label, "%"); break;

		case kAzimuthRight	: strcpy (label, "degrees"); break;
		case kElevationRight : strcpy (label, "degrees"); break;
		case kVolumeRight 	: strcpy (label, " dB "); break;
		case kDistanceRight	: strcpy (label, "    "); break;
		case kDistanceFactorRight	: strcpy (label, " dB "); break;
		case kCentreRight	: strcpy (label, "    "); break;
		case kZerothRight	: strcpy (label, " % "); break;
		case kFirstRight	: strcpy (label, " % "); break;

#ifdef _SPACIAL_	// only need this for the master effect version
		case kLRPanParam	: strcpy (label, "    "); break;
		case kFBPanParam	: strcpy (label, "    "); break;
		case kTypeOfMirror	: strcpy (label, "mirror"); break;
		case kPanAngle		: strcpy (label, "degrees"); break;
#endif
	}
}
//-----------------------------------------------------------------------------------------
bool ABFPan::getInputProperties (long index, VstPinProperties* properties)
{
	if (index < kNumInputs)
	{
		sprintf (properties->label, "Channel %1d", (int)(index + 1));
		properties->flags = kVstPinIsActive;
		return true;
	}
	return false;
}
//-----------------------------------------------------------------------------------------
bool ABFPan::getOutputProperties (long index, VstPinProperties* properties)
{
	if (index < kNumOutputs)
	{
		sprintf (properties->label, "BForm %1d", (int)(index + 1));
		properties->flags = kVstPinIsActive;
		return true;
	}
	return false;
}




//-----------------------------------------------------------------------------------------
void ABFPan::process(float **inputs, float **outputs, long sampleFrames)
{
	double dAzLeft = dALeft;
	double dAzRight = dARight;
// 	double dElLeft = dELeft;
// 	double dElRight = dERight;

	float *in1  =  inputs[0];
	float *in2  =  inputs[1];
	float *out1 = outputs[0];
	float *out2 = outputs[1];
	float *out3 = outputs[2];
	float *out4 = outputs[3];

#ifdef _SPACIAL_	// only need this for the master effect version
	double dLRPanParam = double (fLRPanParam);
	double dFBPanParam = double (fFBPanParam);
	double dPanAngle = computePanAngle(dLRPanParam,dFBPanParam);
	if (dPanAngle != dOldPanAngle)
		updateDisplay();
	dOldPanAngle = dPanAngle;
	double dPanDistance = computePanDistance(dLRPanParam,dFBPanParam);
#endif

	double dDistanceRight = double (fDistanceRight);
	double dDistanceLeft = double (fDistanceLeft);
	double dCentreRight = double (fCentreRight);
	double dCentreLeft = double (fCentreLeft);
	double dVolumeLeft = gainLaw(double (fVolumeLeft));
	double dVolumeRight = gainLaw(double (fVolumeRight));

	double dFirstLeft = fFirstLeft;
	double dZerothLeft = fZerothLeft;
	double dFirstLeftDistance = 1.0;
	double dZerothLeftDistance = 1.0;
 	double dDistanceMultiplierLeft = 0.0;
	double dZerothRight = fZerothRight;
	double dFirstRight = fFirstRight;
	double dZerothRightDistance = 1.0;
	double dFirstRightDistance = 1.0;
	double dDistanceMultiplierRight = 0.0;

#ifdef _SPACIAL_	// only need this for the master effect version
	long lMirrorType = (long ((fTypeOfMirror * 5) + 0.51f));

 	switch (lMirrorType)
  	{
		case kNoMirror	:
						dDistanceLeft = dDistanceRight = dPanDistance;
						dCentreLeft = dCentreRight = 0.1;
						dAzRight += (dPanAngle + k2PI); // right channel needs wrapping to far side of circle
						dAzLeft  += dPanAngle;
						 // fall through to kNoPan

		case kNoPan	:
#endif
						dXLeft = cos(dAzLeft)* cos(dELeft);
						dXRight = cos(dAzRight)* cos(dERight);


						dYLeft = sin(dAzLeft)* cos(dELeft);
						dYRight = sin(dAzRight)* cos(dERight);

						dZLeft = sin(dELeft);
						dZRight = sin(dERight);
#ifdef _SPACIAL_	// only need this for the master effect version
						break;

		case kYMirror	:
						dXLeft = 2 * (0.5 - dFBPanParam) ;
						dXRight = 2 * (0.5 - dFBPanParam) ;

						dYLeft = 2 * (0.5 - dLRPanParam) ;
						dYRight = 2 * (dLRPanParam - 0.5) ;

						dZLeft = sin(dELeft);
						dZRight = sin(dERight);

						dDistanceLeft = dDistanceRight = dPanDistance;
						dCentreLeft = dCentreRight = 0.1;
						break;

		case kXMirror	:
						dXLeft = 2 * (0.5 - dFBPanParam) ;
						dXRight = 2 * (dFBPanParam - 0.5) ;

						dYLeft = 2 * (0.5 - dLRPanParam) ;
						dYRight = 2 * (0.5 - dLRPanParam) ;

						dZLeft = sin(dELeft);
						dZRight = sin(dERight);

						dDistanceLeft = dDistanceRight = dPanDistance;
						dCentreLeft = dCentreRight = 0.1;
						break;

		case kXYMirror	:
		default:
						dXLeft = 2 * (0.5 - dFBPanParam) ;
						dXRight = 2 * (dFBPanParam - 0.5) ;

						dYLeft = 2 * (0.5 - dLRPanParam) ;
						dYRight = 2 * (dLRPanParam - 0.5) ;

						dZLeft = sin(dELeft);
						dZRight = sin(dERight);

						dDistanceLeft = dDistanceRight = dPanDistance;
						dCentreLeft = dCentreRight = 0.1;

	}
#endif
 			if ( dDistanceLeft >= dCentreLeft ) {

 				dDistanceMultiplierLeft = distancingLeft(kdBFactor);	// kdBFactor is a constan factor
 																		// which allows us to set the maxloss
 																		// per doubling of distance

 				dZerothLeftDistance = dVolumeLeft * dZerothLeft * dDistanceMultiplierLeft;
 				dFirstLeftDistance =  dVolumeLeft * dFirstLeft * dDistanceMultiplierLeft;
 			} else {
 				dZerothLeftDistance = dVolumeLeft * dZerothLeft * (2 - (dDistanceLeft / dCentreLeft));
 				dFirstLeftDistance =  dVolumeLeft * dFirstLeft * (dDistanceLeft / dCentreLeft) ;
 			}

 			if ( fDistanceRight >= fCentreRight ) {
 				dDistanceMultiplierRight = distancingRight(kdBFactor);

 				dZerothRightDistance = dVolumeRight * dZerothRight * dDistanceMultiplierRight;
 				dFirstRightDistance =  dVolumeRight * fFirstRight * dDistanceMultiplierRight;
 			} else {
 				dZerothRightDistance = dVolumeRight * dZerothRight * (2 - (dDistanceRight / dCentreRight));
 				dFirstRightDistance =  dVolumeRight * dFirstRight * (dDistanceRight / dCentreRight) ;
 			}

			dWLeft = kAntiDenorm + dZerothLeftDistance;
			dXLeft = kAntiDenorm + dFirstLeftDistance * dXLeft;
			dYLeft = kAntiDenorm + dFirstLeftDistance * dYLeft;
			dZLeft = kAntiDenorm + dFirstLeftDistance * dZLeft;

			dWRight = kAntiDenorm + dZerothRightDistance;
			dXRight = kAntiDenorm + dFirstRightDistance * dXRight;
			dYRight = kAntiDenorm + dFirstRightDistance * dYRight;
			dZRight = kAntiDenorm + dFirstRightDistance * dZRight;


    while(--sampleFrames >= 0)
    {
        (*out1) +=  float(kAntiDenorm + (*in1) * dWLeftOld);      // accumulating
        (*out2) +=  float(kAntiDenorm + (*in1) * dXLeftOld);
        (*out3) +=  float(kAntiDenorm + (*in1) * dYLeftOld);      // accumulating
        (*out4) +=  float(kAntiDenorm + (*in1++) * dZLeftOld);
        (*out1++) +=  float(kAntiDenorm + (*in2) * dWRightOld);   // accumulating
        (*out2++) +=  float(kAntiDenorm + (*in2) * dXRightOld);
        (*out3++) +=  float(kAntiDenorm + (*in2) * dYRightOld);   // accumulating
        (*out4++) +=  float(kAntiDenorm + (*in2++) * dZRightOld);
         dWLeftOld	= (dWLeftOld * kAvCoef1) + (kAvCoef2 * dWLeft);
		 dXLeftOld	= (dXLeftOld * kAvCoef1) + (kAvCoef2 * dXLeft);
		 dYLeftOld	= (dYLeftOld * kAvCoef1) + (kAvCoef2 * dYLeft);
		 dZLeftOld	= (dZLeftOld * kAvCoef1) + (kAvCoef2 * dZLeft);
		 dWRightOld	= (dWRightOld * kAvCoef1) + (kAvCoef2 * dWRight);
		 dXRightOld	= (dXRightOld * kAvCoef1) + (kAvCoef2 * dXRight);
		 dYRightOld	= (dYRightOld * kAvCoef1) + (kAvCoef2 * dYRight);
		 dZRightOld	= (dZRightOld * kAvCoef1) + (kAvCoef2 * dZRight);

    }
}

//-----------------------------------------------------------------------------------------
void ABFPan::processReplacing(float **inputs, float **outputs, long sampleFrames)
{
	double dAzLeft = dALeft;
	double dAzRight = dARight;
// 	double dElLeft = dELeft;
// 	double dElRight = dERight;

	float *in1  =  inputs[0];
	float *in2  =  inputs[1];
	float *out1 = outputs[0];
	float *out2 = outputs[1];
	float *out3 = outputs[2];
	float *out4 = outputs[3];

#ifdef _SPACIAL_	// only need this for the master effect version
	double dLRPanParam = double (fLRPanParam);
	double dFBPanParam = double (fFBPanParam);
	double dPanAngle = computePanAngle(dLRPanParam,dFBPanParam);
	if (dPanAngle != dOldPanAngle)
		updateDisplay();
	dOldPanAngle = dPanAngle;
	double dPanDistance = computePanDistance(dLRPanParam,dFBPanParam);
#endif

	double dDistanceRight = double (fDistanceRight);
	double dDistanceLeft = double (fDistanceLeft);
	double dCentreRight = double (fCentreRight);
	double dCentreLeft = double (fCentreLeft);
	double dVolumeLeft = gainLaw(double (fVolumeLeft));
	double dVolumeRight = gainLaw(double (fVolumeRight));

	double dFirstLeft = fFirstLeft;
	double dZerothLeft = fZerothLeft;
	double dFirstLeftDistance = 1.0;
	double dZerothLeftDistance = 1.0;
 	double dDistanceMultiplierLeft = 0.0;
	double dZerothRight = fZerothRight;
	double dFirstRight = fFirstRight;
	double dZerothRightDistance = 1.0;
	double dFirstRightDistance = 1.0;
	double dDistanceMultiplierRight = 0.0;

	if (dCentreLeft < 0.001 )	// set limits to how small the centres can be to avoid nasty graunching sounds
		dCentreLeft = 0.001;
	if (dCentreRight < 0.001 )
		dCentreRight = 0.001;


#ifdef _SPACIAL_	// only need this for the master effect version
	long lMirrorType = (long ((fTypeOfMirror * 5) + 0.51f));

 	switch (lMirrorType)
  	{
		case kNoMirror	:
						dDistanceLeft = dDistanceRight = dPanDistance;
						dCentreLeft = dCentreRight = 0.1;
						dAzRight += (dPanAngle + k2PI); // right channel needs wrapping to far side of circle
						dAzLeft  += dPanAngle;
						 // fall through to kNoPan

		case kNoPan	:
#endif
						dXLeft = cos(dAzLeft)* cos(dELeft);
						dXRight = cos(dAzRight)* cos(dERight);


						dYLeft = sin(dAzLeft)* cos(dELeft);
						dYRight = sin(dAzRight)* cos(dERight);

						dZLeft = sin(dELeft);
						dZRight = sin(dERight);
#ifdef _SPACIAL_	// only need this for the master effect version
						break;

		case kYMirror	:
						dXLeft = 2 * (0.5 - dFBPanParam) ;
						dXRight = 2 * (0.5 - dFBPanParam) ;

						dYLeft = 2 * (0.5 - dLRPanParam) ;
						dYRight = 2 * (dLRPanParam - 0.5) ;

						dZLeft = sin(dELeft);
						dZRight = sin(dERight);

						dDistanceLeft = dDistanceRight = dPanDistance;
						dCentreLeft = dCentreRight = 0.1;
						break;

		case kXMirror	:
						dXLeft = 2 * (0.5 - dFBPanParam) ;
						dXRight = 2 * (dFBPanParam - 0.5) ;

						dYLeft = 2 * (0.5 - dLRPanParam) ;
						dYRight = 2 * (0.5 - dLRPanParam) ;

						dZLeft = sin(dELeft);
						dZRight = sin(dERight);

						dDistanceLeft = dDistanceRight = dPanDistance;
						dCentreLeft = dCentreRight = 0.1;
						break;

		case kXYMirror	:
		default:
						dXLeft = 2 * (0.5 - dFBPanParam) ;
						dXRight = 2 * (dFBPanParam - 0.5) ;

						dYLeft = 2 * (0.5 - dLRPanParam) ;
						dYRight = 2 * (dLRPanParam - 0.5) ;

						dZLeft = sin(dELeft);
						dZRight = sin(dERight);

						dDistanceLeft = dDistanceRight = dPanDistance;
						dCentreLeft = dCentreRight = 0.1;

	}
#endif
		if ( dDistanceLeft >= dCentreLeft ) {

 				dDistanceMultiplierLeft = distancingLeft(kdBFactor);	// kdBFactor is a constan factor
 																		// which allows us to set the maxloss
 																		// per doubling of distance


 				dZerothLeftDistance = dVolumeLeft * dZerothLeft * dDistanceMultiplierLeft;
 				dFirstLeftDistance =  dVolumeLeft * dFirstLeft * dDistanceMultiplierLeft;
 			} else {
 				dZerothLeftDistance = dVolumeLeft * dZerothLeft * (2 - (dDistanceLeft / dCentreLeft));
 				dFirstLeftDistance =  dVolumeLeft * dFirstLeft * (dDistanceLeft / dCentreLeft) ;
 			}

 			if ( fDistanceRight >= fCentreRight ) {
 				dDistanceMultiplierRight = distancingRight(kdBFactor);

 				dZerothRightDistance = dVolumeRight * dZerothRight * dDistanceMultiplierRight;
 				dFirstRightDistance =  dVolumeRight * fFirstRight * dDistanceMultiplierRight;
 			} else {
 				dZerothRightDistance = dVolumeRight * dZerothRight * (2 - (dDistanceRight / dCentreRight));
 				dFirstRightDistance =  dVolumeRight * dFirstRight * (dDistanceRight / dCentreRight) ;
 			}

 			dWLeft = kAntiDenorm + dZerothLeftDistance;
 			dXLeft = kAntiDenorm + dFirstLeftDistance * dXLeft;
 			dYLeft = kAntiDenorm + dFirstLeftDistance * dYLeft;
 			dZLeft = kAntiDenorm + dFirstLeftDistance * dZLeft;

 			dWRight = kAntiDenorm + dZerothRightDistance;
 			dXRight = kAntiDenorm + dFirstRightDistance * dXRight;
 			dYRight = kAntiDenorm + dFirstRightDistance * dYRight;
 			dZRight = kAntiDenorm + dFirstRightDistance * dZRight;

    while(--sampleFrames >= 0)
    {
        (*out1) =  float(kAntiDenorm + (*in1) * dWLeftOld);      // replacing
        (*out2) =  float(kAntiDenorm + (*in1) * dXLeftOld);		// replacing
        (*out3) =  float(kAntiDenorm + (*in1) * dYLeftOld);      // replacing
        (*out4) =  float(kAntiDenorm + (*in1++) * dZLeftOld);	// replacing
        (*out1++) +=  float(kAntiDenorm + (*in2) * dWRightOld);	// replacing with local accumulation
        (*out2++) +=  float(kAntiDenorm + (*in2) * dXRightOld);	// replacing with local accumulation
        (*out3++) +=  float(kAntiDenorm + (*in2) * dYRightOld);	// replacing with local accumulation
        (*out4++) +=  float(kAntiDenorm + (*in2++) * dZRightOld);	// replacing with local accumulation
         dWLeftOld	= (dWLeftOld * kAvCoef1) + (kAvCoef2 * dWLeft);
		 dXLeftOld	= (dXLeftOld * kAvCoef1) + (kAvCoef2 * dXLeft);
		 dYLeftOld	= (dYLeftOld * kAvCoef1) + (kAvCoef2 * dYLeft);
		 dZLeftOld	= (dZLeftOld * kAvCoef1) + (kAvCoef2 * dZLeft);
		 dWRightOld	= (dWRightOld * kAvCoef1) + (kAvCoef2 * dWRight);
		 dXRightOld	= (dXRightOld * kAvCoef1) + (kAvCoef2 * dXRight);
		 dYRightOld	= (dYRightOld * kAvCoef1) + (kAvCoef2 * dYRight);
		 dZRightOld	= (dZRightOld * kAvCoef1) + (kAvCoef2 * dZRight);

    }
}
//-----------------------------------------------------------------------------------------

