/*-----------------------------------------------------------------------------

 1999, Steinberg Soft und Hardware GmbH, All Rights Reserved

-----------------------------------------------------------------------------*/

#include <iostream>

#include <AudioEffect.cpp>
#include <audioeffectx.h>
#include <audioeffectx.cpp>
#include "../_vstgui/vstgui.cpp"
#include "../_vstgui/vstcontrols.cpp"

#include "VstPlugin.cpp"
#include "Editor.cpp"

//==============================================================================
extern "C" __attribute__((visibility("default"))) AEffect* main_plugin (audioMasterCallback audioMaster) asm ("main");

extern "C" __attribute__((visibility("default"))) AEffect *main_plugin (audioMasterCallback audioMaster)
{
    VstPlugin* const plugin = new VstPlugin (audioMaster);

    if (plugin)
        return plugin->getAeffect();

    return 0;
}

__attribute__((constructor)) void myPluginInit() // this is called when the library is unoaded
{
//    std::cout << "myPluginInit" << std::endl;
}

__attribute__((destructor)) void myPluginFini() // this is called when the library is unoaded
{
//    std::cout << "myPluginFini" << std::endl;
}
