
// Copyright (c) 2007, Nils Jonas Norberg, See COPYING.txt for details

#include <stdio.h>

// #include "guieditor.h"

#include "audjoo_helix.h"
#include "types.h"
#include "synth.h"
#include "params.h"

#define _USE_MATH_DEFINES
#include <math.h>

void write_float_in_char_buffer(u8 *wb, float f)
{
	u32& it = *(u32*)(&f);
	wb[0] = (u8)(it >> 24);
	wb[1] = (u8)((it >> 16) & 255);
	wb[2] = (u8)((it >> 8) & 255);
	wb[3] = (u8)((it & 255));
}

float read_float_from_char_buffer(u8 *wb)
{
	u32 it = wb[0] << 24;
	it |= wb[1] << 16;
	it |= wb[2] << 8;
	it |= wb[3];
	return *(float*)&it;
}

// the defaultbank...
// #include "defaultBank.h"


//-----------------------------------------------------------------------------------------
// audjoo_helix
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
audjoo_helix::audjoo_helix (audioMasterCallback audioMaster)
	: AudioEffectX (audioMaster, kNumPrograms, 219)
	, eventCount(0)
{
	// initialize programs
	std::vector<Params::Param>& pv = s.getParams()->data;
	int pcount = pv.size();
	int str_size = 0;
	for ( int i = 0 ; i != pcount ; ++i )
	{
		str_size += pv[i].GetLongName().size() + 1; // added one for null-term
	}

	str_size += 1; // sentinel zero

	program_data_size = 48 + 24 + sizeof(float)*pcount + str_size;
	bank_data_size = 48 + ( 24 + sizeof(float)*pcount )*kNumPrograms + str_size;

	// the layout of a program is like this:
	//   48B header "JonasNorberg "
	//              "Helix        "
	//              "Program      "
	//   all strings describing the order of parameters, 0-terminated
    //
	//   name of the program as char[24]
	//   floats (describing not sliders but values in the synth)

		// the layout of a bank is like this:
	//   48B header "JonasNorberg "
	//              "Helix        "
	//              "Bank         "
	//   all strings describing the order of parameters, 0-terminated
	//
	//   all programs
	//   {
	//     names of the programs as char[24]
	//     all floats (describing not sliders but values in the synth)
	//   }

	program_data = new u8[ program_data_size ];
	memset(program_data, 0, program_data_size );
	u8* run = program_data;
	memcpy( run, "JonasNorberg####Helix###########Program#########", 48 );
	run += 48;

	// write out the parameters in their order
	for ( int i = 0 ; i != pcount ; ++i )
	{
		const std::string& s = pv[i].GetLongName();
		memcpy(run, s.c_str(), s.size() + 1);
		run += s.size() + 1; // add one for the null-term
	}
	*run = 0; // sentinel-terminator
	++run;

	header_size = run - program_data;

	memcpy( run, "Default Program\0\0\0\0\0\0\0\0", 24 );
	run += 24;

	for ( int i = 0 ; i != pcount ; ++i )
	{
		float v = pv[i].GetRangeValue();
		write_float_in_char_buffer( run, v );
		run += 4;
	}

	// phew, done program, let's do the bank

	bank_data = new u8[ bank_data_size ];
	memset(bank_data, 0, bank_data_size );
	run = bank_data;
	memcpy( run, "JonasNorberg####Helix###########Bank############", 48 );
	run += 48;

	// write out the parameters in their order
	for ( int i = 0 ; i != pcount ; ++i )
	{
		const std::string& s = pv[i].GetLongName();
		memcpy(run, s.c_str(), s.size() + 1);
		run += s.size() + 1; // add one for the null-term
	}
	*run = 0; // sentinel-terminator
	++run;

	for (int p = 0 ; p != kNumPrograms ; ++p )
	{
		sprintf( (char*)run, "Program %d", p );
		run += 24;
		for ( int i = 0 ; i != pcount ; ++i )
		{
			float v = pv[i].GetRangeValue();
			write_float_in_char_buffer( run, v );
			run += 4;
		}
	}

	// --- // --- // --- // --- // --- // --- // ---

	if (audioMaster)
	{
  		setNumInputs (kNumInputs);		// 0 inputs
		setNumOutputs (kNumOutputs);	// 2 outputs
		canProcessReplacing ();
		hasVu (false);
		hasClip (false);
		isSynth ();
		programsAreChunks();
		setUniqueID ('x!!$');			// <<<! FIXME!!! *must* change this!!!!
	}

//	editor = new guiEditor (this);

/*
    static const u8 fxbHeaderSize = 0xa0;
	setChunk( defaultBank + fxbHeaderSize, sizeof(defaultBank) );
*/

	initProcess ();
	suspend ();
}

//-----------------------------------------------------------------------------------------
audjoo_helix::~audjoo_helix ()
{
	delete [] program_data;
	program_data = 0;

    delete [] bank_data;
	bank_data = 0;
}


long audjoo_helix::getChunk (void** data, bool isPreset)
{
	if (isPreset)
	{
        *data = program_data;
		return program_data_size;
	}

	// prog-name and params
	int prog_size = s.getParams()->data.size() * sizeof(float) + 24;

	// save the current program to the bank...
	u8* bank_data0 = bank_data + header_size + (curProgram * prog_size );
	u8* prog_data = program_data + header_size;
	memcpy( bank_data0, prog_data, prog_size );

	*data = bank_data;
	return bank_data_size;
}

// returns -1 on no null
int strnlen(const u8* s, int max_len)
{
	const u8* o_s = s;
	for ( ; max_len ; --max_len, ++s)
	{
		if (*s == 0)
			return s - o_s;
	}
	return -1;
}

int find_index(const Params *p, const u8* s)
{
	const std::vector<Params::Param>& pv = p->data;
	int index = pv.size() - 1;
	for ( ; index != -1 ; --index )
	{
		if ( strcmp((char*)s, pv[index].GetLongName().c_str() ) == 0)
			break;
	}
	return index;
}

long audjoo_helix::setChunk (void* data, long byteSize, bool isPreset)
{
	u8 *cdata = static_cast<u8*>(data);

	if (byteSize < 49)
	{
		// FIXME!!! error message
		return -1;
	}

	if ( memcmp(cdata, "JonasNorberg####", 16) != 0 )
	{
		// FIXME!!! error message
		return -1;
	}

	Params* p = s.getParams();

	u8* run = cdata;
	u8* run_stop = cdata + byteSize;

	run += 48; // skip header

	// maps from the chunk to the params...
	std::vector<int> mapping;

	// build mapping to params
	while (*run)
	{
		u8* paramname = run;
		int len = strnlen( paramname, run_stop - run);
		if (len == -1)
		{
			// FIXME!!! error message
			return -1;
		}

		int index = find_index(p,paramname);
		mapping.push_back(index);
		run += len+1; // add one for terminating null
	}
	++run; // skip sentinel

	// reset the params so that all values are "init"
	p->Reset();

	// FIXME check that the remaining size is at least what we expect
	int remaining_size = run_stop - run;
	int prog_size = mapping.size() * sizeof(float) + 24;

	if (isPreset)
	{
		if (remaining_size < prog_size)
		{
			// FIXME!! error message
			return -1;
		}

		u8* writer = program_data + header_size;
		memcpy(writer,run,24); // copy name
		writer += 24;
		run += 24;

		// set all params (from the chunk)
		for( u32 i = 0 ; i != mapping.size() ; ++i )
		{
			int param_index = mapping[i];
			if (param_index != -1)
			{
				// go through params to properly clamp values
				float v = read_float_from_char_buffer(run);
				p->data[param_index].SetValueFromRange(v);
			}
			run+=4;
		}

		// get all params and write to program
		for( u32 i = 0 ; i != p->data.size() ; ++i )
		{
			float good_v = p->data[i].GetRangeValue();
			write_float_in_char_buffer(writer,good_v);
			writer+=4;
		}
	}
	else
	{
		// is a whole bank
		u8* writer = bank_data + header_size;

		// at least a whole program left
		while (run < (run_stop-prog_size+1) )
		{
			memcpy(writer,run,24); // copy name
			writer += 24;
			run += 24;

			// set all params (from the chunk)
			for( u32 i = 0 ; i != mapping.size() ; ++i )
			{
				int param_index = mapping[i];
				if (param_index != -1)
				{
					// go through params to properly clamp values
					float v = read_float_from_char_buffer(run);
					p->data[param_index].SetValueFromRange(v);
				}
				run+=4;
			}

			// get all params and write to bank
			for( u32 i = 0 ; i != p->data.size() ; ++i )
			{
				float good_v = p->data[i].GetRangeValue();
				write_float_in_char_buffer(writer,good_v);
				writer+=4;
			}
		}

		// prog-name and params
		int prog_size = s.getParams()->data.size() * sizeof(float) + 24;

		// overwrite the current program with data from the bank...
		u8* bank_data0 = bank_data + header_size + (curProgram * prog_size );
		u8* prog_data = program_data + header_size;
		memcpy( prog_data, bank_data0, prog_size );
	}

	FeedProgramToParams();
	return 1;
}

void audjoo_helix::FeedProgramToParams()
{
	// set params from program...
	std::vector<Params::Param>& pv = s.getParams()->data;
	u8* values = program_data + header_size + 24;

	int pcount = pv.size();
	for ( int i = 0 ; i != pcount ; ++i )
	{
		float v = read_float_from_char_buffer(values);
		pv[i].SetValueFromRange(v);
		values += 4;
	}

//	if (editor)
//		((guiEditor*)editor)->ParamChanged();
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::setProgram (long program)
{
	if ( program < 0 || program >= kNumPrograms )
		return;

	// prog-name and params
	int prog_size = s.getParams()->data.size() * sizeof(float) + 24;

	// save the current program to the bank...
	u8* bank_data0 = bank_data + header_size + (curProgram * prog_size );
	u8* bank_data1 = bank_data + header_size + (program * prog_size );
	u8* prog_data = program_data + header_size;

	memcpy( bank_data0, prog_data, prog_size );

	curProgram = program;

	// replace current program from the bank...
	memcpy( prog_data, bank_data1, prog_size );

	FeedProgramToParams();
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::setProgramName (char *name)
{
	memcpy( program_data + header_size, name, 24);
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::getProgramName (char *name)
{
	memcpy(name, program_data + header_size, 24 );
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::getParameterLabel (long index, char *label)
{
	Params* p = s.getParams();
	if ((u32)index >= p->data.size())
	{
		*label = 0;
		return;
	}
	p->data[index].GetUnit(label);
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::getParameterDisplay (long index, char *text)
{
	Params* p = s.getParams();
	if ((u32)index >= p->data.size())
	{
		*text = 0;
		return;
	}
	p->data[index].GetDisplay(text);
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::getParameterName (long index, char *label)
{
	Params* p = s.getParams();
	if ((u32)index >= p->data.size())
	{
		*label = 0;
		return;
	}
	p->data[index].GetShortName(label);
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::setParameter (long index, float value)
{
	Params* p = s.getParams();
	if ((u32)index >= p->data.size())
		return;

	p->data[index].SetValueFromSlider(value);

	// write to program (keep it updated)
	float v = p->data[index].GetRangeValue();
	u8* valuep = program_data + header_size + 24 + index * sizeof(float);
	write_float_in_char_buffer(valuep, v);

	// notify graphics
//	if (editor)
//		((guiEditor*)editor)->ParamChanged(index);
}

//-----------------------------------------------------------------------------------------
float audjoo_helix::getParameter (long index)
{
	Params* p = s.getParams();
	if ((u32)index >= p->data.size())
	{
		return 0;
	}
	return p->data[index].GetSliderValue();
}

//-----------------------------------------------------------------------------------------
bool audjoo_helix::getOutputProperties (long index, VstPinProperties* properties)
{
	if (index < kNumOutputs)
	{
		sprintf (properties->label, "Vstx %1d", index + 1);
		properties->flags = kVstPinIsActive;
		if (index < 2)
			properties->flags |= kVstPinIsStereo;	// make channel 1+2 stereo
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------
bool audjoo_helix::getProgramNameIndexed (long /*category*/, long index, char* text)
{
	if (index < kNumPrograms)
	{
		int prog_size = s.getParams()->data.size() * sizeof(float) + 24;
		memcpy(text, bank_data + header_size + (index * prog_size ), 24);
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------
bool audjoo_helix::copyProgram (long destination)
{
	if (destination < kNumPrograms)
	{
		int prog_size = s.getParams()->data.size() * sizeof(float) + 24; // including name
		memcpy( bank_data + header_size + (destination * prog_size ), program_data + header_size, prog_size );
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------
bool audjoo_helix::getEffectName (char* name)
{
	strcpy (name, "audjoo_helix");
	return true;
}

//-----------------------------------------------------------------------------------------
bool audjoo_helix::getVendorString (char* text)
{
	strcpy (text, "Jonas Norberg");
	return true;
}

//-----------------------------------------------------------------------------------------
bool audjoo_helix::getProductString (char* text)
{
	strcpy (text, "audjoo_helix");
	return true;
}

long audjoo_helix::getVendorVersion ()
{
	return 1000;
}

//-----------------------------------------------------------------------------------------
long audjoo_helix::canDo (char* text)
{
	if (!strcmp (text, "receiveVstEvents"))
		return 1;
	if (!strcmp (text, "receiveVstMidiEvent"))
		return 1;
	return -1;	// explicitly can't do; 0 => don't know
}

