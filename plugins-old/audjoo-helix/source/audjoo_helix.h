#ifndef __audjoo_helix__
#define __audjoo_helix__

// Copyright (c) 2007, Nils Jonas Norberg, See COPYING.txt for details

#ifndef __audioeffectx__
#include "audioeffectx.h"
#endif


#define _USE_MATH_DEFINES
#include <math.h>

#include "synth.h"

//------------------------------------------------------------------------------------------
enum
{
	// Global
	kNumPrograms = 64,
	kNumInputs = 0,
	kNumOutputs = 2
};

//------------------------------------------------------------------------------------------
// audjoo_helix
//------------------------------------------------------------------------------------------

class audjoo_helix : public AudioEffectX
{
public:
	audjoo_helix (audioMasterCallback audioMaster);
	~audjoo_helix ();

	virtual void process (float **inputs, float **outputs, long sampleframes);
	virtual void processReplacing (float **inputs, float **outputs, long sampleframes);

	virtual void processImpl(float **inputs, float **outputs, long sampleframes, bool accum);

	virtual long processEvents (VstEvents* events);

	virtual void setProgram (long program);
	virtual void setProgramName (char *name);
	virtual void getProgramName (char *name);
	virtual bool getProgramNameIndexed (long category, long index, char* text);
	virtual bool copyProgram (long destination);

	virtual void setParameter (long index, float value);
	virtual float getParameter (long index);
	virtual void getParameterLabel (long index, char *label);
	virtual void getParameterDisplay (long index, char *text);
	virtual void getParameterName (long index, char *text);

	virtual void setSampleRate (float sampleRate);
	virtual void setBlockSize (long blockSize);

	virtual void resume ();

	virtual bool getOutputProperties (long index, VstPinProperties* properties);

	virtual bool getEffectName (char* name);
	virtual bool getVendorString (char* text);
	virtual bool getProductString (char* text);
	virtual long getVendorVersion ();
	virtual long canDo (char* text);

	Synth& getSynth() { return s; }
	const Synth& getSynth() const { return s; }

	long getChunk (void** data, bool isPreset = false);
	long setChunk (void* data, long byteSize, bool isPreset = false);

private:
	void FeedProgramToParams();
	void processReplacingInternalDontHandleEvents(float* left, float* right, long sampleframes);

	void initProcess ();
	void fillProgram (long channel, long prg, MidiProgramName* mpn);

	enum EventType
	{
		RAWMIDI,ON,OFF,ALLOFF
	};

	struct Event
	{
		long deltaFrames;
		EventType eventType;
        char status;
		char note; // or CC number
		char vel; // or CC value
	};

	static const int EVENTMAX = 512;
	Event eventQueue[EVENTMAX];
	int eventCount;

	void queueMidi(long deltaFrames, char status, char midi1, char midi2);
	void queueNoteOn(long deltaFrames, char note, char velocity);
	void queueNoteOff(long deltaFrames, char note);
	void queueNoteOffAll(long deltaFrames );
	void fixNewEvent(); // called by the queue functions to make sure all events are queued

	void HandleEvent(const Event& event);

	Synth s;

	u8* program_data;
	u8* bank_data;
	int program_data_size;
	int bank_data_size;
	int header_size; // to make it easier to tap right in to the programs...
};

#endif
