
// Copyright (c) 2007, Nils Jonas Norberg, See COPYING.txt for details

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include <assert.h>

#include <list>

#pragma warning( push )
#pragma warning( disable : 100 )
#ifndef __guiEditor__
#include "guiEditor.h"
#endif
#pragma warning( pop )


#include "params.h"
#include "audjoo_helix.h"
#include "fastmath.inl"


// try opening the skin...
// for getting dll-path...
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#include <vector>
#include <psdlite.h>


void stringConvert (float value, char* string);

struct MyFont
{
    MyFont( psdlite::bitmap& bm )
        :myBm(bm)
    {
        int w = bm.get_size().x;
        int h = bm.get_size().y;

        for ( int x = 0 ; x < w ; ++x )
        {
            charPixels.push_back(x);

            // skip character
            for ( ; x < w ; ++x )
            {
                int y = 0;
                for ( ; y != h ; ++y )
                {
                    psdlite::pixel p;
                    p = bm.get_pixel(x,y);
                    if ( p.a() > 1 )
                        break;
                }
                if ( y == h )
                    break;
            }
        }

        // just so that we can get the w by [i+1] - [i]
        charPixels.push_back(x);

        for (int i = 0 ; i < charPixels.size() ; ++i )
        {
            int x = charPixels[i];
        }
    };

    psdlite::bitmap myBm;
    std::vector< int > charPixels;
};




























void get_skin_image_name(std::string& str)
{
	str.clear();
	std::vector<char> pathname(4096);
	extern void* hInstance;
	HINSTANCE hi = (HINSTANCE)hInstance;
	int len = GetModuleFileName( hi, &pathname[0], 4096 );
	char* path = &pathname[0];
	char* ext = path + len - 4;
	if ( strcmp(ext,".dll") == 0 || strcmp(ext,".DLL") == 0)
	{
		str.assign(path, ext );
		str += "_skin.psd";
	}
}

// load the psd image into memory...
void get_image( psdlite::layered_image& li )
{
	std::string filename;
	get_skin_image_name(filename);
	psdlite::load_layered_image( li, filename.c_str() );

	static const int LAYERMAXDEPTH = 256;
	int nameStack[LAYERMAXDEPTH];
	int nameStackCount = 0;

	// parse the names
	std::string context;
	context.reserve(1024);

	for ( int i = li.layers_.size()-1 ; i != -1 ; --i)
	{
        psdlite::layer& l = li.layers_[i];
		std::string& name = l.name_;

		const psdlite::vi2& s = l.data_.get_size();
		if (s.x == 0 && s.y == 0 && !name.empty() )
		{
			// is layer start or end...
			if (name == "</Layer set>")
			{
				// pop...
				if (!nameStackCount)
					return; // FIXME warn about illformed layers
				--nameStackCount;
				context.erase( nameStack[nameStackCount] );
			}
			else
			{
				int len = name.size();
				// push...
				nameStack[nameStackCount] = context.size();
				++nameStackCount;
				if (nameStackCount >= LAYERMAXDEPTH)
				{
					// FIXME!!! warn about illformed layers...
					return;
				}

				if (name[0] == '\"' && name[len-1] == '\"')
				{
					context.append( name.begin() + 1, name.begin() + len-1 );
				}
			}
		}
		else
		{
			// fixup name
			l.name_ = context + l.name_;
		}

	}

}

CBitmap* getBitmap( CFrame& frame, CDrawContext* dc, const psdlite::bitmap& b )
{
	psdlite::vi2 s = b.get_size();

	CBitmap* bm = new CBitmap( frame, s.x, s.y);

	COffscreenContext* temp_oc = new COffscreenContext( dc , bm, true );

	// draw stuff to bitmap...
	CPoint p;
	CColor col;
	for ( p.y = 0 ; p.y != s.y ; ++p.y)
	{
		for ( p.x = 0 ; p.x != s.x ; ++p.x)
		{
			psdlite::pixel pix = b.get_pixel(p.x,p.y);
			col( pix.r(), pix.g(), pix.b(), pix.a() );
			temp_oc->drawPoint( p, col );
		}
	}

	delete temp_oc;
	return bm;
}

struct BitmapResult
{
	BitmapResult()
		:bm(0)
	{}

	BitmapResult( const BitmapResult& a )
	{
		bm = a.bm;
		if (bm)
			bm->remember();
		name = a.name;
	}

	~BitmapResult()
	{
		if (bm)
			bm->forget();
	}

	void swap( BitmapResult& a )
	{
		std::swap( bm, a.bm );
		name.swap( a.name );
	}

	void operator=( const BitmapResult& a )
	{
		BitmapResult b(a);
		this->swap(b);
	}

	CBitmap* bm;
	std::string name;
};

struct ControlResult
{
	ControlResult()
		:knob(0)
	{}

	std::string name;
	CBitmap* knob;
	CRect rect;
};


CBitmap* findBitmap( const std::vector<BitmapResult>& bm, const std::string& name )
{
	for ( unsigned int i = 0 ; i != bm.size() ; ++i )
		if (bm[i].name == name)
			return bm[i].bm;
	return 0;
}

void paintTextBg( CBitmap* bm, CFrame& frame, const CRect& size, const std::string& str, MyFont* font )
{
	HWND hwnd = (HWND)frame.getSystemWindow();
	HDC hdc = GetDC(hwnd);
	CDrawContext* temp_dc = new CDrawContext( &frame, hdc, hwnd );
	COffscreenContext* temp_oc = new COffscreenContext( temp_dc , bm, true );

    int height = font->myBm.get_size().y;

	CPoint p;
    p.x = size.left;
    p.y = size.top;

	CColor col;
    col( 0,0,0, 255 );
	for ( int c = 0 ; c != str.size() ; ++c )
	{
        char ch = str[c];
        if (ch == ' ')
        {
            p.x += 5.f;
        }
        else
        {
            // make uppercase
            if ('a' <= ch && ch <= 'z' )
                ch -= 'a'-'A';

            int i = ch - '!';
            if (i < 0)
                i = 0;
            else if (i > font->charPixels.size() - 1 )
                i = 0;

            int cx = font->charPixels[i];
            int cw = font->charPixels[i+1] - cx - 1;

            for ( int y = 0 ; y < height ; ++y )
            {
                for ( int x = 0 ; x < cw ; ++x)
                {
                    psdlite::pixel pix = font->myBm.get_pixel( cx + x, y );
                    if ( pix.a() > 0 )
                    {
                        col( pix.r(), pix.g(), pix.b(), 255 );
                        CPoint q;
                        q( p.x + x, p.y + y);
                        temp_oc->drawPoint( q, col );
                    }
                }
            }

            p.x += cw + 1;
        }
	}

    delete temp_oc;
	delete temp_dc;
}

void paintBg( CBitmap* bm, CFrame& frame )
{
	HWND hwnd = (HWND)frame.getSystemWindow();
	HDC hdc = GetDC(hwnd);
	CDrawContext* temp_dc = new CDrawContext( &frame, hdc, hwnd );
	COffscreenContext* temp_oc = new COffscreenContext( temp_dc , bm, true );


	CPoint p;
	CColor col;
	for ( p.y = 0 ; p.y != bm->getHeight() ; ++p.y)
	{
		for ( p.x = 0 ; p.x != bm->getWidth() ; ++p.x)
		{
            int v = 150 + (rand() & 31);

			col( v, v, v, 255 );
			temp_oc->drawPoint( p, col );
		}
	}

    delete temp_oc;
	delete temp_dc;
}

void getBitmapsToKeep(
	std::vector<BitmapResult>& out1,	// "remembers" the bitmaps
	std::vector<ControlResult>& out2,	// no ownership
	CFrame& frame,
	psdlite::layered_image& li
	)
{
	// windows specific...
	HWND hwnd = (HWND)frame.getSystemWindow();
	HDC hdc = GetDC(hwnd);
	CDrawContext* temp_dc = new CDrawContext( &frame, hdc, hwnd );

	// loop for creating bitmaps...
	for ( unsigned int i = 0 ; i != li.layers_.size() ; ++i )
	{
		std::string& name = li.layers_[i].name_;
		if (name.empty() || name[0] != '#')
			continue;

		out1.push_back( BitmapResult() );
		BitmapResult& br = out1.back();
		br.bm = getBitmap( frame, temp_dc , li.layers_[i].data_ );
		br.name.assign( name.c_str() + 1 );
	}

	// loop for creating named rectangles
	for ( unsigned int i = 0 ; i != li.layers_.size() ; ++i )
	{
		std::string& name = li.layers_[i].name_;
		if (name.empty())
			continue;

		CBitmap* bm = 0;

		char* bmname = strchr( name.c_str(), '|' );
		if (bmname)
		{
			++bmname;
			bm = findBitmap( out1, bmname );
			name.erase( bmname - 1 - name.c_str() );
		}


		out2.push_back( ControlResult() );
		ControlResult& cr = out2.back();
		cr.knob = bm;
		cr.name = name;
		cr.rect.left = li.layers_[i].offs_.x;
		cr.rect.top = li.layers_[i].offs_.y;
		cr.rect.right = cr.rect.x + li.layers_[i].data_.get_size().x;
		cr.rect.bottom = cr.rect.y + li.layers_[i].data_.get_size().y;
	}

	delete temp_dc;
}

ControlResult* findLayer(std::vector<ControlResult>& img, const std::string& layer)
{
	for ( unsigned int i = 0 ; i != img.size() ; ++i )
	{
		ControlResult& cr = img[i];
		if ( layer == cr.name )
		{
			return &cr;
		}
	}
	return 0;
}

void getRectFromBitmap( CRect& rect, std::vector<ControlResult>& img, const std::string& layer)
{
	ControlResult* cr = findLayer(img, layer);
	if (cr)
		rect = cr->rect;
}

// -1 not found
psdlite::bitmap* findLayer(psdlite::layered_image& img, const std::string& layer)
{
	for ( unsigned int i = 0 ; i != img.layers_.size() ; ++i )
	{
		if ( layer == img.layers_[i].name_ )
		{
			return &(img.layers_[i].data_);
		}
	}
	return 0;
}




























static CColor kDarkBgCColor = {8, 8, 32, 0};
static CColor kDarkGreenCColor = {0, 128, 0, 0};
static CColor kEmptyBgCColor = {64, 64, 64, 0};

class MegaToggle : public CControl
{
public:
	MegaToggle( const CRect& where, CControlListener* listener, int tag )
        : CControl( where, listener, tag )
    {
    }

	void mouse(CDrawContext *pContext, CPoint& where)
    {
        (void)where;

      	if (!bMouseEnabled)
    		return;

        value = (value > .5f) ? 0 : 1.f;
		listener->valueChanged (pContext, this);
	}

	void draw (CDrawContext *pContext)
    {
       	CRect r;
	    getViewSize(r);

        float value = getValue();

        if ( value > .5f )
        {
	        pContext->setFillColor( kGreenCColor );
        }
        else
        {
	        pContext->setFillColor( kBlueCColor );
        }
        pContext->fillRect(r);
    }
};



class VUDisplay : public CView
{
public:
	VUDisplay( const CRect& where, const Synth* in_synth )
        : CView( where )
        , myVu( 0.f )
        , s( in_synth )
    {
    }

    void testDirty()
    {
        float vu = s->GetVu();

        // adjust for presentation
        vu = 1.1f * vu / ( vu + .1f );

        if(myVu == vu)
            return;

        myVu = vu;
        setDirty();
    }

	void draw (CDrawContext *pContext)
    {
        CRect left(size);
        CRect right(size);

        if (myVu >= 1.f)
        {
            myVu = 1.f;
    	    pContext->setFillColor( kRedCColor );
        }
        else
        {
    	    pContext->setFillColor( kGreenCColor );
        }

        int mid = (int)(left.left + (left.right - left.left) * myVu);
        left.right = mid;
        right.left = mid;
    	pContext->fillRect( left );

        if (myVu == 0.f )
        {
    	    pContext->setFillColor( kBlueCColor );
        }
        else
        {
    	    pContext->setFillColor( kBlackCColor );
        }
    	pContext->fillRect( right );
    }

private:
    float myVu;
	const Synth* s;
};













class VoiceDisplay : public CView
{
public:
	VoiceDisplay ( const CRect& where, const Synth* in_synth )
        : CView( where )
        , s( in_synth )
        , voiceCount( 0 )
    {
        for (int i = 0 ; i != Synth::VOICEMAX ; ++i )
        {
            voiceStates[ i ] = VoiceInstance::Unused;
        }
    }

    void testDirty()
    {
        const VoiceInstance* instances = s->GetVoiceInstances();

        int differ = 0;

        differ += (voiceCount ^ s->GetVoiceInstanceCount());

        voiceCount = s->GetVoiceInstanceCount();

        for (int i = 0 ; i != Synth::VOICEMAX ; ++i )
        {
            VoiceInstance::VoiceInstanceState state = instances[i].state;
            differ += (state ^ voiceStates[i] );
            voiceStates[ i ] = state;
        }

        if ( differ )
            setDirty();
    }

	void draw (CDrawContext *pContext)
    {
        // fill bg
      	pContext->setFillColor( kDarkBgCColor );
    	pContext->fillRect( size );

        int count = 0;
        int dim = 3;
        int xlimit = size.width() / dim;
        int ylimit = size.height() / dim ;

        for ( int y = 0 ; (y != ylimit) && (count != voiceCount) ; ++y )
        {
            for ( int x = 0 ; (x != xlimit) && (count != voiceCount) ; ++x, ++count )
            {
            VoiceInstance::VoiceInstanceState state = voiceStates[count];

            switch ( state )
            {
            case VoiceInstance::Used:    pContext->setFillColor( kGreenCColor ); break;
            case VoiceInstance::Release: pContext->setFillColor( kDarkGreenCColor ); break;
            case VoiceInstance::Unused:  pContext->setFillColor( kBlueCColor ); break;
            }

            CRect dot;
            dot.left = size.left + x * dim;
            dot.right = dot.left + dim;
            dot.top = size.top + y * dim;
            dot.bottom = dot.top + dim;
            pContext->fillRect( dot );
            }
        }
    }

private:
	const Synth* s;
    VoiceInstance::VoiceInstanceState voiceStates[Synth::VOICEMAX];
    int voiceCount;
};














class WaveDisplay : public CView
{
public:
	WaveDisplay( const CRect& where, const OscillatorSettings* in_oscs, int wave_group = -1 );
	void draw (CDrawContext *pContext);
	void testDirty();
private:
	const OscillatorSettings* oscs;
    int waveGroup;
};

WaveDisplay::WaveDisplay( const CRect& where, const OscillatorSettings* in_oscs, int wave_group )
:CView( where )
,oscs(in_oscs)
,waveGroup( wave_group )
{
}

void WaveDisplay::testDirty()
{
	if (oscs->isDirty() || oscs->isDispDirty() )
		setDirty();
}

void WaveDisplay::draw (CDrawContext *pContext)
{
	// clear rect...
	// bg
	pContext->setFillColor( kDarkBgCColor );
	pContext->fillRect( size );

	pContext->setFillColor( kWhiteCColor );
	pContext->setFrameColor( kGreyCColor );
	pContext->setLineWidth( 1 );
	pContext->setLineStyle( kLineSolid );

	oscs->GetFreshWaveGroup();
	const WaveGroup& wg = oscs->getWaveGroup( waveGroup );

	const float* buffer = wg.get_wave(GROUPCYCLES-1);
	int buflen = CYCLELEN;

    float mid = (size.top + size.bottom) * .5f;
	float radius = size.top - mid;

    CPoint p;
    p.x = size.left;
	p.y = (long)(mid - radius * buffer[0]);
    pContext->moveTo( p );

	for ( p.x = size.left ; p.x != size.right ; ++p.x )
	{
		int bufferindex = (p.x - size.left) * buflen / (size.right - size.left);
		if (bufferindex >= buflen)
			bufferindex = buflen-1;

		float v = buffer[bufferindex];
		p.y = (long)(mid - radius * v);

        if (p.y < size.top+1)
            p.y = size.top+1;

        if (p.y > size.bottom-1)
            p.y = size.bottom-1;

		pContext->lineTo( p );
	}
}


















class MegaSlider : public CControl
{
public:
	MegaSlider( const CRect& where, CControlListener* listener, int tag, float x=16, float y=128, bool canGray = true );
	void mouse(CDrawContext *pContext, CPoint &where);
	void draw (CDrawContext *pContext);
private:
	float x_scale;
	float y_scale;
    bool can_gray;
};

float valueFromPoint( const CPoint& p, float height )
{
	float v = -p.y / height;
	return v;
}

void MegaSlider::mouse(CDrawContext *pContext, CPoint &where)
{
	if (!bMouseEnabled)
		return;

	long button = pContext->getMouseButtons ();
	if (!(button & (kLButton|kRButton)))
		return;

	// set the default value
	if (button == (kControl|kLButton))
	{
		value = getDefaultValue ();
		if (isDirty ())
			listener->valueChanged (pContext, this);
		return;
	}

	float old = value;
	CPoint firstPoint(where);

	// begin of edit parameter
	getParent ()->beginEdit (tag);

	do
	{
		if (where != firstPoint)
		{
			CPoint diff( where.x - firstPoint.x, where.y - firstPoint.y );
			float diffv = valueFromPoint( diff, y_scale );
            if ((button & kRButton) != 0)
                diffv /= 10.f;

			value = old + diffv;
			bounceValue();
		}

		if (isDirty())
			listener->valueChanged(pContext, this);

        doIdleStuff();
		pContext->getMouseLocation(where);
		button = pContext->getMouseButtons ();
	} while (button & (kLButton|kRButton));

	// make sure always update...
	listener->valueChanged(pContext, this);
	getParent ()->endEdit (tag);
}

MegaSlider::MegaSlider( const CRect& where, CControlListener* listener, int tag, float x, float y, bool canGray )
: CControl( where, listener, tag )
, x_scale( x )
, y_scale( y )
, can_gray( canGray )
{
}

void MegaSlider::draw(CDrawContext *pContext)
{
	// 0,0########
	// #         #
	// #         #
	// #         #
	// ########4,4

	CRect r;
	getViewSize(r);

    float value = getValue();
    float defaultValue = getDefaultValue();

    if ( can_gray && fabsf( value - defaultValue ) < 1.192e-7f )
    {
	    pContext->setFillColor( kEmptyBgCColor );
    }
    else
    {
	    pContext->setFillColor( kDarkBgCColor );
    }

    pContext->fillRect(r);


    int totalDiff = r.bottom - r.top - 2;
    int line = r.bottom - 1 - (int)((float)totalDiff * value);

    if ( line < r.top + 1)
        line = r.top + 1;

    if ( line > r.bottom - 1)
        line = r.bottom - 1;

    // line
    r.top = line - 1;
    r.bottom = line + 1;

	pContext->setFillColor( kGreyCColor );
	pContext->fillRect(r);
}











//-----------------------------------------------------------------------------
// CLabel declaration
//-----------------------------------------------------------------------------
class CLabel : public CParamDisplay
{
public:
	CLabel (CRect &size, const std::string& text);

	void draw (CDrawContext *pContext);

	void setLabel (const std::string& text);

protected:
	std::string label;
};

//-----------------------------------------------------------------------------
// CLabel implementation
//-----------------------------------------------------------------------------
CLabel::CLabel (CRect &size, const std::string& text)
: CParamDisplay (size)
{
	setLabel (text);
    setFrameColor( kGreyCColor );
}

//------------------------------------------------------------------------
void CLabel::setLabel (const std::string& text)
{
	label = text;
	setDirty();
}

//------------------------------------------------------------------------
void CLabel::draw (CDrawContext *pContext)
{
	pContext->setFrameColor(frameColor);
	pContext->fillRect (size);
	pContext->setFont (fontID);
	pContext->setFontColor (fontColor);
	pContext->drawString (label.c_str(), size, false, kLeftText);
}












//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
guiEditor::guiEditor (AudioEffect *effect)
:	AEffGUIEditor (effect)
{
    myFont = 0;
	frame = 0;
	oldTicks = 0;

	rect.left   = 0;
	rect.top    = 0;
	rect.right  = 1;
	rect.bottom = 1;

	ActiveControl = 0;

    cBigSliderLabel = 0;
    cBigSliderValue = 0;
    cBigSlider = 0;

    // we decide in this plugin to open all bitmaps in the open function
}

//-----------------------------------------------------------------------------
guiEditor::~guiEditor ()
{
}

void guiEditor::ParamChanged( int index )
{
	if (!frame)
		return;

 	// displays that needs to test if they are dirty does that here...
    for ( int i = 0 ; i != OSC_PER_VOICE*3 ; ++i )
    	((WaveDisplay*)cWaveDisplay[i])->testDirty();

	const std::vector<Params::Param>& pv = ((audjoo_helix*)effect)->getSynth().getParams()->data;
	int a = pv.size();
	int b = cControls.size();

	// in process of getting opened... FIXME!!! FIXME!!!
	if (a != b)
		return;

	assert(a == b);

	int i = 0;
	int end = cControls.size();

	if (i > end)
		return;

	if (index > -1)
	{
		i = index;
		end = i+1;
	}

	for ( ; i != end ; ++i )
	{
		cControls[i]->setValue( pv[i].GetSliderValue() * cControls[i]->getMax() );
		cControls[i]->setDirty();
	}

	// update little display...
	{
		audjoo_helix* nls = (audjoo_helix*)effect;

		// get the param that is used...
		const Params* params = nls->getSynth().getParams();
		if ((u32)ActiveControl < params->data.size())
		{
			const Params::Param& p = params->data[ActiveControl];
			CLabel* label = (CLabel*)cBigSliderLabel;
			label->setLabel(p.GetLongName().c_str());
            std::stringstream disp;
			p.GetRangeValueString(disp);
            char temp[512];
            strncpy( temp, disp.str().c_str(), 512 );
            temp[511] = 0;
            cBigSliderValue->setText( temp);
        }
	}

	// call this to be sure that the graphic will be updated
	postUpdate();
}


CControl* createControl( CControlListener* listener, int tag, const Params::Param& p, CRect& rect, std::vector<ControlResult>& img, const std::string& layer )
{
	ControlResult* cr = findLayer( img, layer );
	if (cr)
		rect = cr->rect;

	// use fancy knob bitmaps...

	// switch type... knob, on-off, other?

	CControl* ret = 0;

	float mid = p.GetMid();
	float range = p.GetRange();
	bool isToggle = false;

	if (Params::Param::INTEGER == p.GetMode() )
	{
		isToggle = (.5f == mid) && (.5f == range) && 0 == p.GetEnum(0);

		if (!isToggle && (range < 10.f))
		{
			COptionMenu *options_menu = new COptionMenu (rect, listener, tag);
			if (options_menu)
			{
				options_menu->setFont (kNormalFontSmall);
				options_menu->setFontColor (kGreyCColor);
				options_menu->setBackColor ( kDarkBgCColor );
				options_menu->setHoriAlign (kLeftText);
				int start = fast_ftol1023(p.GetMid() - p.GetRange() + 512.5f) - 512;
				int stop = fast_ftol1023(p.GetMid() + p.GetRange() + 513.5f) - 512;
				for (int j = start ; j < stop ; ++j)
				{
					char txt[256];
					sprintf (txt, "%d", j);
					const char* label = p.GetEnum(j);
					if (!label)
						label = txt;
					options_menu->addEntry((char*)label);
				}
				options_menu->setMax(stop - start - 1.f );
			}
			ret = options_menu;
		}
	}

   	if (!ret && isToggle)
	{
		MegaToggle* oob = new MegaToggle(rect, listener, tag );
		ret = oob;
	}

	// not integer...
	if (!ret && cr )
	{
		CPoint offs;
		int height = cr->knob->getWidth();
		int maps = cr->knob->getHeight() / height;
		CAnimKnob* ak = new CAnimKnob( rect, listener, tag, maps, height, cr->knob, offs );
		ret = ak;
	}

	// default to magaslider...
	if (!ret)
    {
		ret = new MegaSlider(rect, listener, tag );
        ret->setDefaultValue( p.GetDefaultSliderValue() );
    }

	return ret;
}


struct GuiFrame
{
    GuiFrame( bool param, int index )
    {
        mBox( 0,0,0,0 );
        mParam = param;
        mIndex = index;
    }

    void CalcBox( Params* par, int x, int y )
    {
        mBox.left = x;
        mBox.top = y;

        if ( mParam )
        {
            Params::Param& p = par->data[ mIndex ];
            mBox.right = mBox.left + p.screenw_;
            mBox.bottom = mBox.top + p.screenh_;

            p.screenx_ = x;
            p.screeny_ = y;
        }
        else
        {
            Params::LayoutDirective lod;

            if ( mIndex != -1 )
                lod = par->layout[ mIndex ];

            switch( lod.type_ )
            {
                // only look at kids
                case Params::LAYOUT_HGROUP_BEGIN:
                    {
                        int maxy = y;
                        x += lod.w_;
                        y += lod.h_;
                        std::list<GuiFrame>::iterator it( mChildren.begin() );
                        std::list<GuiFrame>::iterator endit( mChildren.end() );
                        for ( ; it != endit ; ++it )
                        {
                            it->CalcBox( par, x, y );
                            x = it->mBox.right + lod.w_;
                            if (it->mBox.bottom > maxy)
                                maxy = it->mBox.bottom;
                        }
                        mBox.right = x;
                        mBox.bottom = maxy + lod.h_;
                    }
                    break;

                case Params::LAYOUT_VGROUP_BEGIN:
                    {
                        std::list<GuiFrame>::iterator it( mChildren.begin() );
                        std::list<GuiFrame>::iterator endit( mChildren.end() );

                        int maxx = x;
                        x += lod.w_;
                        y += lod.h_;
                        for ( ; it != endit ; ++it )
                        {
                            it->CalcBox( par, x, y );
                            y = it->mBox.bottom + lod.h_;
                            if (it->mBox.right > maxx)
                                maxx = it->mBox.right;
                        }
                        mBox.right = maxx + lod.w_;
                        mBox.bottom = y;
                    }
                    break;

                case Params::LAYOUT_MAKE_SPACE:
                case Params::LAYOUT_LABEL:
                    {
                        mBox.right = mBox.left + lod.w_;
                        mBox.bottom = mBox.top + lod.h_;
                    }
                    break;
            }

            // write back
            if ( mIndex != -1 )
            {
                Params::LayoutDirective& lodr = par->layout[ mIndex ];
                lodr.x_ = mBox.left;
                lodr.y_ = mBox.top;
            }
        }
    }

    CRect mBox;
    bool mParam; // true if its referring to a Param.
    u32 mIndex;
    std::list<GuiFrame> mChildren;
};

void CalcLayout( GuiFrame& root, Params* par )
{
    u32 p = 0;

    std::vector<GuiFrame*> stack;
    stack.push_back( &root );

    for ( u32 lay = 0 ; lay != par->layout.size() ; ++lay )
    {
        const Params::LayoutDirective& lod = par->layout[lay];
        u32 stop = lod.place_;
        for ( ; p != stop ; ++p )
        {
            GuiFrame gf( true, p );
            stack.back()->mChildren.push_back( gf );
        }

        switch ( lod.type_ )
        {

        case Params::LAYOUT_HGROUP_BEGIN:
        case Params::LAYOUT_VGROUP_BEGIN:
            {
                GuiFrame gf( false, lay );
                stack.back()->mChildren.push_back( gf );
                stack.push_back( &stack.back()->mChildren.back() );
            }
            break;

        case Params::LAYOUT_GROUP_END:
            {
                stack.pop_back();
            }
            break;

        case Params::LAYOUT_MAKE_SPACE:
        case Params::LAYOUT_LABEL:
            {
                GuiFrame gf( false, lay );
                stack.back()->mChildren.push_back( gf );
            }
        }
    }

    root.CalcBox( par, 0, 0 );
}

bool FindSpace( CRect& rect, Params* par, const std::string& name )
{
    for ( u32 i = 0 ; i != par->layout.size() ; ++i )
    {
        if (
            ( par->layout[i].type_ == Params::LAYOUT_MAKE_SPACE ) &&
            (name == par->layout[i].name_ )
           )
        {
            rect.left = par->layout[i].x_;
            rect.right = rect.left + par->layout[i].w_;
            rect.top = par->layout[i].y_;
            rect.bottom = rect.top + par->layout[i].h_;
            return true;
        }
    }
    return false;
}

//-----------------------------------------------------------------------------
long guiEditor::open(void *ptr)
{
	// always call this !!!
	AEffGUIEditor::open (ptr);

    // calculate the layout based on all parameters...
  	Params* par = ((audjoo_helix*)effect)->getSynth().getParams();
    GuiFrame root( false,  -1 );
    CalcLayout( root, par );


    // create a frame of the right size


//	std::vector<BitmapResult> bitmapsToKeep;
	std::vector<ControlResult> controlPositions;
//	getBitmapsToKeep( bitmapsToKeep, controlPositions, *frame, img );

	// try to open skin first...
    //{
    //    psdlite::layered_image img;
	   // get_image(img);

    //    // get the font-bitmap
    //    psdlite::bitmap* f = findLayer( img, "smallFont" );
    //    if (f)
    //        myFont = new MyFont( *f );
    //}

    CRect size = root.mBox;

    const static int width = 16;
    const static int height = 13;

    size.right += 1;
    size.bottom += 2;
    int left = size.right;
    int top = size.bottom;
    size.right += width;
    size.bottom += height;

    rect.left = (short)size.left;
    rect.top = (short)size.top;
    rect.right = (short)size.right;
    rect.bottom = (short)size.bottom;

	// get version
	int version = getVstGuiVersion ();
	int verMaj = (version & 0xFF00) >> 16; (void)verMaj;
	int verMin = (version & 0x00FF); (void)verMin;

	const Synth& synth = ((audjoo_helix*)effect)->getSynth();

	frame = new CFrame (size, ptr, this);

	CBitmap *background = 0;
    background = new CBitmap( *frame, size.right, size.bottom );
    paintBg( background, *frame );
	frame->setBackground (background);

    size( 0, top, 200, rect.bottom );
    frame->addView( new CLabel( size, "Helix beta: " __DATE__ ) );

    size( 202, top, 234, rect.bottom );
    voiceDisplay = new VoiceDisplay( size, &synth );
    frame->addView( voiceDisplay );

    size( 236, top, 268, rect.bottom );
    vuDisplay = new VUDisplay( size, &synth );
    frame->addView( vuDisplay );

    size( 270, top, 500, rect.bottom );
	cBigSliderLabel = new CLabel( size, "Big Slider");
	cBigSliderLabel->setDirty();
	frame->addView( cBigSliderLabel );

    size( 502, top, 584, rect.bottom );
	cBigSliderValue = new CTextEdit( size, this, -2, "Big Slider Value");
	cBigSliderValue->setDirty();
	frame->addView( cBigSliderValue );

   	// debug fix huge slier
  	size( left, 1, rect.right, rect.bottom - 1 );
	cBigSlider = new MegaSlider(size, this, -1, 24, 600, false );
	cBigSlider->setValue( .5f );
	cBigSlider->setDirty();
	frame->addView( cBigSlider );

	CPoint point (0, 33);

	for ( unsigned int i = 0 ; i != par->data.size() ; ++i )
	{
		const Params::Param& p = par->data[i];
		size.left = p.screenx_;
		size.top = p.screeny_;
		size.right = size.left + p.screenw_;
		size.bottom = size.top + p.screenh_;

		CControl* control = createControl(this, i, p,size,controlPositions, p.GetLongName() );
		cControls.push_back( control );

		cControls.back()->setValue( p.GetSliderValue() * cControls.back()->getMax() );
		cControls.back()->setDirty();
	}

	for (unsigned int i = 0 ; i != cControls.size() ; ++i)
		frame->addView( cControls[i] );

    // TEST FONT
    //size(100,100,100,100);
    //paintTextBg( background, *frame, size, "!\"#$%&'()*+,-./0123456789:;<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_", myFont );

    // look for labels and create them...
    for ( u32 i = 0 ; i != par->layout.size() ; ++i )
    {
        Params::LayoutDirective& lod = par->layout[i];
        if (lod.type_ == Params::LAYOUT_LABEL )
        {
            // TEST FONT PAINT LABEL
            size( lod.x_, lod.y_, lod.x_ + lod.w_, lod.y_ + lod.h_ );
	        CLabel* label = new CLabel( size, lod.name_ );
            label->setHoriAlign( kCenterText );
            label->setFont( kNormalFontVerySmall );

            label->setFrameColor( kBlackCColor );
            label->setShadowColor( kBlackCColor );
            label->setBackColor( kBlackCColor );

	        frame->addView( label );

            //            paintTextBg( background, *frame, size, lod.name_, myFont );
        }
    }

    assert(OSC_PER_VOICE == 4);

    // create wave displays
    for ( u32 o = 0 ; o != OSC_PER_VOICE ; ++o )
    {
        for ( u32 w = 0 ; w != 3 ; ++w )
        {
            char tmp[256];
    	    _snprintf( tmp, 255, "O%dWaveDisplay%d", o, w );
    	    size( 0,0,12,12);
            FindSpace( size, par, tmp  );

            int grp = OscillatorSettings::WAVEGROUPS-1;
            if ( w == 0 )
                grp = 0;
            else if ( w == 2 )
                grp = -1;

            cWaveDisplay[o*3+w] = new WaveDisplay(size, &synth.GetVoices().oscs[o], grp );
    	    frame->addView( cWaveDisplay[o*3+w] );
        }
    }

	return true;
}


void guiEditor::draw(CDrawContext *pContext)
{
	if (frame)
    {
		frame->draw(pContext);
    }
}

//-----------------------------------------------------------------------------
bool guiEditor::keysRequired ()
{
	//	if (frame && frame->getEditView ())
	//		return true;
	//	else
	return false;
}

//-----------------------------------------------------------------------------
long guiEditor::onKeyDown (VstKeyCode &/*keyCode*/)
{
	//if (frame && cLabel && (keyCode.character >= 'a' && keyCode.character <= 'z'))
	//{
	//	char val[64];
	//	char modifiers[32];
	//	strcpy (modifiers, "");
	//	if (keyCode.modifier & MODIFIER_SHIFT)
	//		strcpy (modifiers, "Shift+");
	//	if (keyCode.modifier & MODIFIER_ALTERNATE)
	//		strcat (modifiers, "Alt+");
	//	if (keyCode.modifier & MODIFIER_COMMAND)
	//		strcat (modifiers, "Cmd+");
	//	if (keyCode.modifier & MODIFIER_CONTROL)
	//		strcat (modifiers, "Ctrl+");

	//	sprintf (val, "onKeyDown : '%s%c'", modifiers, (char)(keyCode.character));
	//	cLabel->setLabel (val);
	//	return 1;
	//}

	//if (frame && (keyCode.virt == VKEY_UP || keyCode.virt == VKEY_DOWN))
	//{
	//	CView *pView = frame->getCurrentView ();
	//	if (pView == cVerticalSlider || pView == cKnob || pView == cViewContainer)
	//	{
	//		CControl *control = (CControl*)pView;
	//		if (pView == cViewContainer)
	//		{
	//			pView = (CControl*)(cViewContainer->getCurrentView ());
	//			if (pView == cHorizontalSlider)
	//				control = (CControl*)pView;
	//			else
	//				return -1;
	//		}

	//		float inc;
	//		if (keyCode.virt == VKEY_UP)
	//			inc = 0.05f;
	//		else
	//			inc = -0.05f;
	//		float val = control->getValue () + inc;
	//		float min = control->getMin ();
	//		float max = control->getMax ();
	//		if (val > max)
	//			val = max;
	//		else if (val < min)
	//			val = min;
	//		control->setValue (val);

	//		return 1;
	//	}
	//}
	return -1;
}

//-----------------------------------------------------------------------------
long guiEditor::onKeyUp (VstKeyCode &/*keyCode*/)
{
	//if (cLabel && (keyCode.character >= 'a' && keyCode.character <= 'z'))
	//{
	//	char val[64];
	//	char modifiers[32];
	//	strcpy (modifiers, "");
	//	if (keyCode.modifier & MODIFIER_SHIFT)
	//		strcpy (modifiers, "Shift+");
	//	if (keyCode.modifier & MODIFIER_ALTERNATE)
	//		strcat (modifiers, "Alt+");
	//	if (keyCode.modifier & MODIFIER_COMMAND)
	//		strcat (modifiers, "Cmd+");
	//	if (keyCode.modifier & MODIFIER_CONTROL)
	//		strcat (modifiers, "Ctrl+");

	//	sprintf (val, "onKeyUp : '%s%c'", modifiers, (char)(keyCode.character));
	//	cLabel->setLabel (val);
	//	return 1;
	//}

	return -1;
}

//-----------------------------------------------------------------------------
void guiEditor::resume ()
{
	// called when the plugin will be On
}

//-----------------------------------------------------------------------------
void guiEditor::suspend ()
{
	// called when the plugin will be Off
}

//-----------------------------------------------------------------------------
void guiEditor::close ()
{
	// don't forget to remove the frame !!
	if (frame)
		delete frame;
	frame = 0;

    if (myFont)
        delete myFont;

	// does the frame delete all added views??? I sure hope so...
	cControls.clear();
	cBigSlider = 0;
	cBigSliderLabel = 0;
	cBigSliderValue = 0;

    for ( u32 i = 0 ; i != OSC_PER_VOICE*3 ; ++i )
    	cWaveDisplay[i] = 0;

    voiceDisplay = 0;
    vuDisplay = 0;
}

//-----------------------------------------------------------------------------
void guiEditor::idle ()
{
	AEffGUIEditor::idle ();		// always call this to ensure update

    ((VoiceDisplay*)voiceDisplay)->testDirty();
    ((VUDisplay*)vuDisplay)->testDirty();
}

//-----------------------------------------------------------------------------
void guiEditor::setParameter (long tag, float value)
{
	// called from the Aeffect to update the control's value
	audjoo_helix* nls = (audjoo_helix*)effect;

	if (tag < 0)
	{
        // is it textfield?
        if (tag == -2)
        {
            char temp[256];
            cBigSliderValue->getText( temp );
            float realvalue = (float)atof( temp );
            value = nls->getSynth().getParams()->data[ ActiveControl ].RangeToSlider( realvalue );
        }
		// it's the big slider...
		nls->setParameter( ActiveControl, value );
	}
	else
	{
		// save the active control...
		ActiveControl = tag;
		nls->setParameter( tag, value );
	}

	// test if the plug is opened
	if (!frame)
		return;

	if (tag < 0)
	{
		if (ActiveControl < cControls.size() )
		{
			cControls[ActiveControl]->setValue(value);
			cControls[ActiveControl]->setDirty();
		}
	}
	else
	{
		cBigSlider->setValue(value);
		cBigSlider->setDirty();
	}

	ParamChanged( ActiveControl );
}

//-----------------------------------------------------------------------------
void guiEditor::valueChanged (CDrawContext* /*context*/, CControl* control)
{
	// called when something changes in the UI (mouse, key..)

    // make sure it's updated soon...
    control->setDirty();

	// make sure to translate from enum to slider
	float value = control->getValue() / control->getMax();
	int tag = control->getTag();
	setParameter(tag,value);
}

//-----------------------------------------------------------------------------
void stringConvert (float value, char* string)
{
	sprintf (string, "p %.2f", value);
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
