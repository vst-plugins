
#include <iostream>

#include "AudioEffect.cpp"
#include "audioeffectx.h"
#include "audioeffectx.cpp"

#include "amppan.cpp"
#include "delay.cpp"
#include "dynamics.cpp"
#include "eg.cpp"
#include "filter.cpp"
#include "kbd.cpp"
#include "lfo.cpp"
#include "midicc.cpp"
#include "osc.cpp"
#include "params.cpp"
#include "reverb.cpp"
#include "seq.cpp"
#include "shaper.cpp"
#include "synth.cpp"
#include "voice.cpp"
#include "wavegroup.cpp"
#include "sec/security.cpp"
#include "kiss_fft_v1_2_5/tools/kiss_fftr.c"
#include "kiss_fft_v1_2_5/kiss_fft.c"

#include "audjoo_helix.cpp"
#include "audjoo_helixproc.cpp"

//==============================================================================
extern "C" AEffect* main_plugin (audioMasterCallback audioMaster) asm ("main");
extern "C" AEffect* main_plugin (audioMasterCallback audioMaster)
{
    std::cout << "main" << std::endl;

    if(!audioMaster (0, audioMasterVersion, 0, 0, 0, 0))
        return 0;  // old version

    audjoo_helix* const plugin = new audjoo_helix (audioMaster);

    if (plugin)
        return plugin->getAeffect();

    return 0;
}

__attribute__((constructor)) void myPluginInit() // this is called when the library is unoaded
{
//    std::cout << "myPluginInit" << std::endl;
}

__attribute__((destructor)) void myPluginFini() // this is called when the library is unoaded
{
//    std::cout << "myPluginFini" << std::endl;
}

