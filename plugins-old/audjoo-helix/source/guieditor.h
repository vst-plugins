
// Copyright (c) 2007, Nils Jonas Norberg, See COPYING.txt for details

#ifndef __guiEditor__
#define __guiEditor__

#ifndef __vstgui__
#include "vstgui.h"
#endif

#include <vector>


class Test;
class CLabel;

class MyFont;

//-----------------------------------------------------------------------------
class guiEditor : public AEffGUIEditor, public CControlListener
{
public:
	guiEditor (AudioEffect *effect);
	virtual ~guiEditor ();

	void suspend ();
	void resume ();
	bool keysRequired ();

	void ParamChanged(int index = -1); // -1 means everything changed...

protected:
	virtual long open (void *ptr);
	virtual void idle ();
	void setParameter (long index, float value);
	virtual void close ();

	void draw (CDrawContext *pContext);

	// VST 2.1
	virtual long onKeyDown (VstKeyCode &keyCode);
	virtual long onKeyUp (VstKeyCode &keyCode);

private:
	void valueChanged (CDrawContext* context, CControl* control);

	// all knobs for layer
	std::vector<CControl*> cControls; // all are in here...
	CView* cBigSliderLabel;
	CTextEdit* cBigSliderValue;

	CControl* cBigSlider;
	CView* cWaveDisplay[4*3];

   	CView* voiceDisplay;
   	CView* vuDisplay;

    MyFont* myFont;

	unsigned int ActiveControl; // what param are the bigslider connected to

		// others
	CLabel            *cLabel;

	long              oldTicks;
};

#endif
