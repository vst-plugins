
// Copyright (c) 2007, Nils Jonas Norberg, See COPYING.txt for details

#ifndef __audjoo_helix__
#include "audjoo_helix.h"
#endif

#include <stdlib.h>

//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
void audjoo_helix::setSampleRate (float sampleRate)
{
	AudioEffectX::setSampleRate( sampleRate );
	s.setSampleRate( sampleRate );
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::setBlockSize (long blockSize)
{
	AudioEffectX::setBlockSize (blockSize);
	// you may need to have to do something here...
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::resume ()
{
	wantEvents ();
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::initProcess ()
{
}

void audjoo_helix::HandleEvent(const Event& event)
{
	switch( event.eventType)
	{
	case RAWMIDI: s.RawMidi( event.status, event.note, event.vel ); break;
	case ON: s.noteOn( event.note, (float)event.vel / 127.f ); break;
	case OFF: s.noteOff( event.note ); break;
	case ALLOFF: s.noteOffAll(); break;
	}
}

void audjoo_helix::processImpl(float ** /*inputs*/, float **outputs, long sampleFrames, bool accum)
{
	long currentEvent = 0;
	long currentOffset = 0;

	float *outs_left = outputs[0];
	float *outs_right = outputs[1];

	while ( sampleFrames )
	{
		long temp_count = sampleFrames;

		if (currentEvent != eventCount)
		{
			long smps_left_to_next_event = eventQueue[currentEvent].deltaFrames - currentOffset;
			if (temp_count > smps_left_to_next_event )
				temp_count = smps_left_to_next_event;
		}

		if (temp_count < 0)
		{
			// buggy host?
			eventCount = 0;
			return;
		}

		s.process( outs_left, outs_right, temp_count, accum );

		sampleFrames -= temp_count;
		outs_left += temp_count;
		outs_right += temp_count;
		currentOffset += temp_count;

		if (currentEvent > eventCount)
		{
			// buggy host?
			eventCount = 0;
			return;
		}

		if (currentEvent != eventCount && eventQueue[currentEvent].deltaFrames == currentOffset)
			HandleEvent(eventQueue[currentEvent++]);
	}

	// handle all remaining events (don't care about deltaframes)
	for ( ; currentEvent < eventCount ; ++currentEvent)
		HandleEvent(eventQueue[currentEvent]);

	// clear eventqueue
	eventCount = 0;
}

void audjoo_helix::process(float **inputs, float **outputs, long sampleFrames)
{
	processImpl( inputs, outputs, sampleFrames, true);
}

//-----------------------------------------------------------------------------------------
void audjoo_helix::processReplacing (float **inputs, float **outputs, long sampleFrames)
{
	processImpl( inputs, outputs, sampleFrames, false);
}

void audjoo_helix::fixNewEvent()
{
	// no going back in time
	if (eventCount>1 && eventQueue[eventCount-2].deltaFrames > eventQueue[eventCount-1].deltaFrames)
		eventQueue[eventCount-1].deltaFrames = eventQueue[eventCount-2].deltaFrames;
}

void audjoo_helix::queueMidi(long deltaFrames, char status, char num, char val)
{
	if (eventCount == EVENTMAX)
		return;

	eventQueue[eventCount].eventType = RAWMIDI;
	eventQueue[eventCount].deltaFrames = deltaFrames;
	eventQueue[eventCount].note = num;
	eventQueue[eventCount].vel = val;
	eventQueue[eventCount].status = status;
	++eventCount;
	fixNewEvent();
}

void audjoo_helix::queueNoteOn(long deltaFrames, char note, char velocity)
{
	if (eventCount == EVENTMAX)
		return;

	eventQueue[eventCount].eventType = ON;
	eventQueue[eventCount].deltaFrames = deltaFrames;
	eventQueue[eventCount].note = note;
	eventQueue[eventCount].vel = velocity;
	++eventCount;
	fixNewEvent();
}

void audjoo_helix::queueNoteOff(long deltaFrames, char note)
{
	if (eventCount == EVENTMAX)
		return;

	eventQueue[eventCount].eventType = OFF;
	eventQueue[eventCount].deltaFrames = deltaFrames;
	eventQueue[eventCount].note = note;
	++eventCount;
	fixNewEvent();
}

void audjoo_helix::queueNoteOffAll(long deltaFrames)
{
	if (eventCount == EVENTMAX)
		return;

	eventQueue[eventCount].eventType = ALLOFF;
	eventQueue[eventCount].deltaFrames = deltaFrames;
	++eventCount;
	fixNewEvent();
}

//-----------------------------------------------------------------------------------------
long audjoo_helix::processEvents (VstEvents* ev)
{
	// FIXME!!! spred out midi events to respective parts... when I add parts that is..
	// debug_Voices("processEvents in");

	for (long i = 0; i < ev->numEvents; i++)
	{
		if ((ev->events[i])->type != kVstMidiType)
			continue;
		VstMidiEvent* event = (VstMidiEvent*)ev->events[i];
		char* midiData = event->midiData;
		long status = midiData[0] & 0xf0;		// ignoring channel

		if (status == 0x90 || status == 0x80)	// we only look at notes
		{
			char note = midiData[1] & 0x7f;
			char velocity = midiData[2] & 0x7f;
			if (status == 0x80)
				velocity = 0;	// note off by velocity 0
			if (!velocity)
				queueNoteOff (event->deltaFrames, note);
			else
				queueNoteOn(event->deltaFrames, note, velocity );
		}
		else if (status == 0xb0)
		{
			if (midiData[1] == 0x7e || midiData[1] == 0x7b)	// all notes off
				queueNoteOffAll(event->deltaFrames);
		}
        else if ( s.LikesRawMidi( status, midiData[1], midiData[2]) )
        {
            queueMidi( event->deltaFrames, status, midiData[1], midiData[2] );
        }

        event++;
	}

	return 1;	// want more
}

//-----------------------------------------------------------------------------------------
