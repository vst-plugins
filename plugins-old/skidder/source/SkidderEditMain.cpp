#include <iostream>

#include <AudioEffect.cpp>
#include <audioeffectx.h>
#include <audioeffectx.cpp>
#include "../_vstgui/vstgui.cpp"
#include "../_vstgui/vstcontrols.cpp"

#include "vstchunk.cpp"
#include "dfxmisc.cpp"
#include "dfxgui.cpp"
#include "dfxguiMultiControls.cpp"
#include "TempoRateTable.cpp"
#include "mskidderMidi.cpp"
#include "skidderFormalities.cpp"
#include "skidderProcess.cpp"
#include "SkidderEditor.cpp"
#include "SkidderEdit.cpp"

#ifndef __SkidderEdit
#include "SkidderEdit.hpp"
#endif

//-----------------------------------------------------------------------------------------
AEffect* main_plugin (audioMasterCallback audioMaster) asm ("main");
#define main main_plugin

AEffect *main (audioMasterCallback audioMaster)
{
//    std::cout << "main" << std::endl;

	SkidderEdit* effect = new SkidderEdit (audioMaster);

	if (!effect)
		return 0;

	return effect->getAeffect ();
}


__attribute__((constructor)) void myLoad ()
{
//    std::cout << "myLoad" << std::endl;
}

__attribute__((destructor)) void myUnload ()
{
//    std::cout << "myUnload" << std::endl;
}
