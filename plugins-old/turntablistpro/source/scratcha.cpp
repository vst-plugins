
#include <stdio.h>
#include <math.h>

#ifndef __SCRATCHA__
#include "scratcha.h"
#endif

#ifndef __SCRATCHA_EDITOR__
#include "ScratchaEditor.h"
#endif

#include "AEffEditor.hpp"

#if _WIN32
#include <mmsystem.h>
#endif

#if MACX
#include "mach-o/dyld.h"
#include "dlfcn.h"
#elif MAC
#include <CodeFragments.h>
#endif

#include <time.h>
#include <stdarg.h>
#include "sndfile.h"

const double midiScaler = (1. / 127.);

#define ROOT_KEY 60

//-----------------------------------------------------------------------------------------
// VstXSynthProgram
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
VstXSynthProgram::VstXSynthProgram ()
{
	m_fVolume = 1.0f;
	strcpy (name, "Basic");
}

//-----------------------------------------------------------------------------------------
// VstXSynth
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
VstXSynth::VstXSynth (audioMasterCallback audioMaster) : AudioEffectX (audioMaster, kNumPrograms, kVolume+1)
{
	openlog();

	//programs = new VstXSynthProgram[kNumPrograms];
	//if (programs)
		//setProgram (0);
	if (audioMaster)
	{
		setNumInputs (0);				// no inputs
		setNumOutputs (kNumOutputs);	// 2 outputs, 1 for each osciallator
		canProcessReplacing ();
		hasVu (false);
		hasClip (false);
		isSynth ();
		needIdle();
		programsAreChunks();
		setUniqueID ('TTP0');			// <<<! *must* change this!!!!

		//replacingInputs1 = new float[16384];
		//replacingInputs2 = new float[16384];

		// SCRATCHA
		strcpy(m_szProgramName, "default");
		m_fBuffer = 0;
		m_fLeft = 0;
		m_fRight = 0;

		m_fSystemSampleRate = getSampleRate();

		m_nNumChannels = 0;
		m_nNumSamples = 0;
		m_nSampleRate = 0;
//		m_nPosition = 0;

		// SET UP DEFAULT PROGRAM
		m_fPower = 1.0f; // ON
//		m_fScratch = 0.0f; // OFF
		m_fMute = 0.0f; // OFF
		m_fPitchAmount = .5f; // 0
		m_fDirection = 0.0f; // FORWARD
		m_fScratchSpeed = 0.33333333f;


		m_fScratchAmount = 0.5f;

		// modifiers
		m_fSpinUpSpeed = 1.0f;
		m_fSpinDownSpeed = 1.0f;
//		m_fUsedSpinUpSpeed = 1.0f;
//		m_fUsedSpinDownSpeed = 1.0f;
		m_fNoteMode = 0.0f; // RESET
		m_fLoopMode = 1.0f; // ON
		m_fPitchRange = 0.0f; // none
		m_fKeyTracking = 0.0f;
		m_fVolume = 1.0f;	// full volume
		m_fSampleRateDisplay = 0.0f;

#ifdef _MIDI_LEARN_
		m_fMidiLearn = 0.0f;
		m_nMidiLearn = -1;
		memset(m_nMappedControl,0,sizeof(m_nMappedControl));
#endif

		m_fPosition = 0.0f;

//		m_bPoweringUp = false;
//		m_bRecordStopped = false;
//		m_bStoppedRateRestored = false;
		m_bPlayedReverse = false;

		m_fPosition = 0.0f;
		m_fPosOffset = 0.0f;
		m_fNumSamples = 0.0f;

		m_fLastScratchAmount = 0.5f;
		m_nPitchBend = 0x2000;
		m_bPitchBendSet = false;
		m_bScratching = false;
		m_bWasScratching = false;
		m_bPlayForward = true;
		m_fScratchMode = 0.0f;
		m_fScratchSubMode = 1.0f; // speed based from realtime scratch mode

		m_nScratchDelay = 0;
		m_pVstEvents = 0;
		m_bProcessing = false;

		m_fNPTrack = 0.0f; //off
		m_bNPTrack = false;

		m_fPrevLeft = 0.0f;
		m_fPrevRight = 0.0f;

//		m_fLastGoodValueL = 0.0f;
//		m_fLastGoodValueR = 0.0f;

		setRootKey();

		currentNote = (m_nRootKey - ROOT_KEY) + m_nRootKey;

		//
		m_bDataReady = false;
//		m_bScratch = false;
		m_bScratchStop = false;

		strcpy(m_szFileName, "");

		m_fTemporary = 0;

//		m_nCurrBank = 0;
//		m_nCurrProgram = 0;

//		m_nNextBank = -1;
//		m_nNextProgram = -1;

//		SetCurrentBank(0);
//		SetCurrentProgram(0);

//		SetTorqueSpeed();

		m_fScratchVolumeModifier = 0.0f;

		m_bScratchAmountSet = false;
		m_fDesiredScratchRate = 0.0f;
		m_nScratchInterval = 0;

		m_fDesiredOffset = 0;
		m_fPrevDesiredOffset = 0;
		m_fDesiredPosition = 0;
		m_fPrevDesiredPosition = 0;

		//loadWavFile("d:\\dev01\\DopeBeat2.wav");
		#if MACX
//		writelog("attempting to load test.aif\n");
	//	loadWavFile("test.aif");
		#elif MAC
		m_connID = 0;
		//writelog("attempting to load test.aif\n");
		//loadWavFile("test.aif");
		#endif
//		bFirstSuspend = true;
//		bFirstResume = true;
//		writelog("constructor ");
//		PrintDirectory();
#if MACX
		dHandle = 0;
#endif
	}
	initProcess ();
	suspend ();

}

//-----------------------------------------------------------------------------------------
VstXSynth::~VstXSynth ()
{
		closelog();
//	if (programs)
//		delete[] programs;

	if (m_fBuffer)
		delete [] m_fBuffer;

//	if (replacingInputs1)
//		delete[] replacingInputs1;
//	if (replacingInputs2)
//		delete[] replacingInputs2;

}

//-----------------------------------------------------------------------------------------
void VstXSynth::suspend()
{
/*
	if (bFirstSuspend)
	{
		bFirstSuspend = false;
		writelog("suspend ");
		PrintDirectory();
	}
*/
	wantEvents ();
}


//-----------------------------------------------------------------------------------------
void VstXSynth::resume()
{
/*
	if (bFirstResume)
	{
		bFirstResume = false;
		writelog("resume ");
		PrintDirectory();
	}
*/
	wantEvents ();
}

//-----------------------------------------------------------------------------------------
void VstXSynth::setProgram (long program)
{
	/*
	VstXSynthProgram *ap = &programs[program];
	curProgram = program;
	*/
}

//-----------------------------------------------------------------------------------------
void VstXSynth::setProgramName (char *name)
{
	//strcpy (programs[curProgram].name, name);
	strcpy(m_szProgramName, name);
}

//-----------------------------------------------------------------------------------------
void VstXSynth::getProgramName (char *name)
{
//	strcpy (name, programs[curProgram].name);
	strcpy(name, m_szProgramName);
}

//-----------------------------------------------------------------------------------------
void VstXSynth::getParameterLabel (long index, char *label)
{
	switch (index)
	{
		case kPitchAmount:		strcpy (label, "  %  ");		break;
		case kPitchRange:		strcpy (label, "  %  ");		break;
//		case kToneArmWeight:	strcpy (label, "grams");		break;
		case kVolume:			strcpy (label, "  dB  ");		break;
		default:
			strcpy(label, " "); break;
	}
}


//-----------------------------------------------------------------------------------------
void VstXSynth::getParameterDisplay (long index, char *text)
{
	text[0] = 0;
	switch (index)
	{
		case kPower:
			if (m_fPower < .5)
				strcpy (text, "POWER OFF");
			else
				strcpy (text, "POWER ON");
			break;
		case kNPTrack:
			float2string (m_fNPTrack, text);	break;
		case kMute:
			if (m_fMute < .5)
				strcpy (text, "MUTE OFF");
			else
				strcpy (text, "MUTE ON");
			break;

		case kPitchAmount:		float2string ((((m_fPitchAmount-0.5f)*2.0f) * (m_fPitchRange * MAX_PITCH_RANGE*100)), text);	break;

		case kDirection:
			if (m_fDirection < .5)
				strcpy (text, "Forward");
			else
				strcpy (text, "Reverse");

			break;

		case kScratchSpeed:
			//float2string (m_fHandSize, text);	break;
			//long2string (m_fHandSize, text);	break;
			float2string (m_fScratchSpeed, text);	break;
			/*
			if (m_fHandSize < .5)
				strcpy (text, "Normal");
			else
				strcpy (text, "Large");
			break;
			*/
		case kScratchAmount:
			{
				/*
				float fHalf = 0.5f;
				float fDouble = 2.0f;
				float2string (((m_fScratchAmount-fHalf)*fDouble), text);
*/
				//sprintf(text, "%f %f", m_fTemporary, m_fPosition);
				float2string (m_fPlaySampleRate, text); //m_fPlaySampleRate
			}
			break;
//		case kTorque:			float2string (m_fTorque, text);	break;
		case kSpinUpSpeed:		float2string (m_fUsedSpinUpSpeed, text);	break;
		case kSpinDownSpeed:	float2string (m_fUsedSpinDownSpeed, text);	break;

		case kScratchMode:
			if (m_fScratchMode < .5)
			{
				strcpy (text, "SCRATCH MODE 1: realtime");
			}
			else
			{
				strcpy (text, "SCRATCH MODE 2: seq");
			}
			break;
/*
		case kScratchSubMode:
			if (m_fScratchSubMode < .5)
			{
				strcpy (text, "SUB MODE 1: pos");
			}
			else
			{
				strcpy (text, "SUB MODE 2: speed");
			}
			break;
*/
		case kNoteMode:
			if (m_fNoteMode < .5)
				strcpy (text, "Reset ");
			else
				strcpy (text, "Resume");
			break;
		case kLoopMode:
			if (m_fLoopMode < .5)
				strcpy (text, "LOOP OFF");
			else
				strcpy (text, "LOOP ON");
			break;

		case kPitchRange:	float2string ((m_fPitchRange*MAX_PITCH_RANGE*100), text);	break;
//		case kToneArmWeight:		float2string (((m_fToneArmWeight-0.5f)*2.0f), text);	break;

		case kKeyTracking:
			if (m_fKeyTracking < .5)
				strcpy (text, "KEYTRACK OFF");
			else
				strcpy (text, "KEYTRACK ON");

			//float2string (m_fKeyTracking, text);
			break;
//		case kRootKey:		float2string (m_fRootKey, text);	break;
		case kVolume:		dB2string (m_fVolume, text);		break;
		case kSampleRateDisplay:			float2string (m_fSampleRateDisplay, text);	break;
		case kPlay:
			if (m_fPlay < .5)
				strcpy (text, "NOTE OFF");
			else
				strcpy (text, "NOTE ON");
			break;
#ifdef _MIDI_LEARN_
		case kMidiLearn:
			if (m_fMidiLearn< .5)
				strcpy (text, "MIDI LEARN OFF");
			else
				strcpy (text, "MIDI LEARN ON");
			break;
#endif
	}
}




//-----------------------------------------------------------------------------------------
void VstXSynth::getParameterName (long index, char *label)
{
	switch (index)
	{
		case kPower:			strcpy (label, "Power");		break;
		case kNPTrack:			strcpy (label, "NPTrack");		break;
		case kScratchAmount:	strcpy (label, "Scratch Amount");		break;
		case kMute:				strcpy (label, "Audio");		break;
		case kPitchAmount:		strcpy (label, "Pitch Amt");		break;
		case kDirection:			strcpy (label, "Play Dir");		break;
		case kScratchSpeed:		strcpy (label, "Scratch Speed");		break;
//		case kTorque:			strcpy (label, "Torque");		break;
		case kSpinUpSpeed:		strcpy (label, "Spin Up");		break;
		case kSpinDownSpeed:	strcpy (label, "Spin Down");		break;
		case kScratchMode:		strcpy (label, "Scratch Mode"); break;
//		case kScratchSubMode:	strcpy (label, "Scratch Sub Mode"); break;
		case kNoteMode:			strcpy (label, "Note Mode");		break;
		case kLoopMode:			strcpy (label, "Loop");		break;
		case kPitchRange:		strcpy (label, "Pitch Range");		break;
//		case kToneArmWeight:	strcpy (label, "Tone Arm");		break;
		case kKeyTracking:		strcpy (label, "KeyTrack");		break;
//		case kRootKey:			strcpy (label, "RootKey");		break;
		case kVolume:			strcpy (label, "Volume");		break;
#ifdef _MIDI_LEARN_
		case kMidiLearn:		strcpy (label, "Midi Learn");		break;
#endif
		case kSampleRateDisplay:			strcpy (label, "SampleRate");		break;
		case kPlay:				strcpy (label, "Play");		break;

	}
}

//-----------------------------------------------------------------------------------------

void VstXSynth::setParameter (long index, float value)
{
	bool bCanLearn = true;
	value = FixMidiData(index, value);
	//VstXSynthProgram *ap = &programs[curProgram];
	switch (index)
	{
		case kPower:
			{
				m_fPower = value;
			}
			break;

		case kNPTrack:
			{
				m_fNPTrack = value;
				if (m_fNPTrack == 0.0f)
				{
					m_bNPTrack = false;
				}
				else
				{
					m_bNPTrack = true;
				}
			}
			break;
		case kMute:			m_fMute = value;	break;
		case kPitchAmount:
			{
				m_fPitchAmount = value;
				processPitch();
			}
			break;
		case kScratchAmount:
			{
				m_fScratchAmount = value;
				m_bScratchAmountSet = true;
			//	if (m_fScratchMode == 0.0f)
			//	{
					if (m_fScratchAmount == .5f)
					{
						m_bScratching = false;
						writelog("bScratching == false\n");
					}
					else
					{
						if (!m_bScratching) // first time in so init stuff here
						{
							m_fPrevDesiredPosition = m_fPosition;
							m_fDesiredScratchRate2 = m_fPlaySampleRate;
							m_fScratchCenter = m_fPosition;
							m_nScratchCenter = (int)m_fPosition;

							writelog("set center!\n");
						}
						m_bScratching = true;
					}
			//	}
			//	else
			///	{
			//		m_bScratching = true;
			//	}
				processScratch();
			}
			break;
		case kDirection:
			{
				m_fDirection = value;
				processDirection();
			}
			break;
		case kScratchSpeed:
			{
				m_fScratchSpeed = value;
				//SetScratchSpeed2();
			}
			break;
			/*
		case kTorque:
			{
				m_fTorque= value;
				SetTorqueSpeed();
			}
			break;
			*/
		case kSpinUpSpeed:
			{
				m_fSpinUpSpeed = value;
				m_fUsedSpinUpSpeed = (((exp(10.0f*m_fSpinUpSpeed)-1)/(exp(10.0f)-1)) * (float)m_nSampleRate)/m_nPowerIntervalEnd;
			}
			break;
		case kSpinDownSpeed:
			{
				m_fSpinDownSpeed = value;
				m_fUsedSpinDownSpeed = (((exp(10.0f*m_fSpinDownSpeed)-1)/(exp(10.0f)-1)) * (float)m_nSampleRate)/m_nPowerIntervalEnd;
			}
			break;
		case kScratchMode:		m_fScratchMode = value; break;
//		case kScratchSubMode:	m_fScratchSubMode = value; break;
		case kNoteMode:			m_fNoteMode= value;	break;
		case kLoopMode:			m_fLoopMode = value;	break;
		case kPitchRange:
			{
				m_fPitchRange = value;
				processPitch();
			}
			break;
//		case kToneArmWeight:	m_fToneArmWeight = value;	break;
		case kKeyTracking:		m_fKeyTracking = value;	break;
//		case kRootKey:			m_fRootKey = value;	 setRootKey(); break;
		case kVolume:
			{
				m_fVolume = value;
				m_fNoteVolume = (float)(m_fVolume * (double)currentVelocity * midiScaler);
			}
			break;
		case kPlay:
			{
				PlayNote(value);
			}
			break;
#ifdef _MIDI_LEARN_
		case kMidiLearn:
			{
				m_fMidiLearn = value;
				if (m_fMidiLearn == 0.0f)
				{
					m_nMidiLearn = -1;
					bCanLearn = false;
				}
			}
			break;
#endif
			/*
		case kBank0:
		case kBank1:
		case kBank2:
		case kBank3:
		case kBank4:
		case kBank5:
		case kBank6:
		case kBank7:
			{
				if (value == 1.0f)
				{
					SetCurrentBank(index - kBank0);
				}
				bCanLearn = false;
			}
			break;
		case kProgram0:
		case kProgram1:
		case kProgram2:
		case kProgram3:
		case kProgram4:
		case kProgram5:
		case kProgram6:
		case kProgram7:
			{
				if (value == 1.0f)
				{
					SetCurrentProgram(index - kProgram0);
				}
				bCanLearn = false;
			}
			break;
			*/
		default:
			bCanLearn = false;
			break;
	}
#ifdef _MIDI_LEARN_
	if (index == kMidiLearn) bCanLearn = false;

	if (m_fMidiLearn == 1.0f)
	{
		if (bCanLearn)
		{
			m_nMidiLearn = index;
		}
	}
#endif

	if (editor) ((ScratchaEditor*)editor)->setParameter(index, value);

	if (editor)
		editor->postUpdate();

	updateDisplay();
}

//-----------------------------------------------------------------------------------------
float VstXSynth::getParameter (long index)
{
	float value = 0;
	switch (index)
	{
		case kPower:			value = m_fPower;	break;
		case kNPTrack:			value = m_fNPTrack;	break;
		case kMute:				value = m_fMute;	break;
		case kPitchAmount:		value = m_fPitchAmount;	break;
		case kScratchAmount:	value = m_fScratchAmount;	break;
		case kDirection:		value = m_fDirection;	break;
		case kScratchSpeed:		value = m_fScratchSpeed;	break;
//		case kTorque:			value = m_fTorque;	break;
		case kSpinUpSpeed:		value = m_fSpinUpSpeed;	break;
		case kSpinDownSpeed:	value = m_fSpinDownSpeed;	break;
		case kScratchMode:		value = m_fScratchMode; break;
//		case kScratchSubMode:	value = m_fScratchSubMode; break;
		case kNoteMode:			value = m_fNoteMode;	break;
		case kLoopMode:			value = m_fLoopMode;	break;
		case kPitchRange:		value = m_fPitchRange;	break;
//		case kToneArmWeight:	value = m_fToneArmWeight;	break;
		case kKeyTracking:		value = m_fKeyTracking;	break;
		//case kRootKey:			value = m_fRootKey;	break;
		case kVolume:			value = m_fVolume;	break;
#ifdef _MIDI_LEARN_
		case kMidiLearn:		value = m_fMidiLearn;	break;
#endif
		case kSampleRateDisplay:			value = m_fSampleRateDisplay;	break;
		case kPlay:				value = m_fPlay; writelog("PLAY:%f\n",value); break;
			/*
		case kBank0:
		case kBank1:
		case kBank2:
		case kBank3:
		case kBank4:
		case kBank5:
		case kBank6:
		case kBank7:
			{
				if (m_nCurrBank  == (index-kBank0))
				{
					value = 1.0f;
				}
				else
				{
					value = 0.0f;
				}
			}
			break;
		case kProgram0:
		case kProgram1:
		case kProgram2:
		case kProgram3:
		case kProgram4:
		case kProgram5:
		case kProgram6:
		case kProgram7:
			{
				if (m_nCurrProgram  == (index-kProgram0))
				{
					value = 1.0f;
				}
				else
				{
					value = 0.0f;
				}
			}
			break;
			*/

	}
	return value;
}

//-----------------------------------------------------------------------------------------
bool VstXSynth::getOutputProperties (long index, VstPinProperties* properties)
{
	if (index < kNumOutputs)
	{
		sprintf (properties->label, "TTP %1d", index + 1);
		properties->flags = kVstPinIsActive;
		if (index < 2)
			properties->flags |= kVstPinIsStereo;	// test, make channel 1+2 stereo
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------
bool VstXSynth::getProgramNameIndexed (long category, long index, char* text)
{
	/*
	if (index < kNumPrograms)
	{
		strcpy (text, programs[index].name);
		return true;
	}
	*/
	return false;
}

//-----------------------------------------------------------------------------------------
bool VstXSynth::copyProgram (long destination)
{
	/*
	if (destination < kNumPrograms)
	{
		programs[destination] = programs[curProgram];
		return true;
	}
	*/
	return false;
}

//-----------------------------------------------------------------------------------------
bool VstXSynth::getEffectName (char* name)
{
	strcpy (name, "turntablist pro");
	return true;
}

//-----------------------------------------------------------------------------------------
bool VstXSynth::getVendorString (char* text)
{
	strcpy (text, "bioroid");
	return true;
}

//-----------------------------------------------------------------------------------------
bool VstXSynth::getProductString (char* text)
{
	strcpy (text, "bioroid turntablist pro");
	return true;
}

//-----------------------------------------------------------------------------------------
long VstXSynth::canDo (char* text)
{
	if (!strcmp (text, "receiveVstEvents"))
		return 1;
	if (!strcmp (text, "receiveVstMidiEvent"))
		return 1;
	return -1;	// explicitly can't do; 0 => don't know
}

bool VstXSynth::loadWavFile(char * file)
{
	writelog("VstXSynth::loadWavFile(%s)\n",(char *)&file[0]);
//	return 0;
	if (file[0] == 0)
	{
		return false;
	}

	char  buffer [256] ;
	memset(buffer, 0, 256);
	int buflen = sf_command (NULL, SFC_GET_LIB_VERSION, buffer, sizeof (buffer)) ;
	writelog("sf_get_lib_version = %d '%s'\n", buflen, buffer);

	float * tempBuffer = 0;

	SF_INFO sfInfo;
	memset(&sfInfo, 0, sizeof(SF_INFO));
	SNDFILE * sndFile = 0;

	sndFile = sf_open (file, SFM_READ, &sfInfo);

	if (sndFile == 0)
	{
		//print error
		memset(buffer, 0, 256);
		sf_error_str (sndFile, buffer, 256);
		writelog("sf_error_str = '%s'\n", buffer);

	}
	if (sndFile != 0)
	{
		writelog("**** SF_INFO dump for: %s\n", file);
		writelog("     samplerate: %d\n", sfInfo.samplerate);
		writelog("     frames: %d (0x%08x)\n", sfInfo.frames, sfInfo.frames);
		writelog("     channels: %d\n", sfInfo.channels);
		writelog("     format: %d\n", sfInfo.format);
		writelog("     sections: %d\n", sfInfo.sections);
		writelog("     seekable: %d\n", sfInfo.seekable);

		m_bDataReady = false;
		sf_command (sndFile, SFC_SET_NORM_FLOAT, NULL, SF_TRUE) ;


		m_nNumChannels = (int)sfInfo.channels;
		m_nSampleRate = (int)sfInfo.samplerate;
		m_nNumSamples  = (int)sfInfo.frames;

		m_fPlaySampleRate = (float) m_nSampleRate;
		m_fSampleRate = (float) m_nSampleRate;
		m_fUsedSpinDownSpeed = (((exp(10.0f*m_fSpinDownSpeed)-1)/(exp(10.0f)-1)) * (float)m_nSampleRate)/m_nPowerIntervalEnd;
		m_fUsedSpinUpSpeed = (((exp(10.0f*m_fSpinUpSpeed)-1)/(exp(10.0f)-1)) * (float)m_nSampleRate)/m_nPowerIntervalEnd;
		writelog("loaded m_fPlaySampleRate %f m_fSampleRate %f\n",m_fPlaySampleRate ,m_fSampleRate);
		m_fPosition = 0.0f;
		m_fPosOffset = 0.0f;
		m_fNumSamples = (float)m_nNumSamples;

  		if (m_fBuffer)
		{
			delete m_fBuffer;
			m_fBuffer = 0;
			m_fLeft = 0;
			m_fRight = 0;
		}

		m_fBuffer = new float[m_nNumChannels * m_nNumSamples];
		tempBuffer = new float[m_nNumSamples];

		m_fLeft = (float *)&m_fBuffer[0];
		m_fRight = (float *)&m_fBuffer[m_nNumSamples * (m_nNumChannels-1)];

		// do file loading here!!
		sf_count_t sizein, sizeout;
		sizein = sfInfo.frames * sfInfo.channels;
		sizeout = sf_readf_float (sndFile, &m_fBuffer[0], sizein);
		if (sizeout != sizein)
		{
			//error?
		}
		writelog("sizein: %d sizeout: %d\n", sizein, sizeout);

		if (m_nNumChannels == 2)
		{
			if (tempBuffer != 0)
			{
				// do swaps
				int z;
				for (z=0; z<m_nNumSamples; z++)
				{
					m_fBuffer[z] = 0;
					tempBuffer[z] = m_fBuffer[(z*2)+1]; // copy channel 1 into buffer
					m_fBuffer[z] = m_fBuffer[z*2]; // handle channel 0
				}

				for (z=0;z<m_nNumSamples; z++)
				{
					m_fBuffer[m_nNumSamples+z] = tempBuffer[z]; // make channel 1
				}
			}
		}

     	if (tempBuffer)
		{
			delete tempBuffer;
			tempBuffer = 0;
		}

		// end file loading here!!

		if (sf_close (sndFile) != 0)
		{
			// error closing
		}

		sndFile = 0;
		strcpy(m_szFileName, file);

		processPitch(); // set up stuff
		m_bDataReady = true;	// ready to play

		return true;
	}
	else
	{
		writelog("sndFile == 0 - returning false\n");
		//sf_perror(sndFile);
		return false;
	}
}

//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
void VstXSynth::setSampleRate (float sampleRate)
{
	AudioEffectX::setSampleRate (sampleRate);
	m_fSystemSampleRate = sampleRate;
	//fScaler = (float)((double)kWaveSize / (double)sampleRate);
	m_nPowerIntervalEnd = (int)sampleRate/120; // set process interval to 10 times a second - should maybe update it to 60?
	m_nScratchIntervalEnd = (int)sampleRate/SCRATCH_INTERVAL; // set scratch interval end to 1/16 second
	m_nScratchIntervalEndBase = m_nScratchIntervalEnd;
	writelog("sampleRate = %f m_nScratchIntervalEnd = %d\n",sampleRate ,m_nScratchIntervalEnd);
	//writelog("fDiff:\t\tInterval\tSampleRate\tTinyScratch\tDesiredRate\tfIntervalScaler\n");

}

//-----------------------------------------------------------------------------------------
void VstXSynth::setBlockSize (long blockSize)
{
	AudioEffectX::setBlockSize (blockSize);
	// you may need to have to do something here...
}

//-----------------------------------------------------------------------------------------
void VstXSynth::initProcess ()
{
	//fScaler = (float)((double)kWaveSize / 44100.);	// we don't know the sample rate yet
	m_bPlayedReverse = false;
	noteIsOn = false;
	currentDelta = 0;
}

void VstXSynth::doTheProcess(float **outputs, long sampleFrames)
{
/*
	double sampleRate = 0;
	double ppqPos = 0;				// 1 ppq
	double tempo = 0;				// in bpm
	double barStartPos = 0;			// last bar start, in 1 ppq
	long flags = 0;
	long kick = 0;
	double beat =0.0f;

	VstTimeInfo* TimeInfo = getTimeInfo (kVstPpqPosValid| kVstTempoValid | kVstBarsValid);

	flags= TimeInfo->flags;
	sampleRate= TimeInfo->sampleRate;
	if (flags & kVstPpqPosValid)
		ppqPos= TimeInfo->ppqPos;
	if (flags & kVstTempoValid)
		tempo= TimeInfo->tempo;
	if (flags & kVstBarsValid)
		barStartPos= TimeInfo->barStartPos;

	beat= barStartPos;

	while (beat < ppqPos)
		beat+= 0.25f;

	kick= (long)
		((beat - ppqPos)*60.0f*(sampleRate / tempo));

	writelog("srate: %f ppqpos: %f tempo: %f barspos: %f\n", sampleRate, ppqPos, tempo, barStartPos);
*/


	float* out1 = outputs[0];
	float* out2 = outputs[1];

	long eventFrame = -1; // -1 = no events
	long currFrame = 0;
	long numEvents = 0;
	long currEvent = 0;

	if (m_pVstEvents != 0)
	{
		numEvents =  m_pVstEvents->numEvents;
	}


//	m_bProcessing = true;

	if (numEvents == 0)
	{
		eventFrame = -1;
		m_pVstEvents = 0;
	}
	else
	{
		// find first event midi event
		while(currEvent < numEvents)
		{
			if ((m_pVstEvents->events[currEvent])->type == kVstMidiType)
			{
				eventFrame = (m_pVstEvents->events[currEvent])->deltaFrames;
				break;
			}
			else
			{
				currEvent++;
			}
		}
	}

	while (currFrame < sampleFrames)
	{
		//
		// process VstEvent(s) if any
		//
		while (currFrame == eventFrame) // change this to while (eventFrame == frameCounter)
		{
			processMidiEvent(currEvent);
			m_fSampleRateDisplay = m_fPlaySampleRate;
			currEvent++; // check next event

			while (currEvent < numEvents)
			{
				if ((m_pVstEvents->events[currEvent])->type == kVstMidiType)
				{
					eventFrame = (m_pVstEvents->events[currEvent])->deltaFrames;
					break;
				}
				else
				{
					currEvent++;
				}
			}

			if (currEvent >= numEvents)
			{
				eventFrame = -1; // no more events
				m_pVstEvents = 0;
			}
		} // while (eventFrame == currFrame)

		if (m_bScratching) // handle scratching
		{
			m_nScratchInterval++;
			if (m_nScratchInterval > m_nScratchIntervalEnd)
			{
				if (m_fScratchMode == 1.0f)
				{
					//m_nScratchInterval = 0;
					processScratch();
				}
				else
				{
					//m_nScratchInterval = 0;
					processScratchStop();
				}
			}
			else
			{
				if (m_fScratchMode == 0.0f)
				{
					// nudge samplerate
					m_fPlaySampleRate += m_fTinyScratchAdjust;
					// speed mode
					if (m_fTinyScratchAdjust > 0.0f) //positive
					{
						if (m_fPlaySampleRate > m_fDesiredScratchRate2)
						{
							m_fPlaySampleRate = m_fDesiredScratchRate2;
						}
					}
					else // negative
					{
						if (m_fPlaySampleRate < m_fDesiredScratchRate2)
						{
							m_fPlaySampleRate = m_fDesiredScratchRate2;
						}
					}
				}
			}
		}
		else // not scratching so just handle power
		{
			if (m_fPower < .5) // power off - spin down
			{
				if (m_fPlaySampleRate > 0.0f) // too high, spin down
				{
					m_fPlaySampleRate -= m_fUsedSpinDownSpeed; // adjust
					if (m_fPlaySampleRate < 0.0f) // too low so we past it
					{
						m_fPlaySampleRate = 0.0f; // fix it
					}
				}
			}
			else // power on - spin up
			{
				if (m_fPlaySampleRate < m_fDesiredPitch) // too low so bring up
				{
					m_fPlaySampleRate += m_fUsedSpinUpSpeed; // adjust
					if (m_fPlaySampleRate > m_fDesiredPitch) // too high so we past it
					{
						m_fPlaySampleRate = m_fDesiredPitch; // fix it
					}
				}
				else if (m_fPlaySampleRate > m_fDesiredPitch) // too high so bring down
				{
					m_fPlaySampleRate -= m_fUsedSpinUpSpeed; // adjust
					if (m_fPlaySampleRate < m_fDesiredPitch) // too low so we past it
					{
						m_fPlaySampleRate = m_fDesiredPitch; // fix it
					}
				}
			}
		}

		// handle rest of processing here
		if (noteIsOn)
		{
			//adjust gnPosition for new sample rate

			// set pos offset based on current sample rate

			// safey check
			if (m_fPlaySampleRate <= 0.0f)
			{
				m_fPlaySampleRate = 0.0f;
				if (m_bNPTrack == true)
				{
					if (!m_bScratching)
					{
						noteIsOn = false;
						if (m_fNoteMode < .5) // reset
						{
							m_fPosition = 0.0f;
						}
						m_fPlay = 0.0f;
						m_bPlayedReverse = false;
					}
				}
			}

			m_fPosOffset = m_fPlaySampleRate/m_fSystemSampleRate;  //m_fPlaySampleRate/m_fSampleRate;

			if (m_bDataReady)
			{
				if (noteIsOn == true)
				{

					if (!m_bPlayForward) // if play direction = reverse
					{
						if (m_fPlaySampleRate != 0.0f)
						{
							m_fPosition -= m_fPosOffset;
							while (m_fPosition < 0) // was if
							{
								if (m_fLoopMode < .5) // off
								{
									if (m_bPlayedReverse)
									{
										noteIsOn = false;
										m_fPosition = 0.0f;
										m_bPlayedReverse = false;

									}
									else
									{
										m_fPosition += m_fNumSamples; // - 1;
										m_bPlayedReverse = true;
									}
								}
								else
								{
									m_fPosition += m_fNumSamples; // - 1;
								}
							}
						}
					} // if(!bPlayForward)

					if (m_fMute < .5f) // if audio on
					{
//#define _NO_INTERPOLATION_
//#define _LINEAR_INTERPOLATION_
#define _CUBIC_INTERPOLATION_
						//no interpolation start
#ifdef _NO_INTERPOLATION_
						float fLeft = m_fLeft[(long)m_fPosition];
						float fRight = m_fRight[(long)m_fPosition];
						//no interpolation end
#endif

						//linear interpolation start
#ifdef _LINEAR_INTERPOLATION_
						float floating_part = (float)(m_fPosition - (long)(m_fPosition));
						long big_part1 = (long)(m_fPosition);
						long big_part2 = big_part1+1;
						if (big_part2>m_nNumSamples)
						{
							big_part2 = 0;
						}
						float fLeft = (floating_part * m_fLeft[big_part1]) + ((1-floating_part) * m_fLeft[big_part2]);
						float fRight = (floating_part * m_fRight[big_part1]) + ((1-floating_part) * m_fRight[big_part2]);
#endif
						//linear interpolation end

#ifdef _CUBIC_INTERPOLATION_
						//cubic interpolation start
						long inpos = (long) m_fPosition;
						float finpos = (float)(m_fPosition - (long)(m_fPosition));
						//float xm1, x0, x1, x2;
						float a, b, c;
						float xarray[4]; //0 = xm1, 1 = x0, 2 = x1, 3 = x2
						int posarray[4];
						posarray[0] = inpos - 1;
						posarray[1] = inpos;
						posarray[2] = inpos + 1;
						posarray[3] = inpos + 2;
						for (int pos = 0; pos < 4; pos++)
						{
							if (m_fLoopMode < .5) //off - set to -1/0
							{
								if ((posarray[pos] < 0) || (posarray[pos] >= m_nNumSamples))
								{
									posarray[pos] = -1;
								}
							}
							else //on - set to new pos
							{
								if (posarray[pos] < 0)
								{
									posarray[pos] += m_nNumSamples;
								}
								else if (posarray[pos] >= m_nNumSamples)
								{
									posarray[pos] -= m_nNumSamples;
								}
							}
						}
						// left channel
						for (int pos2 = 0; pos2 < 4; pos2++)
						{
							if (posarray[pos2] == -1)
							{
								xarray[pos2] = 0.0f;
							}
							else
							{
								xarray[pos2] = m_fLeft[posarray[pos2]];
							}
						}
						a = (3 * (xarray[1]-xarray[2]) - xarray[0] + xarray[3]) / 2;
						b = 2*xarray[2] + xarray[0] - (5*xarray[1] + xarray[2]) / 2;
						c = (xarray[2] - xarray[0]) / 2;
						float fLeft = (((a * finpos) + b) * finpos + c) * finpos + xarray[1];

						// right channel
						for (int pos3 = 0; pos3 < 4; pos3++)
						{
							if (posarray[pos3] == -1)
							{
								xarray[pos3] = 0.0f;
							}
							else
							{
								xarray[pos3] = m_fRight[posarray[pos3]];
							}
						}
						a = (3 * (xarray[1]-xarray[2]) - xarray[0] + xarray[3]) / 2;
						b = 2*xarray[2] + xarray[0] - (5*xarray[1] + xarray[2]) / 2;
						c = (xarray[2] - xarray[0]) / 2;
						float fRight = (((a * finpos) + b) * finpos + c) * finpos + xarray[1];
#endif
						/*
						Name: Cubic interpollation
						Type: Cubic Hermite interpolation
						References: posted by Olli Niemitalo
						Notes:
						finpos is the fractional, inpos the integer part.
						Allso see other001.gif

						xm1 = x [inpos - 1];
						x0  = x [inpos + 0];
						x1  = x [inpos + 1];
						x2  = x [inpos + 2];
						a = (3 * (x0-x1) - xm1 + x2) / 2;
						b = 2*x1 + xm1 - (5*x0 + x2) / 2;
						c = (x1 - xm1) / 2;
						y [outpos] = (((a * finpos) + b) * finpos + c) * finpos + x0;
						*/
						//m_nNumSamples
						//cubic interpolation end




/*
						if (m_bScratching) // handle tone arm weight
						{
							float fSensitivity = m_fToneArmWeight; //((m_fToneArmWeight-0.5f)*2.0f);

							m_fScratchVolumeModifier = (m_fPlaySampleRate/m_fSampleRate);
							if (m_fScratchVolumeModifier > 1.5f)
							{
								m_fScratchVolumeModifier = 1.5f;
							}

							if (m_fScratchVolumeModifier < 1.0f)
							{
								m_fScratchVolumeModifier = 1.0f;
							}

							fLeft = fLeft * m_fScratchVolumeModifier;
							if (fLeft > 1.0f)
							{
								fLeft = 1.0f;
							}

							if (fLeft < -1.0f)
							{
								fLeft = -1.0f;
							}

							fRight = fRight * m_fScratchVolumeModifier;
							if (fRight > 1.0f)
							{
								fRight = 1.0f;
							}

							if (fRight < -1.0f)
							{
								fRight = -1.0f;
							}


// TODO: try changing handling note volume before using scratch vol modifier so we dont clip as easily
//
							out1[currFrame] = fLeft * m_fNoteVolume;
							out2[currFrame] = fRight * m_fNoteVolume;

						//	out1[currFrame] = 0.0f;
						//	out2[currFrame] = 0.0f;

						}
						else
						{
						*/
						if (m_fPlaySampleRate == 0.0f)
						{
							out1[currFrame] = 0.0f;
							out2[currFrame] = 0.0f;
						}
						else
						{
							out1[currFrame] = fLeft * m_fNoteVolume;
							out2[currFrame] = fRight * m_fNoteVolume;

//							out1[currFrame] = m_fLeft[(int)m_fPosition] * m_fNoteVolume; //(*out1++) += m_fLeft[(int)m_fPosition] * m_fNoteVolume;
//							out2[currFrame] = m_fRight[(int)m_fPosition] * m_fNoteVolume; //(*out2++) += m_fRight[(int)m_fPosition] * m_fNoteVolume;
						}

						//	out1[currFrame] = 0.0f;
						//	out2[currFrame] = 0.0f;
						//}
					}


					if (m_bPlayForward)	// if play direction = forward
					{
						m_bPlayedReverse = false;
						if (m_fPlaySampleRate != 0.0f)
						{
							m_fPosition += m_fPosOffset;

							while (m_fPosition > m_fNumSamples)
							{
								m_fPosition -= m_fNumSamples;
								if (m_fLoopMode < .5) // off
								{
									noteIsOn = false;
									m_bPlayedReverse = false;
								}

							}
						}
					} // if (bPlayForward)

				} // if (noteIsOn == true)

			}
			else
			{
				out1[currFrame] = 0.0f; //					(*out1++) = 0.0f;
				out2[currFrame] = 0.0f; //					(*out2++) = 0.0f;
			}
		}
		else
		{
			out1[currFrame] = 0.0f;
			out2[currFrame] = 0.0f;
		} // if (noteIsOn)

		currFrame++; // increment frame
	} // (currFrame < sampleFrames)

	//m_pVstEvents = 0; // clear out pointer

//	m_bProcessing = false;
}


//logic
void VstXSynth::process(float **inputs, float **outputs, long sampleFrames)
{
	m_bProcessing = true;
	doTheProcess(outputs, sampleFrames);
	m_bProcessing = false;
}

//-----------------------------------------------------------------------------------------
// cubase/fruity loops
void VstXSynth::processReplacing(float **inputs, float **outputs, long sampleFrames)
{
	m_bProcessing = true;
	// copy the inputs array because Cubase shares the same pointer for inputs & outputs
//	memcpy(replacingInputs1, inputs[0], sampleFrames * sizeof(float));
//	memcpy(replacingInputs2, inputs[1], sampleFrames * sizeof(float));
//	for (long e=0; (e < sampleFrames); e++)
//	{
//		replacingInputs1[e] = inputs[0][e];
//		replacingInputs2[e] = inputs[1][e];
//	}


	// whipe out what's in the output array ...
	memset(outputs[0], 0, sampleFrames * sizeof(float));
	memset(outputs[1], 0, sampleFrames * sizeof(float));
//	for (long i=0; (i < sampleFrames); i++)
//	{
//		outputs[0][i] = 0.0f;
//		outputs[1][i] = 0.0f;
//	}

	// ... & then call the regular process function
	doTheProcess(outputs, sampleFrames);
	m_bProcessing = false;
}

void VstXSynth::processMidiEvent(long currEvent)
{
	VstMidiEvent* event = (VstMidiEvent*)m_pVstEvents->events[currEvent];

	char* midiData = event->midiData;
	long status = midiData[0] & 0xf0;		// ignoring channel
	if (status == 0x90 || status == 0x80)	// we only look at notes
	{
		long note = midiData[1] & 0x7f;
		long velocity = midiData[2] & 0x7f;
		if (status == 0x80)
			velocity = 0;

		noteOn(note, velocity, event->deltaFrames); // handle note on or off
/*
		if (!velocity && (note == currentNote))
		{
			noteIsOn = false;	// note off by velocity 0
			if (m_fNoteMode < .5) // reset
			{
				m_fPosition = 0.0f;
			}
			m_bPlayedReverse = false;
		}
		else
			noteOn(note, velocity, event->deltaFrames);
*/
	}
#ifdef _MIDI_CC_
	else if (status == 0xb0) // controller change
	{
		if (midiData[1] == 0x7e)	// all notes off
		{
			noteIsOn = false;
			if (m_fNoteMode < .5) // reset
			{
				m_fPosition = 0.0f;
			}
			m_bPlayedReverse = false;
		}

		if ((midiData[1]>=64) && (midiData[1]<=64 + kNumParams - 1))
		{
			long param = midiData[1]-64;
			long new_data = FixMidiData(param, midiData[2]);
			float value = ((float)new_data)/127.f;
#ifdef _DEMO_
			setParameter(param, value);
#else
			setParameterAutomated(param, value);
#endif

		}
		else
		{
#ifdef _MIDI_LEARN_
			if (m_nMidiLearn == -1)
			{
				// see if control is mapped and convert to message
				for (int x=0; x<(kVolume+1); x++)
				{
					if (m_nMappedControl[x] == midiData[1])
					{
						long new_data = FixMidiData(x, midiData[2]);
						float value = ((float)new_data)/127.f;
#ifdef _DEMO_
						setParameter(x, value);
#else
						setParameterAutomated(x, value);
#endif
					}
				}
			}
			else
			{
				// map control
				m_nMappedControl[m_nMidiLearn] = midiData[1];
			}
#endif
		}

	}
#endif
#ifdef _MIDI_PITCH_BEND_
	else if (status == 0xe0) // pitch bend
	{
		// handle pitch bend here
		unsigned short _14bit;
		_14bit = (unsigned short)midiData[2];
		_14bit<<=7;
		_14bit|=(unsigned short)midiData[1];
		m_nPitchBend = _14bit;
		m_bPitchBendSet = true;
		//if (m_fScratchMode > 0.0f)
		//{
		//	m_bScratching = true;
		//}
		//processScratch(true);

		float fPitchBend;

		if (m_nPitchBend == 0x2000)
		{
			fPitchBend = 0.5f;
		}
		else
		{
			fPitchBend = (float)m_nPitchBend/16383.f;
			if (fPitchBend > 1.0f) fPitchBend = 1.0f;
			if (fPitchBend < 0.0f) fPitchBend = 0.0f;

		}

		setParameter (kScratchAmount, fPitchBend);	 //was 16384
	}
#endif
	/*
					m_fScratchAmount = value;
				m_bScratchAmountSet = true;
			//	if (m_fScratchMode == 0.0f)
			//	{
					if (m_fScratchAmount == .5f)
		{
						m_bScratching = false;
						writelog("bScratching == false\n");
					}
					else
					{
						if (!m_bScratching)
						{
							m_fScratchCenter = m_fPosition;
							m_nScratchCenter = m_fPosition;
							writelog("set center!\n");
						}
			m_bScratching = true;
		}
			//	}
			//	else
			///	{
			//		m_bScratching = true;
			//	}
				processScratch();
	*/
}
/*
void VstXSynth::SetTorqueSpeed()
{
	m_fTorqueSpeed = (float)(( (exp(10.0f*m_fTorque)-1)/(exp(10.0f)-1) ) * (float)m_nSampleRate);
	//m_fTorqueSpeed = fabs(m_fDesiredScratchRate2 - m_fPlaySampleRate) * m_fTorque;
	//m_fTorqueSpeed = fabs(m_fDesiredScratchRate2 - m_fPlaySampleRate) * m_fTorque;
}
*/
void VstXSynth::processScratchStop()
{
	m_nScratchDir = kScratchForward;
//	m_bRecordStopped = true;
//	m_bStoppedRateRestored = false;
	m_fPlaySampleRate = 0.0f;
	if (m_bScratchStop == false)
	{
		m_bScratchStop = true;
	//	m_nScratchInterval = m_nScratchIntervalEnd /2;
	}
	else
	{

	// TO DO:
	// 1. set the position to where it should be to adjust for errors

		m_fPlaySampleRate = 0.0f;
		m_fDesiredScratchRate = 0.0f;
		m_fDesiredScratchRate2 = 0.0f;
		m_fTinyScratchAdjust = 0.0f;
		m_nScratchInterval = 0;
	}



	processDirection();
}

void VstXSynth::processScratch(bool bSetParameter)
{
	// cubase: 0, 235, 314, 512, 826, 1024, 1338, 1536, 1850, 2048, 2560
	// fruity: 0, 1771, 1968, 2165, 2362, 2560
	writelog("scratch interval: %d\n", m_nScratchInterval);
	int bLogSetParameter;
	float fIntervalScaler = 0.0f;

	if (bSetParameter)
	{
		bLogSetParameter = 1;
	}
	else
	{
		bLogSetParameter = 0;
	}

	float fLogDiff;

	if (m_bPitchBendSet)
	{
		// set scratch amount to scaled pitchbend
		float fPitchBend = (float)m_nPitchBend;
		if (bSetParameter)
		{
			setParameter (kScratchAmount, (float)(fPitchBend/16383.f));	 //was 16384
			m_bScratchAmountSet = true;
		}
		else
		{
			m_fScratchAmount = (float)(fPitchBend/16383.f); //was 16384
			m_bScratchAmountSet = true;
		}

		//todo fix pithcbend
		/*
<agoz4> i noticed that with pitch bend at maximum , value is not 1.000000
<agoz4> but 0.9999
<agoz4> division by 128/ 127 something like that?
<agoz4> ok, now that i moved the fader by mouse instead of using pitch wheel,
		it doesn't reset to full speed at half fader
		*/

		if (m_fScratchAmount > 1.0f)
		{
			m_fScratchAmount = 1.0f;
		}

		if (m_fScratchAmount < 0.0f)
		{
			m_fScratchAmount = 0.0f;
		}
		/*
		if (editor)
		{
			((ScratchaEditor*)editor)->setParameter(kScratchAmount, m_fScratchAmount);
			editor->postUpdate();
		}
		*/
		m_bPitchBendSet = false;
	}


	if (m_bScratching) // scratching
	{
//			float fPitchDiff;
/*
		if (m_bPitchBendSet)
		{
			// set scratch amount to scaled pitchbend
			float fPitchBend = (float)m_nPitchBend;
		//	setParameter (kScratchAmount, (float)(fPitchBend/16384.f));
			m_fScratchAmount = (float)(fPitchBend/16384.f);

			//if (editor)
			//{
			//	((ScratchaEditor*)editor)->setParameter(kScratchAmount, m_fScratchAmount);
			//	editor->postUpdate();
			//}

			m_bPitchBendSet = false;
		}
*/
/*
		if (m_fScratchAmount == m_fLastScratchAmount)
		{
#define DELAY_AMOUNT 100
//				if (m_nScratchDelay > DELAY_AMOUNT)
//				{
				m_nScratchDir = kScratchNone;
				fPitchDiff = 0;
				m_bRecordStopped = true;
				m_bStoppedRateRestored = false;
				m_nScratchDelay = 0;
//				}
//				else
//				{
//					m_nScratchDelay++;
//				}
		}
		else if (m_fScratchAmount > m_fLastScratchAmount)
		{
			m_nScratchDir = kScratchForward;
			fPitchDiff = m_fScratchAmount - m_fLastScratchAmount;
			m_bRecordStopped = false;
		}
		else  // m_fScratchAmount < m_fLastScratchAmount
		{
			m_nScratchDir = kScratchBackward;
			fPitchDiff = m_fLastScratchAmount - m_fScratchAmount;
			m_bRecordStopped = false;
		}

		// debug info
		m_fToneArmWeight = (float)m_nScratchDir;  // debug
		m_fRootKey = (float)fPitchDiff; // debug

#define HANDSIZE 4*4096.f
		m_fScratchRate = (float)fabs(((float)fPitchDiff)*(m_fBasePitch)) * HANDSIZE;
*/
		//kScratchNone,
		//kScratchForward,
		//kScratchBackward

		//m_fLastScratchAmount = m_fScratchAmount;
		if (m_fScratchMode == 1.0f)
		{
			if (m_fScratchAmount == 0.5f)
			{
				m_nScratchDir = kScratchForward;
//				m_bRecordStopped = true;
//				m_bStoppedRateRestored = false;
			}
			else if (m_fScratchAmount > 0.5f)
			{
				m_nScratchDir = kScratchForward;
//				m_bRecordStopped = false;
//				m_bStoppedRateRestored = true;
			}
			else
			{
				m_nScratchDir = kScratchBackward;
//				m_bRecordStopped = false;
//				m_bStoppedRateRestored = true;
			}
		}

	//todo:
	// handle scratching like power
	// scratching will set target sample rate and system will catch up based on scratch speed parameter
	// sort of like spin up/down speed param..

	// if curr rate below target rate then add samplespeed2
	// it gone over then set to target rate
	// if curr rate above target rate then sub samplespeed2
	// if gone below then set to target rate


		// calculate hand size
		float fScaler = (m_fScratchSpeed*7.0f) + 1.0f;

//#define SCALER 2.0f
		// set target sample rate
		m_fDesiredScratchRate = (float)fabs(((m_fScratchAmount-0.5f)*2.0f) * fScaler * m_fBasePitch);

		if (m_fScratchMode == 1.0f) // mode 2
		{
			m_fPlaySampleRate = m_fDesiredScratchRate;
			m_bScratchStop = false;
			m_nScratchInterval = 0;
		}
		else // mode 1
		{
		//	int oldtime = timeGetTime();  //time in nanoseconds

			//	NEWEST MODE 2
			// TO DO:
			// make position based
			// use torue to set the spin down time
			// and make the torque speed be samplerate/spindown time

			if (m_fScratchSubMode == 0.0f)
			{
// POSITION BASED:
				if (m_bScratchAmountSet)
				{
					m_bScratchStop = false;

					fScaler = 1.0f;
					fIntervalScaler = ((float)m_nScratchInterval/(float)m_nScratchIntervalEnd) + 1.0f;

					// if 0.5 to 5.0 seconds (m_fScratchSpeed) past the m_fCenterPosition is greater than 1/2 of the entire sample
					// then just max out the distance to 1/2
					/*
					if ((fabs(m_fScratchAmount) * (m_fScratchSpeed*4.5f + 0.5f) * m_nSampleRate) > m_nNumSamples/2)
					{
						if (m_fScratchAmount > 0) // positive
							m_fDesiredPosition = m_fScratchCenter +  m_nNumSamples/2;
						else // negative
							m_fDesiredPosition = m_fScratchCenter - m_nNumSamples/2;
					}
					else
					*/
					m_fDesiredPosition = m_fScratchCenter + (m_fScratchAmount* (m_fScratchSpeed*4.5f + 0.5f) * m_nSampleRate);

					m_fTemporary = m_fDesiredPosition;

					float fDesiredDelta;

					if (m_nScratchInterval == 0)
					{
						m_fPosition = m_fDesiredPosition;
						m_fTinyScratchAdjust = 0;
					//	fDesiredDelta = 0;
					}
					else
					{
						fDesiredDelta = (m_fDesiredPosition - m_fPosition); //m_nScratchInterval;
						m_fTinyScratchAdjust = (fDesiredDelta - m_fPlaySampleRate)/m_nSampleRate;
						//m_nScratchIntervalEnd = m_nSampleRate;

					}








					// m_fPosition, m_fDesiredPosition, m_nScratchInterval
					//if (m_fPosition == m_fDesiredPosition)
					//{
					//	// m_fPlaySampleRate = 0;
					//}
					// do something wiht desireddelta and scratchinterval

					// normalize m_fDesiredPosition
//					while (m_fDesiredPosition > m_fNumSamples)	m_fDesiredPosition -= m_fNumSamples;
//					while (m_fDesiredPosition < 0)				m_fDesiredPosition += m_fNumSamples;

					// figure out direction
					float fDiff = m_fScratchAmount - m_fLastScratchAmount;

					if (fDiff < 0.0)
					{
						fDiff = -fDiff;
						m_nScratchDir = kScratchBackward;
					}
					else
						m_nScratchDir = kScratchForward;

					//m_fDesiredScratchRate2 = (fDiff * (float)m_nSampleRate * (m_fScratchSpeed*4.5f + 0.5f)*(float)SCRATCH_INTERVAL) / fIntervalScaler;
					//if (m_nScratchInterval != 0)
					//{
						m_fDesiredScratchRate2 = m_nSampleRate * (m_fScratchSpeed*4.5f + 0.5f) *m_nScratchInterval;
					//}

				//	m_fPosition += m_fDesiredOffset;
/*
					while (m_fPosition > m_fNumSamples)
					{
						m_fPosition -= m_fNumSamples;
					}

					while (m_fPosition < 0)
					{
						m_fPosition += m_fNumSamples;
					}
					*/
/*
					if (m_nScratchDir == kScratchForward)
						m_fDesiredOffset = m_fDesiredScratchRate2;
					else
						m_fDesiredOffset = -m_fDesiredScratchRate2;
*/
					// figure out destination position and current position
					// figure out distance between the two
					// samples to cover = fDiff * m_nNumSamples
					// need algorithm to get the acceleration needed to cover the distance given current speed
					// calculate the sample rate needed to cover that distance in that amount of time (interval end)
					// figure out desiredscratchrate2
					// m_nScratchIntervalEnd/m_nSampleRate = m_nPosDiff/x
					//m_fDesiredScratchRate2 = (m_nSampleRate/m_nScratchIntervalEnd)*m_nPosDiff;
					//if fDiff == 1.0 then it means ur moving thru the entire sample in one quick move
					//
					//
					// desired rate = distance / time
					// distance is samples to cover, time is interval
					//accel = (desired rate - curr rate)/ time
					// accel is tiny scratch adjust

					// TO DO:
					// 1. figure out how to handle wrapping around the sample

//					m_bRecordStopped = false;
//					m_bStoppedRateRestored = true;
					//new
					// add fScaler = (m_fScratchSpeed*7.0f)



					//m_fDesiredScratchRate2 = (float)(m_fSampleRate * (fDiff*20.0) * fIntervalScaler * fScaler); //40
					//m_fDesiredScratchRate2 = (fDiff * (float)m_nNumSamples * 20.0f) / fIntervalScaler;
					// make sure m_fScratchSpeed isn't 0!!
				//	if (m_fScratchSpeed == 0.0f)
				//	{
				//		m_fDesiredScratchRate2 = (fDiff * m_nSampleRate * 20.0f) / fIntervalScaler;
					//m_fDesiredScratchRate2 = (fDiff * (float)m_nNumSamples * 20.0f) / fIntervalScaler;
				//	}
				//	else
				//	{
					//	m_fDesiredScratchRate2 = (fDiff * (float)m_nSampleRate * (m_fScratchSpeed*4.5f + 0.5f)* 20.0f) / fIntervalScaler;
				//	}
					//m_fDesiredScratchRate2 = (fDiff * (float)m_nSampleRate * (m_fScratchSpeed*4.5f + 0.5f));
					//m_fDesiredScratchRate2 = (fDiff * (float)m_nSampleRate * (m_fScratchSpeed*4.5f + 0.5f)* 20.0f) / fIntervalScaler;

					//m_fTorqueSpeed = (float)fabs(m_fDesiredScratchRate2 - m_fPlaySampleRate) * m_fTorque;

					//m_fDesiredPosition;

					m_fDesiredScratchRate2 = (m_fDesiredScratchRate2+m_fPlaySampleRate)/2;
			//		m_fTinyScratchAdjust = (m_fDesiredScratchRate2 /*- m_fPlaySampleRate*/)/m_nScratchIntervalEnd;

					//m_fPlaySampleRate = (m_fPlaySampleRate + m_fDesiredScratchRate2)/2;
				//	m_fPlaySampleRate += m_fTinyScratchAdjust;
					//m_fPlaySampleRate = 44100.0f;
					//fDiff = 1.57f
					m_bScratchAmountSet = false;
					//if (logfile)
					//{
					//	fprintf(logfile, "fDiff: %f m_nScratchInterval %d m_nScratchIntervalEnd %d m_fPlaySampleRate %f\n", fDiff, m_nScratchInterval, m_nScratchIntervalEnd, m_fPlaySampleRate);
					//}

					m_fPrevDesiredPosition = m_fDesiredPosition;
				}
			}
			else
			{
			/*
			if (m_bScratchAmountSet)
			{
				// TO DO NEW:
				// change so slider it absolute position in sample ie middle = 50%
				m_bScratchStop = false;

				//1. figure out desired position
				float fDiff = m_fScratchAmount - m_fLastScratchAmount;

				if (fDiff < 0.0f)
				{
					fDiff = -fDiff;
					m_nScratchDir = kScratchBackward;
				}
				else
				{
					m_nScratchDir = kScratchForward;
				}

				float fDesiredPosition = (m_fScratchAmount * m_nNumSamples);

				float fPosDiff = fabs(fDesiredPosition - m_fPosition);
				m_fPosition = fDesiredPosition; // hack

				m_fPlaySampleRate = (fPosDiff/(m_nScratchIntervalEnd - m_nScratchInterval))*m_fSampleRate;

				//m_fTinyScratchAdjust = fPosDiff/m_nScratchIntervalEnd;

				writelog("Center: %f Pos: %f DesPos: %f PosDiff: %f Rate: %f\n", m_fScratchCenter, m_fPosition, fDesiredPosition, fPosDiff, m_fPlaySampleRate);

				// set tiny sample rate adjust based on current position in relation to desired

				// must make sure to be able to loop around

				m_bRecordStopped = false;
				m_bStoppedRateRestored = true;
				//new
				fScaler = 1.0f;
				fIntervalScaler = ((float)m_nScratchInterval/(float)m_nScratchIntervalEnd) + 1.0f;

				//m_fDesiredScratchRate2 = (float)(m_fSampleRate * (fDiff*20.0) * fIntervalScaler * fScaler); //40
				//m_fTorqueSpeed = fabs(m_fDesiredScratchRate2 - m_fPlaySampleRate) * m_fTorque;

				//m_fTinyScratchAdjust = (m_fDesiredScratchRate2 - m_fPlaySampleRate)/m_nScratchIntervalEnd;

				//m_fTinyScratchAdjust = (m_fDesiredScratchRate2 - m_fPlaySampleRate)/m_nScratchIntervalEnd;

			//	m_fPlaySampleRate += m_fTinyScratchAdjust;

				m_bScratchAmountSet = false;

			}
*/

// SPEED BASED:
				if (m_bScratchAmountSet)
				{
					m_bScratchStop = false;

					float fDiff = m_fScratchAmount - m_fLastScratchAmount;
					fLogDiff = fDiff;

					if (fDiff < 0.0)
					{
						fDiff = -fDiff;
						m_nScratchDir = kScratchBackward;
					}
					else
					{
						m_nScratchDir = kScratchForward;
					}


//					m_bRecordStopped = false;
//					m_bStoppedRateRestored = true;
					//new
					fScaler = 1.0f;
					/*
					if (m_fPlaySampleRate >= m_nSampleRate/4)
					{
						m_nScratchIntervalEnd = m_nScratchIntervalEndBase;
					}
					else
					{
						m_nScratchIntervalEnd = m_nSampleRate/4;
					}
					*/
					fIntervalScaler = ((float)m_nScratchInterval/(float)m_nScratchIntervalEnd) + 1.0f;

				//	m_fDesiredScratchRate2 = (((fDiff+0.5f)*2.0f)* m_fSampleRate) * fScaler; //fScaler
//					m_fDesiredScratchRate2 = (float)(m_fSampleRate * (fDiff*20.0) * fIntervalScaler * fScaler); //40
					m_fDesiredScratchRate2 = (fDiff * (float)m_nSampleRate * (m_fScratchSpeed*4.5f + 0.5f)*(float)SCRATCH_INTERVAL) / fIntervalScaler;
				//	m_fDesiredScratchRate2 = m_fSampleRate * (fDiff*20.0*fIntervalScaler*10.0); //40
//					m_fTorqueSpeed = fabs(m_fDesiredScratchRate2 - m_fPlaySampleRate) * m_fTorque;
					//m_fTorqueSpeed = m_fDesiredScratchRate2 * m_fTorque;

//					m_nScratchIntervalEnd = m_nScratchIntervalEndBase - m_nScratchIntervalEndBase*(m_fPrevDesiredScratchRate2 - m_fDesiredScratchRate2);

//					if (m_nScratchIntervalEnd < m_nScratchIntervalEndBase)
//						m_nScratchIntervalEnd = m_nScratchIntervalEndBase;

					m_fTinyScratchAdjust = (m_fDesiredScratchRate2 - m_fPlaySampleRate)/m_nScratchIntervalEnd;


//					m_fPrevDesiredScratchRate2 = m_fDesiredScratchRate2;

					//m_fPlaySampleRate = (m_fPlaySampleRate + m_fDesiredScratchRate2)/2;
					m_fPlaySampleRate += m_fTinyScratchAdjust;
					//m_fPlaySampleRate = 44100.0f;
					//fDiff = 1.57f
					m_bScratchAmountSet = false;
					//if (logfile)
					//{
					//	fprintf(logfile, "fDiff: %f m_nScratchInterval %d m_nScratchIntervalEnd %d m_fPlaySampleRate %f\n", fDiff, m_nScratchInterval, m_nScratchIntervalEnd, m_fPlaySampleRate);
					//}
				}
			}
//
			//writelog("fDiff: %f Interval %d SampleRate %f TinyScratch %f DesiredRate %f\n", fLogDiff, m_nScratchInterval, m_fPlaySampleRate, m_fTinyScratchAdjust, m_fDesiredScratchRate2);
//			writelog("%f\t%d\t%f\t%f\t%f\t%f\n", fLogDiff, m_nScratchInterval, m_fPlaySampleRate, m_fTinyScratchAdjust, m_fDesiredScratchRate2, fIntervalScaler);


//			if (m_bScratchAmountSet)
//			{
//				m_fPlaySampleRate = m_fDesiredScratchRate;
//				m_bScratchAmountSet = false;
//			}
			//
//			m_fDesiredScratchRate = 0.0f;

//			if (m_fLastScratchAmount != m_fScratchAmount)
//			{
//				m_bScratchStop = false;
//			}

			m_fLastScratchAmount = m_fScratchAmount;
/*
			if (m_fPlaySampleRate < m_fDesiredScratchRate)
			{
				m_fPlaySampleRate += m_fTorqueSpeed;
				if (m_fPlaySampleRate > m_fDesiredScratchRate)
				{
					m_fPlaySampleRate = m_fDesiredScratchRate;
				}
			}
			else if (m_fPlaySampleRate > m_fDesiredScratchRate)
			{
				m_fPlaySampleRate -= m_fTorqueSpeed;
				if (m_fPlaySampleRate < m_fDesiredScratchRate)
				{
					m_fPlaySampleRate = m_fDesiredScratchRate;
				}
			}
*/
//			if (m_fPlaySampleRate == m_fDesiredScratchRate)
//			{
//				m_nScratchDir = kScratchForward;
//				m_bRecordStopped = true;
//				m_bStoppedRateRestored = false;
//				m_fPlaySampleRate = 0.0f;
//				m_bScratchStop = true;
//			//	m_bScratching = false;
//			}

			// NEWEST MODE 2

			// NEW MODE 2
/*
			static float fDiff;

			if (m_bScratchAmountSet)
			{
				fDiff = m_fScratchAmount - m_fLastScratchAmount;

				if (fDiff < 0.0)
				{
					fDiff = -fDiff;
					m_nScratchDir = kScratchBackward;
				}
				else
				{
					m_nScratchDir = kScratchForward;
				}


				m_bRecordStopped = false;
				m_bStoppedRateRestored = true;

				m_fDesiredScratchRate2 = (((fDiff+0.5f)*2.0f)* m_fSampleRate) * fScaler; //fScaler
				m_fTorqueSpeed = fabs(m_fDesiredScratchRate2 - m_fPlaySampleRate) * m_fTorque;
				//m_fTorqueSpeed = m_fDesiredScratchRate2 * m_fTorque;
				m_bScratchAmountSet = false;
			}

			if (m_fLastScratchAmount != m_fScratchAmount)
			{
				m_bScratchStop = false;
			}

			m_fLastScratchAmount = m_fScratchAmount;

			// make torque a modifier of the distance moved
			if (m_fPlaySampleRate < m_fDesiredScratchRate2)
			{
				m_fPlaySampleRate += m_fTorqueSpeed * fDiff;///fDiff; //fDiff/100000.0f; //(m_fTorqueSpeed) + (fDiff+0.5); // * fDiff);
				if (m_fPlaySampleRate > m_fDesiredScratchRate2)
				{
					m_fPlaySampleRate = m_fDesiredScratchRate2;
				}
			}
			else if (m_fPlaySampleRate > m_fDesiredScratchRate2)
			{
				m_fPlaySampleRate -= m_fTorqueSpeed * fDiff;///fDiff; //fDiff/100000.0f; //(m_fTorqueSpeed) + (fDiff+0.5); // * fDiff);
				if (m_fPlaySampleRate < m_fDesiredScratchRate2)
				{
					m_fPlaySampleRate = m_fDesiredScratchRate2;
				}
			}

			if (m_fPlaySampleRate == m_fDesiredScratchRate2)
			{
				m_nScratchDir = kScratchForward;
				m_bRecordStopped = true;
				m_bStoppedRateRestored = false;
				m_fPlaySampleRate = 0.0f;
				m_bScratchStop = true;
			//	m_bScratching = false;
			}
			// NEW MODE 2 END
*/

			// OLD MODE 2
			//
	/*
			if (m_fLastScratchAmount != m_fScratchAmount)
			{
				m_bScratchStop = false;
			}

			m_fLastScratchAmount = m_fScratchAmount;

			if (m_fPlaySampleRate < m_fDesiredScratchRate)
			{
				m_fPlaySampleRate += m_fTorqueSpeed;
				if (m_fPlaySampleRate > m_fDesiredScratchRate)
				{
					m_fPlaySampleRate = m_fDesiredScratchRate;
				}
			}
			else if (m_fPlaySampleRate > m_fDesiredScratchRate)
			{
				m_fPlaySampleRate -= m_fTorqueSpeed;
				if (m_fPlaySampleRate < m_fDesiredScratchRate)
				{
					m_fPlaySampleRate = m_fDesiredScratchRate;
				}
			}

			if (m_fPlaySampleRate == m_fDesiredScratchRate)
			{
				m_nScratchDir = kScratchForward;
				m_bRecordStopped = true;
				m_bStoppedRateRestored = false;
				m_fPlaySampleRate = 0.0f;
				m_bScratchStop = true;
			//	m_bScratching = false;
			}
		*/
			// OLD MODE 2

		}
//((((exp(10.0f*m_fSpinUpSpeed)-1)/(exp(10.0f)-1));
		m_fScratchVolumeModifier = (m_fPlaySampleRate/m_fSampleRate);
		if (m_fScratchVolumeModifier > 1.5f)
		{
			m_fScratchVolumeModifier = 1.5f;
		}
	//	if ((m_fScratchVolumeModifier > 0.5f) && (m_fScratchVolumeModifier < 1.5f))
	//	{
	//		m_fScratchVolumeModifier = 1.0f;
	//	}

		if (!m_bScratching)
		{
			m_bScratching = true;
		}

		if (m_bScratchStop)
		{
			m_fPlaySampleRate = 0.0f;
			m_fDesiredScratchRate = 0.0f;
			m_fDesiredScratchRate2 = 0.0f;
		}

		m_nScratchInterval = 0;


		//m_fScratchRate

		//m_fPlaySampleRate = m_fScratchRate;

		processDirection();
	}
	else
	{
//		if (m_bScratching)
//		{
			m_nWasScratchingDir = m_nScratchDir;
//			m_bRecordStopped = false;
//			m_bStoppedRateRestored = true;
			m_bScratching = false;
			m_bWasScratching = true;
			processDirection();
//		}
	}


}

void VstXSynth::processPitch()
{
	float temp = m_fDesiredPitch;
	if (m_fKeyTracking == 1.0f)
	{
		int note2play = currentNote - m_nRootKey;
		/*
		if (note2play < 0)
		{
			note2play = 0;
		}
		if (note2play > 127)
		{
			note2play = 127;
		}
		*/

//		currentNote = m_nRootKey;
#if MACX
#define powf pow
#endif
		m_fBasePitch = ((float)m_nSampleRate)*powf(2,(((float)note2play)/12.f));
		//m_fPitchAdjust = ((m_fPitchAmount-0.5f)*2.0f) * (m_fPitchRange * MAX_PITCH_RANGE);
		m_fPitchAdjust = ((m_fPitchAmount-0.5f) * (m_fPitchRange * MAX_PITCH_RANGE));
		m_fDesiredPitch = m_fBasePitch + (m_fBasePitch * m_fPitchAdjust);
	}
	else
	{

		//m_fBasePitch = (float)m_nSampleRate;
		m_fBasePitch = (float)m_nSampleRate;
		//m_fPitchAdjust = ((m_fPitchAmount-0.5f)*2.0f) * (m_fPitchRange * MAX_PITCH_RANGE);
		m_fPitchAdjust = ((m_fPitchAmount-0.5f) * (m_fPitchRange * MAX_PITCH_RANGE));
		m_fDesiredPitch = m_fBasePitch + (m_fBasePitch * m_fPitchAdjust);
		//	m_fPlaySampleRate = m_fDesiredPitch;
	}

	if (temp == m_fPlaySampleRate)
	{
		m_fPlaySampleRate = m_fDesiredPitch; // do instant adjustment
	}
}

void VstXSynth::processDirection()
{
	m_bPlayForward = true;

	if (m_bScratching)
	{
		if (m_nScratchDir == kScratchBackward)
		{
			m_bPlayForward = false;
		}
	}
	else
	{
		if (m_bWasScratching)
		{
			if (m_fDirection >= .5)
			{
				m_bPlayForward = false;
			}
		//	if (m_nWasScratchingDir == kScratchBackward)
		//	{
		//		m_bPlayForward = false;
		//	}
		}
		else
		{
			if (m_fDirection >= .5)
			{
				m_bPlayForward = false;
			}
		}
	}
}

void VstXSynth::setRootKey()
{
	m_nRootKey = (int)(0.5f * 127.f);

	// this will let u tweak root key while note is playing and affect pitch
	/*
	if (m_fPlay == 1.0)
	{
		currentNote = (m_nRootKey - 64) + m_nRootKey;
		processPitch();
	}
	*/
}
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
long VstXSynth::processEvents(VstEvents* ev)
{

	m_pVstEvents = ev;
	return 1;	// want more
}

//-----------------------------------------------------------------------------------------
void VstXSynth::noteOn(long note, long velocity, long delta)
{
	currentNote = note;
	currentVelocity = velocity;
	currentDelta = delta;
	if (velocity == 0)
	{
		if (m_bNPTrack == true)
		{
			setParameter(kPower, 0.0f);
			noteIsOn = true;
			m_fPlay = 0.0f;
//			if (m_fNoteMode < .5) // reset
//			{
//				m_fPosition = 0.0f;
//			}
		}
		else
		{
			noteIsOn = false;
			m_fPlay = 0.0f;
			if (m_fNoteMode < .5) // reset
			{
				m_fPosition = 0.0f;
			}
			m_bPlayedReverse = false;
		}
	}
	else
	{
		noteIsOn = true;
		m_fPlay = 1.0f;

		if (m_fNoteMode < .5) // reset
		{
			m_fPosition = 0.0f;
		}
		// calculate note volume
		m_fNoteVolume = (float)(m_fVolume * (double)currentVelocity * midiScaler);
		//44100*(2^(0/12)) = C-3
		processPitch();

		if (m_bNPTrack == true)
		{
			setParameter(kPower, 1.0f);
		}
	}

	if (editor) ((ScratchaEditor*)editor)->setParameter(kPlay, m_fPlay);

	if (editor)
		editor->postUpdate();
}

void VstXSynth::PlayNote(float value)
{
	//m_fPlay = value;
	if (value == 0.0f)
	{
		noteOn(currentNote, 0, 0); // note off
	}
	else
	{
		//currentNote - m_nRootKey = ?
		// - 64       - 0          = -64
		// 64          - 64         = 0;
		// 190           - 127        = 63;
		currentNote = (m_nRootKey - ROOT_KEY) + m_nRootKey;
		noteOn(currentNote, 127, 0); // note on
	}
}

long VstXSynth::FixMidiData(long param, char value)
{
	long new_value = value;

	switch(param)
	{
		case kPower:
		case kNPTrack:
		case kMute:
		case kPlay:
		case kNoteMode:
		case kDirection:
		case kScratchMode:
//		case kScratchSubMode:
		case kLoopMode:
		case kKeyTracking:
			{
				if (value < 64)
				{
					new_value = 0;
				}
				else
				{
					new_value = 127;
				}
			}
			break;
		default:
			break;
	}

	return new_value;
}

float VstXSynth::FixMidiData(long param, float value)
{
	float new_value = value;

	switch(param)
	{
		case kPower:
		case kNPTrack:
		case kMute:
		case kPlay:
		case kNoteMode:
		case kDirection:
		case kScratchMode:
//		case kScratchSubMode:
		case kLoopMode:
		case kKeyTracking:
			{
				if (value < .5f)
				{
					new_value = 0.0f;
				}
				else
				{
					new_value = 1.0f;
				}
			}
			break;
		default:
			break;
	}

	return new_value;
}

/*
<64 = 0ff >=64 = 0n
*/

#define _LOG_ 1

void VstXSynth::openlog()
{
	logfile = 0;
#ifdef _LOG_
	logfile = fopen("/home/kororaa/ttp.log", "w"); //text mode - write

	time_t currtime;
	time(&currtime);
	tm * tm_struct = localtime(&currtime);

	writelog(" *** TURNTABLIST LOG FILE OPEN %s *** \n\n", asctime(tm_struct));
#endif
}

void VstXSynth::closelog()
{
	if (logfile)
	{
		writelog(" *** TURNTABLIST LOG FILE CLOSED *** \n");
		fclose(logfile);
	}
}

void VstXSynth::writelog(const char * fmt, ...)
{
	int r;

	if (logfile)
	{
		va_list ap;
		va_start(ap,fmt);
		r=vfprintf(logfile,fmt,ap);
		va_end(ap);
		//result = fprintf( logfile, const char *format [, argument ]...);
	}
}

long VstXSynth::getChunk(void** data, bool isPreset)
{
#ifdef _TTP_CHUNK_
	//writelog("getChunk isPreset: %d\n", isPreset);
	// both presets and bank are treated the same
//	if (!isPreset) return 0; // only handle instruments, not banks

	long index;
	//ignore ispreset in 1.0
	//set up structure
	Preset.cId[0] = 'T';
	Preset.cId[1] = 'T';
	Preset.cId[2] = 'P';
	Preset.cId[3] = '0';
	Preset.iVersion[0] = 1;
	Preset.iVersion[1] = 0;
	for (index=0; index<kVolume; index++)
	{
		Preset.fParam[index] = getParameter(index);
#ifdef MAC
		// do endian swap here
		char temp;
		char * swapper = (char *)&Preset.fParam[index];
		temp = swapper[0];
		swapper[0] = swapper[3];
		swapper[3] = temp;
		temp = swapper[1];
		swapper[1] = swapper[2];
		swapper[2] = temp;
#endif
		writelog("fParam[%d]=%f\n", index, Preset.fParam[index]);
	}
	//float fParam[kVolume+1];
	memset(Preset.cFile, 0, 256);
	strcpy(Preset.cFile, m_szFileName);
	writelog("Preset.cFile=%s\n", Preset.cFile);
//	char cFile[256];
	// fill data
	*data = &Preset;
	writelog("sizeof(Preset) is %d\n", sizeof(Preset));
	return sizeof (Preset);
#else
	return 0;
#endif
}	// returns byteSize


long VstXSynth::setChunk(void* data, long byteSize, bool isPreset)
{
#ifdef _TTP_CHUNK_
	//writelog("setChunk isPreset: %d\n", isPreset);
	//if (!isPreset) return 0; // only handle instruments, not banks
	// both presets and bank are treated the same
	long index;
	t_TTP_Preset *myPreset = (t_TTP_Preset*)data;
	//ignore ispreset
	if (editor) ((ScratchaEditor*)editor)->SetDebugString("setChunk");

	// verify structure
	if (byteSize != sizeof(t_TTP_Preset))
	{
		if (editor) ((ScratchaEditor*)editor)->SetDebugString("structure invalid");
		return 0;
	}

	// read data
	if ((myPreset->cId[0] == 'T') &&
		(myPreset->cId[1] == 'T') &&
		(myPreset->cId[2] == 'P') &&
		(myPreset->cId[3] == '0') &&
		(myPreset->iVersion[0] == 1) &&
		(myPreset->iVersion[1] == 0))
	{
		for (index=0; index<kVolume; index++)
		{
			float value = myPreset->fParam[index];
#ifdef MAC
		// do endian swap here
		char temp;
		char * swapper = (char *)&value;
		temp = swapper[0];
		swapper[0] = swapper[3];
		swapper[3] = temp;
		temp = swapper[1];
		swapper[1] = swapper[2];
		swapper[2] = temp;
#endif
			setParameter(index, value);
			writelog("fParam[%d]=%f\n", index, myPreset->fParam[index]);
		}
	}
	else
	{
		if (editor) ((ScratchaEditor*)editor)->SetDebugString("id and version invalid");
	}

	//void ScratchaEditor::SetDebugString(char * string)
	// load wave file
	writelog("Preset.cFile=%s\n", myPreset->cFile);
	strcpy(m_szFileName, myPreset->cFile);
	loadWavFile(m_szFileName);

	char szFileName[256];
	memset(szFileName, 0, 256);
	int iLen = strlen(m_szFileName);
	int x = iLen;
	while (x>0)
	{
		if (m_szFileName[x-1] == '\\')
		{
			break;
		}
		x--;
	}

	//writelog("found \\ at position %d\n",(x-1));
	strncpy(szFileName, &m_szFileName[x], iLen-x);

	if (editor) ((ScratchaEditor*)editor)->SetDebugString(szFileName);

	return 1;
#else
	return 0;
#endif
}


/*
void VstXSynth::SetCurrentProgram(long program)
{
	long oldprogram = m_nCurrProgram;
	m_nCurrProgram = program;

	// turn off current program
	if (editor) ((ScratchaEditor*)editor)->setParameter(kProgram0 + oldprogram, 0.0f);

	if (editor) ((ScratchaEditor*)editor)->setParameter(kProgram0 + m_nCurrProgram, 1.0f);
	//if (editor) ((ScratchaEditor*)editor)->setParameter(index, value);
}

void VstXSynth::SetCurrentBank(long bank)
{
	long oldbank = m_nCurrBank;
	m_nCurrBank = bank;

	// turn off current program
	if (editor) ((ScratchaEditor*)editor)->setParameter(kBank0 + oldbank, 0.0f);

	if (editor) ((ScratchaEditor*)editor)->setParameter(kBank0 + m_nCurrBank, 1.0f);
	//if (editor) ((ScratchaEditor*)editor)->setParameter(index, value);
}
*/
#if MACX
void * VstXSynth::getNSSymbolAddress(const char * symbol)
{
	NSSymbol nssym = 0;

	if (NSIsSymbolNameDefined(symbol))
	{
		nssym = NSLookupAndBindSymbol(symbol);
	}
	if (!nssym)
	{
		writelog("Symbol \"%s\" Not found", symbol);
		return NULL;
	}
	return NSAddressOfSymbol(nssym);
}
#endif
