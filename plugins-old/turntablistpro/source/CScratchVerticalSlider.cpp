
#include "CScratchVerticalSlider.h"

CScratchVerticalSlider::CScratchVerticalSlider (const CRect& size,
     CControlListener *listener,
     int tag,
     int iMinXPos,  // min X position in pixel
     int iMaxXPos,  // max X position in pixel
     CBitmap  *handle,  // bitmap slider
     CBitmap  *background,  // bitmap background
     CPoint &offset, int style):
	CVerticalSlider(size, listener, tag, iMinXPos, iMaxXPos, handle, background, offset, style)
{
};

void CScratchVerticalSlider::mouse (CDrawContext *pContext, CPoint &where, long buttons)
{
	if (!bMouseEnabled)
		return;

	long button = pContext->getMouseButtons ();

	// set the default value
	if (button == (kControl|kLButton))
	{
		value = getDefaultValue ();
		if (isDirty () && listener)
			listener->valueChanged (pContext, this);
		return;
	}

	// allow left mousebutton only
	if (!(button & kLButton))
		return;

	long delta;
	if (style & kHorizontal)
		delta = size.left + offsetHandle.h;
	else
		delta = size.top + offsetHandle.v;
	if (!bFreeClick)
	{
		float fValue;
		if (style & kLeft || style & kTop)
			fValue = value;
		else
			fValue = 1.f - value;
		long actualPos;
		CRect rect;

		if (style & kHorizontal)
		{
			actualPos = offsetHandle.h + (int)(fValue * rangeHandle) + size.left;

			rect.left   = actualPos;
			rect.top    = size.top  + offsetHandle.v;
			rect.right  = rect.left + widthOfSlider;
			rect.bottom = rect.top  + heightOfSlider;

			if (!where.isInside (rect))
				return;
			else
				delta += where.h - actualPos;
		}
		else
		{
			actualPos = offsetHandle.v + (int)(fValue * rangeHandle) + size.top;

			rect.left   = size.left  + offsetHandle.h;
			rect.top    = actualPos;
			rect.right  = rect.left + widthOfSlider;
			rect.bottom = rect.top  + heightOfSlider;

			if (!where.isInside (rect))
				return;
			else
				delta += where.v - actualPos;
		}
	}
	else
	{
		if (style & kHorizontal)
			delta += widthOfSlider / 2 - 1;
		else
			delta += heightOfSlider / 2 - 1;
	}

	float oldVal    = value;
	long   oldButton = button;

	// begin of edit parameter
	getParent ()->beginEdit (tag);
//	getParent ()->setEditView (this);

	while (1)
	{
		button = pContext->getMouseButtons ();
		if (!(button & kLButton))
		{
			value = .5f;
			listener->valueChanged (pContext, this);
			break;
		}

		if ((oldButton != button) && (button & kShift))
		{
			oldVal = value;
			oldButton = button;
		}
		else if (!(button & kShift))
			oldVal = value;

		if (style & kHorizontal)
			value = (float)(where.h - delta) / (float)rangeHandle;
		else
			value = (float)(where.v - delta) / (float)rangeHandle;

		if (style & kRight || style & kBottom)
			value = 1.f - value;

		if (button & kShift)
			value = oldVal + ((value - oldVal) / zoomFactor);
		bounceValue ();

		if (isDirty () && listener)
			listener->valueChanged (pContext, this);

		pContext->getMouseLocation (where);

		doIdleStuff ();
	}

	// end of edit parameter
	getParent ()->endEdit (tag);
}

