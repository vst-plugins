//
// Scratcha.h
//
/*
HISTORY:

1.03 - Sept 29, 2003
Fixed:
	- audio stopping when scratching with NPTRACK enabled
	- filename display issues

1.02 beta - Sept 19, 2003
Changed:
	- first OSX mach-o version
	- updated to libsndfile 1.0.5
	- updated to VST SDK 2.3

1.01 - July 29, 2003 - Release online

1.01 - May 04, 2003 - Release in Computer Music
*/


// FIX BUGS:
// 1) when u hit play, and hide the interface and show it, the play button is now depressed as it should be`
// this is because kPlay is not an editable param.. change it?
// 2) merge the vstfileselect for os9/osx/win - merge more
// 3) cancel loading crash?
//
// 1.1 TO DO:
// 1) fix position based scratching
// how fix
// calculate new desired speed/position
// when processcratch gets called
// 1) missed offset = scratch interval * tiny sample rate
// 2) new offset = blah + missed offsef

// 1.2 TO DO:
// 1) put midi learn back in - remove hardcoded midi cc's
// 2) add settings (midi learn) saving/loading
// 3) add sequence mode - sub mode 2

// 1.5 TO DO:
// 1) add in 64 program/bank system
//

#ifndef __SCRATCHA__
#define __SCRATCHA__

#include <string.h>
#include <stdio.h>

//#include "CWaveFile.h"

#ifndef __AudioEffectX__
#include "audioeffectx.h"
#endif

const double Pi = 3.141592653f;

/////////////////////////////
// DEFINES

//#define _MIDI_LEARN_
#define _TTP_CHUNK_
// _DEMO_ allows automation
//#define _DEMO_
// _MIDI_PITCH_BEND allows pitch bend scratching
#define _MIDI_PITCH_BEND_
// _MIDI_CC_ allows midi cc for controls
#define _MIDI_CC_

#define PITCH_CENTER 0x1FFF
#define SCRATCH_INTERVAL 16 //24

//#define _LOG_

//////////////////////////////////////


#if WIN32
#include <windows.h>
#endif

#if MACX
#elif MAC
#include <CodeFragments.h>
#endif

#include "sndfile.h"

enum
{
	kScratchForward = 0,
	kScratchBackward
};


// parameters
enum
{
	// performance parameters
	kPower = 0,  //64
	kMute,
	kPitchAmount,
	kPitchRange,
	kDirection, // 68
	kNPTrack,
	kScratchAmount,
	kScratchSpeed,

	// support parameters
	kSpinUpSpeed,
	kSpinDownSpeed,
	kNoteMode, // 75
	kLoopMode,
	kScratchMode,
//	kScratchSubMode,
	kKeyTracking,
	kVolume,
#ifdef _MIDI_LEARN_
	kMidiLearn,
#endif
	/*
	kClearPgm,
	kClearBank,
	kClearAll,
	kBank0,
	kBank1,
	kBank2,
	kBank3,
	kBank4,
	kBank5,
	kBank6,
	kBank7,
	kProgram0,
	kProgram1,
	kProgram2,
	kProgram3,
	kProgram4,
	kProgram5,
	kProgram6,
	kProgram7,
	*/
	kSampleRateDisplay, // for displaying purposes only
	kLoad, // load button
	kPlay, // play/note activity
	kOptions,
//	kSave,
	kAbout,
	kFileName,
	kNumParams
};

enum
{
	kNumPrograms = 1,
	kNumOutputs = 2
};

#define MAX_PITCH_RANGE .5f // 50%

class VstXSynthProgram
{
friend class VstXSynth;
public:
	VstXSynthProgram();
	~VstXSynthProgram() {}
private:
	float m_fVolume;
	char name[24];
};

struct t_TTP_Preset
{
	char cId[4];
	char iVersion[2];
	float fParam[kVolume+1];
	char cFile[256];
};

struct t_TTP_Program
{
	SF_INFO sfInfo;
	float * fBuffer;
	float * fLeft;
	float * fRight;

	t_TTP_Program()
	{
		memset((void *)&sfInfo, 0, sizeof(SF_INFO));
		fBuffer = 0;
		fLeft =0;
		fRight = 0;
	}
	~t_TTP_Program()
	{
		memset((void *)&sfInfo, 0, sizeof(SF_INFO));
		if (fBuffer)
		{
			delete [] fBuffer;
			fBuffer = 0;
			fLeft =0;
			fRight = 0;
		}
	}
};

#define NUM_PROGRAMS 64
//------------------------------------------------------------------------------------------
class VstXSynth : public AudioEffectX
{
friend class MoogProgram;
public:
	VstXSynth(audioMasterCallback audioMaster);
	~VstXSynth();

	virtual void process(float **inputs, float **outputs, long sampleframes);
	virtual void processReplacing(float **inputs, float **outputs, long sampleframes);
	virtual long processEvents(VstEvents* events);
	virtual void doTheProcess(float **outputs, long sampleFrames);

	virtual void setProgram(long program);
	virtual void setProgramName(char *name);
	virtual void getProgramName(char *name);
	virtual void setParameter(long index, float value);
	virtual float getParameter(long index);
	virtual void getParameterLabel(long index, char *label);
	virtual void getParameterDisplay(long index, char *text);
	virtual void getParameterName(long index, char *text);
	virtual void setSampleRate(float sampleRate);
	virtual void setBlockSize(long blockSize);
	virtual void suspend();
	virtual void resume();

	virtual bool getOutputProperties (long index, VstPinProperties* properties);
	virtual bool getProgramNameIndexed (long category, long index, char* text);
	virtual bool copyProgram (long destination);
	virtual bool getEffectName (char* name);
	virtual bool getVendorString (char* text);
	virtual bool getProductString (char* text);
	virtual long getVendorVersion () {return 1;}
	virtual long canDo (char* text);
//	virtual long fxIdle ();
	virtual bool canParameterBeAutomated (long index) { return true; }
	virtual long getChunk(void** data, bool isPreset = false); // {return 0;}	// returns byteSize
	virtual long setChunk(void* data, long byteSize, bool isPreset = false); // {return 0;}

	bool	loadWavFile(char * file);
	void	processMidiEvent(long currEvent);
	void	processScratch(bool bSetParameter = false);
	void	processScratchStop();
	void	processPower(bool bIntervalEnded = false);
	void	processPitch();
	void	processDirection();
	void	setRootKey();

	//LibSndFile functions
	void writelog(const char * fmt, ...);

private:
	void initProcess();
	void noteOn(long note, long velocity, long delta);
	void PlayNote(float value);
	long FixMidiData(long param, char value);
	float FixMidiData(long param, float value);
	void SetTorqueSpeed();

//	void SetCurrentBank(long bank);
//	void SetCurrentProgram(long program);

	// log file functions
	void openlog();
	void closelog();

	t_TTP_Program _Program[NUM_PROGRAMS];

	FILE * logfile;

//	float *replacingInputs1, *replacingInputs2;

	float fScaler;
	//VstXSynthProgram* programs;

	VstFileSelect vstFile;

	long currentNote;
	long currentVelocity;
	long currentDelta;
	bool noteIsOn;

	char m_szFileName[256];

	// SCRATCHA STUFF y0!
	char m_szProgramName[24];

#ifdef _MIDI_LEARN_
	long m_nMappedControl[kVolume+1];
#endif

	float * m_fBuffer;
	float * m_fLeft;
	float * m_fRight;

//	int m_nCurrBank;
//	int m_nCurrProgram;

//	int m_nNextBank;
//	int m_nNextProgram;

	// our 32bit floating point wave info
	int m_nNumChannels;	// 1=mono, 2=stereo, 0=yomama
	int m_nNumSamples;	// number of samples per channel
	int m_nSampleRate;
	float m_fSampleRate;

	// optional
	float m_fFreq;
	float m_fBasePitch;
	float m_fPitchAdjust;
	float m_fPlaySampleRate;
	float m_fSystemSampleRate;

	// switches
	float m_fPower; // on/off
	float m_fNPTrack; // scratch mode on/off
	float m_fScratchAmount;
	float m_fLastScratchAmount;
	float m_fMute; // on/off
	float m_fPitchAmount;
	float m_fDirection;
	float m_fScratchSpeed;
#ifdef _MIDI_LEARN_
	float m_fMidiLearn;
	int m_nMidiLearn;
#endif

	// modifiers
	float m_fNoteMode; // reset/resume
	float m_fLoopMode; // on/ofF
	float m_fPitchRange;
	float m_fSpinDownSpeed; // ADD ME
	float m_fSpinUpSpeed;  // ADD ME
	float m_fKeyTracking;  // ADD ME
	float m_fRootKey;  // ADD ME
	float m_fVolume;
	float m_fSampleRateDisplay;


	float m_fUsedSpinDownSpeed;
	float m_fUsedSpinUpSpeed;

//	bool m_bRecordStopped;
//	bool m_bStoppedRateRestored;

	bool m_bPlayedReverse;
	long m_nRootKey;

	double m_fPosition;
	float m_fPosOffset;
	float m_fNumSamples;

//	float m_fStoppedRate;

	float m_fPlay;

//	bool m_bScratch;

	bool m_bPitchBendSet;
	int m_nPitchBend;
	int m_nScratchDir;
	float m_fScratchRate;
	bool m_bScratching;
	bool m_bWasScratching;
	int m_nWasScratchingDir;
	float m_fPreScratchRate;
	int m_nScratchDelay;

	float m_fDesiredPitch;

	float m_fScratchMode;
	float m_fScratchSubMode;


	float m_fNoteVolume;  // recalculated on note on and volume changes
	VstEvents * m_pVstEvents;

	int m_nPowerIntervalEnd;

	bool m_bPlayForward;


	bool m_bProcessing;

	float m_fPrevLeft;
	float m_fPrevRight;

	bool m_bNPTrack;

//	float m_fLastGoodValueL; // for tone arm weight stuff
//	float m_fLastGoodValueR; //

	float m_fDesiredScratchRate;
//	float m_fTorqueSpeed;
	int m_nScratchInterval;
	int m_nScratchIntervalEnd;
	int m_nScratchIntervalEndBase;
	bool m_bScratchStop;

	int m_nScratchCenter; // center position where scratching starts
	float m_fScratchCenter;
	float m_fDesiredOffset;
	float m_fDesiredPosition;
	float m_fPrevDesiredOffset;
	float m_fPrevDesiredPosition;
	float m_fPrevDesiredScratchRate2;

	float m_fTemporary;

	float m_fPrevScratchAmount;

	float m_fScratchVolumeModifier;

	bool m_bScratchAmountSet;

	float m_fDesiredScratchRate2;

	t_TTP_Preset Preset;

	bool	m_bDataReady;

	float m_fTinyScratchAdjust;

	// new power variables to do sample accurate powering up/down
	float m_fDesiredPowerRate;
	float m_fTinyPowerAdjust;

//	bool bFirstSuspend;
//	bool bFirstResume;

};

#endif
