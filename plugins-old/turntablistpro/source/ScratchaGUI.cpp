
#include "ScratchaGUI.h"

#ifndef __SCRATCHA_EDITOR__
#include "ScratchaEditor.h"
#endif

#include <string.h>


//-----------------------------------------------------------------------------
ScratchaGUI::ScratchaGUI (audioMasterCallback audioMaster)
 : VstXSynth (audioMaster)
{
    editor = 0;
    cEffect.flags |= effFlagsHasEditor; // has editor
}

//-----------------------------------------------------------------------------
ScratchaGUI::~ScratchaGUI ()
{
	// the editor gets deleted by the
	// AudioEffect base class
}

//-----------------------------------------------------------------------------
void ScratchaGUI::setParameter (long index, float value)
{
	VstXSynth::setParameter (index, value);

	if (editor)
		((AEffGUIEditor*)editor)->setParameter (index, value);
}

//-----------------------------------------------------------------------------------------
long ScratchaGUI::dispatcher (long opCode, long index, long value, void *ptr, float opt)
{
    int result = 0;

    switch (opCode)
    {
    case effEditClose:
        if (editor)
        {
            editor->close ();
            delete editor;
            editor = 0;
        }
        break;

    case effEditOpen:
        if (display == 0)
            display = (Display*) value;

        if (editor == 0)
            editor = new ScratchaEditor (this);

    default:
        result = AudioEffectX::dispatcher (opCode, index, value, ptr, opt);
    }
    return result;
}
