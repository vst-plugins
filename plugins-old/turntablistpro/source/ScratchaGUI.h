#ifndef __ScratchaGUI_H
#define __ScratchaGUI_H

#include "scratcha.h"
#include <string.h>

class ScratchaGUI : public VstXSynth
{
public:
    ScratchaGUI (audioMasterCallback audioMaster);
    ~ScratchaGUI ();

    virtual void setParameter (long index, float value);

    long dispatcher (long opCode, long index, long value, void *ptr, float opt);
};

#endif
