// simple test editor using the library vstgui.


#ifndef __SCRATCHA_EDITOR__
#include "ScratchaEditor.h"
#endif

#ifndef __SCRATCHA__
#include "Scratcha.h"
#endif

#include <stdio.h>

// skin manager required includes
#if WIN32
#include <tchar.h>
#endif
#include <math.h>

#ifdef MACX
#elif MAC
	#include <StandardFile.h>
	#include <Files.h>
#endif

enum
{
	kButtonWidth = 34,
	kButtonHeight = 37,
	kKnobWidth = 32,
	kKnobHeight = 32,
	kKnobFrames = 61,
	kFaderWidth = 22,
	kFaderHeight = 199,
	kSModeWidth = 34,
	kSModHeight = 18,
	kGramButtonWidth=32,
	kGramButtonHeight=32
};

// positions
ERect CtrlPos[kNumParams+1] =
{
	{25, 123, kButtonWidth, kButtonHeight}, //kPower
	{115, 182, kButtonWidth, kButtonHeight}, //kMute

	{31, 248, kFaderWidth, kFaderHeight}, //kPitchAmount
	{26, 183, kKnobWidth, kKnobHeight}, //kPitchRange
	{70, 300, kButtonWidth, kButtonHeight}, //kDirection
	{160, 123, kButtonWidth, kButtonHeight}, //kNPTrack
	{166, 248, kFaderWidth, kFaderHeight}, //kScratchAmount
	{160, 183, kKnobWidth, kKnobHeight}, //kScratchSpeed

	{116, 124, kKnobWidth, kKnobHeight}, //kSpinUpSpeed
	{71, 124, kKnobWidth, kKnobHeight}, //kSpinDownSpeed

	{115, 241, kButtonWidth, kButtonHeight}, //kNoteMode
	{115, 300, kButtonWidth, kButtonHeight}, //kLoopMode

	{ 71, 360, kButtonWidth, kButtonHeight}, //kScratchMode
//	{115, 360, kButtonWidth, kButtonHeight}, //kScratchSubMode

	{115, 418, kButtonWidth, kButtonHeight}, //kKeyTracking

	{71, 183, kKnobWidth, kKnobHeight}, //kVolume

#ifdef _MIDI_LEARN_
	{71, 419, kButtonWidth, kButtonHeight}, //kMidiLearn
#endif
/*
	{225, 355, kGramButtonWidth, kGramButtonHeight}, //kClearPgm
	{260, 355, kGramButtonWidth, kGramButtonHeight}, //kClearBank
	{295, 355, kGramButtonWidth, kGramButtonHeight}, //kClearAll

	{260,  40, kGramButtonWidth, kGramButtonHeight}, //kBank0
	{260,  75, kGramButtonWidth, kGramButtonHeight}, //kBank1
	{260, 110, kGramButtonWidth, kGramButtonHeight}, //kBank2
	{260, 145, kGramButtonWidth, kGramButtonHeight}, //kBank3
	{260, 180, kGramButtonWidth, kGramButtonHeight}, //kBank4
	{260, 215, kGramButtonWidth, kGramButtonHeight}, //kBank5
	{260, 250, kGramButtonWidth, kGramButtonHeight}, //kBank6
	{260, 285, kGramButtonWidth, kGramButtonHeight}, //kBank7

	{295,  40, kGramButtonWidth, kGramButtonHeight}, //kProgram0
	{295,  75, kGramButtonWidth, kGramButtonHeight}, //kProgram1
	{295, 110, kGramButtonWidth, kGramButtonHeight}, //kProgram2
	{295, 145, kGramButtonWidth, kGramButtonHeight}, //kProgram3
	{295, 180, kGramButtonWidth, kGramButtonHeight}, //kProgram4
	{295, 215, kGramButtonWidth, kGramButtonHeight}, //kProgram5
	{295, 250, kGramButtonWidth, kGramButtonHeight}, //kProgram6
	{295, 285, kGramButtonWidth, kGramButtonHeight}, //kProgram7
*/
	{0, 0, 0, 0}
};




//-----------------------------------------------------------------------------
// resource id's
enum {
	// bitmaps
	kBackgroundId = 128,
	kFaderBodyId,
	kFaderHandleId,
	kGreenOnOffButtonId,
	kOnOffButtonId,
	kAnimKnobId,
	kDirectionButtonId,
	kLoopButtonId,
	kNoteModeButtonId,
	kSplashScreenId,
	kDisplayBackgroundId,
	kDisplayBackgroundId2,
	kScratchModeId,
	kGramButtonId,

	// positions
	kFaderX = 31, //20, //18,
	kFaderY = 248, //174, //10,

	kFaderInc = 18,

	kFileNameX = 31,
	kFileNameY = 25,
	kFileNameWidth = 158,  //30,
	kFileNameHeight = 14,

	kDisplayX = 31,
	kDisplayY = 459,
	kDisplayWidth = 122,  //30,
	kDisplayHeight = 14,

	kPlayButtonX = 70,
	kPlayButtonY = 241,

	kLoadButtonX = 160, //115,
	kLoadButtonY = 64,
	kLoadButtonWidth = 34,
	kLoadButtonHeight = 37,

//	kAboutButtonX = 25,
//	kAboutButtonY = 64,

	kAboutSplashX = 13,
	kAboutSplashY = 64,
	kAboutSplashWidth = 137,
	kAboutSplashHeight = 28,

	kOptionsButtonX = 0, //70,
	kOptionsButtonY = 0 //64,

//	kSaveButtonX = 160,
//	kSaveButtonY = 64
};

#if MOTIF
// resource for MOTIF (format XPM)
#include "images/bmp00128.bmp.xpm"
#include "images/bmp00129.bmp.xpm"
#include "images/bmp00130.bmp.xpm"
#include "images/bmp00131.bmp.xpm"
#include "images/bmp00132.bmp.xpm"
#include "images/bmp00133.bmp.xpm"
#include "images/bmp00134.bmp.xpm"
#include "images/bmp00135.bmp.xpm"
#include "images/bmp00136.bmp.xpm"
#include "images/bmp00137.bmp.xpm"
#include "images/bmp00138.bmp.xpm"
#include "images/bmp00139.bmp.xpm"
#include "images/bmp00140.bmp.xpm"
#include "images/bmp00141.bmp.xpm"

CResTable xpmResources = {
  {kBackgroundId ,         bmp00128},
  {kFaderBodyId  ,         bmp00129},
  {kFaderHandleId,         bmp00130},
  {kGreenOnOffButtonId,    bmp00131},
  {kOnOffButtonId,         bmp00132},
  {kAnimKnobId,            bmp00133},
  {kDirectionButtonId,     bmp00134},
  {kLoopButtonId,          bmp00135},
  {kNoteModeButtonId,      bmp00136},
  {kSplashScreenId,        bmp00137},
  {kDisplayBackgroundId,   bmp00138},
  {kDisplayBackgroundId2,  bmp00139},
  {kScratchModeId,         bmp00140},
  {kGramButtonId,          bmp00141},
  {0, 0}
};
#endif

//-----------------------------------------------------------------------------
// prototype string convert float -> percent
void SetUniversalType(int type);
void percentStringConvert (float value, char* string);
void OffOnStringConvert(float value, char* string);
void DirectionStringConvert(float value, char* string);
void VolumeStringConvert(float value, char* string);
void UniversalStringConvert (float value, char* string);
void UniversalStringConvert (float value, char* string, void* userData);
void float2StringConvert(float value, char *text);
void long2StringConvert(long value, char *text);
void NoteModeStringConvert(float value, char* string);
void PitchRangeStringConvert(float value, char* string);
void PitchAmountStringConvert(float value, char* string);
void ScratchAmountStringConvert(float value, char* string);
//void ToneArmWeightStringConvert(float value, char* string);
void FileNameStringConvert(float value, char * text, void * userData);
void SetDisplayIndex(long index);

void db2StringConvert(float value, char* string); // for volume
//void RootKeyConvert(float value, char* string);

enum
{
	kSTfloat = 0,
	kSTdb,
	kSTlong,
	kSTonoff,
	kSTdir,
	kSTnotemode,
	kSTpamount,
	kSTprange
};

int giStringType;
int giIndex;

void SetUniversalType(int type)
{
	giStringType = type;
}

bool gbBlah = false;

float counter = 0.0f;

void SetDisplayIndex(long index)
{
	giIndex = index;
}

void UniversalStringConvert(float value, char * text)
{
	switch(giStringType)
	{
		case(kSTfloat):		float2StringConvert(value, text); break;
		case(kSTdb):		db2StringConvert(value, text); break;
		case(kSTlong):		long2StringConvert((long)(value*127.0f), text); break;
		case(kSTonoff):		OffOnStringConvert(value, text); break;
		case(kSTdir):		DirectionStringConvert(value, text); break;
		case(kSTnotemode):	NoteModeStringConvert(value, text); break;
		case(kSTpamount):	PitchAmountStringConvert(value, text); break;
		case(kSTprange):	PitchRangeStringConvert(value, text); break;
		default:
			float2StringConvert(value, text);
	}

}

void FileNameStringConvert(float value, char * text, void * userData)
{
	counter++;

//	((ScratchaEditor *)userData)->writelog("FileNameStringConvert1: %s\n", text);
	float2StringConvert(counter, text);
	((ScratchaEditor *)userData)->SetFileName(text);
//	((ScratchaEditor *)userData)->writelog("FileNameStringConvert2: %s\n", text);
/*
	if (gbBlah)
	{
		strcpy(text, _szFileName);
	}
	else
	{
		strcpy(text, "test");
	}
	*/
}


void float2StringConvert(float value, char *text)
{
	long c = 0, neg = 0;
	char string[32];
	char *s;
	double v, integ, i10, mantissa, m10, ten = 10.;

	v = (double)value;
	if(v < 0)
	{
		neg = 1;
		value = -value;
		v = -v;
		c++;
		if(v > 9999999.)
		{
			strcpy(string, " Huge!  ");
			return;
		}
	}
	else if(v > 99999999.)
	{
		strcpy(string, " Huge!  ");
		return;
	}

	s = string + 31;
	*s-- = 0;
	*s-- = '.';
	c++;

	integ = floor(v);
	i10 = fmod(integ, ten);
	*s-- = (char)((long)i10 + '0');
	integ /= ten;
	c++;
	while(integ >= 1. && c < 8)
	{
		i10 = fmod(integ, ten);
		*s-- = (char)((long)i10 + '0');
		integ /= ten;
		c++;
	}
	if(neg)
		*s-- = '-';
	strcpy(text, s + 1);
	if(c >= 8)
		return;

	s = string + 31;
	*s-- = 0;
	mantissa = fmod(v, 1.);
	mantissa *= pow(ten, (double)(8 - c));
	while(c < 8)
	{
		if(mantissa <= 0)
			*s-- = '0';
		else
		{
			m10 = fmod(mantissa, ten);
			*s-- = (char)((long)m10 + '0');
			mantissa /= 10.;
		}
		c++;
	}
	strcat(text, s + 1);
}

void percentStringConvert (float value, char* string)
{
	 sprintf (string, "%d%%", (int)(100 * value));
}

void OffOnStringConvert(float value, char* string)
{
	if (value < .5)
		strcpy (string, "OFF");
	else
		strcpy (string, "ON");
}

void DirectionStringConvert(float value, char* string)
{
	if (value < .5)
		strcpy (string, "Forward");
	else
		strcpy (string, "Reverse");
}

void NoteModeStringConvert(float value, char* string)
{
	if (value < .5)
		strcpy (string, "Reset");
	else
		strcpy (string, "Resume");
}

void db2StringConvert(float value, char* string)
{
	if(value <= 0)
		strcpy(string, "  -oo   ");
	else
		float2StringConvert((float)(20. * log10(value)), string);
}

void PitchRangeStringConvert(float value, char* string)
{
	float2StringConvert ((value*MAX_PITCH_RANGE*100), string);
}

void ScratchAmountStringConvert(float value, char* string)
{
	float2StringConvert (((value-0.5f)*2.0f), string);
}
/*
void ToneArmWeightStringConvert(float value, char* string)
{
	float2StringConvert (((value-0.5f)*2.0f), string);
}
*/
void PitchAmountStringConvert(float value, char* string)
{
	float2StringConvert (((value-0.5f)*2.0f), string);
}

void long2StringConvert(long value, char *text)
{
	char string[32];

	if(value >= 100000000)
	{
		strcpy(text, " Huge!  ");
		return;
	}
	sprintf(string, "%7d", value);
	string[8] = 0;
	strcpy(text, (char *)string);
}

/*

		case kPitchAmount:		float2string ((((m_fPitchAmount-0.5f)*2.0f) * (m_fPitchRange * MAX_PITCH_RANGE*100)), text);	break;
*/

void UniversalStringConvert (float value, char* string, void* userData)
{
}


//-----------------------------------------------------------------------------
// ADEditor class implementation
//-----------------------------------------------------------------------------
ScratchaEditor::ScratchaEditor (AudioEffect *effect)
 : AEffGUIEditor (effect)
{
	hFaderBody   = 0;
	hFaderHandle = 0;
	bmpLoadButton = 0;
	bmpOnOffButton = 0;
	bmpAnimKnob = 0;
	bmpGreenOnOffButton = 0;
	bmpDirectionButton = 0;
	bmpLoopButton = 0;
	bmpNoteModeButton = 0;
	bmpSplashScreen = 0;
	bmpDisplayBackground = 0;
	bmpDisplayBackground2 = 0;
	bmpScratchMode = 0;
	bmpGramButton = 0;


	// buttons
//	kbAbout = 0;
	kbOptions = 0;
	kbLoad = 0;
//	kbSave = 0;

	//kbClearPgm = 0;
	//kbClearBank = 0;
	//kbClearAll = 0;

	btnPlay = 0;
	fsFileSelector = 0;

	btnPower = 0;
	btnMute = 0;
	btnDirection = 0;
	btnNoteMode = 0;
	btnLoop = 0;
	btnNPTrack = 0;
	btnKeyTrack = 0;
/*
	btnProgram[0] = 0;
	btnProgram[1] = 0;
	btnProgram[2] = 0;
	btnProgram[3] = 0;
	btnProgram[4] = 0;
	btnProgram[5] = 0;
	btnProgram[6] = 0;
	btnProgram[7] = 0;

	btnBank[0] = 0;
	btnBank[1] = 0;
	btnBank[2] = 0;
	btnBank[3] = 0;
	btnBank[4] = 0;
	btnBank[5] = 0;
	btnBank[6] = 0;
	btnBank[7] = 0;
*/
	// anim knobs
	akPitchRange = 0;
	akScratchSpeed = 0;
	akSpinUpSpeed = 0;
	akSpinDownSpeed = 0;
	akVolume = 0;

	// faders
	vsPitchAmount = 0;
	vsScratchAmount = 0;

	// displays
	pdDisplay = 0;
	pdFileName = 0;

	teDisplay = 0;

	// splash screen
	ssAbout = 0;

	// load the background bitmap
	// we don't need to load all bitmaps, this could be done when open is called
	hBackground  = new CBitmap (kBackgroundId);

	// init the size of the plugin
	rect.left   = 0;
	rect.top    = 0;
	rect.right  = (short)hBackground->getWidth ();
	rect.bottom = (short)hBackground->getHeight ();

	memset(_szFileName, 0, 1024);
	strcpy(_szFileName, "Turntablist Pro 1.04");
	memset(_szLastPath, 0, 1024);
	strcpy(_szLastPath, "/home");
//	memset(_szFileName, 'b', 10);
}

//-----------------------------------------------------------------------------
ScratchaEditor::~ScratchaEditor ()
{
	// free background bitmap
	if (hBackground)
		hBackground->forget ();
	hBackground = 0;

//	if (fsFileSelector)
//	{
//		delete fsFileSelector;
//	}
}

void ScratchaEditor::SetFileName(char * string)
{
//	((VstXSynth*) (effect))->writelog("SetFileName1: %s\n", _szFileName);
	strcpy(string, _szFileName);
//	((VstXSynth*) (effect))->writelog("SetFileName2: %s\n", string);
}

void ScratchaEditor::writelog(const char * fmt, ...)
{
	((VstXSynth*) (effect))->writelog(fmt);
}

void ScratchaEditor::SetControlSize(int control_id, CRect & size)
{
	size (CtrlPos[control_id].top, CtrlPos[control_id].left, CtrlPos[control_id].top + CtrlPos[control_id].bottom, CtrlPos[control_id].left + CtrlPos[control_id].right);
}

void ScratchaEditor::SetDisplaySize(int control_id, CRect & size)
{
	SetControlSize(control_id, size);
	size.left = (size.left - ((kDisplayWidth - CtrlPos[control_id].right)/2));
	size.top = (size.bottom + 4);
	size.right = (size.left + kDisplayWidth);
	size.bottom = (size.top + kDisplayHeight);
}

//-----------------------------------------------------------------------------
long ScratchaEditor::open (void *ptr)
{
	// !!! always call this !!!
	AEffGUIEditor::open (ptr);

	// load some bitmaps
	if (!hFaderBody)
		hFaderBody = new CBitmap (kFaderBodyId);

	if (!hFaderHandle)
		hFaderHandle = new CBitmap (kFaderHandleId);

	if (!bmpGreenOnOffButton)
		bmpGreenOnOffButton = new CBitmap(kGreenOnOffButtonId);

	if (!bmpOnOffButton)
		bmpOnOffButton = new CBitmap(kOnOffButtonId);

	if (!bmpAnimKnob)
		bmpAnimKnob = new CBitmap(kAnimKnobId);

	if (!bmpDirectionButton)
		bmpDirectionButton = new CBitmap(kDirectionButtonId);

	if (!bmpLoopButton)
		bmpLoopButton = new CBitmap(kLoopButtonId);

	if (!bmpNoteModeButton)
		bmpNoteModeButton = new CBitmap(kNoteModeButtonId);

	if (!bmpSplashScreen)
		bmpSplashScreen = new CBitmap(kSplashScreenId);

	if (!bmpDisplayBackground)
		bmpDisplayBackground = new CBitmap(kDisplayBackgroundId);

	if (!bmpDisplayBackground2)
		bmpDisplayBackground2 = new CBitmap(kDisplayBackgroundId2);

	if (!bmpScratchMode)
		bmpScratchMode = new CBitmap(kScratchModeId);

	if (!bmpGramButton)
		bmpGramButton = new CBitmap(kGramButtonId);


	//--init background frame-----------------------------------------------
	CRect size (0, 0, hBackground->getWidth (), hBackground->getHeight ());
	frame = new CFrame (size, ptr, this);
	frame->setBackground (hBackground);


	//--init the faders------------------------------------------------
	int minPos = kFaderY + 1;
	int maxPos = kFaderY + hFaderBody->getHeight () - hFaderHandle->getHeight ();// - 1;
	CPoint point (0, 0);
	CPoint offset (1, 0);

/*
	// Clear buttons
	SetControlSize(kClearPgm, size);
	kbClearPgm = new CKickButton (size, this, kClearPgm, (bmpGramButton->getHeight()/2), bmpGramButton, point);
	kbClearPgm->setValue (effect->getParameter (kClearPgm));
	frame->addView (kbClearPgm);

	SetControlSize(kClearBank, size);
	kbClearBank = new CKickButton (size, this, kClearBank, (bmpGramButton->getHeight()/2), bmpGramButton, point);
	kbClearBank->setValue (effect->getParameter (kClearBank));
	frame->addView (kbClearBank);

	SetControlSize(kClearAll, size);
	kbClearAll = new CKickButton (size, this, kClearAll, (bmpGramButton->getHeight()/2), bmpGramButton, point);
	kbClearAll->setValue (effect->getParameter (kClearAll));
	frame->addView (kbClearAll);

	// bank
	SetControlSize(kBank0, size);
	btnBank[0] = new COnOffButton (size, this, kBank0, bmpGramButton);
	btnBank[0]->setValue (effect->getParameter (kBank0));
	frame->addView (btnBank[0]);

	SetControlSize(kBank1, size);
	btnBank[1] = new COnOffButton (size, this, kBank1, bmpGramButton);
	btnBank[1]->setValue (effect->getParameter (kBank1));
	frame->addView (btnBank[1]);

	SetControlSize(kBank2, size);
	btnBank[2] = new COnOffButton (size, this, kBank2, bmpGramButton);
	btnBank[2]->setValue (effect->getParameter (kBank2));
	frame->addView (btnBank[2]);

	SetControlSize(kBank3, size);
	btnBank[3] = new COnOffButton (size, this, kBank3, bmpGramButton);
	btnBank[3]->setValue (effect->getParameter (kBank3));
	frame->addView (btnBank[3]);

	SetControlSize(kBank4, size);
	btnBank[4] = new COnOffButton (size, this, kBank4, bmpGramButton);
	btnBank[4]->setValue (effect->getParameter (kBank4));
	frame->addView (btnBank[4]);

	SetControlSize(kBank5, size);
	btnBank[5] = new COnOffButton (size, this, kBank5, bmpGramButton);
	btnBank[5]->setValue (effect->getParameter (kBank5));
	frame->addView (btnBank[5]);

	SetControlSize(kBank6, size);
	btnBank[6] = new COnOffButton (size, this, kBank6, bmpGramButton);
	btnBank[6]->setValue (effect->getParameter (kBank6));
	frame->addView (btnBank[6]);

	SetControlSize(kBank7, size);
	btnBank[7] = new COnOffButton (size, this, kBank7, bmpGramButton);
	btnBank[7]->setValue (effect->getParameter (kBank7));
	frame->addView (btnBank[7]);

	// program
	SetControlSize(kProgram0, size);
	btnProgram[0] = new COnOffButton (size, this, kProgram0, bmpGramButton);
	btnProgram[0]->setValue (effect->getParameter (kProgram0));
	frame->addView (btnProgram[0]);

	SetControlSize(kProgram1, size);
	btnProgram[1] = new COnOffButton (size, this, kProgram1, bmpGramButton);
	btnProgram[1]->setValue (effect->getParameter (kProgram1));
	frame->addView (btnProgram[1]);

	SetControlSize(kProgram2, size);
	btnProgram[2] = new COnOffButton (size, this, kProgram2, bmpGramButton);
	btnProgram[2]->setValue (effect->getParameter (kProgram2));
	frame->addView (btnProgram[2]);

	SetControlSize(kProgram3, size);
	btnProgram[3] = new COnOffButton (size, this, kProgram3, bmpGramButton);
	btnProgram[3]->setValue (effect->getParameter (kProgram3));
	frame->addView (btnProgram[3]);

	SetControlSize(kProgram4, size);
	btnProgram[4] = new COnOffButton (size, this, kProgram4, bmpGramButton);
	btnProgram[4]->setValue (effect->getParameter (kProgram4));
	frame->addView (btnProgram[4]);

	SetControlSize(kProgram5, size);
	btnProgram[5] = new COnOffButton (size, this, kProgram5, bmpGramButton);
	btnProgram[5]->setValue (effect->getParameter (kProgram5));
	frame->addView (btnProgram[5]);

	SetControlSize(kProgram6, size);
	btnProgram[6] = new COnOffButton (size, this, kProgram6, bmpGramButton);
	btnProgram[6]->setValue (effect->getParameter (kProgram6));
	frame->addView (btnProgram[6]);

	SetControlSize(kProgram7, size);
	btnProgram[7] = new COnOffButton (size, this, kProgram7, bmpGramButton);
	btnProgram[7]->setValue (effect->getParameter (kProgram7));
	frame->addView (btnProgram[7]);
*/
	// COnOffButtons
	SetControlSize(kPower, size);
	//size (CtrlPos[kPower].X, CtrlPos[kPower].Y, CtrlPos[kPower].X2, CtrlPos[kPower].Y2);
	btnPower = new COnOffButton (size, this, kPower, bmpOnOffButton);
	btnPower->setValue (effect->getParameter (kPower));
	frame->addView (btnPower);

	SetControlSize(kMute, size);
	//size (CtrlPos[kMute].X, CtrlPos[kMute].Y, CtrlPos[kMute].X2, CtrlPos[kMute].Y2);
	btnMute = new COnOffButton (size, this, kMute, bmpOnOffButton);
	btnMute->setValue (effect->getParameter (kMute));
	frame->addView (btnMute);

	SetControlSize(kDirection, size);
	//size (CtrlPos[kDirection].X, CtrlPos[kDirection].Y, CtrlPos[kDirection].X2, CtrlPos[kDirection].Y2);
	btnDirection = new COnOffButton (size, this, kDirection, bmpDirectionButton);
	btnDirection->setValue (effect->getParameter (kDirection));
	frame->addView (btnDirection);

	SetControlSize(kNoteMode, size);
	//size (CtrlPos[kNoteMode].X, CtrlPos[kNoteMode].Y, CtrlPos[kNoteMode].X2, CtrlPos[kNoteMode].Y2);
	btnNoteMode = new COnOffButton (size, this, kNoteMode, bmpNoteModeButton);
	btnNoteMode->setValue (effect->getParameter (kNoteMode));
	frame->addView (btnNoteMode);

	SetControlSize(kLoopMode, size);
	//size (CtrlPos[kLoopMode].X, CtrlPos[kLoopMode].Y, CtrlPos[kLoopMode].X2, CtrlPos[kLoopMode].Y2);
	btnLoop = new COnOffButton (size, this, kLoopMode, bmpLoopButton);
	btnLoop->setValue (effect->getParameter (kLoopMode));
	frame->addView (btnLoop);

	SetControlSize(kNPTrack, size);
	//size (CtrlPos[kScratch].X, CtrlPos[kScratch].Y, CtrlPos[kScratch].X2, CtrlPos[kScratch].Y2);
	btnNPTrack = new COnOffButton (size, this, kNPTrack, bmpOnOffButton);
	btnNPTrack->setValue (effect->getParameter (kNPTrack));
	frame->addView (btnNPTrack);
#ifdef _MIDI_LEARN_
	SetControlSize(kMidiLearn, size);
	//size (CtrlPos[kKeyTracking].X, CtrlPos[kKeyTracking].Y, CtrlPos[kKeyTracking].X2, CtrlPos[kKeyTracking].Y2);
	btnMidiLearn = new COnOffButton (size, this, kMidiLearn, bmpOnOffButton);
	btnMidiLearn->setValue (effect->getParameter (kMidiLearn));
	frame->addView (btnMidiLearn);
#endif
	SetControlSize(kKeyTracking, size);
	//size (CtrlPos[kKeyTracking].X, CtrlPos[kKeyTracking].Y, CtrlPos[kKeyTracking].X2, CtrlPos[kKeyTracking].Y2);
	btnKeyTrack = new COnOffButton (size, this, kKeyTracking, bmpOnOffButton);
	btnKeyTrack->setValue (effect->getParameter (kKeyTracking));
	frame->addView (btnKeyTrack);

	SetControlSize(kScratchMode, size);
	//size (CtrlPos[kKeyTracking].X, CtrlPos[kKeyTracking].Y, CtrlPos[kKeyTracking].X2, CtrlPos[kKeyTracking].Y2);
	btnScratchMode = new COnOffButton (size, this, kScratchMode, bmpOnOffButton);
	btnScratchMode->setValue (effect->getParameter (kScratchMode));
	frame->addView (btnScratchMode);
/*
	SetControlSize(kScratchSubMode, size);
	//size (CtrlPos[kKeyTracking].X, CtrlPos[kKeyTracking].Y, CtrlPos[kKeyTracking].X2, CtrlPos[kKeyTracking].Y2);
	btnScratchSubMode = new COnOffButton (size, this, kScratchSubMode, bmpOnOffButton);
	btnScratchSubMode->setValue (effect->getParameter (kScratchSubMode));
	frame->addView (btnScratchSubMode);
*/
	// CAnimKnobs
	SetControlSize(kPitchRange, size);
	//size (CtrlPos[kPitchRange].X, CtrlPos[kPitchRange].Y, CtrlPos[kPitchRange].X2, CtrlPos[kPitchRange].Y2);
	akPitchRange = new CAnimKnob (size, this, kPitchRange, kKnobFrames, kKnobHeight, bmpAnimKnob, offset);
	akPitchRange->setValue (effect->getParameter (kPitchRange));
	frame->addView (akPitchRange);

	SetControlSize(kScratchSpeed, size);
	//size (CtrlPos[kScratchSpeed].X, CtrlPos[kScratchSpeed].Y, CtrlPos[kScratchSpeed].X2, CtrlPos[kScratchSpeed].Y2);
	akScratchSpeed = new CAnimKnob (size, this, kScratchSpeed, kKnobFrames, kKnobHeight, bmpAnimKnob, offset);
	akScratchSpeed->setValue (effect->getParameter (kScratchSpeed));
	frame->addView (akScratchSpeed);

	SetControlSize(kSpinUpSpeed, size);
	//size (CtrlPos[kSpinUpSpeed].X, CtrlPos[kSpinUpSpeed].Y, CtrlPos[kSpinUpSpeed].X2, CtrlPos[kSpinUpSpeed].Y2);
	akSpinUpSpeed = new CAnimKnob (size, this, kSpinUpSpeed, kKnobFrames, kKnobHeight, bmpAnimKnob, offset);
	akSpinUpSpeed->setValue (effect->getParameter (kSpinUpSpeed));
	frame->addView (akSpinUpSpeed);

	SetControlSize(kSpinDownSpeed, size);
	//size (CtrlPos[kSpinDownSpeed].X, CtrlPos[kSpinDownSpeed].Y, CtrlPos[kSpinDownSpeed].X2, CtrlPos[kSpinDownSpeed].Y2);
	akSpinDownSpeed = new CAnimKnob (size, this, kSpinDownSpeed, kKnobFrames, kKnobHeight, bmpAnimKnob, offset);
	akSpinDownSpeed->setValue (effect->getParameter (kSpinDownSpeed));
	frame->addView (akSpinDownSpeed);

	SetControlSize(kVolume, size);
	//size (CtrlPos[kVolume].X, CtrlPos[kVolume].Y, CtrlPos[kVolume].X2, CtrlPos[kVolume].Y2);
	akVolume = new CAnimKnob (size, this, kVolume, kKnobFrames, kKnobHeight, bmpAnimKnob, offset);
	akVolume->setValue (effect->getParameter (kVolume));
	frame->addView (akVolume);

	// PitchAmount
	SetControlSize(kPitchAmount, size);
	vsPitchAmount = new CPitchVerticalSlider (size, this, kPitchAmount, minPos, maxPos, hFaderHandle, hFaderBody, point);
	vsPitchAmount->setOffsetHandle (offset);
	vsPitchAmount->setValue (effect->getParameter (kPitchAmount));
	vsPitchAmount->setDefaultValue (0.75f);
	frame->addView (vsPitchAmount);

	// ScratchAmount
	SetControlSize(kScratchAmount, size);
	vsScratchAmount = new CScratchVerticalSlider (size, this, kScratchAmount, minPos, maxPos, hFaderHandle, hFaderBody, point);
	vsScratchAmount->setOffsetHandle (offset);
	vsScratchAmount->setValue (0.5f);
	frame->addView (vsScratchAmount);

	// pd filename
	size (kFileNameX, kFileNameY, kFileNameX + kFileNameWidth, kFileNameY + kFileNameHeight);
	pdFileName = new CParamDisplay (size, bmpDisplayBackground, kCenterText); // bmpDisplayBackground //hBackground
	pdFileName->setFont (kNormalFontSmall);
	pdFileName->setFontColor (kWhiteCColor);
	pdFileName->setBackColor (kBlackCColor);
	pdFileName->setFrameColor (kBlueCColor);
	pdFileName->setValue (0.0f);
	pdFileName->setStringConvert(FileNameStringConvert, this);
	pdFileName->setTextTransparency(true);
	frame->addView (pdFileName);


	// teDisplay
	size (kDisplayX, kDisplayY, kDisplayX + kFileNameWidth, kDisplayY + kFileNameHeight);
	teDisplay = new CTextEdit(size, this, kCenterText, 0, bmpDisplayBackground, 0); // kDoubleClickStyle);
	teDisplay->setFont (kNormalFontSmall);
	teDisplay->setFontColor (kWhiteCColor);
	teDisplay->setBackColor (kBlackCColor);
	teDisplay->setFrameColor (kBlueCColor);
	teDisplay->setValue (0.0f);
	teDisplay->setTextTransparency(true);
	frame->addView (teDisplay);

	// INIT THE BUTTONS
	size (kLoadButtonX, kLoadButtonY,
          kLoadButtonX + bmpOnOffButton->getWidth (), kLoadButtonY + (bmpOnOffButton->getHeight()/2));
	kbLoad = new CKickButton (size, this, kLoad, (bmpOnOffButton->getHeight()/2), bmpOnOffButton, point);
	frame->addView (kbLoad);

	size (kPlayButtonX, kPlayButtonY,
          kPlayButtonX + bmpGreenOnOffButton->getWidth (), kPlayButtonY + (bmpGreenOnOffButton->getHeight()/2));
	btnPlay = new COnOffButton (size, this, kPlay, bmpGreenOnOffButton);
	frame->addView (btnPlay);

	fsFileSelector = 0;
	fsFileSelector = new CFileSelector((AudioEffectX *)effect);

	// splash screen
	size (kAboutSplashX, kAboutSplashY, kAboutSplashX + kAboutSplashWidth, kAboutSplashY + kAboutSplashHeight);
	//size (0, 0, bmpOnOffButton->getWidth (), bmpOnOffButton->getHeight());
	CRect toDisplay(0, 0, bmpSplashScreen->getWidth(), bmpSplashScreen->getHeight());
	//size2.offset(50,50);
	ssAbout = new CSplashScreen (size, this, kAbout, bmpSplashScreen, toDisplay, point);
	frame->addView(ssAbout);

	return true;
}

//-----------------------------------------------------------------------------
void ScratchaEditor::close ()
{
	delete frame;
	frame = 0;

    delete fsFileSelector;
    fsFileSelector = 0;

	// free some bitmaps
	if (hFaderBody)
		hFaderBody->forget ();
	hFaderBody = 0;

	if (hFaderHandle)
		hFaderHandle->forget ();
	hFaderHandle = 0;

	if (bmpLoadButton)
		bmpLoadButton->forget();
	bmpLoadButton =0;
}

//-----------------------------------------------------------------------------
void ScratchaEditor::setParameter (long index, float value)
{
	if (!frame)
		return;

	// called from ADelayEdit
	switch (index)
	{
	case kPower:
		if (btnPower)
			btnPower->setValue (effect->getParameter (index));
		break;
	case kMute:
		if (btnMute)
			btnMute->setValue (effect->getParameter (index));
		break;
	case kPitchAmount:
		if (vsPitchAmount)
			vsPitchAmount->setValue (effect->getParameter (index));
		break;
	case kPitchRange:
		if (akPitchRange)
			akPitchRange->setValue (effect->getParameter (index));
		break;
	case kDirection:
		if (btnDirection)
			btnDirection->setValue (effect->getParameter (index));
		break;
	case kNPTrack:
		if (btnNPTrack)
			btnNPTrack->setValue (effect->getParameter (index));
		break;
	case kScratchAmount:
		if (vsScratchAmount)
			vsScratchAmount->setValue (effect->getParameter (index));
		break;
	case kScratchSpeed:
		if (akScratchSpeed)
			akScratchSpeed->setValue (effect->getParameter (index));
		break;
	case kSpinUpSpeed:
		if (akSpinUpSpeed)
			akSpinUpSpeed->setValue (effect->getParameter (index));
		break;
	case kSpinDownSpeed:
		if (akSpinDownSpeed)
			akSpinDownSpeed->setValue (effect->getParameter (index));
		break;
	case kScratchMode:
		if (btnScratchMode)
			btnScratchMode->setValue (effect->getParameter (index));
		break;
//	case kScratchSubMode:
//		if (btnScratchSubMode)
//			btnScratchSubMode->setValue (effect->getParameter (index));
//		break;
	case kNoteMode:
		if (btnNoteMode)
			btnNoteMode->setValue (effect->getParameter (index));
		break;
	case kLoopMode:
		if (btnLoop)
			btnLoop->setValue (effect->getParameter (index));
		break;
	case kKeyTracking:
		if (btnKeyTrack)
			btnKeyTrack->setValue (effect->getParameter (index));
		break;
	case kVolume:
		if (akVolume)
			akVolume->setValue (effect->getParameter (index));
		break;
	case kPlay:
		if (btnPlay)
			btnPlay->setValue (effect->getParameter (index));
		break;
		/*
	case kBank0:
	case kBank1:
	case kBank2:
	case kBank3:
	case kBank4:
	case kBank5:
	case kBank6:
	case kBank7:
		if (btnBank[index-kBank0])
			btnBank[index-kBank0]->setValue (effect->getParameter (index));
		break;
	case kProgram0:
	case kProgram1:
	case kProgram2:
	case kProgram3:
	case kProgram4:
	case kProgram5:
	case kProgram6:
	case kProgram7:
		if (btnProgram[index-kProgram0])
			btnProgram[index-kProgram0]->setValue (effect->getParameter (index));
		break;
		*/
#ifdef _MIDI_LEARN_
	case kMidiLearn:
		if (btnMidiLearn)
			btnMidiLearn->setValue (effect->getParameter (index));
		break;
#endif
	}


	char display[256];
	if (teDisplay)
	{
		effect->getParameterDisplay(index, display);
		teDisplay->setText(display);
		teDisplay->setDirty();
	}

	if (pdDisplay)
	{
		SetUniversalDisplayType(index);
		pdDisplay->setValue (effect->getParameter (index));
	}

	postUpdate ();
}

//-----------------------------------------------------------------------------
void ScratchaEditor::SetUniversalDisplayType(long index)
{
	SetDisplayIndex(index);

	switch(index)
	{
		case kPitchAmount:
			SetUniversalType(kSTpamount);
			break;
		case kPitchRange:
			SetUniversalType(kSTprange);
			break;
		case kDirection:
			SetUniversalType(kSTdir);
			break;
		case kNoteMode:
			SetUniversalType(kSTnotemode);
			break;
		case kScratchAmount:
		case kScratchSpeed:
		case kSpinUpSpeed:
		case kSpinDownSpeed:
			SetUniversalType(kSTfloat);
			break;
		case kVolume:
			SetUniversalType(kSTdb);
			break;
		case kPower:
		case kMute:
		case kNPTrack:
		case kLoopMode:
		case kKeyTracking:
		case kPlay:
#ifdef _MIDI_LEARN_
		case kMidiLearn:
#endif
			/*
		case kBank0:
		case kBank1:
		case kBank2:
		case kBank3:
		case kBank4:
		case kBank5:
		case kBank6:
		case kBank7:
		case kProgram0:
		case kProgram1:
		case kProgram2:
		case kProgram3:
		case kProgram4:
		case kProgram5:
		case kProgram6:
		case kProgram7:
		*/
			SetUniversalType(kSTonoff);
			break;
		default:
			SetUniversalType(kSTfloat);
	}

}


//-----------------------------------------------------------------------------
void ScratchaEditor::valueChanged (CDrawContext* context, CControl* control)
{
	long tag = control->getTag ();
	switch (tag)
	{
	case kPower:
	case kMute:
	case kPitchAmount:
	case kPitchRange:
	case kDirection:
	case kNPTrack:
	case kScratchAmount:
	case kScratchSpeed:
	case kSpinUpSpeed:
	case kSpinDownSpeed:
	case kScratchMode:
//	case kScratchSubMode:
	case kNoteMode:
	case kLoopMode:
	case kKeyTracking:
	case kVolume:
#ifdef _MIDI_LEARN_
	case kMidiLearn:
#endif
		/*
	case kBank0:
	case kBank1:
	case kBank2:
	case kBank3:
	case kBank4:
	case kBank5:
	case kBank6:
	case kBank7:
	case kProgram0:
	case kProgram1:
	case kProgram2:
	case kProgram3:
	case kProgram4:
	case kProgram5:
	case kProgram6:
	case kProgram7:
	*/
	case kPlay:
		effect->setParameterAutomated (tag, control->getValue ());
		control->update (context);
		break;
	case kOptions:
		break;
	case kLoad:

		if (control->getValue())
		{
			VstFileType aiffType ("AIFF File", "AIFF", "aif", "aiff", "audio/aiff", "audio/x-aiff");
			VstFileType aifcType ("AIFC File", "AIFC", "aif", "aifc", "audio/x-aifc");
			VstFileType waveType ("Wave File", ".WAV", "wav", "wav",  "audio/wav", "audio/x-wav");
			VstFileType sdIIType ("SoundDesigner II File", "Sd2f", "sd2", "sd2");

			VstFileSelect vstFileSelect;
			memset (&vstFileSelect, 0, sizeof (VstFileType));

			vstFileSelect.command     = kVstFileLoad;
			vstFileSelect.type        = kVstFileType;
			strcpy (vstFileSelect.title, "Test for open file selector");
			vstFileSelect.nbFileTypes = 1;
			vstFileSelect.fileTypes   = &waveType;
			vstFileSelect.returnPath  = new char[1024];
			//vstFileSelect.initialPath  = new char[1024];
			vstFileSelect.initialPath = 0;

			char szFileName[256];
			char szDirName[256];
			char szFullPath[256];

			long blah = fsFileSelector->run(&vstFileSelect);

			((VstXSynth*) (effect))->writelog("nbReturnPath: %d  sizeReturnPath: %d\n",vstFileSelect.nbReturnPath,vstFileSelect.sizeReturnPath);

			//strcpy(_szFileName, filestruct.returnPath);
			//strcpy(_szFileName, "blah1");

			gbBlah = true;

			if (vstFileSelect.nbReturnPath > 0)
			{
				((VstXSynth*) (effect))->writelog("filepath: %s sizeReturnPath: %d\n",vstFileSelect.returnPath, vstFileSelect.sizeReturnPath);

				memset(szFileName, 0, 256);
				int iLen = strlen(vstFileSelect.returnPath);
				int x = iLen;
				while (x>0)
				{
					if (vstFileSelect.returnPath[x-1] == '\\')
					{
						break;
					}
					x--;
				}

				((VstXSynth*) (effect))->writelog("found \\ at position %d\n",(x-1));
				strncpy(szFileName, &vstFileSelect.returnPath[x], iLen-x);

				((VstXSynth*) (effect))->writelog("szFileName: %s\n",szFileName);

				strcpy(_szFileName, szFileName);
				((VstXSynth*) (effect))->writelog("szFileNameZ: %s\n",_szFileName);
			}

			if (((VstXSynth*) (effect))->loadWavFile(vstFileSelect.returnPath))
			{
				//strcpy(_szFileName, filestruct.returnPath);
				//strcpy(_szFileName, "true");
				strcpy(_szLastPath, vstFileSelect.returnPath);  // save valid path to use for the next path
			}
			else
			{
				//memset(_szFileName, 0, 1024);
				//strcpy(_szFileName, "false");
			}

			if (pdFileName)
			{
				pdFileName->setValue(1.0f);
				pdFileName->update (context);
				pdFileName->setValue(0.0f);
				pdFileName->update (context);
			}

			delete []vstFileSelect.returnPath;
			if (vstFileSelect.initialPath)
				delete []vstFileSelect.initialPath;

		}
		update();
		break;
//	case kSave:
//		break;
	default:
		control->update (context);
	}

}

void ScratchaEditor::SetDebugString(char * string)
{
	strcpy(_szFileName, string);

	if (pdFileName)
	{
		pdFileName->setValue(1.0f);
	//	pdFileName->update(0);
	}
}

