
#ifndef __SCRATCHA_EDITOR__
#define __SCRATCHA_EDITOR__


// include VSTGUI
#include "../_vstgui/vstgui.h"

#include "CPitchVerticalSlider.h"
#include "CScratchVerticalSlider.h"

//-----------------------------------------------------------------------------
class ScratchaEditor : public AEffGUIEditor, public CControlListener
{
public:
	ScratchaEditor (AudioEffect *effect);
	virtual ~ScratchaEditor ();
	virtual void setParameter (long index, float value);
	void SetDebugString(char * string);
	void SetFileName(char * string);
	void writelog(const char * fmt, ...);

protected:
	virtual long open (void *ptr);
	virtual void close ();
	virtual void valueChanged (CDrawContext* context, CControl* control);

private:

	void SetUniversalDisplayType(long index);

	char _szFileName[1024];
	char _szLastPath[1024];

	// Controls
	CKickButton *kbOptions;
	CKickButton *kbLoad;
	/*
	CKickButton *kbClearPgm;
	CKickButton *kbClearBank;
	CKickButton *kbClearAll;
	*/
//	CKickButton *kbSave;

	COnOffButton *btnPlay;

	VstFileSelect vstFile;
	CFileSelector *fsFileSelector;

	//COnOffButtons
	COnOffButton *btnPower;
	COnOffButton *btnMute;
	COnOffButton *btnDirection;
	COnOffButton *btnNoteMode;
	COnOffButton *btnLoop;
	COnOffButton *btnNPTrack;
	COnOffButton *btnKeyTrack;
	COnOffButton *btnScratchMode;
	COnOffButton *btnScratchSubMode;
	COnOffButton *btnMidiLearn;
/*
	COnOffButton *btnBank[8];
	COnOffButton *btnProgram[8];
*/
	//CAnimKnobs
	CAnimKnob *akPitchRange;
	CAnimKnob *akScratchSpeed;
	CAnimKnob *akSpinUpSpeed;
	CAnimKnob *akSpinDownSpeed;
	CAnimKnob *akVolume;

	// Vertical Sliders

	CPitchVerticalSlider *vsPitchAmount;
	CScratchVerticalSlider *vsScratchAmount;


	// Param Displays
	CParamDisplay *pdDisplay;
	CParamDisplay *pdFileName;

	CTextEdit *teDisplay;

	// Bitmaps
	CBitmap *hBackground;
	CBitmap *hFaderBody;
	CBitmap *hFaderHandle;
	CBitmap *bmpLoadButton;
	CBitmap *bmpOnOffButton;
	CBitmap *bmpAnimKnob;
	CBitmap *bmpGreenOnOffButton;
	CBitmap *bmpDirectionButton;
	CBitmap *bmpLoopButton;
	CBitmap *bmpNoteModeButton;
	CBitmap *bmpSplashScreen;
	CBitmap *bmpDisplayBackground;
	CBitmap *bmpDisplayBackground2;
	CBitmap *bmpScratchMode;
	CBitmap *bmpGramButton;

	// SplashScreen
	CSplashScreen *ssAbout;

	void SetControlSize(int control_id, CRect & size);
	void SetDisplaySize(int control_id, CRect & size);
#ifdef MAC
	void fss2path(char *path, FSSpec *fss);
#endif
};

#endif
