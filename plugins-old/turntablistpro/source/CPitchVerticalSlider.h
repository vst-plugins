

#ifndef __CPitchVerticalSlider__
#define __CPitchVerticalSlider__

#include "../_vstgui/vstcontrols.h"

class CPitchVerticalSlider : public CVerticalSlider
{
public:
	 CPitchVerticalSlider (const CRect& size,
     CControlListener *listener,
     int tag,
     int iMinXPos,  // min X position in pixel
     int iMaxXPos,  // max X position in pixel
     CBitmap  *handle,  // bitmap slider
     CBitmap  *background,  // bitmap background
     CPoint &offset, int style = kBottom);

	virtual void mouse (CDrawContext *pContext, CPoint &where, long buttons = -1);
};

#endif
