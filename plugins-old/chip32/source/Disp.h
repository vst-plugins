//---------------------------------------------------------------------------

#ifndef DispH
#define DispH
//---------------------------------------------------------------------------

#include "CHIP.hpp"
#include "../_vstgui/vstgui.h"

//---------------------------------------------------------------------------
class DISP : public CView
{
private:	
        class CHIP *pCHIP;
        void  WTDraw(CDrawContext *context,int index);
        void  SlidDraw(CDrawContext *context,int index);
        void  HexDraw(CDrawContext *context,int num,int x,int y);
        class CBitmap *Screen;
	class CBitmap *Bg;
	class COffscreenContext *SecSuf;
public:		
        void  Paint(CDrawContext *context);
        void  draw(CDrawContext *context);
	void  update(CDrawContext *context);
	 ~DISP(void);
	DISP(CRect &size,class CHIP *p);
       void  mouse(CDrawContext *context,CPoint &Pos);
};

#endif
