/*-----------------------------------------------------------------------------
REP.cpp
2002.5.18 sam
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <float.h>
#include "CHIP.hpp"
#include "REP.hpp"
#include "ChipEdit.hpp"
#include <AEffEditor.hpp>


const char TblLabel[kNumParams][8]={
	"EG-AR","EG-DR","EG-SL","EG-RR","BIT","FILTER",
	"WT0", "WT1","WT2","WT3","WT4","WT5","WT6","WT7","WT8","WT9","WT10",
	"WT11","WT12","WT13","WT14","WT15","WT16","WT17","WT18","WT19","WT20",
	"WT21","WT22","WT23","WT24","WT25","WT26","WT27","WT28","WT29","WT30",
	"WT31"
};

//-----------------------------------------------------------------------------------------
REP::REP (audioMasterCallback audioMaster) : AudioEffectX (audioMaster, kNumPrograms, kNumParams)
{
	pCHIP = new CHIP;
	if(pCHIP){setProgram (0);}
	if (audioMaster)
	{
		setNumInputs (0);				// no inputs
		setNumOutputs (kNumOutputs);	//
		canProcessReplacing ();
		isSynth ();
		setUniqueID (CCONST ('C', 'h', 'i', 'p'));
	}
	initProcess ();
	suspend ();

	editor = 0;
    // cEffect.flags |= effFlagsHasEditor; // has editor
}

//-----------------------------------------------------------------------------------------
REP::~REP ()
{
	if (pCHIP){
		delete pCHIP;
		pCHIP=NULL;
	}
}
 //------------------------------------------------------------------------
long REP::vendorSpecific (long lArg1, long lArg2, void* ptrArg, float floatArg)
{
//MouseWhell Enable 
#ifdef WINGUI
	if (editor && lArg1 == CCONST('s','t','C','A') && lArg2 == CCONST('W','h','e','e'))
	{
		return editor->onWheel (floatArg) == true ? 1 : 0;
	}
	else
#endif
	{
		return AudioEffectX::vendorSpecific (lArg1, lArg2, ptrArg, floatArg);
	}
}
//-----------------------------------------------------------------------------------------
void REP::setProgram (long program)
{
	pCHIP->setProgNum(program);
	curProgram=program;
	updateDisplay();
	AudioEffectX::updateDisplay();
	if(editor)editor->postUpdate();
}

//-----------------------------------------------------------------------------------------
void REP::setProgramName (char *name)
{
	pCHIP->setProgName(name);
}

//-----------------------------------------------------------------------------------------
void REP::getProgramName (char *name)
{
	strcpy (name, pCHIP->getProgName());
}

//-----------------------------------------------------------------------------------------
void REP::getParameterLabel (long index, char *label)
{
	switch (index)
	{
		case kIIR:strcpy (label, "  Tap   ");break;
		case kBIT:strcpy (label, "  Bit   ");break;
		default:	strcpy (label, "  HEX   ");break;
	}
}

//-----------------------------------------------------------------------------------------
void REP::getParameterDisplay (long index, char *text)
{
	class CHDATA *pVo;
	pVo=&(pCHIP->VoTbl[curProgram]);
	text[0] = 0;
	switch (index){
		case kAR:	sprintf(text,"%02X",pVo->AR);break;
		case kDR:	sprintf(text,"%02X",pVo->DR);break;
		case kSL:	sprintf(text,"%02X",pVo->SL);break;
		case kRR:	sprintf(text,"%02X",pVo->RR);break;
		case kBIT:sprintf(text,"%02X",pVo->BIT+1);break;
		case kIIR:sprintf(text,"%02d",pVo->TAP+1);break;
		default:
			if(index>=kWT0 && index<=kWT31){
				sprintf(text,"%02X",pVo->WT[index-kWT0]);
			}
			break;
	}
}

//-----------------------------------------------------------------------------------------
void REP::getParameterName (long index, char *label)
{
	strcpy (label, TblLabel[index]);
}

//-----------------------------------------------------------------------------------------
void REP::setParameter (long index, float value)
{
	class CHDATA *pVo;
	unsigned char data;
	pVo=&(pCHIP->VoTbl[curProgram]);

	switch (index){
		case kAR:data=(unsigned char) (value*255.0);pVo->AR=data;break;
		case kDR:data=(unsigned char) (value*255.0);pVo->DR=data;break;
		case kSL:data=(unsigned char) (value*255.0);pVo->SL=data;break;
		case kRR:data=(unsigned char) (value*255.0);pVo->RR=data;break;
		case kBIT:data=(unsigned char) (value*7.0);pVo->BIT=data;break;
		case kIIR:data=(unsigned char) (value*31.0);pVo->TAP=data;break;
		default:
			if(index>=kWT0 && index<=kWT31){
				data=(unsigned char) (value*255.0);pVo->WT[index-kWT0]=data;break;
			}
	}
	if(editor)editor->postUpdate();
}

//-----------------------------------------------------------------------------------------
float REP::getParameter (long index)
{
	float value = 0;
	class CHDATA *pVo;
	
	pVo=&(pCHIP->VoTbl[curProgram]);
	switch (index){
		case kAR:value=pVo->AR/255.0;break;
		case kDR:value=pVo->DR/255.0;break;
		case kSL:value=pVo->SL/255.0;break;
		case kRR:value=pVo->RR/255.0;break;
		case kBIT:value=pVo->BIT/7.0;break;
		case kIIR:value=pVo->TAP/31.0;break;
		default:
			if(index>=kWT0 && index<=kWT31){
				value=((float)pVo->WT[index-kWT0])/255.0;break;
			}
	}
	return value;
}

//-----------------------------------------------------------------------------------------
bool REP::getOutputProperties (long index, VstPinProperties* properties)
{
	if (index < kNumOutputs){
		sprintf (properties->label, "Chip %1d", index + 1);
		properties->flags = kVstPinIsActive;
		if (index < 2)
			properties->flags |= kVstPinIsStereo;	// test, make channel 1+2 stereo
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------
bool REP::getProgramNameIndexed (long category, long index, char* text)
{
	if (index < kNumPrograms){
		strcpy (text, pCHIP->getProgName(index));
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------
bool REP::copyProgram (long destination)
{
	if (destination < kNumPrograms){
		pCHIP->VoTbl[destination] = pCHIP->VoTbl[curProgram];
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------
bool REP::getEffectName (char* name)
{
	strcpy (name, "Chip32");
	return true;
}

//-----------------------------------------------------------------------------------------
bool REP::getVendorString (char* text)
{
	strcpy (text, "Sam");
	return true;
}

//-----------------------------------------------------------------------------------------
bool REP::getProductString (char* text)
{
	strcpy (text, "Vst Chip32 ");
	return true;
}

//-----------------------------------------------------------------------------------------
long REP::canDo (char* text)
{
	if (!strcmp (text, "receiveVstEvents"))
		return 1;
	if (!strcmp (text, "receiveVstMidiEvent"))
		return 1;
		//if(!strcmp(text, "midiProgramNames"))
		//		return 1;
	return -1;	// explicitly can't do; 0 => don't know
}

long REP::getMidiProgramName (long channel, MidiProgramName* midiProgramName) {
	pCHIP->getMidiProgram(channel,midiProgramName);
	return(16);
}


long REP::getCurrentMidiProgram (long channel, MidiProgramName* currentProgram) {
	return(pCHIP->getMidiProgram(channel,currentProgram));
}

long REP::getChunk(void** data, bool isPreset) {
#ifdef WIN32
	MessageBox(NULL,"This is Evaluation Version.\nCan't Export.","CHIP32",MB_OK);
#endif
	return(-1);
}
long REP::setChunk(void* data, long byteSize, bool isPreset) {
#ifdef WIN32
	MessageBox(NULL,"This is Evaluation Version.\nCan't Import.","CHIP32",MB_OK);
#endif
	return(-1);
}

//-----------------------------------------------------------------------------------------
long REP::dispatcher (long opCode, long index, long value, void *ptr, float opt)
{
    int result = 0;

    switch (opCode)
    {
    case effEditClose:
        if (editor)
        {
            editor->close ();
            delete editor;
            editor = 0;
        }
        break;

    case effEditOpen:
        if (display == 0)
            display = (Display*) value;

        if (editor == 0)
            editor = new CHIPEdit (this);

    default:
        result = AudioEffectX::dispatcher (opCode, index, value, ptr, opt);
    }
    return result;
}

