//CHIPEdit.hpp

#ifndef __CHIPEDIT__
#define __CHIPEDIT__

#include "CHIP.hpp"
#include <AEffEditor.hpp>
#include "REP.hpp"

#include "../_vstgui/vstgui.h"

class CHIPEdit : public AEffGUIEditor{
public:
	CHIPEdit(class REP *effect);
	virtual ~CHIPEdit();
	virtual long getRect(ERect **);
	virtual long open(void *ptr);
	virtual void close();
	virtual void idle() { if(updateFlag) {updateFlag = 0; update();} }

#if MAC
	virtual void draw(ERect *rect) {rect = rect;}
	virtual long mouse(long x, long y) {x = x; y = y; return 0;}
	virtual long key(long keyCode) {keyCode = keyCode; return 0;}
	virtual void top() {}
	virtual void sleep() {}
#endif
	virtual void update();
	virtual void postUpdate() {updateFlag = 1;}

private:
	class CHDATA *pVoTbl;
	class CHIP *pChip;
	class DISP *pDisp;
	class REP *Effect;
	CBitmap *hBackground;
};

#endif
