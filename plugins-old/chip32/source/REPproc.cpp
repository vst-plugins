/*-----------------------------------------------------------------------------
REPproc.cpp
2002.5.18 sam
-----------------------------------------------------------------------------*/

#ifndef __REPH__
#include "REP.hpp"
#endif
#include "CHIP.hpp"
#include <math.h>
#include <AEffEditor.hpp>
//#define LOG
#ifdef LOG
extern FILE *Flog;
#endif


//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
void REP::setSampleRate (float sampleRate)
{
	AudioEffectX::setSampleRate (sampleRate);
		SampleRate=(long)sampleRate;
		if(pCHIP)pCHIP->SetSamprate((int)sampleRate);
}

//-----------------------------------------------------------------------------------------
void REP::setBlockSize (long blockSize)
{
	AudioEffectX::setBlockSize (blockSize);
	// you may need to have to do something here...
}

//-----------------------------------------------------------------------------------------
void REP::resume ()
{
	int i;

	wantEvents ();
}
//-----------------------------------------------------------------------------------------
void REP::suspend ()
{
	pCHIP->ForceAllOff();
}

//-----------------------------------------------------------------------------------------
void REP::initProcess ()
{
	pCHIP->ForceAllOff();
}

//-----------------------------------------------------------------------------------------
void REP::process (float **inputs, float **outputs, long sampleFrames)
{
	long fs;
	fs=SampleRate/100;
	rate+=sampleFrames;
	while(rate>fs){
		rate-=fs;
		pCHIP->AutoSeq(0);
	}
	pCHIP->pcmset(sampleFrames,outputs,false);

}

//-----------------------------------------------------------------------------------------
void REP::processReplacing (float **inputs, float **outputs, long sampleFrames)
{
	long fs;
	fs=SampleRate/100;
	rate+=sampleFrames;
	while(rate>fs){
		rate-=fs;
		pCHIP->AutoSeq(0);
	}
	pCHIP->pcmset(sampleFrames,outputs,true);


}

//-----------------------------------------------------------------------------------------
long REP::processEvents (VstEvents* ev)
{
#ifdef EVA
	static int NoteOnlimit=0;
#endif
	for (long i = 0; i < ev->numEvents; i++)
	{
		if ((ev->events[i])->type != kVstMidiType)
			continue;
		VstMidiEvent* event = (VstMidiEvent*)ev->events[i];
		char* midiData = event->midiData;
		pCHIP->setDelta((unsigned int)event->deltaFrames);
		unsigned char status = midiData[0] & 0xf0;		// ignoring channel
		char MidiCh = midiData[0] & 0x0f;
		char note = midiData[1] & 0x7f;
		char velocity = midiData[2] & 0x7f;
			#ifdef LOG
				fprintf(Flog,"s=%x,M=%x,n=%x,v=%x,d=%x\n",status,MidiCh,note,velocity,event->deltaFrames);
			#endif
		switch(status){
			case 0x80:
				pCHIP->NoteOff(MidiCh,note);
				break;
			case 0x90:
				if(velocity==0){
					pCHIP->NoteOff(MidiCh,note);
				}else{
				#ifdef EVA
					NoteOnlimit++;
					if(NoteOnlimit==20)
					{NoteOnlimit=19;}else
				#endif 
					{pCHIP->NoteOn(MidiCh,note,velocity);}
				}
				break;
			case 0xb0:
				switch(note){
				case 0x05://PortaTime
					pCHIP->setPortaSpeed(MidiCh,velocity);
					break;
				case 0x06://DataH Entr
					pCHIP->setData(MidiCh,velocity);
					break;
				case 0x0A://pan
					pCHIP->PanCng(MidiCh,velocity);
					break;
				case 0x0C://WT-X
					pCHIP->setWTX(MidiCh,velocity);
					updateDisplay();
					if(editor)editor->postUpdate();
					break;
				case 0x0D://WT-Y
					pCHIP->setWTY(MidiCh,velocity);
					updateDisplay();
					if(editor)editor->postUpdate();
					break;
				case 0x07://ch-vol
					pCHIP->VolCng(MidiCh,velocity);
					break;
				case 0x41://portaOnOff
					pCHIP->setPortaOnOff(MidiCh,(velocity>=64)?true:false);
					break;
				case 0x54://Porta Ctr
					pCHIP->setPortaCtr(MidiCh,velocity);
					break;
				case 0x62://NRPNL
					pCHIP->setNRPNL(MidiCh,velocity);
					break;
				case 0x63://NRPNH
					pCHIP->setNRPNH(MidiCh,velocity);
					break;

				case 0x64://RPNL
					pCHIP->setRPNL(MidiCh,velocity);
					break;
				case 0x65://RPNH
					pCHIP->setRPNH(MidiCh,velocity);
					break;
				case 0x78://All sound off
					pCHIP->NoteAllOff(MidiCh);
					break;
				case 0x7B://All Note Off
					pCHIP->NoteAllOff(MidiCh);
					break;
				case 0x7C:
					pCHIP->setOmni(false);
					break;
				case 0x7D:
					pCHIP->setOmni(true);
					break;
				case 0x7E://Mono
					pCHIP->setPoly(false);
					break;
				case 0x7F://Poly
					pCHIP->setPoly(true);
					break;
				}

				break;
			case 0xc0:
				pCHIP->VoCng(MidiCh,note);
				break;
			case 0xe0:
					pCHIP->BendCng(MidiCh,(velocity<<7)+note);
				break;
		}
		//event++;
	}
	return 1;	// want more
}

//-----------------------------------------------------------------------------------------

