/*-----------------------------------------------------------------------------
disp.cpp
2002.10.25 copyright sam
Chip32 tone GUI Editor for MS-Windows  build by C++Builder,use vstgui.lib
-----------------------------------------------------------------------------*/


#include "Disp.h"
#include "CHIP.hpp"
#include "REP.hpp"
#include "ResChip32.h"
//---------------------------------------------------------------------------

CRect SScRect;
CRect DScRect;
CRect SBgRect;
CRect DBgRect;
#define ScX 14
#define ScY 10
#define ScXOff 0

const int StrX[6]={340,340,340,340,416,416};
const int StrY[6]={17,28,39,50,17,28};
//---------------------------------------------------------------------------
 DISP::DISP(CRect &size,class CHIP *p)
		: CView(size)
{
	pCHIP=p;
	Screen = new class CBitmap(IDI_SCREEN);
	Bg = new class CBitmap(IDI_BG);
	SScRect=CRect(0+ScXOff,0,452+ScXOff,220);
	DScRect=CRect(ScX,ScY,452+ScX,220+ScY);
	SBgRect=CRect(0,0,488,256);
	DBgRect=CRect(0,0,488,256);


}
//---------------------------------------------------------------------------
 DISP::~DISP(void)
{
	Bg->forget();
	Screen->forget();
}

void DISP::update(CDrawContext *context){
	draw(context);
}
//---------------------------------------------------------------------------
void  DISP::draw(CDrawContext *context)
{
	int index,y,x,n,Num,H,L;
	CRect FRect,HRect,LRect,DRect;

	SecSuf = new class COffscreenContext(context,Screen);
	CColor clGreen={0,64,64,0};
	SecSuf->setFrameColor(clGreen);
	SecSuf->setFillColor(clGreen);

	//WavetableWrite
	n=0;
	for(index=kWT0;index<=kWT31;index++,n++){
		x=21+n*8;
		Num=pCHIP->getPrm(index);
		y=67+127-(Num/2);
		FRect=CRect(x,y,x+8,67+64);
		SecSuf->fillRect(FRect);
	}
	HexDraw(SecSuf,0,27,198);
	HexDraw(SecSuf,0,45,198);
	n=0;
	for(index=kAR;index<=kIIR;index++,n++){
		x=22+272+n*25;
		Num=pCHIP->getPrm(index);
		y=66+128-(Num/2);
		if(index==kBIT){y=66+128-(Num+1)*16;Num++;}
		if(index==kIIR){
			y=66+128-((Num+1)*4);
			Num++;
		}

		FRect=CRect(x,y,x+14,y+3);

		SecSuf->drawRect(FRect);
		HexDraw(SecSuf,Num,StrX[index],StrY[index]);
	}

	SecSuf->copyFrom(context,DScRect,CPoint(0,0));
	delete SecSuf;
}

void  DISP::WTDraw(CDrawContext *context,int index)
{
	int Num,x,y,H,L;
	CRect SRect,FRect,DRect;
	CColor clGreen={0,64,64,0};
	static COffscreenContext Os(context,Screen);

	COffscreenContext *pOffcontext;
	pOffcontext=&Os;

	pOffcontext->setFrameColor(clGreen);
	pOffcontext->setFillColor(clGreen);
	x=21+(index-kWT0)*8;
	Num=pCHIP->getPrm(index);
	y=67+127-(Num/2);
	SRect=CRect(x+ScXOff,66,x+8+ScXOff,67+128+1);
	CPoint SPoint(x+ScXOff,66);

	DRect=CRect(x,66,x+8,67+128+1);
	Screen->draw(pOffcontext,DRect,CPoint(x+ScXOff,66));
	FRect=CRect(x,y,x+8,67+64);
	pOffcontext->fillRect(FRect);
	DRect=CRect(x+ScX,66+ScY,x+8+ScX,67+128+1+ScY);
	pOffcontext->copyFrom (context,DRect, SPoint);
	HexDraw(context,index-kWT0,27+ScX,198+ScY);
	HexDraw(context,Num,45+ScX,198+ScY);
}

void  DISP::HexDraw(CDrawContext *context,int num,int x,int y)
{
	CRect SRect,DRect;
	int H,L;

	H=(num>>4)&0x0f;
	L=num&0x0f;
	CPoint SPoint(6*H+ScXOff,222);
	DRect=CRect(x,y,x+6,y+8);
	Screen->draw(context,DRect,SPoint);
	SPoint=CPoint(6*L+ScXOff,222);
	DRect=CRect(x+6,y,x+12,y+8);
	Screen->draw(context,DRect,SPoint);
}

void  DISP::SlidDraw(CDrawContext *context,int index)
{
	CRect SRect,FRect,HRect,LRect,DRect;
	CColor clGreen={0,64,64,0};
	int Num,x,y,H,L;
	 static COffscreenContext Os(context,Screen);
	 COffscreenContext *pOffcontext;
	 pOffcontext=&Os;
	pOffcontext->setFrameColor(clGreen);
	x=22+272+index*25;
	Num=pCHIP->getPrm(index);
	y=66+128-(Num/2);
	if(index==kBIT){y=66+128-(Num+1)*16;Num++;}
	if(index==kIIR){
		y=66+128-((Num+1)*4);
		Num++;
	}
	CPoint SPoint(x+ScXOff,65);
	DRect=CRect(x,65,x+15,68+129+1);
	Screen->draw(pOffcontext,DRect,SPoint);
	FRect=CRect(x,y,x+14,y+3);
	pOffcontext->drawRect(FRect);
	DRect=CRect(x+ScX,65+ScY,x+15+ScX,68+129+1+ScY);
	pOffcontext->copyFrom (context,DRect, SPoint);
	HexDraw(context,Num,StrX[index]+ScX,StrY[index]+ScY);
}

void  DISP::mouse(CDrawContext *context,CPoint &Pos)
{
	int NumX,NumY;
	int Num,data;
	int X,Y;
	long MouseStat;
	long Cnt;
	CPoint CuPoint;

	Cnt=0;
	while((MouseStat=context->getMouseButtons())&(kLButton|kRButton)){
		context->getMouseLocation(CuPoint);
		X=CuPoint.x;
		Y=CuPoint.y;
		// if Mouse Position is not eq befor time,

		if(X>=(ScX+20) && X<(ScX+434) && Y>=(ScY+66-8) && Y<=(ScY+66+128+8)){
			if(Cnt<=1){NumX=(X-(ScX+20))/8;}
			NumY=128-(Y-(ScY+67));
			NumY*=2;
			if(NumY<=0){NumY=0;}
			if(NumY>=256){NumY=255;}
			if(NumX>=0 && NumX<=31){

			//WaveTable

				//ShifLightButton or RightButton ON (only first time)
        		if(
						(
							((MouseStat&(kShift|kLButton))==(kShift|kLButton)) ||
							(MouseStat&kRButton)
						)&& Cnt==0
				){
					data=pCHIP->getPrm(NumX+kWT0);
					if(data>NumY){data--;}else{data++;}
					if(data>=256){data=255;}
					if(data<0){data=0;}
					pCHIP->setPrm(NumX+kWT0,data);
					WTDraw(context,NumX+kWT0);
				}else{
                	//LeftButton ON
					if((MouseStat&(kLButton|kShift))==kLButton){
						pCHIP->setPrm(NumX+kWT0,NumY);
						WTDraw(context,NumX+kWT0);
						Cnt=0;
					}
                }
			}else{
			//Slider
				switch(NumX){
					case 34:case 35://Attack
						Num=kAR;
						break;
					case 37:case 38://Decay
						Num=kDR;
						break;
					case 40:case 41://Sustain
						Num=kSL;
						break;
					case 43:case 44://Release
						Num=kRR;
						break;
					case 46:case 47://Bit
						Num=kBIT;
						NumY/=32;
						break;
					case 49:case 50://Filter
						Num=kIIR;
						NumY/=8;
						break;
					default:
						Num=-1;
						break;
				}

				if(Num>=kAR && Num<=kIIR){

					if((
								((MouseStat&(kShift|kLButton))==(kShift|kLButton)) ||
								(MouseStat&kRButton)
							)&& Cnt==0
						){
						data=pCHIP->getPrm(Num);
						if(NumY>data){
							data++;
						}else{
							if(NumY<data){data--;}
						}
						pCHIP->setPrm(Num,data);
						SlidDraw(context,Num);
					}else{
                    	if((MouseStat&(kShift|kLButton))==kLButton){
							data=NumY;
							pCHIP->setPrm(Num,data);
							SlidDraw(context,Num);
                        }
					}
				}
			}
		}
		if(Cnt++>=100){Cnt=100;}
		getParent()->doIdleStuff();
	}
}
//---------------------------------------------------------------------------

