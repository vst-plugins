/*-----------------------------------------------------------------------------
REP.hpp
2002.5.18 Sam
-----------------------------------------------------------------------------*/
#ifndef __REPH__
#define __REPH__

#include <string.h>
#include <audioeffectx.h>

//const double Pi = 3.141592653f;

enum
{
	kNumPrograms = 16,
	kNumOutputs = 2,

	kAR=0,
	kDR,
	kSL,
	kRR,
	kBIT,
	kIIR,
	kWT0,
	kWT1,
	kWT2,
	kWT3,
	kWT4,
	kWT5,
	kWT6,
	kWT7,
	kWT8,
	kWT9,
	kWT10,
	kWT11,
	kWT12,
	kWT13,
	kWT14,
	kWT15,
	kWT16,
	kWT17,
	kWT18,
	kWT19,
	kWT20,
	kWT21,
	kWT22,
	kWT23,
	kWT24,
	kWT25,
	kWT26,
	kWT27,
	kWT28,
	kWT29,
	kWT30,
	kWT31,
	kNumParams
};


//------------------------------------------------------------------------------------------
class REP : public AudioEffectX
{
public:
	REP(audioMasterCallback audioMaster);
	~REP();
	class CHIP *pCHIP;

	virtual void process(float **inputs, float **outputs, long sampleframes);
	virtual void processReplacing(float **inputs, float **outputs, long sampleframes);
	virtual long processEvents(VstEvents* events);

	virtual void setProgram(long program);
	virtual void setProgramName(char *name);
	virtual void getProgramName(char *name);
	virtual void setParameter(long index, float value);
	virtual float getParameter(long index);
	virtual void getParameterLabel(long index, char *label);
	virtual void getParameterDisplay(long index, char *text);
	virtual void getParameterName(long index, char *text);
	virtual void setSampleRate(float sampleRate);
	virtual void setBlockSize(long blockSize);
	virtual void resume();
    virtual void suspend();

	virtual bool getOutputProperties (long index, VstPinProperties* properties);
	virtual bool getProgramNameIndexed (long category, long index, char* text);
	virtual bool copyProgram (long destination);
	virtual bool getEffectName (char* name);
	virtual bool getVendorString (char* text);
	virtual bool getProductString (char* text);
	virtual long getVendorVersion () {return 1;}
	virtual long canDo (char* text);
	virtual long getChunk(void** data, bool isPreset = false);	// returns byteSize
	virtual long setChunk(void* data, long byteSize, bool isPreset = false);
	long vendorSpecific (long lArg1, long lArg2, void* ptrArg, float floatArg);
	void initProcess();
	virtual long getMidiProgramName (long channel, MidiProgramName* midiProgramName);
								// Struct will be filled with information for 'thisProgramIndex'.
								// Returns number of used programIndexes.
								// If 0 is returned, no MidiProgramNames supported.
	virtual long getCurrentMidiProgram (long channel, MidiProgramName* currentProgram);
								// returns the programIndex of the current program. -1 means not supported.
								// Struct will be filled with information for the current program.
	virtual bool hasMidiProgramsChanged (long channel) {return true;}
								// returns true if the MidiProgramNames, MidiKeyNames or 
								// MidiControllerNames had changed on this channel.
	
	virtual bool beginSetProgram () { return false; } // called before a program is loaded
	virtual bool endSetProgram () { return false; }   // called after...


    long dispatcher (long opCode, long index, long value, void *ptr, float opt);

private:
	int oome;
		long rate;
		long SampleRate;
	void noteOn(long note, long velocity, long delta);
};

#endif
