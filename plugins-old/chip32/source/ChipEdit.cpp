/*-----------------------------------------------------------------------------
ChipEdit.cpp
2002.10.25 copyright sam
Chip32 tone GUI Editor for MS-Windows  build by C++Builder,use vstgui.lib
-----------------------------------------------------------------------------*/

#ifndef __AEffEditor
#include <AEffEditor.hpp>
#endif
#include <AudioEffect.hpp>
#include <stdio.h>
#include "ChipEdit.hpp"
#include "REP.hpp"
#include "CHIP.hpp"
#include "Disp.h"

#include "../_vstgui/vstgui.h"
#include "../_vstgui/vstcontrols.h"

//-----------------------------------------------------------------------------
// resource id's
//-----------------------------------------------------------------------------
#ifdef WIN32
	#include "ResChip32.h"
#endif

#define VERSION "Version 0.09  2003.12.01 "

//screen postion
#define ScX 14
#define ScY 10
#define ScW 452
#define ScH 220


#if MOTIF
// resource for MOTIF (format XPM)
#include "images/bg.xpm"
#include "images/waku.xpm"
#include "images/about.xpm"

CResTable xpmResources = {
	{IDI_BG,      bg_xpm},
	{IDI_SCREEN,  waku_xpm},
    {IDI_ABOUT,   about_xpm},
    {0, 0}
};
#endif


//-----------------------------------------------------------------------------
// CAbout Declaration
//-----------------------------------------------------------------------------
class CAbout : public CSplashScreen{
public:
	CAbout (CRect &size, CControlListener *listener, int tag, CBitmap *handle,
		CRect &toDisplay, CPoint &offset, CPoint &textOffset,CHIP *p);

	void draw (CDrawContext *pContext);
    void mouse (CDrawContext *context, CPoint&where);
private:
	CPoint textOffset;
	CHIP *pChip;
};
//-----------------------------------------------------------------------------
// CAbout implementation
//-----------------------------------------------------------------------------
CAbout::CAbout (CRect &size, CControlListener *listener, int tag, CBitmap *handle,
				CRect &toDisplay, CPoint &offset, CPoint &textOffset,CHIP *p)
				: CSplashScreen (size, listener, tag, handle, toDisplay, offset),
				textOffset (textOffset)
{
	pChip=p;
}
//-----------------------------------------------------------------------------
void CAbout::draw (CDrawContext *pContext){

	if (getValue ())	{
		CSplashScreen::draw (pContext);
		CRect rect (0, 0, getWidth (), 15);
		rect.offset (toDisplay.left + textOffset.h, toDisplay.top + textOffset.v);
		
		char text[128];
		sprintf (text, "%s",VERSION);
		pContext->setFont (kNormalFontSmall);
		pContext->setFontColor (kBlackCColor);
		pContext->drawString (text, rect, 0, kLeftText);

	}



 }
void CAbout::mouse (CDrawContext *context, CPoint&where){

	int i;

    pChip->ForceAllOff();
    CSplashScreen::mouse (context, where);
}


// prototype string convert float -> percent
void percentStringConvert (float value, char* string);
void percentStringConvert (float value, char* string)
{
	sprintf (string, "%d%%", (int)(100 * value));
}
//-----------------------------------------------------------------------------
// ChipEdit class implementation
//-----------------------------------------------------------------------------
//CHIPEdit::CHIPEdit (AudioEffect *effect)
CHIPEdit::CHIPEdit (REP *effect)
 : AEffGUIEditor (effect)
{
	// effect->setEditor(this);
	pVoTbl=effect->pCHIP->VoTbl;
	pChip=effect->pCHIP;
	Effect=effect;
	// load the background bitmap
	// we don't need to load all bitmaps, this could be done when open is called
	hBackground = new CBitmap (IDI_BG);

	// init the size of the plugin
	rect.left	 = 0;
	rect.top	= 0;
	rect.right	= (short)hBackground->getWidth ();
	rect.bottom = (short)hBackground->getHeight ();
}

//-----------------------------------------------------------------------------
CHIPEdit::~CHIPEdit ()
{
	// free the background bitmap
	if (hBackground)
		hBackground->forget();
	hBackground=0;
}

//-----------------------------------------------------------------------------
long CHIPEdit::open (void *ptr)
{
	AEffGUIEditor::open (ptr);
	
	//--init background frame-----------------------------------------------
	CRect size (0, 0, hBackground->getWidth (), hBackground->getHeight ());
	frame = new CFrame(size,ptr,this);
	
	frame->setBackground (hBackground);
	CRect sizeS(ScX,ScY,ScX+ScW,ScY+ScH);
	pDisp = new DISP(size,pChip);
	frame->addView (pDisp);

	CBitmap *pAboutBitmap = new CBitmap (IDI_ABOUT);
	CPoint point (0, 0);
	CRect toDisplay (100, 50, pAboutBitmap->getWidth ()+100, pAboutBitmap->getHeight ()+50);
	size (0, 0, 60, 20);
	size.offset (420, 230);
	CPoint textOffset (40,60);
	CAbout *pAbout = new CAbout (size,(CControlListener *)0,99, pAboutBitmap, toDisplay, point, textOffset,pChip);
	frame->addView (pAbout);

	return true;
}

//-----------------------------------------------------------------------------
void CHIPEdit::close ()
{
	delete frame;
	frame=0;

}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

void CHIPEdit::update()
{
	if(frame){
		frame->draw((CView*)0);
	}
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
long CHIPEdit::getRect (ERect **erect)
{
	static struct ERect r={0,0,255,484};
	*erect =&r;
	return(true);
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
