/*-----------------------------------------------------------------------------
REPmain.cpp
2002.5.18 sam
-----------------------------------------------------------------------------*/

#include <iostream>

#include "AudioEffect.cpp"
#include "audioeffectx.h"
#include "audioeffectx.cpp"
#include "../_vstgui/vstgui.cpp"
#include "../_vstgui/vstcontrols.cpp"

#include "REP.cpp"
#include "REPproc.cpp"
#include "CHIP.cpp"
#include "Disp.cpp"
#include "ChipEdit.cpp"

//==============================================================================
extern "C" AEffect* main_plugin (audioMasterCallback audioMaster) asm ("main");
extern "C" AEffect* main_plugin (audioMasterCallback audioMaster)
{
    std::cout << "main" << std::endl;

	if(!audioMaster (0, audioMasterVersion, 0, 0, 0, 0))
		return 0;  // old version

    REP* const plugin = new REP (audioMaster);

    if (plugin)
        return plugin->getAeffect();

    return 0;
}

__attribute__((constructor)) void myPluginInit() // this is called when the library is unoaded
{
//    std::cout << "myPluginInit" << std::endl;
}

__attribute__((destructor)) void myPluginFini() // this is called when the library is unoaded
{
//    std::cout << "myPluginFini" << std::endl;
}

