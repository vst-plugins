#ifndef __CHIPH__
#define __CHIPH__

#include <list>
#include <audioeffectx.h>
using namespace std;

#define CHMAX 16
#define VOMAX 16

//EG State
#define S_AT	 1
#define S_DK	 2
#define S_SUS	3
#define S_RR	 4
#define S_IDLE 0

//OpCode

#define OP_NOTE 0x00
#define OP_VO	 0x20
#define OP_PAN	0x40
#define OP_VOL	0x80
#define OP_FRQ	0xa0

//#define M_CLK 1577960
#define M_CLK 440*1024*1024
#define CMD_MAX 0xfff
#define CMNDBUFSIZE 0xfff

class CHDATA{
	
public:
	unsigned int AR;
	unsigned int DR;
	unsigned int SL;
	unsigned int RR;
	unsigned int BIT;
	unsigned int WT[32];
	unsigned int TAP;
	char Name[16];
	CHDATA(void);
// ~CHDATA(void);

};

struct TBLMIDICH{
	int VoNum; 
	int Bend;	
	int Vol;	 
	int Pan;	
	int BendRang; 
	unsigned int RPN;
	unsigned int NRPN;
	bool CuRPN;
	int BfNote;	
	int PortaSpeed;  
	bool PortaOnOff;
};

class CHIP{

private:
	unsigned int SampleTime; 
	char CuProgName[64];		
	int CuProgNum;					 
	unsigned int TblPow12[12];
	bool PolyMono;						
	bool OmniMode;						

	list<int> PlayCh,WaitCh;
	struct{
		int VoNum;
		int MidiCh;
		int Note;	
		int Vel;	
		int PortaBend; 
		int PortaSpeed;
	}TblCh[CHMAX];

	struct TBLMIDICH TblMidiCh[16]; 

	int AtachCh(void);		
	void DelCh(int);		
	int ChkEnCh(void);			
	int ChkSonCh(int,int);	
	int NonCh(int,int,int);	
	int NoffCh(int,int);		

public:
	CHIP(void);							

	class CHDATA VoTbl[VOMAX];		
	MidiProgramName MidiProg[16];	
	int Init(void);										
	void setDelta(unsigned int);			
	void NoteOn(int Ch,int Note,int Vol);	
	void NoteOff(int Ch,int Note);				
	void NoteAllOff(int Ch);							
	void ForceAllOff(void);							
	void BendCng(int Ch,int Bend);				
	void VolCng(int Ch,int Vol);					
	void VoCng(int Ch,int VoNum);					
	void PanCng(int Ch,int Pan);					
	int getProgNum(void);									
	void setProgNum(long );								
	void setProgName(char* );							
	char* getProgName(void);							
	char* getProgName(int);								
	unsigned char getPrm(int);						//index(Vst kNum)
	void setPrm(int,unsigned char);				//index(Vst kNum)
	void setRPNH(int,int);
	void setNRPNH(int,int);
	void setRPNL(int,int);
	void setNRPNL(int,int);
	void setData(int,int);
	void setPoly(bool);
	void setWTY(int ch,int num);
	void setWTX(int ch,int num);
	void pcmset(long,float **,bool);
	void AutoSeq(int sampleframes);
	void setPortaSpeed(int MidiCh,int speed);
	void setPortaOnOff(int MidiCh,bool onoff);
	void setPortaCtr(int MidiCh,int Note);
	void SetSamprate(int sampleRate);
	void setOmni(bool Omni);
~CHIP(void);									
	long getMidiProgram(long channel,MidiProgramName *curProg);

private:
	void CalcPd(int);
	void setReg(unsigned char RegNo,unsigned int data);
	int WaveGen(int ch);

	void ExecuteCmd(unsigned int SamplePoint);
	void Envelope(int ch);

	struct EG{
		int state;
		int Cont;
		int Rest;
	}TblEG[CHMAX];

	int PHA[CHMAX];
	int EGDiv;

	int PDCont[CHMAX];
	int PDReset[CHMAX];
	int RestDelta[CHMAX];
	unsigned char CmdOpBuf[CMD_MAX+1];
	unsigned int CmdDataBuf[CMD_MAX+1];
	unsigned int CmdPos[CMD_MAX+1];
	int CmdIdxW;
	int CmdIdxR;
	int Rate;
	int SampRate;
	int CPan[CHMAX];
	int CVol[CHMAX];
	int pan[CHMAX][2];
	int VoNum[CHMAX];
	int Lpf[CHMAX][32];
	int Sum[CHMAX];
	float LpfX[2][3];
	float LpfY[2][2];
	int CuWT;
};

#endif
