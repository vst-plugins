//CHIP.cpp
//2002.5.18 sam

#include "REP.hpp"
#include "CHIP.hpp"
#include <stdio.h>
#include <ctype.h>
#include <math.h>

//#define LOG
//debug log

#ifdef LOG
FILE *Flog;
#endif


const unsigned int TblPow64[64]={
	
	0x1000,0x0ffc,0x0ff8,0x0ff4,
	0x0ff1,0x0fed,0x0fe9,0x0fe6,
	0x0fe2,0x0fde,0x0fdb,0x0fd7,
	0x0fd3,0x0fd0,0x0fcc,0x0fc8,
	0x0fc5,0x0fc1,0x0fbd,0x0fba,
	0x0fb6,0x0fb3,0x0faf,0x0fab,
	0x0fa8,0x0fa4,0x0fa1,0x0f9d,
	0x0f99,0x0f96,0x0f92,0x0f8e,
	0x0f8b,0x0f87,0x0f84,0x0f80,
	0x0f7d,0x0f79,0x0f75,0x0f72,
	0x0f6e,0x0f6e,0x0f6b,0x0f67,
	0x0f60,0x0f5c,0x0f59,0x0f55,
	0x0f52,0x0f4e,0x0f4b,0x0f47,
	0x0f44,0x0f40,0x0f3d,0x0f39,
	0x0f36,0x0f32,0x0f2f,0x0f2b,
	0x0f28,0x0f24,0x0f21,0x0f1d
};

//Class CHDATA
CHDATA::CHDATA(void){
	AR=0xf0;
	DR=0x20;
	SL=0xa0;
	RR=0xf0;
	BIT=7;
	
	int i;
	for(i=0;i<32;i++){
		if (i>16){WT[i]=0xff;}else{WT[i]=0x00;}
	}
}
 
//Class CHIP

CHIP::CHIP(void){
int i,j;
	PolyMono=true;
	OmniMode=false;
	CuWT=0;

	for(i=0;i<CHMAX;i++){
		TblCh[i].VoNum=0;
		TblCh[i].MidiCh=i;
		TblCh[i].Note=0xff; 
		TblCh[i].Vel=0x00;
		TblCh[i].PortaBend=0;
		TblCh[i].PortaSpeed=0;
	}

	for(i=0;i<CHMAX;i++){
		WaitCh.push_back(i);
	}
	PlayCh.clear();
	for(i=0;i<16;i++){
		TblMidiCh[i].VoNum=0;
		TblMidiCh[i].Bend=0;
		TblMidiCh[i].Vol=127;
		TblMidiCh[i].Pan=0x40;
		TblMidiCh[i].BendRang=2;
		TblMidiCh[i].PortaSpeed=0;
		TblMidiCh[i].PortaOnOff=false;
		TblMidiCh[i].BfNote=0x3f;
	}

	for(i=0;i<CHMAX;i++){
		TblEG[i].state=S_IDLE;
		PHA[i]=0;
		PDCont[i]=0x0000;
		PDReset[i]=0x8000;
		CPan[i]=0x40;
		CVol[i]=0x40;
		pan[i][0]=0x40;
		pan[i][1]=0x40;
		VoNum[i]=0;
		RestDelta[i]=0;
	}

	for(i=0;i<16;i++){
		VoTbl[i].TAP=0;
		
		switch(i&0x3){
			case 0:
				for(j=0;j<32;j++){
						VoTbl[i].WT[j]=(j>16)?0xff:0x00;
				}
				sprintf(VoTbl[i].Name,"SquareWave\0");
				break;
			case 1:
				for(j=0;j<32;j++){
						VoTbl[i].WT[j]=j*8;
				}
				sprintf(VoTbl[i].Name,"SawWave\0");
				break;
			case 2:
				for(j=0;j<16;j++){
					VoTbl[i].WT[j]=j*16;
					VoTbl[i].WT[j+16]=255-j*16;
				}
				sprintf(VoTbl[i].Name,"TriangleWave\0");
				break;
			case 3:
				for(j=0;j<8;j++){
					VoTbl[i].WT[j]=0xff-(0x7f>>j);
					VoTbl[i].WT[15-j]=VoTbl[i].WT[j];
					VoTbl[i].WT[16+j]=0x7f>>j;
					VoTbl[i].WT[31-j]=VoTbl[i].WT[16+j];
				}
				sprintf(VoTbl[i].Name,"AsSinWave\0");
				break;
		}
	}
	
	for(i=0;i<12;i++){
		TblPow12[i]= (unsigned int) (65536.0*4.0*pow(2,(-1.0*i)/12.0));
	}
	CmdIdxW=0;
	CmdIdxR=0;
	SampRate=44100;

#ifdef LOG
	Flog=fopen("log.txt","wt");
#endif
}

CHIP::~CHIP(void){
#ifdef LOG
 fclose(Flog);
#endif
}

void CHIP::setReg(unsigned char RegNo,unsigned int data)
{
	if (CmdIdxR != ((CmdIdxW+1)&CMNDBUFSIZE)) {
		#ifdef LOG
				fprintf(Flog,"SetReg %x,%x t=%x\n",RegNo,data,SampleTime);
		#endif

		CmdOpBuf[CmdIdxW]= RegNo;
		CmdDataBuf[CmdIdxW]= data;
		CmdPos[CmdIdxW] = SampleTime;
		CmdIdxW++;
		CmdIdxW&=CMNDBUFSIZE;
	}
}

void CHIP::ExecuteCmd(unsigned int SamplePoint) {
	
	unsigned int data,i;
	unsigned char CuCh;

	while(CmdIdxR != CmdIdxW) {
		unsigned char	regno;
		regno = CmdOpBuf[CmdIdxR];
		data = CmdDataBuf[CmdIdxR];

		if(SamplePoint!=CmdPos[CmdIdxR]){return;}

		CmdIdxR++; CmdIdxR&=CMNDBUFSIZE;
#ifdef LOG
		fprintf(Flog,"CmndExec %x,%x t=%x\n",regno,data,SamplePoint);
#endif

		CuCh=regno&0x1f;
		switch (regno&0xe0) {
			case OP_NOTE:
				if(data==0){
					TblEG[CuCh].state=S_RR;
				}else{
					TblEG[CuCh].state=S_AT;
					TblEG[CuCh].Cont=0;
					RestDelta[CuCh]=0;
					PDCont[CuCh]=0;
				}
				break;
			case OP_VO:
				VoNum[CuCh]=data;
				break;
			case OP_VOL:
				CVol[CuCh]=data;
				pan[CuCh][1]=(CVol[CuCh]*CPan[CuCh])>>7;
				pan[CuCh][0]=(CVol[CuCh]*(0x7f-CPan[CuCh]))>>7;
				break;
			case OP_PAN:
				CPan[CuCh]=data;
				pan[CuCh][1]=(CVol[CuCh]*CPan[CuCh])>>7;
				pan[CuCh][0]=(CVol[CuCh]*(0x7f-CPan[CuCh]))>>7;
				break;

			case OP_FRQ:
				PDReset[CuCh]=data;
				break;
		}
	}
}

void CHIP::Envelope(int ch){

	int Cont;
	int state;
	
	Cont=TblEG[ch].Cont;
	state=TblEG[ch].state;
	switch(TblEG[ch].state){
		case S_IDLE:
			Cont=0;
			break;
		case S_AT:
			Cont+=(VoTbl[VoNum[ch]].AR<<1);
			if(Cont>=0x10000 || VoTbl[VoNum[ch]].AR==0xff){
				Cont=0xffff;
				state=S_DK;
			}
			break;
		case S_DK:
			Cont-=(VoTbl[VoNum[ch]].DR);
			if(Cont<=(int)(VoTbl[VoNum[ch]].SL<<8) ||
					(VoTbl[VoNum[ch]].DR==0xff)
				){
				Cont=VoTbl[VoNum[ch]].SL<<8;
				state=S_SUS;
			}
			break;
		case S_SUS:
			break;
		case S_RR:
			Cont-=(VoTbl[VoNum[ch]].RR);
			if(Cont<=0x100 || VoTbl[VoNum[ch]].RR==0xff){
				Cont=0;
				state=S_IDLE;
			}
			break;
	}
	TblEG[ch].state=state;
	TblEG[ch].Cont=Cont;
}

int CHIP::WaveGen(int ch){
	
	int Pd,idata,add,itmp,i;
	unsigned int j;
	float fdata;
	
	//for(i=0;i<4;i++){
	itmp=M_CLK/2+RestDelta[ch];
	add=itmp/SampRate;
	PDCont[ch]+=add;
	RestDelta[ch]=itmp-add*SampRate;
	TblEG[ch].Rest+=add;
	while(TblEG[ch].Rest>0){
		TblEG[ch].Rest-=4096*8;
		Envelope(ch);
	}
	/*
		while(PDCont[ch]>=PDReset[ch]){
		PDCont[ch]-=PDReset[ch];
		PHA[ch]++;
		if(PHA[ch]>=32){
			PHA[ch]=0;
		}
	}*/
	
	PHA[ch]+=PDCont[ch]/PDReset[ch];
	PDCont[ch]=PDCont[ch]%PDReset[ch];
	PHA[ch]&=0x1f;

	for(j=VoTbl[VoNum[ch]].TAP;j!=0;j--){
		Lpf[ch][j]=Lpf[ch][j-1];
	}
	//idata=VoTbl[VoNum[ch]].WT[PHA[ch]];
	Lpf[ch][0]=VoTbl[VoNum[ch]].WT[PHA[ch]]-0x80;
	//}

	idata=0;
	if(VoTbl[VoNum[ch]].TAP!=0){
		for(j=0;j<=VoTbl[VoNum[ch]].TAP;j++){
			idata+=Lpf[ch][j];
		}
		idata /= (VoTbl[VoNum[ch]].TAP+1.0);



	}else{idata=Lpf[ch][0];}

	idata+=0x80;
    //bit cut lo-fi process
	idata&=((0x7f80>>(VoTbl[VoNum[ch]].BIT))&0xff);
	idata-=0x80;
	idata*=((TblEG[ch].Cont>>8)&0xff);
	return(idata);
}


void CHIP::pcmset(long ndata,float **PcmBuf,bool replace) {

	int i,j;
	int ch;
	int data;
//	int Out[2];
	float Out[2];
	int PcmBufPtr;
	PcmBufPtr=0;
	for (i=0; i<ndata; ++i) {
		ExecuteCmd((unsigned int)i);
		Out[0]=0.0;
		Out[1]=0.0;
		for(ch=0;ch<CHMAX;ch++){
			if(TblEG[ch].state!=S_IDLE){
				data=WaveGen(ch);
				Out[0]+=data*pan[ch][0];
				Out[1]+=data*pan[ch][1];
			}
		}

		for(ch=0;ch<2;ch++){
			for(j=0;j<1;j++){
				LpfX[ch][2]=LpfX[ch][1];
				LpfX[ch][1]=LpfX[ch][0];
				LpfY[ch][1]=LpfY[ch][0];
//			Out[ch]>>=14;
//			LpfX[ch][0]=((float)Out[ch])/32767.0f;
				LpfX[ch][0]=Out[ch]/(32768.0f*16.0f*1024.0f);
				LpfY[ch][0]=(LpfX[ch][0]+2.0*LpfX[ch][1]+LpfX[ch][2]-LpfY[ch][1])/3.0f;
				Out[ch]=0;
			}
		}

		if(replace==true){
			PcmBuf[0][PcmBufPtr] = LpfY[0][1];//+LpfY[0][1];
			PcmBuf[1][PcmBufPtr] = LpfY[1][1];//+LpfY[1][1];
		}else{
			PcmBuf[0][PcmBufPtr]+= LpfY[0][1];//+LpfY[0][1];
			PcmBuf[1][PcmBufPtr]+= LpfY[1][1];//+LpfY[1][1];
		}
/*
				if(replace==true){
					PcmBuf[0][PcmBufPtr] = ((float)LPF[0])/32767.0f;
					PcmBuf[1][PcmBufPtr] = ((float)LPF[1])/32767.0f;
				}else{
					PcmBuf[0][PcmBufPtr]+= ((float)LPF[0])/32767.0f;
					PcmBuf[1][PcmBufPtr]+= ((float)LPF[1])/32767.0f;
				}
*/
		PcmBufPtr++;
	}
}


void CHIP::setDelta(unsigned int Delta){
	SampleTime=Delta;
}

void CHIP::setProgNum(long ProgNum){
	int i;
	CuProgNum=ProgNum&0x0f;
	for(i=0;i<16;i++){
		TblMidiCh[i].VoNum=CuProgNum;
	};
}

int CHIP::getProgNum(void){
		return((int)CuProgNum);
}

void CHIP::setProgName(char *Name){
	strcpy(VoTbl[CuProgNum].Name,Name);
	strcpy(CuProgName,Name);
}

char* CHIP::getProgName(void){
	strcpy(CuProgName,VoTbl[CuProgNum].Name);
	return(CuProgName);
}
char* CHIP::getProgName(int index){
	return(VoTbl[index].Name);
}

void CHIP::setRPNH(int ch,int data){
	TblMidiCh[ch].RPN&=0x007f;
	TblMidiCh[ch].RPN|=(data&0x007f)<<8;
	TblMidiCh[ch].CuRPN=true;

}

void CHIP::setRPNL(int ch,int data){
	TblMidiCh[ch].RPN&=0x7f00;
	TblMidiCh[ch].RPN|=data&0x007f;
	TblMidiCh[ch].CuRPN=true;
}

void CHIP::setNRPNH(int ch,int data){
	TblMidiCh[ch].NRPN&=0x007f;
	TblMidiCh[ch].NRPN|=(data&0x007f)<<8;
	TblMidiCh[ch].CuRPN=false;
}

void CHIP::setNRPNL(int ch,int data){
	TblMidiCh[ch].NRPN&=0x7f00;
	TblMidiCh[ch].NRPN|=data&0x007f;
	TblMidiCh[ch].CuRPN=false;
}

void CHIP::setData(int ch,int data){
	if(TblMidiCh[ch].CuRPN==true){
	//RPN Data Entr
		switch(TblMidiCh[ch].RPN){
			case 0x0000:
				if(data<=0 || data>=96){data=2;}
				TblMidiCh[ch].BendRang=data;
				break;
		}
	}else{
		//NRPN Data Entr
	}
}

/*Tone Calc format*/
/*PitchBend signed 16bit*/
#define N_FP 16
/*bend flot point 6bit*/
#define B_FP 6
/*Mask B_MSK=pow(2,B_FP)-1*/
#define B_MSK 63
void CHIP::CalcPd(int ch){
	int Oct;
	int tmp;
	int key;
	int bend;
	unsigned int Pd;
	unsigned int Note;
	unsigned char MidiCh;
	
	MidiCh=TblCh[ch].MidiCh;
	Note=TblCh[ch].Note;
	if(Note<0x80){
		bend=TblMidiCh[MidiCh].Bend*TblMidiCh[MidiCh].BendRang;
		tmp=((((int)Note+3)<<N_FP)+(bend<<3)+TblCh[ch].PortaBend)>>(N_FP-B_FP);
		key=tmp>>B_FP;
		Oct=key/12;
		key=key%12;
		if(Oct>8){Oct=8;}
		Pd=(unsigned long)TblPow12[key]*(unsigned long)TblPow64[(tmp&B_MSK)];
		//Pd>>=(Oct+12-3);
		Pd>>=(Oct+12-2);
		setReg(OP_FRQ+ch,(Pd&0xfffff));
	}
}


void CHIP::SetSamprate(int sampleRate){
	SampRate=sampleRate;
}

void CHIP::NoteOn(int MidiCh,int Note,int Vel){
	
	int ch,start_ch,end_ch,i,BfNote,PortaDelta;
	class CHDATA *pCh;

	if(PolyMono==true){
		if((start_ch=NonCh(MidiCh,Note,Vel))==-1){return;}
		end_ch=start_ch+1;
	}else{
		MidiCh&=(CHMAX-1);
		if(OmniMode){
			start_ch=0;
			end_ch=CHMAX;
		}else{
			start_ch=MidiCh;
			end_ch=MidiCh+1;
		}
		for(ch=start_ch;ch<end_ch;ch++){
			BfNote=TblCh[ch].Note;
			TblCh[ch].Note=Note;
			TblCh[ch].Vel=Vel;
			TblCh[ch].MidiCh=MidiCh;
			if(OmniMode){
            	TblCh[ch].VoNum=TblMidiCh[ch].VoNum;
			}else{
				TblCh[ch].VoNum=TblMidiCh[MidiCh].VoNum;
			}
		}
	}

#ifdef LOG
	fprintf(Flog,"NoteOn in %x Ch Note=%x Vel=%x Time=%x\n",MidiCh,Note,Vel,SampleTime);
#endif
	if(TblMidiCh[MidiCh].PortaSpeed ){
		if((TblMidiCh[MidiCh].PortaOnOff==true)||
			(PolyMono==false && BfNote<=0x7f))
		{
			for(ch=start_ch;ch<end_ch;ch++){
				PortaDelta=(TblMidiCh[MidiCh].BfNote-Note);
				TblCh[ch].PortaBend=PortaDelta*0x00010000;

				if(PortaDelta<0){PortaDelta*=-1;}
				TblCh[ch].PortaSpeed=(PortaDelta*0x10000)/TblMidiCh[MidiCh].PortaSpeed;
			}
#ifdef LOG
		fprintf(Flog,"PortaBend=%x Speed=%x\n",TblCh[ch].PortaBend,TblCh[ch].PortaSpeed);
#endif
		}else{
			for(ch=start_ch;ch<end_ch;ch++){
				TblCh[ch].PortaBend=0;
				TblCh[ch].PortaSpeed=0;
			}
		}
	}else{
		for(ch=start_ch;ch<end_ch;ch++){
			if(OmniMode && ch!=MidiCh){
				TblCh[ch].PortaBend=(TblMidiCh[ch].Bend*TblMidiCh[ch].BendRang)<<3;
			}else{
				TblCh[ch].PortaBend=0;
			}
			TblCh[ch].PortaSpeed=0;
		}
	}
	if(OmniMode && PolyMono==false){
		for(ch=start_ch;ch<end_ch;ch++){
			setReg(OP_VO+ch,TblMidiCh[ch].VoNum&0x0f);
			setReg(OP_PAN+ch,TblMidiCh[ch].Pan);
            if(ch==MidiCh){
				setReg(OP_VOL+ch,Vel*TblMidiCh[ch].Vol);
            }else{
                setReg(OP_VOL+ch,Vel*((TblMidiCh[ch].Vol*TblMidiCh[MidiCh].Vol)>>7));
            }
			CalcPd(ch);
			if(BfNote==0xff){
				setReg(OP_NOTE+ch,1);//KeyOn
			}
		}
	}else{
		ch=start_ch;
		setReg(OP_VO+ch,TblMidiCh[MidiCh].VoNum&0x0f);
		setReg(OP_PAN+ch,TblMidiCh[MidiCh].Pan);
		setReg(OP_VOL+ch,Vel*TblMidiCh[MidiCh].Vol);
		CalcPd(ch);
		if(PolyMono==false && BfNote!=0xff){
		}else{
			setReg(OP_NOTE+ch,1);//KeyOn
		}
	}
	TblMidiCh[MidiCh].BfNote=Note;
	return;
}

void CHIP::NoteAllOff(int MidiCh){
	NoteOff(MidiCh,0xff);

}

void CHIP::ForceAllOff(void){

	int ch,MidiCh;
	int i;

#ifdef LOG
	fprintf(Flog,"FNoteAllOff in %x Ch Time=%x\n",MidiCh,SampleTime);
#endif

	for(ch=0;ch<CHMAX;ch++){
		TblEG[ch].Cont=0;
		TblEG[ch].state=S_IDLE;
		TblCh[ch].Note=0xff;
		for(i=0;i<32;i++){Lpf[ch][i]=0;}
	}
	for(MidiCh=0;MidiCh<16;MidiCh++){
		while((ch=ChkSonCh(MidiCh,0xff))!=-1){
			DelCh(ch);
		}
	}
}


void CHIP::NoteOff(int MidiCh,int Note){

	int ch,start_ch,end_ch;
	
#ifdef LOG
	fprintf(Flog,"NoteOff in %x Ch Note=%x Time=%x\n",MidiCh,Note,SampleTime);
#endif

	if(PolyMono==false){
		MidiCh&=(CHMAX-1);
		if(OmniMode){
			start_ch=0;
			end_ch=CHMAX;
		}else{
			start_ch=MidiCh;
			end_ch=MidiCh+1;
		}
		for(ch=start_ch;ch<end_ch;ch++){
			if(TblCh[ch].Note==Note){
	//		if(TblEG[ch].state!=S_IDLE){
					setReg(OP_NOTE+ch,0);
	//		}
				TblCh[ch].Note=0xff;
			}
		}
	}else{
		while((ch=ChkSonCh(MidiCh,Note))!=-1){
	//		if(TblEG[ch].state!=S_IDLE){
				setReg(OP_NOTE+ch,0);	
	//		}
			DelCh(ch);
		}
	}
}

void CHIP::BendCng(int MidiCh,int Bend){

	int i;

	TblMidiCh[MidiCh].Bend=Bend-0x2000;
	for(i=0;i<CHMAX;i++){
		if(TblCh[i].MidiCh==MidiCh && TblCh[i].Note!=0xff){
			CalcPd(i);
		}
	}
}

void CHIP::VolCng(int MidiCh,int Vol){

	int i;

	TblMidiCh[MidiCh].Vol=Vol;
	for(i=0;i<CHMAX;i++){
		if(TblCh[i].MidiCh==MidiCh && TblCh[i].Note!=0xff){
			setReg(OP_VOL+i,Vol*TblCh[i].Vel);
		}
	}
}

void CHIP::VoCng(int MidiCh,int Vo){

	int i;

	TblMidiCh[MidiCh].VoNum=Vo;
	strcpy(MidiProg[MidiCh].name,VoTbl[Vo].Name);
	MidiProg[MidiCh].midiProgram=Vo;

	for(i=0;i<CHMAX;i++){
		if(TblCh[i].MidiCh==MidiCh && TblCh[i].Note!=0xff){
			setReg(OP_VO+i,Vo&0x0f);
		}
	}
}

void CHIP::PanCng(int MidiCh,int Pan){

	int F_Pan,i;

	TblMidiCh[MidiCh].Pan=Pan;
	for(i=0;i<CHMAX;i++){
		if(TblCh[i].MidiCh==MidiCh && TblCh[i].Note!=0xff){
			setReg(OP_PAN+i,Pan);
		}
	}
}


int CHIP::ChkEnCh(void){
 if(WaitCh.size()==0){
		return(-1);
 }else{
		return(WaitCh.front());
 }
}

int CHIP::AtachCh(void){

	int NewCh;
	NewCh=ChkEnCh();
	if(NewCh==-1){
		NewCh=PlayCh.front();
		PlayCh.pop_front();
	}else{
		WaitCh.pop_front();
	}
	PlayCh.push_back(NewCh);
	return(NewCh);
}

void CHIP::DelCh(int ch){
	TblCh[ch].MidiCh=0xff;
	TblCh[ch].Note=0xff;
	PlayCh.remove(ch);
	WaitCh.push_back(ch);
}

int CHIP::ChkSonCh(int MidiCh,int Note){
	list<int>::iterator plist;
	if(PlayCh.size()==0){return(-1);}
	plist=PlayCh.begin();
	while(plist!=PlayCh.end()){
		if(MidiCh==TblCh[*plist].MidiCh){
			if(Note==0xff || Note==TblCh[*plist].Note){
				return(*plist);
			}
		}
		plist++;
	}
	return(-1);
}

int CHIP::NonCh(int MidiCh,int Note,int Vel){

	int ch;

	if(ChkSonCh(MidiCh,Note)!=-1){return(-1);}

	ch=AtachCh();	
	TblCh[ch].Note=Note;
	TblCh[ch].Vel=Vel;
	TblCh[ch].MidiCh=MidiCh;
	TblCh[ch].VoNum=TblMidiCh[MidiCh].VoNum;
	return(ch);
}

int CHIP::NoffCh(int MidiCh,int Note){

	int ch;

	if((ch=ChkSonCh(MidiCh,Note))!=-1){
		DelCh(ch);
	}
#ifdef DEBUG
	fprintf(stdout,"NoffSlot:ch=%x Note=%x MidiCh=%x\n",ch,Note,ch);
#endif
	return(ch);
}

unsigned char CHIP::getPrm(int index){
	
	class CHDATA *pVo;
	
	pVo=&VoTbl[CuProgNum];

	switch (index){
		case kAR:return(pVo->AR);
		case kDR:return(pVo->DR);
		case kSL:return(pVo->SL);
		case kRR:return(pVo->RR);
		case kBIT:return(pVo->BIT);
		case kIIR:return(pVo->TAP);
		default:
			if(index>=kWT0 && index<=kWT31){
				return(pVo->WT[index-kWT0]);
			}else{
				return(0);
			}
	}
}

void CHIP::setPrm(int index,unsigned char data){

	class CHDATA *pVo;
	int i;

	pVo=&VoTbl[CuProgNum];
	switch (index){
		case kAR:pVo->AR=data;break;
		case kDR:pVo->DR=data;break;
		case kSL:pVo->SL=data;break;
		case kRR:pVo->RR=data;break;
		case kBIT:pVo->BIT=data;break;
		case kIIR:pVo->TAP=data;break;
		default:
			if(index>=kWT0 && index<=kWT31){
				pVo->WT[index-kWT0]=data;
			}
	}
}

void CHIP::setPoly(bool poly){
	
	int i;
	
	for(i=0;i<CHMAX;i++){
		NoteAllOff(i);
	}
	PolyMono=poly;
}

long CHIP::getMidiProgram(long channel,MidiProgramName *curProg){

	MidiProg[channel].parentCategoryIndex=-1;	
	*curProg=MidiProg[channel];
	return(0);
}

void CHIP::setWTY(int ch,int num){
	setPrm(kWT0+CuWT,num*2);
}

void CHIP::setWTX(int ch,int num){
	CuWT=num/2;
}

void CHIP::setPortaSpeed(int MidiCh,int speed){
	if(speed<0){speed=0;}
	if(speed>127){speed=127;}
	TblMidiCh[MidiCh].PortaSpeed=speed;
}

void CHIP::setPortaOnOff(int MidiCh,bool onoff){
	TblMidiCh[MidiCh].PortaOnOff=onoff;
}

void CHIP::setPortaCtr(int MidiCh,int Note){
	if(Note<0){Note=0;}
	if(Note>127){Note=127;}
	TblMidiCh[MidiCh].BfNote=Note;
}

void CHIP::AutoSeq(int sampleframes){
	int i;
	int Porta;
	int Cnt;
	class CHDATA *pCh;
	
	setDelta(0);
	
	for(i=0;i<CHMAX;i++){
		Porta=TblCh[i].PortaBend;
		if(Porta!=0){
			if(Porta<-1023){
				Porta+=TblCh[i].PortaSpeed;
			}else{
				if(Porta>+1023){
					Porta-=TblCh[i].PortaSpeed;
				}else{
					Porta=0;
				}
			}
			TblCh[i].PortaBend=Porta;
			CalcPd(i);
		}
	}
}
void CHIP::setOmni(bool Omni){
	
	int i;
	
	for(i=0;i<CHMAX;i++){
		NoteAllOff(i);
	}
	OmniMode=Omni;
}
