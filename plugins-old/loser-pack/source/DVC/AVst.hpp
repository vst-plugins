// Copyright (C) 2007, Michael Gruhn.
// License: LGPL - http://www.gnu.org/licenses/lgpl.html

// This uses the VST SDK 2.3. copyright as follows:
//-------------------------------------------------------------------------------------------------------
// VST Plug-Ins SDK
// Example AGain (VST 1.0)
// Stereo plugin which applies a Gain [-oo, 0dB]
// � 2003, Steinberg Media Technologies, All Rights Reserved
//-------------------------------------------------------------------------------------------------------

#ifndef __AVST_H
#define __AVST_H

#include "audioeffectx.h"

//-------------------------------------------------------------------------------------------------------
class AVst : public AudioEffectX
{
public:
	AVst (audioMasterCallback audioMaster);
	~AVst ();

    // Suspend
	virtual void suspend ();

	// Processes
	virtual void process (float **inputs, float **outputs, long sampleFrames);
	virtual void processReplacing (float **inputs, float **outputs, long sampleFrames);

	// Program
	virtual void setProgramName (char *name);
	virtual void getProgramName (char *name);

	// Parameters
	virtual void setParameter (long index, float value);
	virtual float getParameter (long index);
	virtual void getParameterLabel (long index, char *label);
	virtual void getParameterDisplay (long index, char *text);
	virtual void getParameterName (long index, char *text);

	virtual bool getEffectName (char* name);
	virtual bool getVendorString (char* text);
	virtual bool getProductString (char* text);
	virtual long getVendorVersion () { return 1000; }
	
	virtual VstPlugCategory getPlugCategory () { return kPlugCategEffect; }

protected:
	float thresh, threshDB, threshFader;
    float threshLow, threshHigh;
// HOPEFULLY CAN DITCH THOSE AT SOME POINT
    float threshLowDB, threshHighDB;
    
    float ratio, ratioFader;

    float kneeDB, kneeSlopeDB, kneeFader;

    float lookAheadFader;
    unsigned long lookAheadMaxSize, lookAheadSize, lookAheadBufPos;
	float *lookAheadBufferRms, *lookAheadBufferIn1, *lookAheadBufferIn2;

    void FlushLookAheadBuffers ();
    void ReallocLookAheadBuffers ();

    float attack, attackSpls, attackFader;

    float hold, holdFader;

    float release, releaseSpls, releaseFader;

	float rmsFader;
    unsigned long rmsMaxSize, rmsSize, rmsBufPos;
	float *rmsBuffer;
    void FlushRmsBuffer ();
    void ReallocRmsBuffer ();

    float feed;
    
    float modeMakeUp;

    float outVolume, outVolumeFader;
    
    float dryMix, dryMixFader;

// Process:
	float rms, rmsDB, kneeLineDB, maxSpls, rmsSqrSum;
	float gain, seekGain, underthresh, spl0Out, spl1Out, volume;

    unsigned long t;

	char programName[32];
};

#endif
