// Copyright (C) 2007, Michael Gruhn.
// License: LGPL - http://www.gnu.org/licenses/lgpl.html

// This uses the VST SDK 2.3. copyright as follows:
//-------------------------------------------------------------------------------------------------------
// VST Plug-Ins SDK
// Example AGain (VST 1.0)
// Stereo plugin which applies a Gain [-oo, 0dB]
// (C) 2003, Steinberg Media Technologies, All Rights Reserved
//-------------------------------------------------------------------------------------------------------

#include <math.h>
#include <algorithm>
#include <stdlib.h>

#define cAmpDB_ 8.656170245

inline float abs(float x)
{
    return ( x<0 ? -x:x);
}

inline float sign(float x)
{
    return (x>0 ? 1:-1);   
}

inline float min (float x, float y)
{
    return ( x<y ? x:y);
}

inline float max (float x, float y)
{
    return ( x>y ? x:y);
}

inline float absmax (float x, float y)
{
    return max(abs(x),abs(y));
//    float absx = abs(x);
//    float absy = abs(y);
//    return (absx>absy ? absx : absy);
}

#ifndef __AVST_H
#include "AVst.hpp"
#endif

//-------------------------------------------------------------------------------------------------------
AVst::AVst (audioMasterCallback audioMaster)
	: AudioEffectX (audioMaster, 1, 12)	// 1 program, 12 parameters
{

// <sof setting default parameters>

    threshFader = 1;
    threshDB = -.1;
    thresh = threshLow = threshHigh = 0.98851402;
    
    ratioFader = 0;
    ratio = 1;
    
    kneeFader = 0;
    kneeDB = 0;

    lookAheadFader = 0; // no lookAhead
    lookAheadSize = lookAheadMaxSize = 0; // = lookAhead init length [samples]
	lookAheadBufferRms = new float[lookAheadMaxSize];
	lookAheadBufferIn1 = new float[lookAheadMaxSize];
	lookAheadBufferIn2 = new float[lookAheadMaxSize];

    attackFader = 0.376060309; // ~20 ms
    attackSpls = 0.02 * getSampleRate(); // =20 ms
    attack = exp( threshDB / max( attackSpls , 0) / cAmpDB_ );
    
    holdFader = 0;
    hold = 0;
    
    releaseFader = 0.430886938; // ~200ms
    releaseSpls = 0.2 * getSampleRate(); // =200 ms
    release = exp( threshDB / max( releaseSpls , 0) / cAmpDB_  );

    rmsFader = 0; // ~PEAK
    rmsSize = rmsMaxSize = 1; // = RMS init length [samples]
	rmsBuffer = new float[rmsMaxSize];

    feed = 0; // =FF
    modeMakeUp = 1; // =ON

    outVolumeFader = .25; // ~ 0 dB
    outVolume = 1; // = 0 dB

    dryMixFader = 0;
    dryMix = 0;

    volume = 1; // = 0 dB

    // Just for the record the formulas (optimized versions in 'setParameters()' !!!)
    //    threshDB = min( -90+ sqrt(sqrt(threshFader)) * 90 , -.1 );
    //    thresh = exp( threshDB / cAmpDB_ );
    //    threshLow = exp( (threshDB-kneeDB) / cAmpDB_ );
    //    threshHigh = exp( (threshDB+kneeDB) / cAmpDB_ );   		
    //    ratio = 1 - sqrt(ratioFader); 
    //    kneeDB = kneeFader * 24 / 2;
    //    lookAheadSize = 2 * static_cast<unsigned long>(lookAheadFader * 10/1000 * getSampleRate() );
    //    attackSpls = (attackFader*attackFader*attackFader*attackFader) * 1000/1000 * getSampleRate();
    //    attack = exp( threshDB / max( attackSpls , 0) / cAmpDB_ );
    //    hold = (holdFader*holdFader*holdFader*holdFader*holdFader) * 250/1000 * getSampleRate();
    //    releaseSpls = (releaseFader*releaseFader*releaseFader) * 2500/1000 * getSampleRate();
    //    release = exp( threshDB / max( releaseSpls , 0) / cAmpDB_  );
    //    rmsSize = static_cast<int>( rmsFader*rmsFader*rmsFader * getSampleRate() );
    //    outVolume = outVolumeFader * 4;
    //    dryMix = dryMixFader * 2;
    //    volume = outVolume / ( modeMakeUp ? ( exp( (threshDB + (1-threshDB)*ratio) / cAmpDB_ ) : 1 );
    
// <oef setting default parameters>

   

// <sof> setting the VST stuff

	setNumInputs (4);		// stereo in + stereo sidechain (aux)
	setNumOutputs (2);
	setUniqueID (CCONST ('d','v','c','m') );
    canMono ();
    canProcessReplacing ();	// supports both accumulating and replacing output
	strcpy (programName, "Default");	// default program name

// call suspend to flush buffer
	
	suspend(); // flush buffers and reset variables
}

//-------------------------------------------------------------------------------------------------------
AVst::~AVst ()
{
	if (rmsBuffer)
        delete[] rmsBuffer;
	if (lookAheadBufferRms)
        delete[] lookAheadBufferRms;
	if (lookAheadBufferIn1)
        delete[] lookAheadBufferIn1;
	if (lookAheadBufferIn2)
        delete[] lookAheadBufferIn2;
}

void AVst::FlushRmsBuffer ()
{
    memset (rmsBuffer, 0, rmsSize * sizeof(float));
}

void AVst::FlushLookAheadBuffers ()
{
    memset (lookAheadBufferRms, 0, lookAheadSize * sizeof(float));
    memset (lookAheadBufferIn1, 0, lookAheadSize * sizeof(float));
    memset (lookAheadBufferIn2, 0, lookAheadSize * sizeof(float));
}

void AVst::ReallocRmsBuffer ()
{
    rmsBuffer = (float*) realloc(rmsBuffer, rmsMaxSize * sizeof(float));
}

void AVst::ReallocLookAheadBuffers ()
{
    lookAheadBufferRms = (float*) realloc(lookAheadBufferRms, lookAheadMaxSize * sizeof(float));
    lookAheadBufferIn1 = (float*) realloc(lookAheadBufferIn1, lookAheadMaxSize * sizeof(float));
    lookAheadBufferIn2 = (float*) realloc(lookAheadBufferIn2, lookAheadMaxSize * sizeof(float));
}

void AVst::suspend()
{
    gain = seekGain = 1;
    underthresh = rmsSqrSum = rmsBufPos = 0;
    if (rmsMaxSize != rmsSize)
    { 
        rmsMaxSize = rmsSize;
        ReallocRmsBuffer();
    }
    FlushRmsBuffer();

    lookAheadBufPos = 0;
    if (lookAheadMaxSize != lookAheadSize)
    { 
        lookAheadMaxSize = lookAheadSize;
        ReallocLookAheadBuffers();
    }
    FlushLookAheadBuffers();
}

//-------------------------------------------------------------------------------------------------------
void AVst::setProgramName (char *name)
{
	strcpy (programName, name);
}

//-----------------------------------------------------------------------------------------
void AVst::getProgramName (char *name)
{
	strcpy (name, programName);
}

//-----------------------------------------------------------------------------------------
void AVst::setParameter (long index, float value)
{

// Get the values from faders(sliders) and convert and stuff
    switch (index)
	{
		case 0 : 
            threshFader = value;
            threshDB = min( -90+ sqrt(sqrt(threshFader)) * 90 , -.1 );
            thresh = exp( threshDB / cAmpDB_ );
    // SOFTKNEE STUFF
            threshLowDB = threshDB - kneeDB;
            threshHighDB = threshDB + kneeDB;
            kneeSlopeDB = (threshDB + (threshHighDB - threshDB) * ratio - threshLowDB) / (threshHighDB-threshLowDB);
            threshLow = exp( threshLowDB / cAmpDB_ );
            threshHigh = exp( threshHighDB / cAmpDB_ );

            attack = exp( threshDB / attackSpls  / cAmpDB_ );
            release = exp( threshDB / releaseSpls  / cAmpDB_  );        
    // Avoid long fades if thresh to low and thus attack/release too slow...
            if ( threshDB >= -.5 )
                gain = seekGain = 1; 
        break;
        	
        case 1 : 
            ratioFader = value;
            ratio = 1 - sqrt(ratioFader);
        break;

        case 2 : 
            kneeFader = value;
            kneeDB = kneeFader * 12; // * 'max softknee db' / 2 !!!
    // SOFTKNEE STUFF
            threshLowDB = threshDB - kneeDB;
            threshHighDB = threshDB + kneeDB;
            kneeSlopeDB = (threshDB + (threshHighDB - threshDB) * ratio - threshLowDB) / (threshHighDB-threshLowDB);
            threshLow = exp( threshLowDB / cAmpDB_ );
            threshHigh = exp( threshHighDB / cAmpDB_ );
        break;

		case 3 :
            lookAheadFader = value;
            lookAheadSize = static_cast<unsigned long>(lookAheadFader*lookAheadFader*lookAheadFader * 0.25 * getSampleRate() );
            if (lookAheadSize > lookAheadMaxSize)
            {
                lookAheadMaxSize = static_cast<unsigned long>( 0.25 * getSampleRate() ); // set 'lookAheadMaxSize' to max possible size !!!
                ReallocLookAheadBuffers();
            }
            FlushLookAheadBuffers();
            lookAheadBufPos = 0;
            setInitialDelay(lookAheadSize);
        break;

		case 4 :
            attackFader = value;
            attackSpls = (attackFader*attackFader*attackFader*attackFader) * getSampleRate();
            attack = exp( threshDB / attackSpls / cAmpDB_ );
        break;

		case 5 : 
            holdFader = value;
            hold = (holdFader*holdFader*holdFader*holdFader*holdFader) * .25 * getSampleRate();
        break;

		case 6 :
            releaseFader = value;
            releaseSpls = (releaseFader*releaseFader*releaseFader) * 2.5 * getSampleRate();
            release = exp( threshDB / releaseSpls / cAmpDB_  );        
        break;

        case 7 :
            rmsFader = value;
            rmsSize = static_cast<int>( rmsFader*rmsFader*rmsFader * getSampleRate() );
            if (rmsSize > rmsMaxSize)
            {
                rmsMaxSize = static_cast<unsigned long>( getSampleRate() ); // set 'rmsMaxSize' to max possible size !!!
                ReallocRmsBuffer();
            }
            FlushRmsBuffer();
            rmsSqrSum = rmsBufPos = 0;
        break;

        case 8 :
            feed = floor(value*2);
        break;

        case 9 :
            modeMakeUp = ceil(value);
        break;

		case 10 :
            outVolumeFader = value;
            outVolume = outVolumeFader * 4;
        break;

		case 11 :
            dryMixFader = value;
            dryMix = dryMixFader * 2;
        break;
        
// Debug Fader
//        case 12 : break;
        
	}


// MAKING THE AUTO-MAKE-UP TRANSITION BETWEEN RMS AND PEAK SMOOOOTHER ...
    volume = outVolume / (modeMakeUp ? (  exp( (threshDB -threshDB*ratio)/cAmpDB_ ) * (  1 + min( rmsFader*2 , 1 ) * ( rmsSize>1 ) )  ): 1 ) ;
    // The '1 + min( blah blah) * ( blah)' is just random made up shit, suits good, tho

// Set state of no 'underthresh' to avoid stuck compression when not retriggered (i.e. no signal over thresh)
    underthresh = 0;

}

//-----------------------------------------------------------------------------------------
float AVst::getParameter (long index)
{
// use the SDK's v variable and return the xxxFader values, since me is lazy and don't want to convert back, hehe
    float v = 0;
	switch (index)
	{
		case 0 : v = threshFader;      break;
		case 1 : v = ratioFader;      break;
		case 2 : v = kneeFader;       break;
		case 3 : v = lookAheadFader;  break;
		case 4 : v = attackFader;     break;
		case 5 : v = holdFader;       break;
		case 6 : v = releaseFader;    break;
		case 7 : v = rmsFader;        break;
		case 8 : v = feed / 2;        break;
		case 9 : v = modeMakeUp;      break;
		case 10 : v = outVolumeFader;  break;
		case 11 : v = dryMixFader;    break;
// Debug Fader		
//		case 12 : v = rmsMaxSize;  break;
	}
    return v;
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterName (long index, char *label)
{
    switch (index)
	{
		case 0 : strcpy (label, "Threshold");  break;
		case 1 : strcpy (label, "Ratio");     break;
		case 2 : strcpy (label, "Knee");      break;
		case 3 : strcpy (label, "Look Ahead");    break;
		case 4 : strcpy (label, "Attack");    break;
		case 5 : strcpy (label, "Hold");      break;
		case 6 : strcpy (label, "Release");   break;
		case 7 : strcpy (label, "RMS Size");  break;
		case 8 : strcpy (label, "Feed");      break;
		case 9 : strcpy (label, "Make-Up");   break;
		case 10 : strcpy (label, "Output");    break;
		case 11 : strcpy (label, "Dry Mix");  break;
// Debug Fader		
//		case 12 : strcpy (label, "RMS MAX SIZE");    break;
	}
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterDisplay (long index, char *text)
{
	switch (index)
	{
		case 0 : float2string (threshDB, text);        break;
		case 1 : float2string ( 1 / ratio , text);    break;
		case 2 : float2string ( kneeDB * 2 , text);   break;
		case 3 : ms2string (lookAheadSize, text);     break;
		case 4 : ms2string (attackSpls, text);        break;
		case 5 : ms2string (hold, text);              break;
		case 6 : ms2string (releaseSpls, text);       break;
		case 7 :
            if (rmsSize <= 1) strcpy (text, "PEAK");
            else ms2string (rmsSize, text);
        break;
		case 8 : 
                 if (feed == 0) {strcpy (text, "FF");}
            else if (feed == 1) {strcpy (text, "FBQ");}
            else if (feed == 2) {strcpy (text, "SIDE");}
        break;
		case 9 :
                 if (modeMakeUp == 0) {strcpy (text, "OFF");}
            else if (modeMakeUp == 1) {strcpy (text, "ON");}
        break;
		case 10 : float2string (log(outVolume)*cAmpDB_, text);         break;
		case 11 : float2string (log(dryMix)*cAmpDB_, text);         break;
// Debug Fader, hehe
//		case 12 : float2string (rmsMaxSize, text);         break;
	}

}

//-----------------------------------------------------------------------------------------
void AVst::getParameterLabel(long index, char *label)
{
	switch (index)
	{
		case 0 : strcpy (label, "dB");	    break;
		case 1 : strcpy (label, ":1");	    break;
		case 2 : strcpy (label, "dB");	    break;
		case 3 : strcpy (label, "ms");	    break;
		case 4 : strcpy (label, "ms");	    break;
		case 5 : strcpy (label, "ms");	    break;
		case 6 : strcpy (label, "ms");	    break;
		case 7 : strcpy (label, "ms");      break;
		case 8 : strcpy (label, "Mode");    break;
		case 9 : strcpy (label, "Mode");    break;
		case 10 : strcpy (label, "dB");	    break;
		case 11 : strcpy (label, "dB");	    break;
// Debug Fader		
//		case 12 : strcpy (label, "Debug");	    break;
	}
}

//------------------------------------------------------------------------
bool AVst::getEffectName (char* name)
{
	strcpy (name, "Digital Versatile Compressor");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getProductString (char* text)
{
	strcpy (text, "Digital Versatile Compressor");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getVendorString (char* text)
{
	strcpy (text, "LOSER-Development");
	return true;
}

//-----------------------------------------------------------------------------------------
void AVst::process (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *in3  =  inputs[2];
    float *in4  =  inputs[3];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
    // set 'maxSpls' due to the feed
// IMPLEMENT LOW/HIGHPASS FOR DETECTOR HERE!!!
             if (feed == 0) maxSpls = absmax(    *in1 , *in2    ); // FF
        else if (feed == 1) maxSpls = absmax( spl0Out , spl1Out ); // FBQ
        else if (feed == 2) maxSpls = absmax(  *in3++ , *in4++  ); // SIDE

    // RMS via running sum (or not if PEAK)
    if (rmsSize <= 1) rms = maxSpls;
    else
    {
        rmsSqrSum = max( rmsSqrSum-rmsBuffer[rmsBufPos] , 0 ) + ( rmsBuffer[rmsBufPos] = (maxSpls*maxSpls) );
        if ( ++rmsBufPos >= rmsSize ) rmsBufPos = 0;
        rms = sqrt( rmsSqrSum / rmsSize );
    }
    
    // Do lookAhead Thingy
    if (lookAheadSize > 1)
    {
        lookAheadBufferIn1[lookAheadBufPos] = (*in1);
        lookAheadBufferIn2[lookAheadBufPos] = (*in2);
        lookAheadBufferRms[lookAheadBufPos] = rms;
        if ( ++lookAheadBufPos >= lookAheadSize ) lookAheadBufPos = 0;
        (*in1) = lookAheadBufferIn1[lookAheadBufPos];
        (*in2) = lookAheadBufferIn2[lookAheadBufPos];
        t = 0;
// MAKE THIS MUCH MO FASTER DIPSHIT
// ARE YOU BRAIN DEAD? I SAID MO FASTER!
        for (unsigned long i = 0; i < lookAheadSize; i++)
        {
            t = (lookAheadBufferRms[i] > lookAheadBufferRms[t]) ? i:t;
        }
        rms = lookAheadBufferRms[t];
// Hmmm.. okay maybe sort it or somethin' ... 
// sort it and make an extra array (- huge - sucks badass)

// Faster but INACCURATE Version
// rms = max(lookAheadBufferRms[lookAheadBufPos],rms);

    }
    
    // Detect and set new seekGain (VERY CONFUSING CODE HERE)
        if (rms > threshLow)
        {
            underthresh = 0;
            if (rms >= threshHigh)
            {
                if (hold) seekGain = min( thresh * pow(rms/thresh,ratio) / rms , seekGain );
                else seekGain =  thresh * pow(rms/thresh,ratio ) / rms ;
            }
            else
            {
// DO THE NEW SOFTKNEE CODE !!!! HERE BITCH !!!! BTW, MAKE IT BETTER THAN THE OLD ONE !!!!
// BUT NO MATTER WHAT, MAKE IT ROUND !!!!

// old two edges
//                    if (hold) seekGain = min( exp( (threshLowDB + ( (log(rms)*cAmpDB_) - threshLowDB ) * kneeSlopeDB) /cAmpDB_) / rms , seekGain );
//                    else  seekGain = exp( (threshLowDB + ( (log(rms)*cAmpDB_) - threshLowDB ) * kneeSlopeDB) /cAmpDB_) / rms;

// new round
//                    if (hold) seekGain = min( exp( (threshLowDB + ( (log(rms)*cAmpDB_) - threshLowDB ) * kneeSlopeDB) /cAmpDB_) / rms , seekGain );
//                    else  
rmsDB = log(rms)*cAmpDB_;
kneeLineDB = threshLowDB + ( rmsDB - threshLowDB ) * kneeSlopeDB;
if (hold) seekGain = min(exp( (kneeLineDB+ ( threshDB + (threshDB-rmsDB) * ratio  -  kneeLineDB) *(0.5+0.5*abs((rmsDB - threshDB) / kneeDB  )) )  /cAmpDB_) / rms , seekGain);
else seekGain = exp( (kneeLineDB+ ( threshDB + (threshDB-rmsDB) * ratio  -  kneeLineDB) * (0.5+0.5*abs((rmsDB - threshDB) / kneeDB  )) )  /cAmpDB_) / rms;

            }
        }
        else if ( ++underthresh > hold) seekGain = 1;


// Make 'gain' follow seekGain with the specified sttack and release speed
        if (gain > seekGain) gain = max( gain*attack , seekGain );
        else gain = min( gain/release , seekGain );

// Output
        spl0Out = (*in1) * gain;
        spl1Out = (*in2) * gain;

// COPY FROM REPLACING !!!!

// Applying output set feedback, etc...          ACCUMULATING!!!!!  
        (*out1++) += spl0Out * volume  + (*in1++) * dryMix;
        (*out2++) += spl1Out * volume  + (*in2++) * dryMix;


    }
       
}

//-----------------------------------------------------------------------------------------
void AVst::processReplacing (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *in3  =  inputs[2];
    float *in4  =  inputs[3];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
    // set 'maxSpls' due to the feed
// IMPLEMENT LOW/HIGHPASS FOR DETECTOR HERE!!!
             if (feed == 0) maxSpls = absmax(    *in1 , *in2    ); // FF
        else if (feed == 1) maxSpls = absmax( spl0Out , spl1Out ); // FBQ
        else if (feed == 2) maxSpls = absmax(  *in3++ , *in4++  ); // SIDE

    // RMS via running sum (or not if PEAK)
    if (rmsSize <= 1) rms = maxSpls;
    else
    {
        rmsSqrSum = max( rmsSqrSum-rmsBuffer[rmsBufPos] , 0 ) + ( rmsBuffer[rmsBufPos] = (maxSpls*maxSpls) );
        if ( ++rmsBufPos >= rmsSize ) rmsBufPos = 0;
        rms = sqrt( rmsSqrSum / rmsSize );
    }
    
    // Do lookAhead Thingy
    if (lookAheadSize > 1)
    {
        lookAheadBufferIn1[lookAheadBufPos] = (*in1);
        lookAheadBufferIn2[lookAheadBufPos] = (*in2);
        lookAheadBufferRms[lookAheadBufPos] = rms;
        if ( ++lookAheadBufPos >= lookAheadSize ) lookAheadBufPos = 0;
        (*in1) = lookAheadBufferIn1[lookAheadBufPos];
        (*in2) = lookAheadBufferIn2[lookAheadBufPos];
        t = 0;
// MAKE THIS MUCH MO FASTER DIPSHIT
// ARE YOU BRAIN DEAD? I SAID MO FASTER!
        for (unsigned long i = 0; i < lookAheadSize; i++)
        {
            t = (lookAheadBufferRms[i] > lookAheadBufferRms[t]) ? i:t;
        }
        rms = lookAheadBufferRms[t];
// Hmmm.. okay maybe sort it or somethin' ... 
// sort it and make an extra array (- huge - sucks badass)

// Faster but INACCURATE Version
// rms = max(lookAheadBufferRms[lookAheadBufPos],rms);

    }
    
    // Detect and set new seekGain (VERY CONFUSING CODE HERE)
        if (rms > threshLow)
        {
            underthresh = 0;
            if (rms >= threshHigh)
            {
                if (hold) seekGain = min( thresh * pow(rms/thresh,ratio) / rms , seekGain );
                else seekGain =  thresh * pow(rms/thresh,ratio ) / rms ;
            }
            else
            {
// DO THE NEW SOFTKNEE CODE !!!! HERE BITCH !!!! BTW, MAKE IT BETTER THAN THE OLD ONE !!!!
// BUT NO MATTER WHAT, MAKE IT ROUND !!!!

// old two edges
//                    if (hold) seekGain = min( exp( (threshLowDB + ( (log(rms)*cAmpDB_) - threshLowDB ) * kneeSlopeDB) /cAmpDB_) / rms , seekGain );
//                    else  seekGain = exp( (threshLowDB + ( (log(rms)*cAmpDB_) - threshLowDB ) * kneeSlopeDB) /cAmpDB_) / rms;

// new round
//                    if (hold) seekGain = min( exp( (threshLowDB + ( (log(rms)*cAmpDB_) - threshLowDB ) * kneeSlopeDB) /cAmpDB_) / rms , seekGain );
//                    else  
rmsDB = log(rms)*cAmpDB_;
kneeLineDB = threshLowDB + ( rmsDB - threshLowDB ) * kneeSlopeDB;
if (hold) seekGain = min(exp( (kneeLineDB+ ( threshDB + (threshDB-rmsDB) * ratio  -  kneeLineDB) *(0.5+0.5*abs((rmsDB - threshDB) / kneeDB  )) )  /cAmpDB_) / rms , seekGain);
else seekGain = exp( (kneeLineDB+ ( threshDB + (threshDB-rmsDB) * ratio  -  kneeLineDB) * (0.5+0.5*abs((rmsDB - threshDB) / kneeDB  )) )  /cAmpDB_) / rms;

            }
        }
        else if ( ++underthresh > hold) seekGain = 1;


// Make 'gain' follow seekGain with the specified sttack and release speed
        if (gain > seekGain) gain = max( gain*attack , seekGain );
        else gain = min( gain/release , seekGain );

// Output
        spl0Out = (*in1) * gain;
        spl1Out = (*in2) * gain;

// Applying output, set feedback, etc...          REPLACING!!!!!
           
        (*out1++) = spl0Out * volume + (*in1++) * dryMix;
        (*out2++) = spl1Out * volume + (*in2++) * dryMix;

    }

}
