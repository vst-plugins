#include <AudioEffect.cpp>
#include <audioeffectx.h>
#include <audioeffectx.cpp>

#include "AVst.hpp"



#ifdef __GNUC__ 
	AEffect* main_plugin(audioMasterCallback audioMaster) asm ("main");
	#define main main_plugin
#else
	AEffect *main(audioMasterCallback audioMaster);
#endif



AEffect *main(audioMasterCallback audioMaster)
{ 
	AVst* effect = new AVst(audioMaster);
	if (!effect) return 0;
	return effect->getAeffect();
}

