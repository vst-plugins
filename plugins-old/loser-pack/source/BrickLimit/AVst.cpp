// Copyright (C) 2007, Michael Gruhn.
// License: LGPL - http://www.gnu.org/licenses/lgpl.html

// This uses the VST SDK 2.3. copyright as follows:
//-------------------------------------------------------------------------------------------------------
// VST Plug-Ins SDK
// Example AGain (VST 1.0)
// Stereo plugin which applies a Gain [-oo, 0dB]
// (C) 2003, Steinberg Media Technologies, All Rights Reserved
//-------------------------------------------------------------------------------------------------------

#include <math.h>
#define  cAmpDB_     8.656170245 

#ifndef __AVST_H
#include "AVst.hpp"
#endif

//-------------------------------------------------------------------------------------------------------
AVst::AVst (audioMasterCallback audioMaster)
	: AudioEffectX (audioMaster, 1, 1)	// 1 program, 1 parameter only
{
	limit = 1;				// default to 0 dB
	setNumInputs (2);		// stereo in
	setNumOutputs (2);		// stereo out
	setUniqueID ((long)"brlm");	// identify
	canMono ();				// makes sense to feed both inputs with the same signal
	canProcessReplacing ();	// supports both accumulating and replacing output
	strcpy (programName, "Default");	// default program name
}

//-------------------------------------------------------------------------------------------------------
AVst::~AVst ()
{
	// nothing to do here
}

//-------------------------------------------------------------------------------------------------------
void AVst::setProgramName (char *name)
{
	strcpy (programName, name);
}

//-----------------------------------------------------------------------------------------
void AVst::getProgramName (char *name)
{
	strcpy (name, programName);
}

//-----------------------------------------------------------------------------------------
void AVst::setParameter (long index, float value)
{
    limit = value;    
}

//-----------------------------------------------------------------------------------------
float AVst::getParameter (long index)
{
	return limit;
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterName (long index, char *label)
{
	strcpy (label, "Treshold");
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterDisplay (long index, char *text)
{
	float2string (log(limit)*cAmpDB_, text);
}


//-----------------------------------------------------------------------------------------
void AVst::getParameterLabel(long index, char *label)
{
	strcpy (label, "dB");
}

//------------------------------------------------------------------------
bool AVst::getEffectName (char* name)
{
	strcpy (name, "Destructive Clipping Brickwall Limiter");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getProductString (char* text)
{
	strcpy (text, "Destructive Clipping Brickwall Limiter");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getVendorString (char* text)
{
	strcpy (text, "LOSER-Development");
	return true;
}

//-----------------------------------------------------------------------------------------
void AVst::process (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
    if ((*in1) >  limit) (*in1) =  limit;
    if ((*in1) < -limit) (*in1) = -limit;
    if ((*in2) >  limit) (*in2) =  limit;
    if ((*in2) < -limit) (*in2) = -limit;

        (*out1++) += (*in1++);    // accumulating
        (*out2++) += (*in2++);
    }
}

//-----------------------------------------------------------------------------------------
void AVst::processReplacing (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
    if ((*in1) >  limit) (*in1) =  limit;
    if ((*in1) < -limit) (*in1) = -limit;
    if ((*in2) >  limit) (*in2) =  limit;
    if ((*in2) < -limit) (*in2) = -limit;

        (*out1++) = (*in1++);    // replacing
        (*out2++) = (*in2++);
    }
}
