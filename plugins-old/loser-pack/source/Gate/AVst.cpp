// Copyright (C) 2007, Michael Gruhn.
// License: LGPL - http://www.gnu.org/licenses/lgpl.html

// This uses the VST SDK 2.3. copyright as follows:
//-------------------------------------------------------------------------------------------------------
// VST Plug-Ins SDK
// Example AGain (VST 1.0)
// Stereo plugin which applies a Gain [-oo, 0dB]
// (C) 2003, Steinberg Media Technologies, All Rights Reserved
//-------------------------------------------------------------------------------------------------------

#include <math.h>
#include <algorithm>
#include <stdlib.h>

#define cAmpDB_ 8.656170245
#define cDcAdd_ 10e-30

inline float abs(float x)
{
    return ( x<0 ? -x:x);
}

inline float sign(float x)
{
    return (x>0 ? 1:-1);   
}

inline float min (float x, float y)
{
    return ( x<y ? x:y);
}

inline float max (float x, float y)
{
    return ( x>y ? x:y);
}

inline float absmax (float x, float y)
{
    return max(abs(x),abs(y));
//    float absx = abs(x);
//    float absy = abs(y);
//    return (absx>absy ? absx : absy);
}

#ifndef __AVST_H
#include "AVst.hpp"
#endif

//-------------------------------------------------------------------------------------------------------
AVst::AVst (audioMasterCallback audioMaster)
	: AudioEffectX (audioMaster, 1, 9)	// 1 program, 10 parameters
{

// <sof setting default parameters>

    threshOpenFader = 0.604938271;
    threshOpen = 0.099212565;
  
    lookAheadFader = 0; // no lookAhead
    lookAheadSize = lookAheadMaxSize = 0; // = lookAhead init length [samples]
	lookAheadBufferRms = new float[lookAheadMaxSize];
	lookAheadBufferIn1 = new float[lookAheadMaxSize];
	lookAheadBufferIn2 = new float[lookAheadMaxSize];

    attackFader = 0.376060309; // ~20 ms
    attackSpls = 0.02 * getSampleRate(); // =20 ms
    attack = exp( -60 / max( attackSpls , 0) / cAmpDB_ );
    
    holdFader = 0.464158883;
    hold = 0.25 * getSampleRate();
    
    releaseFader = 0.430886938; // ~200ms
    releaseSpls = 0.2 * getSampleRate(); // =200 ms
    release = exp( -60 / max( releaseSpls , 0) / cAmpDB_  );

    rmsFader = 0; // ~PEAK
    rmsSize = rmsMaxSize = 1; // = RMS init length [samples]
	rmsBuffer = new float[rmsMaxSize];

    feed = 0; // =FF

    outVolumeFader = 1; // ~ 0 dB
    outVolume = 1; // = 0 dB

    dryMixFader = 0;
    dryMix = 0;

    // Just for the record the formulas (optimized versions in 'setParameters()' !!!)
    //    threshDB = min( -90+ sqrt(sqrt(threshFader)) * 90 , -.1 );
    //    thresh = exp( threshDB / cAmpDB_ );
    //    threshLow = exp( (threshDB-kneeDB) / cAmpDB_ );
    //    threshHigh = exp( (threshDB+kneeDB) / cAmpDB_ );   		
    //    ratio = 1 - sqrt(ratioFader); 
    //    kneeDB = kneeFader * 24 / 2;
    //    lookAheadSize = 2 * static_cast<unsigned long>(lookAheadFader * 10/1000 * getSampleRate() );
    //    attackSpls = (attackFader*attackFader*attackFader*attackFader) * 1000/1000 * getSampleRate();
    //    attack = exp( threshDB / max( attackSpls , 0) / cAmpDB_ );
    //    hold = (holdFader*holdFader*holdFader*holdFader*holdFader) * 250/1000 * getSampleRate();
    //    releaseSpls = (releaseFader*releaseFader*releaseFader) * 2500/1000 * getSampleRate();
    //    release = exp( threshDB / max( releaseSpls , 0) / cAmpDB_  );
    //    rmsSize = static_cast<int>( rmsFader*rmsFader*rmsFader * getSampleRate() );
    //    outVolume = outVolumeFader * 4;
    //    dryMix = dryMixFader * 2;
    //    volume = outVolume / ( modeMakeUp ? ( exp( (threshDB + (1-threshDB)*ratio) / cAmpDB_ ) : 1 );
    
// <oef setting default parameters>

   

// <sof> setting the VST stuff

	setNumInputs (4);		// stereo in + stereo sidechain (aux)
	setNumOutputs (2);
	setUniqueID ((long)"gate");
    canMono ();
    canProcessReplacing ();	// supports both accumulating and replacing output
	strcpy (programName, "Default");	// default program name

// call suspend to flush buffer
	
	suspend(); // flush buffers and reset variables
}

//-------------------------------------------------------------------------------------------------------
AVst::~AVst ()
{
	if (rmsBuffer)
        delete[] rmsBuffer;
	if (lookAheadBufferRms)
        delete[] lookAheadBufferRms;
	if (lookAheadBufferIn1)
        delete[] lookAheadBufferIn1;
	if (lookAheadBufferIn2)
        delete[] lookAheadBufferIn2;
}

void AVst::FlushRmsBuffer ()
{
    memset (rmsBuffer, 0, rmsSize * sizeof(float));
}

void AVst::FlushLookAheadBuffers ()
{
    memset (lookAheadBufferRms, 0, lookAheadSize * sizeof(float));
    memset (lookAheadBufferIn1, 0, lookAheadSize * sizeof(float));
    memset (lookAheadBufferIn2, 0, lookAheadSize * sizeof(float));
}

void AVst::ReallocRmsBuffer ()
{
    rmsBuffer = (float*) realloc(rmsBuffer, rmsMaxSize * sizeof(float));
}

void AVst::ReallocLookAheadBuffers ()
{
    lookAheadBufferRms = (float*) realloc(lookAheadBufferRms, lookAheadMaxSize * sizeof(float));
    lookAheadBufferIn1 = (float*) realloc(lookAheadBufferIn1, lookAheadMaxSize * sizeof(float));
    lookAheadBufferIn2 = (float*) realloc(lookAheadBufferIn2, lookAheadMaxSize * sizeof(float));
}

void AVst::suspend()
{
    gain = seekGain = 1;
    underthresh = rmsSqrSum = rmsBufPos = 0;
    if (rmsMaxSize != rmsSize)
    { 
        rmsMaxSize = rmsSize;
        ReallocRmsBuffer();
    }
    FlushRmsBuffer();

    lookAheadBufPos = 0;
    if (lookAheadMaxSize != lookAheadSize)
    { 
        lookAheadMaxSize = lookAheadSize;
        ReallocLookAheadBuffers();
    }
    FlushLookAheadBuffers();
}

//-------------------------------------------------------------------------------------------------------
void AVst::setProgramName (char *name)
{
	strcpy (programName, name);
}

//-----------------------------------------------------------------------------------------
void AVst::getProgramName (char *name)
{
	strcpy (name, programName);
}

//-----------------------------------------------------------------------------------------
void AVst::setParameter (long index, float value)
{

// Get the values from faders(sliders) and convert and stuff
    switch (index)
	{
		case 0 : 
            threshOpenFader = value;
            threshOpen = exp( (-90+ sqrt(threshOpenFader) * 90) / cAmpDB_ );
     
        break;
        	
		case 1 :
            lookAheadFader = value;
            lookAheadSize = static_cast<unsigned long>(lookAheadFader * 0.25 * getSampleRate() );
            if (lookAheadSize > lookAheadMaxSize)
            {
                lookAheadMaxSize = static_cast<unsigned long>( 0.25 * getSampleRate() ); // set 'lookAheadMaxSize' to max possible size !!!
                ReallocLookAheadBuffers();
            }
            FlushLookAheadBuffers();
            lookAheadBufPos = 0;
            setInitialDelay(lookAheadSize);
        break;

		case 2 :
            attackFader = value;
            attackSpls = (attackFader*attackFader*attackFader*attackFader) * getSampleRate();
            attack = exp( -60 / attackSpls  / cAmpDB_);
        break;

		case 3 : 
            holdFader = value;
            hold = (holdFader*holdFader*holdFader) * 2.5 * getSampleRate();
        break;

		case 4 :
            releaseFader = value;
            releaseSpls = (releaseFader*releaseFader*releaseFader) * 2.5 * getSampleRate();
            release = exp( -60 / releaseSpls / cAmpDB_);        
        break;

        case 5 :
            rmsFader = value;
            rmsSize = static_cast<int>( rmsFader*rmsFader*rmsFader * getSampleRate() );
            if (rmsSize > rmsMaxSize)
            {
                rmsMaxSize = static_cast<unsigned long>( getSampleRate() ); // set 'rmsMaxSize' to max possible size !!!
                ReallocRmsBuffer();
            }
            FlushRmsBuffer();
            rmsSqrSum = rmsBufPos = 0;
        break;

        case 6 :
            feed = floor(value);
        break;

		case 7 :
            outVolumeFader = value;
            outVolume = outVolumeFader;
        break;

		case 8 :
            dryMixFader = value;
            dryMix = dryMixFader;
        break;
        
// Debug Fader
//        case 9 : break;
        
	}


// Set state of no 'underthresh' to avoid stuck compression when not retriggered (i.e. no signal over thresh)
    underthresh = 0;

    gain = seekGain = 1;
}

//-----------------------------------------------------------------------------------------
float AVst::getParameter (long index)
{
// use the SDK's v variable and return the xxxFader values, since me is lazy and don't want to convert back, hehe
    float v = 0;
	switch (index)
	{
		case 0 : v = threshOpenFader;      break;
		case 1 : v = lookAheadFader;  break;
		case 2 : v = attackFader;     break;
		case 3 : v = holdFader;       break;
		case 4 : v = releaseFader;    break;
		case 5 : v = rmsFader;        break;
		case 6 : v = feed;        break;
		case 7 : v = outVolumeFader;  break;
		case 8 : v = dryMixFader;    break;
// Debug Fader		
//		case 9 : v = rmsMaxSize;  break;
	}
    return v;
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterName (long index, char *label)
{
    switch (index)
	{
		case 0 : strcpy (label, "Threshold (Open)");  break;
		case 1 : strcpy (label, "Look Ahead");    break;
		case 2 : strcpy (label, "Attack");    break;
		case 3 : strcpy (label, "Hold");      break;
		case 4 : strcpy (label, "Release");   break;
		case 5 : strcpy (label, "RMS Size");  break;
		case 6 : strcpy (label, "Feed");      break;
		case 7 : strcpy (label, "Output");    break;
		case 8 : strcpy (label, "Dry Mix");  break;
// Debug Fader		
//		case 9 : strcpy (label, "RMS MAX SIZE");    break;
	}
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterDisplay (long index, char *text)
{
	switch (index)
	{
		case 0 : float2string (log(threshOpen)*cAmpDB_, text);        break;
		case 1 : ms2string (lookAheadSize, text);     break;
		case 2 : ms2string (attackSpls, text);        break;
		case 3 : ms2string (hold, text);              break;
		case 4 : ms2string (releaseSpls, text);       break;
		case 5 :
            if (rmsSize <= 1) strcpy (text, "PEAK");
            else ms2string (rmsSize, text);
        break;
		case 6 : 
                 if (feed == 0) {strcpy (text, "FF");}
            else if (feed == 1) {strcpy (text, "SIDE");}
        break;
		case 7 : float2string (log(outVolume)*cAmpDB_, text);         break;
		case 8 : float2string (log(dryMix)*cAmpDB_, text);         break;
// Debug Fader, hehe
//		case 9 : float2string (rmsMaxSize, text);         break;
	}

}

//-----------------------------------------------------------------------------------------
void AVst::getParameterLabel(long index, char *label)
{
	switch (index)
	{
		case 0 : strcpy (label, "dB");	    break;
		case 1 : strcpy (label, "ms");	    break;
		case 2 : strcpy (label, "ms");	    break;
		case 3 : strcpy (label, "ms");	    break;
		case 4 : strcpy (label, "ms");	    break;
		case 5 : strcpy (label, "ms");      break;
		case 6 : strcpy (label, "Mode");    break;
		case 7 : strcpy (label, "dB");	    break;
		case 8 : strcpy (label, "dB");	    break;
// Debug Fader		
//		case 9 : strcpy (label, "Debug");	    break;
	}
}

//------------------------------------------------------------------------
bool AVst::getEffectName (char* name)
{
	strcpy (name, "Gate");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getProductString (char* text)
{
	strcpy (text, "Gate");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getVendorString (char* text)
{
	strcpy (text, "LOSER-Development");
	return true;
}

//-----------------------------------------------------------------------------------------
void AVst::process (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *in3  =  inputs[2];
    float *in4  =  inputs[3];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
    // set 'maxSpls' due to the feed
// IMPLEMENT LOW/HIGHPASS FOR DETECTOR HERE!
             if (feed == 0) maxSpls = absmax(    *in1 , *in2    ); // FF
        else if (feed == 1) maxSpls = absmax(  *in3++ , *in4++  ); // SIDE

    // RMS via running sum (or not if PEAK)
    if (rmsSize <= 1) rms = maxSpls;
    else
    {
        rmsSqrSum = max( rmsSqrSum-rmsBuffer[rmsBufPos] , 0 ) + ( rmsBuffer[rmsBufPos] = (maxSpls*maxSpls) );
        if ( ++rmsBufPos >= rmsSize ) rmsBufPos = 0;
        rms = sqrt( rmsSqrSum / rmsSize );
    }
    
    // Do lookAhead Thingy
    if (lookAheadSize > 1)
    {
        lookAheadBufferIn1[lookAheadBufPos] = (*in1);
        lookAheadBufferIn2[lookAheadBufPos] = (*in2);
        lookAheadBufferRms[lookAheadBufPos] = rms;
        if ( ++lookAheadBufPos >= lookAheadSize ) lookAheadBufPos = 0;
        (*in1) = lookAheadBufferIn1[lookAheadBufPos];
        (*in2) = lookAheadBufferIn2[lookAheadBufPos];
//        t = 0;
// MAKE THIS MUCH MO FASTER DIPSHIT
// ARE YOU BRAIN DEAD? I SAID MO FASTER!
//        for (int i = 0; i < lookAheadSize; i++)
//        {
//            t = (lookAheadBufferRms[i] > lookAheadBufferRms[t]) ? i:t;
//        }
//        rms = lookAheadBufferRms[t];
// Hmmm.. okay maybe sort it or somethin' ... (cpu? fast? memory? need to figure ever considered that?)
// 2. sort it and make extra array (- huge - sucks badass)
//no // 3. get intel shit ( - cost  - propritarien rights? intel machine only?)

rms = max(lookAheadBufferRms[lookAheadBufPos],rms);

    }
    
    // Detect and set new seekGain (VERY CONFUSING CODE HERE)
        if (rms > threshOpen)
        {
            underthresh = 0;
            seekGain =  1; // + cDcAdd_ but it'd get truncated anyway so leave it out
        }
        else if ( ++underthresh > hold) seekGain = cDcAdd_;

// Make 'gain' follow seekGain with the specified sttack and release speed
// invert release / attack
        if (gain > seekGain) gain = max( gain*release , seekGain );
        else gain = min( gain/attack , seekGain );
 
// COPY FROM REPLACING !!!!

// Applying output set feedback, etc...          ACCUMULATING!!!!!  
        (*out1++) += (*in1++) * (gain - cDcAdd_) * outVolume  + (*in1++) * dryMix;
        (*out2++) += (*in2++) * (gain - cDcAdd_) * outVolume  + (*in2++) * dryMix;

    }
       
}

//-----------------------------------------------------------------------------------------
void AVst::processReplacing (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *in3  =  inputs[2];
    float *in4  =  inputs[3];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
    // set 'maxSpls' due to the feed
// IMPLEMENT LOW/HIGHPASS FOR DETECTOR HERE!
             if (feed == 0) maxSpls = absmax(    *in1 , *in2    ); // FF
        else if (feed == 1) maxSpls = absmax(  *in3++ , *in4++  ); // SIDE

    // RMS via running sum (or not if PEAK)
    if (rmsSize <= 1) rms = maxSpls;
    else
    {
        rmsSqrSum = max( rmsSqrSum-rmsBuffer[rmsBufPos] , 0 ) + ( rmsBuffer[rmsBufPos] = (maxSpls*maxSpls) );
        if ( ++rmsBufPos >= rmsSize ) rmsBufPos = 0;
        rms = sqrt( rmsSqrSum / rmsSize );
    }
    
    // Do lookAhead Thingy
    if (lookAheadSize > 1)
    {
        lookAheadBufferIn1[lookAheadBufPos] = (*in1);
        lookAheadBufferIn2[lookAheadBufPos] = (*in2);
        lookAheadBufferRms[lookAheadBufPos] = rms;
        if ( ++lookAheadBufPos >= lookAheadSize ) lookAheadBufPos = 0;
        (*in1) = lookAheadBufferIn1[lookAheadBufPos];
        (*in2) = lookAheadBufferIn2[lookAheadBufPos];
//        t = 0;
// MAKE THIS MUCH MO FASTER DIPSHIT
// ARE YOU BRAIN DEAD? I SAID MO FASTER!
//        for (int i = 0; i < lookAheadSize; i++)
//        {
//            t = (lookAheadBufferRms[i] > lookAheadBufferRms[t]) ? i:t;
//        }
//        rms = lookAheadBufferRms[t];
// Hmmm.. okay maybe sort it or somethin' ... (cpu? fast? memory? need to figure ever considered that?)
// 2. sort it and make extra array (- huge - sucks badass)
//no // 3. get intel shit ( - cost  - propritarien rights? intel machine only?)

rms = max(lookAheadBufferRms[lookAheadBufPos],rms);

    }
    
    // Detect and set new seekGain (VERY CONFUSING CODE HERE)
        if (rms > threshOpen)
        {
            underthresh = 0;
            seekGain =  1; // + cDcAdd_ but it'd get truncated anyway so leave it out
        }
        else if ( ++underthresh > hold) seekGain = cDcAdd_;

// Make 'gain' follow seekGain with the specified sttack and release speed
// invert release / attack
        if (gain > seekGain) gain = max( gain*release , seekGain );
        else gain = min( gain/attack , seekGain );

// Applying output, set feedback, etc...          REPLACING!!!!!
        (*out1++) = (*in1) * (gain - cDcAdd_) * outVolume + (*in1++) * dryMix;
        (*out2++) = (*in2) * (gain - cDcAdd_) * outVolume + (*in2++) * dryMix;

    }

}
