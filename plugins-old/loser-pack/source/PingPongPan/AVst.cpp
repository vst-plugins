// Copyright (C) 2007, Michael Gruhn.
// License: LGPL - http://www.gnu.org/licenses/lgpl.html

// This uses the VST SDK 2.3. copyright as follows:
//-------------------------------------------------------------------------------------------------------
// VST Plug-Ins SDK
// Example AGain (VST 1.0)
// Stereo plugin which applies a Gain [-oo, 0dB]
// (C) 2003, Steinberg Media Technologies, All Rights Reserved
//-------------------------------------------------------------------------------------------------------

#include <math.h>
#include <algorithm>

#define cAmpDB_ 8.656170245
#define c2Pi_ 6.283185307

inline float abs(float x)
{
    return ( x<0 ? -x:x);
}

inline float absmax (float x, float y)
{
    float absx = abs(x);
    float absy = abs(y);
    return (absx>absy ? absx : absy);
}

inline float min (float x, float y)
{
    return ( x<y ? x:y);
}

inline float max (float x, float y)
{
    return ( x>y ? x:y);
}

#ifndef __AVST_H
#include "AVst.hpp"
#endif

//-------------------------------------------------------------------------------------------------------
AVst::AVst (audioMasterCallback audioMaster)
	: AudioEffectX (audioMaster, 1, 2)	// 1 program, 2 parameters
{
    freqFader = .5;
    waveSpeed = (c2Pi_/2) / getSampleRate();
	
    width = .75;

    
///

	setNumInputs (2);		// stereo in + stereo aux
	setNumOutputs (2);		// stereo out
	setUniqueID ((long)"PPPa");	// identify
	canMono ();				// makes sense to feed both inputs with the same signal
	canProcessReplacing ();	// supports both accumulating and replacing output
	strcpy (programName, "Default");	// default program name
}

//-------------------------------------------------------------------------------------------------------
AVst::~AVst ()
{

}

//-------------------------------------------------------------------------------------------------------
void AVst::setProgramName (char *name)
{
	strcpy (programName, name);
}

//-----------------------------------------------------------------------------------------
void AVst::getProgramName (char *name)
{
	strcpy (name, programName);
}

//-----------------------------------------------------------------------------------------
void AVst::setParameter (long index, float value)
{

    switch (index)
	{
		case 0 : 
        freqFader = value;
        waveSpeed = (c2Pi_ * freqFader)/getSampleRate();
        break;
		
        case 1 : 
        width = value;
        break;

	}

}

//-----------------------------------------------------------------------------------------
float AVst::getParameter (long index)
{
    float v = 0;
	switch (index)
	{
		case 0 : v = freqFader; break;
		case 1 : v = width; break;
	}
    return v;
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterName (long index, char *label)
{
     	switch (index)
	{
		case 0 : strcpy (label, "Frequency"); break;
		case 1 : strcpy (label, "Width"); break;
	}
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterDisplay (long index, char *text)
{
	switch (index)
	{
		case 0 : float2string (freqFader, text); break;
		case 1 : float2string (width*100, text); break;
	}

}

//-----------------------------------------------------------------------------------------
void AVst::getParameterLabel(long index, char *label)
{
	switch (index)
	{
		case 0 : strcpy (label, "Hz");	break;
		case 1 : strcpy (label, "%");	break;
	}
}

//------------------------------------------------------------------------
bool AVst::getEffectName (char* name)
{
	strcpy (name, "Ping Pong Pan");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getProductString (char* text)
{
	strcpy (text, "Ping Pong Pan");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getVendorString (char* text)
{
	strcpy (text, "LOSER-Development");
	return true;
}

//-----------------------------------------------------------------------------------------
void AVst::process (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
    pan = min( max( (sin(wavePos)) * width, -1 ) , 1 );
    if ((wavePos+=waveSpeed) >= c2Pi_) wavePos -= c2Pi_;

    (*out1++) += (*in1++) * (pan > 0 ? 1-pan:1);
    (*out2++) += (*in2++) * (pan < 0 ? 1+pan:1);

    }
}

//-----------------------------------------------------------------------------------------
void AVst::processReplacing (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
    pan = min( max( (sin(wavePos)) * width, -1 ) , 1 );
    if ((wavePos+=waveSpeed) >= c2Pi_) wavePos -= c2Pi_;

    (*out1++) = (*in1++) * (pan > 0 ? 1-pan:1);
    (*out2++) = (*in2++) * (pan < 0 ? 1+pan:1);

    }
}
