// Copyright (C) 2007, Michael Gruhn.
// License: LGPL - http://www.gnu.org/licenses/lgpl.html

// This uses the VST SDK 2.3. copyright as follows:
//-------------------------------------------------------------------------------------------------------
// VST Plug-Ins SDK
// Example AGain (VST 1.0)
// Stereo plugin which applies a Gain [-oo, 0dB]
// (C) 2003, Steinberg Media Technologies, All Rights Reserved
//-------------------------------------------------------------------------------------------------------

#include <math.h>
#include <algorithm>
#include <stdlib.h>

#define cAmpDB_ 8.656170245


#ifndef __AVST_H
#include "AVst.hpp"
#endif

//-------------------------------------------------------------------------------------------------------
AVst::AVst (audioMasterCallback audioMaster)
	: AudioEffectX (audioMaster, 1, 2)	// 1 program, 2 parameter
{
	ceiling = 1;				// default to 0 dB
    gain = 1;
    
	setNumInputs (2);		// stereo in
	setNumOutputs (2);		// stereo out
	setUniqueID ((long)"dclm");	// identify
	canMono ();				// makes sense to feed both inputs with the same signal
	canProcessReplacing ();	// supports both accumulating and replacing output
	strcpy (programName, "Default");	// default program name
}

//-------------------------------------------------------------------------------------------------------
AVst::~AVst ()
{
	// nothing to do here
}

//-------------------------------------------------------------------------------------------------------
void AVst::setProgramName (char *name)
{
	strcpy (programName, name);
}

//-----------------------------------------------------------------------------------------
void AVst::getProgramName (char *name)
{
	strcpy (name, programName);
}

//-----------------------------------------------------------------------------------------
void AVst::setParameter (long index, float value)
{
    switch(index)
    {
        case 0:
            gain = exp(value*12 / cAmpDB_);
        break;
        case 1:
            ceiling = exp((value-1)*6 / cAmpDB_);
        break;
    }
}

//-----------------------------------------------------------------------------------------
float AVst::getParameter (long index)
{
    float v;
    switch(index)
    {
        case 0:
            v = log(gain)*cAmpDB_ / 12;
        break;
        case 1:
            v = log(ceiling)*cAmpDB_ / 6 + 1;
        break;
    }
    return v;
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterName (long index, char *label)
{
    switch(index)
    {
        case 0:
            strcpy (label, "Gain");
        break;
        case 1:
            strcpy (label, "Ceiling");
        break;
    }

}

//-----------------------------------------------------------------------------------------
void AVst::getParameterDisplay (long index, char *text)
{
    switch(index)
    {
        case 0:
            float2string (log(gain)*cAmpDB_, text);
        break;
        case 1:
            float2string (log(ceiling)*cAmpDB_, text);
        break;
    }

}


//-----------------------------------------------------------------------------------------
void AVst::getParameterLabel(long index, char *label)
{
    switch(index)
    {
        case 0:
        	strcpy (label, "dB");
        break;
        case 1:
        	strcpy (label, "dB");
        break;
    }
}

//------------------------------------------------------------------------
bool AVst::getEffectName (char* name)
{
	strcpy (name, "Destructive Clipping Loudness Maximizer");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getProductString (char* text)
{
	strcpy (text, "Destructive Clipping Loudness Maximizer");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getVendorString (char* text)
{
	strcpy (text, "LOSER-Development");
	return true;
}

//-----------------------------------------------------------------------------------------
void AVst::process (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
        (*in1) *= gain;
        (*in2) *= gain;

    if ((*in1) >  ceiling) (*in1) =  ceiling;
    if ((*in1) < -ceiling) (*in1) = -ceiling;
    if ((*in2) >  ceiling) (*in2) =  ceiling;
    if ((*in2) < -ceiling) (*in2) = -ceiling;

        (*out1++) += (*in1++);    // accumulating
        (*out2++) += (*in2++);
    }
}

//-----------------------------------------------------------------------------------------
void AVst::processReplacing (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
        (*in1) *= gain;
        (*in2) *= gain;

    if ((*in1) >  ceiling) (*in1) =  ceiling;
    if ((*in1) < -ceiling) (*in1) = -ceiling;
    if ((*in2) >  ceiling) (*in2) =  ceiling;
    if ((*in2) < -ceiling) (*in2) = -ceiling;

        (*out1++) = (*in1++);    // replacing
        (*out2++) = (*in2++);
    }
}
