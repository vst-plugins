// Copyright (C) 2007, Michael Gruhn.
// License: LGPL - http://www.gnu.org/licenses/lgpl.html

// This uses the VST SDK 2.3. copyright as follows:
//-------------------------------------------------------------------------------------------------------
// VST Plug-Ins SDK
// Example AGain (VST 1.0)
// Stereo plugin which applies a Gain [-oo, 0dB]
// (C) 2003, Steinberg Media Technologies, All Rights Reserved
//-------------------------------------------------------------------------------------------------------

#include <math.h>
#include <algorithm>

#define cAmpDB_ 8.656170245
#define cDcAdd_ 1e-30
#define cPi_ 3.141592654

inline float abs(float x){
    return ( x<0 ? -x:x);
}

inline float min (float x, float y){
    return ( x<y ? x:y);
}

inline float max (float x, float y){
    return ( x>y ? x:y);
}

#ifndef __AVST_H
#include "AVst.hpp"
#endif

//-------------------------------------------------------------------------------------------------------
AVst::AVst (audioMasterCallback audioMaster)
	: AudioEffectX (audioMaster, 1, 0)	// 1 program, 0 parameter only
{

    xHP = exp(-2.0*cPi_*40/getSampleRate());
    a0HP = 1.0-xHP;
    b1HP = -xHP;

///

	setNumInputs (2);		// stereo in
	setNumOutputs (2);		// stereo out
	setUniqueID ((long)"DCas");	// identify
	canMono ();				// makes sense to feed both inputs with the same signal
	canProcessReplacing ();	// supports both accumulating and replacing output
	strcpy (programName, "Default");	// default program name

    suspend();
}

//-------------------------------------------------------------------------------------------------------
AVst::~AVst ()
{

}

void AVst::resume()
{
    xHP = exp(-2.0*cPi_*40/getSampleRate());
    a0HP = 1.0-xHP;
    b1HP = -xHP;
}

void AVst::suspend()
{
    tmp1HP = tmp2HP = 0;
}

//-------------------------------------------------------------------------------------------------------
void AVst::setProgramName (char *name)
{
	strcpy (programName, name);
}

//-----------------------------------------------------------------------------------------
void AVst::getProgramName (char *name)
{
	strcpy (name, programName);
}

//-----------------------------------------------------------------------------------------
void AVst::setParameter (long index, float value)
{

}

//-----------------------------------------------------------------------------------------
// float AVst::getParameter (long index)
// {

// }

//-----------------------------------------------------------------------------------------
void AVst::getParameterName (long index, char *label)
{

}

//-----------------------------------------------------------------------------------------
void AVst::getParameterDisplay (long index, char *text)
{

}

//-----------------------------------------------------------------------------------------
void AVst::getParameterLabel(long index, char *label)
{

}

//------------------------------------------------------------------------
bool AVst::getEffectName (char* name)
{
	strcpy (name, "DC Assassin");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getProductString (char* text)
{
	strcpy (text, "DC Assassin");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getVendorString (char* text)
{
	strcpy (text, "LOSER-Development");
	return true;
}

//-----------------------------------------------------------------------------------------
void AVst::process (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
   
    (*out1++) += (*in1++) - (tmp1HP = a0HP * (*in1) - b1HP * tmp1HP + cDcAdd_) - cDcAdd_;
    (*out2++) += (*in2++) - (tmp2HP = a0HP * (*in2) - b1HP * tmp2HP + cDcAdd_) - cDcAdd_;

    }
}

//-----------------------------------------------------------------------------------------
void AVst::processReplacing (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {
        
    (*out1++) = (*in1++) - (tmp1HP = a0HP * (*in1) - b1HP * tmp1HP + cDcAdd_) - cDcAdd_;
    (*out2++) = (*in2++) - (tmp2HP = a0HP * (*in2) - b1HP * tmp2HP + cDcAdd_) - cDcAdd_;

    }
}
