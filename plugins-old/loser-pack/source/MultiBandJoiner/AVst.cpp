// Copyright (C) 2007, Michael Gruhn.
// License: LGPL - http://www.gnu.org/licenses/lgpl.html

// This uses the VST SDK 2.3. copyright as follows:
//-------------------------------------------------------------------------------------------------------
// VST Plug-Ins SDK
// Example AGain (VST 1.0)
// Stereo plugin which applies a Gain [-oo, 0dB]
// (C) 2003, Steinberg Media Technologies, All Rights Reserved
//-------------------------------------------------------------------------------------------------------

#include <math.h>
#include <algorithm>

#define cAmpDB_ 8.656170245
#define cDcAdd_ 1e-30
#define cPi_ 3.141592654

inline float abs(float x){
    return ( x<0 ? -x:x);
}

inline float min (float x, float y){
    return ( x<y ? x:y);
}

inline float max (float x, float y){
    return ( x>y ? x:y);
}

#ifndef __AVST_H
#include "AVst.hpp"
#endif

//-------------------------------------------------------------------------------------------------------
AVst::AVst (audioMasterCallback audioMaster)
	: AudioEffectX (audioMaster, 1, 5)	// 1 program, 5 parameter only
{

    ch12Vol = ch34Vol = ch56Vol = ch78Vol = ch910Vol = outVol= 1;
    ch12VolF = ch34VolF = ch56VolF = ch78VolF = ch910VolF = outVolF = 0.5f;


///

	setNumInputs (10);		// 5 x stereo in
	setNumOutputs (2);		// stereo out
	setUniqueID ((long)"3Beq");	// identify
	canMono ();				// makes sense to feed both inputs with the same signal
	canProcessReplacing ();	// supports both accumulating and replacing output
	strcpy (programName, "Default");	// default program name

}

//-------------------------------------------------------------------------------------------------------
AVst::~AVst ()
{

}


//-------------------------------------------------------------------------------------------------------
void AVst::setProgramName (char *name)
{
	strcpy (programName, name);
}

//-----------------------------------------------------------------------------------------
void AVst::getProgramName (char *name)
{
	strcpy (name, programName);
}

//-----------------------------------------------------------------------------------------
void AVst::setParameter (long index, float value)
{

    switch (index)
	{
		case 0 : 
        ch12VolF = value;
        ch12Vol = exp( (ch12VolF -.5)*48 / cAmpDB_);
        break;

		case 1 : 
        ch34VolF = value;
        ch34Vol = exp( (ch34VolF -.5)*48 / cAmpDB_);
        break;

		case 2 : 
        ch56VolF = value;
        ch56Vol = exp( (ch56VolF -.5)*48 / cAmpDB_);
        break;

		case 3 : 
        ch78VolF = value;
        ch78Vol = exp( (ch78VolF -.5)*48 / cAmpDB_);
        break;

		case 4 : 
        ch910VolF = value;
        ch910Vol = exp( (ch910VolF -.5)*48 / cAmpDB_);
        break;

		case 5 : 
        outVolF = value;
        outVol = exp( (outVolF -.5)*48 / cAmpDB_);
        break;
	}

}

//-----------------------------------------------------------------------------------------
float AVst::getParameter (long index)
{
    float v = 0;
	switch (index)
	{
		case 0 : v = ch12VolF;   break;
		case 1 : v = ch34VolF;   break;
		case 2 : v = ch56VolF;   break;
		case 3 : v = ch78VolF;   break;
		case 4 : v = ch910VolF;   break;
		case 5 : v = outVolF;   break;
	}
    return v;
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterName (long index, char *label)
{
     	switch (index)
	{
		case 0 : strcpy (label, "Channels  1 + 2");  break;
		case 1 : strcpy (label, "Channels  3 + 4");  break;
		case 2 : strcpy (label, "Channels  5 + 6");  break;
		case 3 : strcpy (label, "Channels  7 + 8");  break;
		case 4 : strcpy (label, "Channels  9 +10");  break;
		case 5 : strcpy (label, "Output");           break;
	}
}

//-----------------------------------------------------------------------------------------
void AVst::getParameterDisplay (long index, char *text)
{
	switch (index)
	{
		case 0 : float2string (log(ch12Vol)*cAmpDB_, text);      break;
		case 1 : float2string (log(ch34Vol)*cAmpDB_, text);      break;
		case 2 : float2string (log(ch56Vol)*cAmpDB_, text);      break;
		case 3 : float2string (log(ch78Vol)*cAmpDB_, text);      break;
		case 4 : float2string (log(ch910Vol)*cAmpDB_, text);      break;
		case 5 : float2string (log(outVol)*cAmpDB_, text);      break;
	}

}

//-----------------------------------------------------------------------------------------
void AVst::getParameterLabel(long index, char *label)
{
	switch (index)
	{
		case 0 : strcpy (label, "dB");	break;
		case 1 : strcpy (label, "dB");	break;
		case 2 : strcpy (label, "dB");	break;
		case 3 : strcpy (label, "dB");	break;
		case 4 : strcpy (label, "dB");	break;
		case 5 : strcpy (label, "dB");	break;
	}
}

//------------------------------------------------------------------------
bool AVst::getEffectName (char* name)
{
	strcpy (name, "Multi-Band Joiner");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getProductString (char* text)
{
	strcpy (text, "Multi-Band Joiner");
	return true;
}

//------------------------------------------------------------------------
bool AVst::getVendorString (char* text)
{
	strcpy (text, "LOSER-Development");
	return true;
}

//-----------------------------------------------------------------------------------------
void AVst::process (float **inputs, float **outputs, long sampleFrames)
{
//     float *in1  =  inputs[0];
//     float *in2  =  inputs[1];
//     float *out1 = outputs[0];
//     float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {

    
    }
}

//-----------------------------------------------------------------------------------------
void AVst::processReplacing (float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *in3  =  inputs[2];
    float *in4  =  inputs[3];
    float *in5  =  inputs[4];
    float *in6  =  inputs[5];
    float *in7  =  inputs[6];
    float *in8  =  inputs[7];
    float *in9  =  inputs[8];
    float *in10  =  inputs[9];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while (--sampleFrames >= 0)
    {

    (*out1++) = ((*in1++)*ch12Vol+(*in3++)*ch34Vol+(*in5++)*ch56Vol+(*in7++)*ch78Vol+(*in9++)*ch910Vol)*outVol;
    (*out2++) = ((*in2++)*ch12Vol+(*in4++)*ch34Vol+(*in6++)*ch56Vol+(*in8++)*ch78Vol+(*in10++)*ch910Vol)*outVol;

    }
}
