
/*-----------------------------------------------------------------------------

 1999, Steinberg Soft und Hardware GmbH, All Rights Reserved

-----------------------------------------------------------------------------*/

#include <iostream>

#include "AudioEffect.cpp"
#include "audioeffectx.h"
#include "audioeffectx.cpp"
#include "../_vstgui/vstgui.cpp"
#include "../_vstgui/vstcontrols.cpp"

#include "SilverBoxEditor.cpp"
#include "SilverBoxGUI.cpp"
#include "SilverBox.cpp"
#include "knobs.cpp"
#include "IIR.cpp"
#include "freq.cpp"
#include "dist.cpp"
#include "CPitchSlider.cpp"


//==============================================================================
extern "C" AEffect* main_plugin (audioMasterCallback audioMaster) asm ("main");
extern "C" AEffect *main_plugin (audioMasterCallback audioMaster)
{
    std::cout << "main" << std::endl;

    SilverBoxGUI* const plugin = new SilverBoxGUI (audioMaster);

    if (plugin)
        return plugin->getAeffect();

    return 0;
}

__attribute__((constructor)) void myPluginInit() // this is called when the library is unoaded
{
//    std::cout << "myPluginInit" << std::endl;
}

__attribute__((destructor)) void myPluginFini() // this is called when the library is unoaded
{
//    std::cout << "myPluginFini" << std::endl;
}
