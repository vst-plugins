#include "SilverBoxGUI.h"

#ifndef __SILVERBOX_EDITOR__
#include "SilverBoxEditor.h"
#endif

#include <string.h>

//-----------------------------------------------------------------------------
SilverBoxGUI::SilverBoxGUI (audioMasterCallback audioMaster)
 : SilverBox (audioMaster)
{
    editor = 0;
    cEffect.flags |= effFlagsHasEditor; // has editor
}

//-----------------------------------------------------------------------------
SilverBoxGUI::~SilverBoxGUI ()
{
	// the editor gets deleted by the
	// AudioEffect base class
}

//-----------------------------------------------------------------------------
void SilverBoxGUI::setParameter (long index, float value)
{
	SilverBox::setParameter (index, value);

	if (editor)
		((AEffGUIEditor*)editor)->setParameter (index, value);
}

//-----------------------------------------------------------------------------------------
long SilverBoxGUI::dispatcher (long opCode, long index, long value, void *ptr, float opt)
{
    int result = 0;

    switch (opCode)
    {
    case effEditClose:
        if (editor)
        {
            editor->close ();
            delete editor;
            editor = 0;
        }
        break;

    case effEditOpen:
        if (display == 0)
            display = (Display*) value;

        if (editor == 0)
            editor = new SilverBoxEditor (this);

    default:
        result = AudioEffectX::dispatcher (opCode, index, value, ptr, opt);
    }
    return result;
}

