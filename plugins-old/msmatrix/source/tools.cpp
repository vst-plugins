/*-----------------------------------------------------------------------------
  
  � 2000, Michael Beer, All Rights Reserved
  
  -----------------------------------------------------------------------------*/

#include <string.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <float.h>

#include "tools.hpp"

//-----------------------------------------------------------------------------
void AdvancedTools::dBptone2string(float value, char *text)
{
  if(value <= 0)
#if MAC
    strcpy(text, "-�");
#else
  strcpy(text, "-oo");
#endif
  else
    floatptone2string((float)( 20. * log10(value) ), text);
}

//-----------------------------------------------------------------------------
void AdvancedTools::dBpttwo2string(float value, char *text)
{
  if(value <= 0)
#if MAC
    strcpy(text, "-�");
#else
  strcpy(text, "-oo");
#endif
  else
    floatpttwo2string((float)( 20. * log10(value) ), text);
}
//-----------------------------------------------------------------------------
void AdvancedTools::floatptzero2string(float value, char *text)
{
  long c = 0, neg = 0;
  char string[32];
  char *s;
  double v, integ, i10, mantissa, m10, ten = 10.;
  //long vorKomma = 5;
  long vorKomma = 6;
  long nachKomma = 0;
  long alleStellen = vorKomma + nachKomma + 2;
  
  if ( value < 0.0f ) {
    value -= 0.5f;
  }
  else {
    value += 0.5f;
  }
  
  v = (double)value;
  if(v < 0)
    {
      neg = 1;
      value = -value;
      v = -v;
      c++;
      if(v > 9999999.)
	{
	  strcpy(string, " Huge!  ");
	  return;
	}
    }
  else if(v > 99999999.)
    {
      strcpy(string, " Huge!  ");
      return;
    }
  
  s = string + 31;
  *s-- = 0;
  //*s-- = '.';
  c++;
  
  integ = floor(v);
  i10 = fmod(integ, ten);
  *s-- = (long)i10 + '0';
  integ /= ten;
  c++;
  while(integ >= 1. && c < vorKomma)
    {
      i10 = fmod(integ, ten);
      *s-- = (long)i10 + '0';
      integ /= ten;
      c++;
    }
  if(neg)
    *s-- = '-';
  strcpy(text, s + 1);
  if(c >= alleStellen)
    return;
  
  s = string + 31;
  *s-- = 0;
  mantissa = fmod(v, 1.);
  mantissa *= pow(ten, (double)(8 - c));
  nachKomma += c; 
  while(c < nachKomma)
    {
      if(mantissa <= 0)
	*s-- = '0';
      else
	{
	  m10 = fmod(mantissa, ten);
	  *s-- = (long)m10 + '0';
	  mantissa /= 10.;
	}
      c++;
    }
  strcat(text, s + 1);
}

//-----------------------------------------------------------------------------
void AdvancedTools::floatptone2string(float value, char *text)
{
  long c = 0, neg = 0;
  char string[32];
  char *s;
  double v, integ, i10, mantissa, m10, ten = 10.;
  long vorKomma = 4;
  long nachKomma = 1;
  long alleStellen = vorKomma + nachKomma + 2;
  
  if ( value < 0.0f ) {
    value -= 0.05f;
  }
  else {
    value += 0.05f;
  }
  
  v = (double)value;
  if(v < 0)
    {
      neg = 1;
      value = -value;
      v = -v;
      c++;
      if(v > 9999999.)
	{
	  strcpy(string, " Huge!  ");
	  return;
	}
    }
  else if(v > 99999999.)
    {
      strcpy(string, " Huge!  ");
      return;
    }
  
  s = string + 31;
  *s-- = 0;
  *s-- = '.';
  c++;
  
  integ = floor(v);
  i10 = fmod(integ, ten);
  *s-- = (long)i10 + '0';
  integ /= ten;
  c++;
  while(integ >= 1. && c < vorKomma)
    {
      i10 = fmod(integ, ten);
      *s-- = (long)i10 + '0';
      integ /= ten;
      c++;
    }
  if(neg)
    *s-- = '-';
  strcpy(text, s + 1);
  if(c >= alleStellen - nachKomma )
    return;
  
  s = string + 31;
  *s-- = 0;
  mantissa = fmod(v, 1.);
  mantissa *= pow(ten, (double)(nachKomma));
  nachKomma += c; 
  while(c < nachKomma)
    {
      if(mantissa <= 0)
	*s-- = '0';
      else
	{
	  m10 = fmod(mantissa, ten);
	  *s-- = (long)m10 + '0';
	  mantissa /= 10.;
	}
      c++;
    }
  strcat(text, s + 1);
}

//-----------------------------------------------------------------------------
void AdvancedTools::floatpttwo2string(float value, char *text)
{
  long c = 0, neg = 0;
  char string[32];
  char *s;
  double v, integ, i10, mantissa, m10, ten = 10.;
  long vorKomma = 4;
  long nachKomma = 2;
  long alleStellen = vorKomma + nachKomma + 2;
  
  if ( value < 0.0f ) {
    value -= 0.005f;
  }
  else {
    value += 0.005f;
  }
  
  v = (double)value;
  if(v < 0)
    {
      neg = 1;
      value = -value;
      v = -v;
      c++;
      if(v > 9999999.)
	{
	  strcpy(string, " Huge!  ");
	  return;
	}
    }
  else if(v > 99999999.)
    {
      strcpy(string, " Huge!  ");
      return;
    }
  
  s = string + 31;
  *s-- = 0;
  *s-- = '.';
  c++;
  
  integ = floor(v);
  i10 = fmod(integ, ten);
  *s-- = (long)i10 + '0';
  integ /= ten;
  c++;
  while(integ >= 1. && c < vorKomma)
    {
      i10 = fmod(integ, ten);
      *s-- = (long)i10 + '0';
      integ /= ten;
      c++;
    }
  if(neg)
    *s-- = '-';
  strcpy(text, s + 1);
  if(c >= alleStellen - nachKomma )
    return;
  
  s = string + 31;
  *s-- = 0;
  mantissa = fmod(v, 1.);
  mantissa *= pow(ten, (double)(nachKomma));
  nachKomma += c; 
  while(c < nachKomma)
    {
      if(mantissa <= 0)
	*s-- = '0';
      else
	{
	  m10 = fmod(mantissa, ten);
	  *s-- = (long)m10 + '0';
	  mantissa /= 10.;
	}
      c++;
    }
  strcat(text, s + 1);
}

//-----------------------------------------------------------------------------
float AdvancedTools::quantize2ptzero( float value )
{
  float ret;
  
  if ( value > 0.0f ) {
    ret = (float)((long)(value + 0.5f));
  }
  else {
    ret = (float)((long)(value - 0.5f));
  }
  
  return( ret );
}


//-----------------------------------------------------------------------------
float AdvancedTools::quantize2ptone( float value )
{
  float ret;
  
  if ( value > 0.0f ) {
    ret = ((float)((long)(10.0f * value + 0.5f))) / 10.0f;
  }
  else {
    ret = ((float)((long)(10.0f * value - 0.5f))) / 10.0f;
  }
  
  return( ret );
}


//-----------------------------------------------------------------------------
float AdvancedTools::quantize2pthalf( float value )
{
  float ret;
  
  if ( value > 0.0f ) {
    ret = ((float)((long)(2.0f * value + 0.5f))) / 2.0f;
  }
  else {
    ret = ((float)((long)(2.0f * value + 0.5f))) / 2.0f;
  }
  
  return( ret );
}


//-----------------------------------------------------------------------------
float AdvancedTools::quantize2pttwo( float value )
{
  float ret;
  
  if ( value > 0.0f ) {
    ret = ((float)((long)(100.0f * value + 0.5f))) / 100.0f;
  }
  else {
    ret = ((float)((long)(100.0f * value + 0.5f))) / 100.0f;
  }
  
  return( ret );
}


//-----------------------------------------------------------------------------
float AdvancedTools::quantize2dBptone( float value )
{
  float ret;
  
  if ( value > 0.0f ) {
    ret = 20.0f * (float)log10( value );
  }
  else if ( value < 0.0f ) {
    ret = 20.0f * (float)log10( -value );
  }
  else {
    ret = -FLT_MAX;
  }
  
  if ( ret > -FLT_MAX ) {
    if ( value > 0.0f ) {
      ret = ((float)((long)(10.0f * ret))) / 10.0f;
      ret = (float)pow( 10.0f, 0.05f * ret );
    }
    else {
      ret = ((float)((long)(10.0f * ret))) / 10.0f;
      ret = -(float)pow( 10.0f, 0.05f * ret );
    }
  }
  else {
    ret = 0.0f;
  }
  
  return( ret );
}


//-----------------------------------------------------------------------------
float AdvancedTools::quantize2dBpthalf( float value )
{
  float ret;
  
  if ( value > 0.0f ) {
    ret = 20.0f * (float)log10( value );
  }
  else if ( value < 0.0f ) {
    ret = 20.0f * (float)log10( -value );
  }
  else {
    ret = -FLT_MAX;
  }
  
  if ( ret > -FLT_MAX ) {
    if ( value > 0.0f ) {
      ret = ((float)((long)(50.0f * ret))) / 50.0f;
      ret = (float)pow( 10.0f, 0.05f * ret );
    }
    else {
      ret = ((float)((long)(50.0f * ret))) / 50.0f;
      ret = -(float)pow( 10.0f, 0.05f * ret );
    }
  }
  else {
    ret = 0.0f;
  }
  
  return( ret );
}


//-----------------------------------------------------------------------------
float AdvancedTools::quantize2dBpttwo( float value )
{
  float ret;
  
  if ( value > 0.0f ) {
    ret = 20.0f * (float)log10( value );
  }
  else if ( value < 0.0f ) {
    ret = 20.0f * (float)log10( -value );
  }
  else {
    ret = -FLT_MAX;
  }
  
  if ( ret > -FLT_MAX ) {
    if ( value > 0.0f ) {
      ret = ((float)((long)(100.0f * ret))) / 100.0f;
      ret = (float)pow( 10.0f, 0.05f * ret );
    }
    else {
      ret = ((float)((long)(100.0f * ret))) / 100.0f;
      ret = -(float)pow( 10.0f, 0.05f * ret );
    }
  }
  else {
    ret = 0.0f;
  }
  
  return( ret );
}


//-----------------------------------------------------------------------------
long AdvancedTools::resize2powOfTwo( long value )
{
  long size = 1;
  
  while ( size < value ) {
    size <<= 1;   // find power of 2 larger
  }
  
  return( size );
}



