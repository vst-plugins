/*-----------------------------------------------------------------------------
  
  � 2000, Michael Beer, All Rights Reserved
  
  -----------------------------------------------------------------------------*/

#ifndef TOOLS_HPP
#define TOOLS_HPP

#define LEFT                 0
#define RIGHT                1

#define MONO                 1
#define STEREO               2

class AdvancedTools
{
public:
  AdvancedTools( void ) { };
  virtual ~AdvancedTools( void ) { };
  
  // advanced tools
  virtual void dBptone2string(float value, char *text);
  virtual void dBpttwo2string(float value, char *text);
  virtual void floatptzero2string(float value, char *string);
  virtual void floatptone2string(float value, char *string);
  virtual void floatpttwo2string(float value, char *string);
  virtual float quantize2ptzero( float value );
  virtual float quantize2ptone( float value );
  virtual float quantize2pthalf( float value );
  virtual float quantize2pttwo( float value );
  virtual float quantize2dBptone( float value );
  virtual float quantize2dBpthalf( float value );
  virtual float quantize2dBpttwo( float value );
  virtual long resize2powOfTwo( long value );
  
};

#endif

