/*-----------------------------------------------------------------------------

� 2000, Michael Beer, All Rights Reserved

-----------------------------------------------------------------------------*/

#ifndef MSMATRIX_HPP
#define MSMATRIX_HPP


#include "audioeffectx.h"
#include "tools.hpp"
#include <string.h>

enum
{
  kInputL,
  kInputR,
  kDirection,

  kNumParams
};

typedef struct {
	float fInputL, fInputR, fDirection;
	char name[24];
} PRESET;

class SDelay;

class SDelayProgram
{
friend class SDelay;
public:
  SDelayProgram( void ) {}
  ~SDelayProgram( void ) {}

  void loadPreset( PRESET preset );
  
private:	
  float fInputL, fInputR, fDirection;
  char name[24];
};

class SDelay : public AudioEffectX, public AdvancedTools
{
public:
  SDelay( audioMasterCallback audioMaster );
  ~SDelay( void );
  
  virtual void process( float **inputs, float **outputs, long sampleframes );
  virtual void processReplacing( float **inputs, float **outputs, long sampleFrames );
  virtual void setProgram( long program );
  virtual void setProgramName( char *name );
  virtual void getProgramName( char *name );
  virtual void setParameter( long index, float value );
  virtual float getParameter( long index );
  virtual void getParameterLabel( long index, char *label );
  virtual void getParameterDisplay( long index, char *text );
  virtual void getParameterName( long index, char *text );
  virtual void suspend( );
  
private:
  SDelayProgram *programs;

  float fInputL, fInputR, fDirection;

};

#endif

