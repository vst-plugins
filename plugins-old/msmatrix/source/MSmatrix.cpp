/*-----------------------------------------------------------------------------

© 2000, Michael Beer, All Rights Reserved

-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "MSmatrix.hpp"
#include "AEffEditor.hpp"

#define NO_OF_PRESETS 2

const PRESET preset[NO_OF_PRESETS] = {
  {
    1.0f, 1.0f, 0.0f, "L/R -> M/S"
  },
  {
    1.0f, 1.0f, 1.0f, "M/S -> L/R"
  }
};

//-----------------------------------------------------------------------------
void SDelayProgram::loadPreset( PRESET preset )
{
	fInputL = preset.fInputL;
	fInputR = preset.fInputR;
	fDirection = preset.fDirection;
	strcpy( name, preset.name );
}

//-----------------------------------------------------------------------------
SDelay::SDelay( audioMasterCallback audioMaster )
	: AudioEffectX( audioMaster, NO_OF_PRESETS, kNumParams )
{
  long i;
  const int maxChannels = 2;

  programs = new SDelayProgram[numPrograms];

  if ( programs ) {
    for ( i = 0; i < numPrograms; i++ ) {
      programs[i].loadPreset( preset[i] );
    }
    
    setProgram( 0 );
  }

  setNumInputs( maxChannels );
  setNumOutputs( maxChannels );

  canProcessReplacing( );
  setUniqueID((long)"xmlr");

  suspend( );		// flush buffer
}

//------------------------------------------------------------------------
SDelay::~SDelay( void )
{
  if ( programs )
    delete[] programs;
}

//------------------------------------------------------------------------
void SDelay::setProgram( long program )
{
  SDelayProgram * ap = &programs[program];

  curProgram = program;
  setParameter( kInputL, ap->fInputL );
  setParameter( kInputR, ap->fInputR );
  setParameter( kDirection, ap->fDirection );
}

//------------------------------------------------------------------------
void SDelay::setProgramName( char *name )
{
  strcpy( programs[curProgram].name, name );
}

//------------------------------------------------------------------------
void SDelay::getProgramName( char *name )
{
  if ( !strcmp( programs[curProgram].name, "Init" ) )
    sprintf( name, "%s %d", programs[curProgram].name, (int)(curProgram + 1 ));
  else
    strcpy( name, programs[curProgram].name );
}

//------------------------------------------------------------------------
void SDelay::suspend( )
{
}

//------------------------------------------------------------------------
void SDelay::setParameter( long index, float value )
{
  SDelayProgram * ap = &programs[curProgram];

  switch ( index ) {
  case kInputL:      
    fInputL = ap->fInputL = quantize2dBptone( value ); 
    break;
  case kInputR:      
    fInputR = ap->fInputR = quantize2dBptone( value ); 
    break;
  case kDirection:
    fDirection = ap->fDirection = value; 
    break;
  }
  
  if ( editor )
    editor->postUpdate( );
}

//------------------------------------------------------------------------
float SDelay::getParameter( long index )
{
  float v = 0;
  
  switch ( index ) {
  case kInputL:
    v = fInputL; 
    break;
  case kInputR:
    v = fInputR; 
    break;
  case kDirection:
    v = fDirection; 
    break;
  }

  return( v );
}

//------------------------------------------------------------------------
void SDelay::getParameterName( long index, char *label )
{
  switch ( index ) {
  case kInputL:
    strcpy (label, "InputLevelL"); 
    break;
  case kInputR:
    strcpy (label, "InputLevelR"); 
    break;
  case kDirection:
    strcpy (label, "Direction  "); 
    break;
  }
}

//------------------------------------------------------------------------
void SDelay::getParameterDisplay( long index, char *text )
{
  switch ( index ) {
  case kInputL:     
    dBptone2string( fInputL, text ); 
    break;
  case kInputR:     
    dBptone2string( fInputR, text ); 
    break;
  case kDirection:
    if ( fDirection < 0.5f )
      strcpy( text, "L/R->M/S" );
    else
      strcpy( text, "M/S->L/R" );
    break;
  }
}

//------------------------------------------------------------------------
void SDelay::getParameterLabel( long index, char *label )
{
  switch ( index ) {
  case kDirection:    
    strcpy( label, "        " );	
    break;
  case kInputL:     
    strcpy( label, "   dB   " );	
    break;
  case kInputR:     
    strcpy( label, "   dB   " );	
    break;
  }
}

//------------------------------------------------------------------------
void SDelay::process( float **inputs, float **outputs, long sampleframes )
{
  int i;
  float tmpL, tmpR;
  float *in1 = inputs[0];
  float *in2 = inputs[1];
  float *out1 = outputs[0];
  float *out2 = outputs[1];
  
  if ( fDirection < 0.5f ) {
    for ( i = 0; i < sampleframes; i++ ) {
      tmpL = in1[i] * fInputL;
      tmpR = in2[i] * fInputR;
      out1[i] = (tmpL + tmpR) * 0.5f;
      out2[i] = (tmpL - tmpR) * 0.5f;
    }
  }
  else {
    for ( i = 0; i < sampleframes; i++ ) {
      tmpL = in1[i] * fInputL;
      tmpR = in2[i] * fInputR;
      out1[i] = tmpL + tmpR;
      out2[i] = tmpL - tmpR;
    }
  }
  
}

//---------------------------------------------------------------------------
// replacing
void SDelay::processReplacing( float **inputs, float **outputs, long sampleframes )
{
  int i;
  float tmpL, tmpR;
  float *in1 = inputs[0];
  float *in2 = inputs[1];
  float *out1 = outputs[0];
  float *out2 = outputs[1];
  
  if ( fDirection < 0.5f ) {
    for ( i = 0; i < sampleframes; i++ ) {
      tmpL = in1[i] * fInputL;
      tmpR = in2[i] * fInputR;
      out1[i] = (tmpL + tmpR) * 0.5f;
      out2[i] = (tmpL - tmpR) * 0.5f;
    }
  }
  else {
    for ( i = 0; i < sampleframes; i++ ) {
      tmpL = in1[i] * fInputL;
      tmpR = in2[i] * fInputR;
      out1[i] = tmpL + tmpR;
      out2[i] = tmpL - tmpR;
    }
  }
  
}
