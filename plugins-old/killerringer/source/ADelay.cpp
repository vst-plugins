#include <stdio.h>
#include <string.h>
#include <math.h>
#include "ADelay.hpp"
#include "AEffEditor.hpp"

#include <AudioEffect.cpp>
#include <audioeffectx.h>
#include <audioeffectx.cpp>

//-----------------------------------------------------------------------------

ADelayProgram::ADelayProgram()
{
    fDelay = 0.5;
    fFeedBack = 0.5;
    fOut = 0.75;
    strcpy (name, "Init");
}

//-----------------------------------------------------------------------------

ADelay::ADelay(audioMasterCallback audioMaster)
        : AudioEffectX (audioMaster,16,kNumParams)
{
    //sets "size" t0 44100 (1 second) - this is our delay memory
    size = 10000;
    // makes the dial make SENSE!!!!!!!!!
    // size = 44100;
    //a buffer of floats is allocated
    buffer = new float[size];
    programs = new ADelayProgram[numPrograms];
    fDelay = fFeedBack = fOut = vu = 0;
    delay = inPos = outPos = 0;
    if (programs) setProgram(0);
    setNumInputs(2);
    setNumOutputs(2);
    canMono();
    hasVu ();
    canProcessReplacing ();
    setUniqueID ((long)"Kllr");
    suspend();
}

//------------------------------------------------------------------------

ADelay::~ADelay()
{
    if (buffer) delete[] buffer;
    if (programs) delete[] programs;
}

//------------------------------------------------------------------------

void ADelay::setProgram(long program)
{
    ADelayProgram * ap = &programs[program];
    curProgram = program;
    setParameter (kDelay, ap->fDelay);
    setParameter (kFeedBack, ap->fFeedBack);
    setParameter (kOut, ap->fOut);
}

//------------------------------------------------------------------------

void ADelay::setDelay(float fdelay)
{
    long oi;
    fDelay = fdelay;
    delay = (long)(fdelay * (float)(size - 1));
    programs[curProgram].fDelay = fdelay;
    oi = inPos - delay;
    if (oi < 0) oi += size;
    outPos = oi;
}

//------------------------------------------------------------------------

void ADelay::setProgramName(char *name)
{
    strcpy (programs[curProgram].name, name);
}

//------------------------------------------------------------------------

void ADelay::getProgramName (char *name)
{
    if (!strcmp (programs[curProgram].name, "Init"))
        sprintf (name, "%s %d", programs[curProgram].name, (int)(curProgram + 1));
    else strcpy (name, programs[curProgram].name);
}

//------------------------------------------------------------------------

void ADelay::suspend()
{
    memset(buffer,0,size*sizeof(float));
}

//------------------------------------------------------------------------

float ADelay::getVu()
{
    float cvu = vu;
    vu = 0;
    return cvu;
}

//------------------------------------------------------------------------

void ADelay::setParameter(long index, float value)
{
    ADelayProgram * ap = &programs[curProgram];
    switch (index)
    {
    case kDelay:
        setDelay (value);
        break;
    case kFeedBack:
        fFeedBack = ap->fFeedBack = value;
        break;
    case kOut:
        fOut = ap->fOut = value;
        break;
    }
    if (editor) editor->postUpdate ();
}

//------------------------------------------------------------------------

float ADelay::getParameter(long index)
{
    float v = 0;
    switch (index)
    {
    case kDelay:
        v = fDelay;
        break;
    case kFeedBack:
        v = fFeedBack;
        break;
    case kOut:
        v = fOut;
        break;
    }
    return v;
}

//------------------------------------------------------------------------

void ADelay::getParameterName(long index, char *label)
{
    switch (index)
    {
    case kDelay:
        strcpy(label,"Root-Pitch");
        break;
    case kFeedBack:
        strcpy(label,"Speed");
        break;
    case kOut:
        strcpy(label,"Amount");
        break;
    }
}

//------------------------------------------------------------------------

void ADelay::getParameterDisplay (long index, char *text)
{
    switch (index)
    {
    case kDelay:
        long2string(delay,text);
        break;
    case kFeedBack:
        float2string(fFeedBack,text);
        break;
    case kOut:
        float2string(fOut,text);
        break;
    }
}

//------------------------------------------------------------------------

void ADelay::getParameterLabel (long index, char *label)
{
    switch (index)
    {
    case kDelay:
        strcpy(label," - ");
        break;
    case kFeedBack:
        strcpy(label," amt ");
        break;
    case kOut:
        strcpy(label," dB ");
        break;
    }
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

float counter = 0;
float countertoo = 0;
//float counterthree = 0;
double quarterSec = (0.000570162/4);
double topFreq = 10000;
double LFOFreq = 10;

//-----------------------------------------------------------------------------------------

void ADelay::process(float **inputs, float **outputs, long sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];
    while (--sampleFrames >= 0)
    {
        (*out1++) += (*in1++);
        (*out2++) += (*in2++);
    }
}
//-----------------------------------------------------------------------------------------

void ADelay::processReplacing(float **inputs, float **outputs, long
                              sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];
    while (--sampleFrames >= 0)
    {
        countertoo = (countertoo + (quarterSec*(LFOFreq*fFeedBack)));
        if (countertoo > (2*3.14159))
        {
            //to cause aliases:
            //counter = 0;
            //To solve aliases:
            countertoo -= (2*3.14159);
        }
        double nuMod = sin(countertoo);
        counter = (counter + (quarterSec*(topFreq*fDelay)+(nuMod*(fOut/8))));
        if (counter > (2*3.14159))
        {
            //to cause aliases:
            //counter = 0;
            //To solve aliases:
            counter -= (2*3.14159);
        }
        double sinMod = sin(counter);
        (*out1++) = ((*in1++)*(sinMod/2));
        (*out2++) = ((*in2++)*(sinMod/2));
        //(*out1++) = (sinMod);
        //(*out2++) = (sinMod);
    }
}


