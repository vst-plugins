/*-----------------------------------------------------------------------------
� 1999, Steinberg Soft und Hardware GmbH, All Rights Reserved
-----------------------------------------------------------------------------*/
// ported by cern.th.skei 08.apr.2007

#include "audioeffectx.h"
#include <string.h>

enum
{
	kDelay,
	kFeedBack,
	kOut,
	kNumParams
};

class ADelay;

//to hold presets:
class ADelayProgram
{
	friend class ADelay;
	public:
		ADelayProgram();
		~ADelayProgram() {}
	private:	
		float fDelay,fFeedBack,fOut;
		char name[24];
};

class ADelay : public AudioEffectX
{
	public:
		ADelay(audioMasterCallback audioMaster);
		~ADelay();
		virtual void process(float **inputs, float **outputs, long sampleframes);
		virtual void processReplacing(float **inputs, float **outputs, long sampleFrames);
		virtual void setProgram(long program);
		//To get and set the names of presets
		virtual void setProgramName(char *name);
		virtual void getProgramName(char *name);
		virtual void setParameter(long index, float value);
		virtual float getParameter(long index);
		virtual void getParameterLabel(long index, char *label);
		virtual void getParameterDisplay(long index, char *text);
		virtual void getParameterName(long index, char *text);
		virtual float getVu();
		virtual void suspend();
	private:
		void setDelay(float delay);
		void subProcess(float *in, float *out1, float *out2,
		float *dest, float *del, long sampleframes);
		void subProcessReplacing(float *in, float *out1, float *out2,
		float *dest, float *del, long sampleframes);
		ADelayProgram *programs;
		float *buffer;
		float fDelay, fFeedBack, fOut;
		float vu;
		long delay;
		long size;
		long inPos;
		long outPos;
};
