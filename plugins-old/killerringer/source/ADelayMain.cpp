/*-----------------------------------------------------------------------------
� 1999, Steinberg Soft und Hardware GmbH, All Rights Reserved
-----------------------------------------------------------------------------*/

#include "ADelay.hpp"

bool oome = false;

//#if MAC
//	#pragma export on
//#endif

#ifdef __GNUC__ 
	#define main main_plugin
	AEffect* main_plugin(audioMasterCallback audioMaster) asm ("main");
//#elif BEOS
//	#define main main_plugin
//	extern "C" __declspec(dllexport) AEffect *main_plugin(audioMasterCallback audioMaster);
#else
	AEffect *main(audioMasterCallback audioMaster);
#endif

AEffect *main(audioMasterCallback audioMaster)
{
	//if (!audioMaster (0, audioMasterVersion, 0, 0, 0, 0)) return 0;  // old version
	AudioEffect* effect = new ADelay (audioMaster);
	if (!effect) return 0;
	if (oome)
	{
		delete effect;
		return 0;
	}
	return effect->getAeffect ();
}

//#if MAC
//#pragma export off
//#endif

//#if WIN32
//#include <windows.h>
//void* hInstance;
//BOOL WINAPI DllMain (HINSTANCE hInst, DWORD dwReason, LPVOID lpvReserved)
//{
//	hInstance = hInst;
//	return 1;
//}
//#endif

