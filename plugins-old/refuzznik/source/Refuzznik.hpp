/*
Copyright (c) 2007 Johan Sarge

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/

#ifndef WP_REFUZZNIK_HPP
#define WP_REFUZZNIK_HPP

// cern
#define NO_GUI

#include <cmath>
#include <cstdio>
#include <cstring>
#include <stdexcept>
#include "audioeffectx.h"
#include "refuzznikparams.h"
#ifndef NO_GUI
#include "RefuzznikEditor.hpp"
#endif

#define VENDOR_VERSION 500l


class Refuzznik : public AudioEffectX {
public:
	// Displayers.
	typedef void (*funcfcp)(float, char *);
	
	static void yesNoDisplayer(float value, char *text);
	static void pcntDisplayer(float value, char *text);
	
	static void inGainDisplayer(float value, char *text);
	static void freqMinDisplayer(float value, char *text);
	static void freqDifDisplayer(float value, char *text);
	
	// Constructor.
	Refuzznik(audioMasterCallback audioMaster);
	
	// Plug info
	virtual bool getEffectName(char* name);
	virtual bool getVendorString(char* text);
	virtual bool getProductString(char* text);
	virtual long getVendorVersion();
	virtual VstPlugCategory getPlugCategory();
	virtual long canDo(char *text);
	
	// Program
	virtual void setProgram(long program);
	virtual void setProgramName(char *name);
	virtual void getProgramName(char *name);
	virtual bool getProgramNameIndexed(long category, long index, char *text);
	
	// Parameters
	virtual void setParameter(long index, float value);
	virtual float getParameter(long index) {return paramValues[index];}
	virtual void getParameterLabel(long index, char *label);
	virtual void getParameterDisplay(long index, char *text);
	virtual void getParameterName(long index, char *text);
	
	// Processes
	virtual bool setBypass(bool onOff);
	virtual void resume();
	virtual void process(float **inputs, float **outputs, long sampleFrames);
	virtual void processReplacing(float **inputs, float **outputs, long sampleFrames);
	
	// Custom.
	bool isOperational() {return operational;}
	
	funcfcp getParamDisplayer(int index) {return paramDisplayers[index];}

private:
	static const char *const paramNames[kNumParams];
	static const char *const paramLabels[kNumParams];
	static const float LN2;
	
	// Plug-in state.
	char programName[32];
	bool operational, bypassed, stereoMode;
	
	float paramValues[kNumParams];
	
	funcfcp paramDisplayers[kNumParams];
	
	bool zeroX;
	float inGain, funcMix, ampMin, freqMin, freqDif;
	float ampDif, periodsDifLog;
	
	// Setters.
	typedef void (Refuzznik::*methodf)(float);
	methodf paramSetters[kNumParams];
	
	void zeroXSetter(float value);
	void inGainSetter(float value);
	void funcMixSetter(float value);
	void ampMinSetter(float value);
	void freqMinSetter(float value);
	void freqDifSetter(float value);
	
	// Processing handlers.
	typedef void (Refuzznik::*method4fpi)(float *, float *, float *, float *, int);
	method4fpi procHandlers[4], procRHandlers[4], procHandler, procRHandler;
	
	void setProcHandlers();
	
	void proc1In1Out(float *in0, float *in1, float *out0, float *out1, int sampleFrames);
	void proc1In1OutB(float *in0, float *in1, float *out0, float *out1, int sampleFrames);
	void proc2In2Out(float *in0, float *in1, float *out0, float *out1, int sampleFrames);
	void proc2In2OutB(float *in0, float *in1, float *out0, float *out1, int sampleFrames);
	
	void procR1In1Out(float *in0, float *in1, float *out0, float *out1, int sampleFrames);
	void procR1In1OutB(float *in0, float *in1, float *out0, float *out1, int sampleFrames);
	void procR2In2Out(float *in0, float *in1, float *out0, float *out1, int sampleFrames);
	void procR2In2OutB(float *in0, float *in1, float *out0, float *out1, int sampleFrames);
	
	// DSP routines
	inline float refuzzZeroX(float in);
	inline float refuzzNoZeroX(float in);
	
	inline float aMap(float ain);
	inline float fMap(float ain);
};

#endif
