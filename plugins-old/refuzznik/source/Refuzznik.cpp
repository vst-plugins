/*
Copyright (c) 2007 Johan Sarge

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/

#ifndef WP_REFUZZNIK_HPP
#include "Refuzznik.hpp"
#endif

// Parameter transform macros.
#define BOOL_T(v) ((v) > 0.5f)
#define GAIN_T(v) (std::pow(2.0f, 8.0f*(v)))
#define F_MIN_T(v) (std::pow(2.0f, 9.0f*(v) - 1.0f))
#define F_DIFF_T(v) (copysignf(std::pow(2.0f, 14.0f*std::fabs((v) - 0.5f)) - 1.0f, (v) - 0.5f))

// Private static data.
const char *const Refuzznik::paramNames[kNumParams] = {
	"ZeroX", "InGain", "FuncMix", "AmpMin", "FreqMin", "FreqDif"
};

const char *const Refuzznik::paramLabels[kNumParams] = {
	"", "dB", "%", "%", "periods", "periods"
};

const float Refuzznik::LN2 = std::log(2.0f);


// Displayers.
void Refuzznik::yesNoDisplayer(float value, char *text) {
	std::strcpy(text, BOOL_T(value) ? "Yes" : "No");
}

void Refuzznik::pcntDisplayer(float value, char *text) {
	std::sprintf(text, "%1.2f", 100.0f*value);
}

void Refuzznik::inGainDisplayer(float value, char *text) {
	std::sprintf(text, "%1.2f", 20.0f*std::log10(GAIN_T(value)));
}

void Refuzznik::freqMinDisplayer(float value, char *text) {
	std::sprintf(text, "%1.2f", F_MIN_T(value));
}

void Refuzznik::freqDifDisplayer(float value, char *text) {
	std::sprintf(text, "%1.2f", F_DIFF_T(value));
}


// Constructor.
Refuzznik::Refuzznik(audioMasterCallback audioMaster) :
AudioEffectX(audioMaster, 1, kNumParams),
operational(true), bypassed(false), stereoMode(false) {
	
	setNumInputs(2); // Stereo in.
	setNumOutputs(2); // Stereo out.
	setUniqueID(CCONST('R','f','z','n')); // Identify.
	canMono(); // Mono in possible.
	canProcessReplacing(); // Supports both accumulating and replacing output.
	std::strcpy(programName, "Default");	// Default program name.
	
	// Initial parameter values.
	paramValues[kZeroX]   = 1.0f;
	paramValues[kInGain]  = 0.0f;
	paramValues[kFuncMix] = 0.5f;
	paramValues[kAmpMin]  = 0.0f;
	paramValues[kFreqMin] = 2.0f / 9.0f;
	paramValues[kFreqDif] = 0.5f;
	
	// Parameter setters.
	paramSetters[kZeroX]   = &Refuzznik::zeroXSetter;
	paramSetters[kInGain]  = &Refuzznik::inGainSetter;
	paramSetters[kFuncMix] = &Refuzznik::funcMixSetter;
	paramSetters[kAmpMin]  = &Refuzznik::ampMinSetter;
	paramSetters[kFreqMin] = &Refuzznik::freqMinSetter;
	paramSetters[kFreqDif] = &Refuzznik::freqDifSetter;
	
	// Set initial parameter values.
	for (int i = 0; i < kNumParams; i++)
		setParameter(i, paramValues[i]);
	
	// Parameter displayers.
	paramDisplayers[kZeroX]   = &yesNoDisplayer;
	paramDisplayers[kInGain]  = &inGainDisplayer;
	paramDisplayers[kFuncMix] = &pcntDisplayer;
	paramDisplayers[kAmpMin]  = &pcntDisplayer;
	paramDisplayers[kFreqMin] = &freqMinDisplayer;
	paramDisplayers[kFreqDif] = &freqDifDisplayer;
	
	// Processing handlers.
	procHandlers[0] = &Refuzznik::proc1In1Out;
	procHandlers[1] = &Refuzznik::proc1In1OutB;
	procHandlers[2] = &Refuzznik::proc2In2Out;
	procHandlers[3] = &Refuzznik::proc2In2OutB;
	
	procRHandlers[0] = &Refuzznik::procR1In1Out;
	procRHandlers[1] = &Refuzznik::procR1In1OutB;
	procRHandlers[2] = &Refuzznik::procR2In2Out;
	procRHandlers[3] = &Refuzznik::procR2In2OutB;
	
	procHandler = &Refuzznik::proc1In1Out;
	procRHandler = &Refuzznik::procR1In1Out;
	
#ifndef NO_GUI
	// Create GUI editor.
	// NOTE: ~AudioEffect will delete the editor if it exists.
	editor = new RefuzznikEditor(this);
#endif
}


// Inherited virtual public methods.
bool Refuzznik::getEffectName(char *text) {
	std::strcpy(text, "Refuzznik");
	return true;
}

bool Refuzznik::getProductString(char *text) {
	std::strcpy(text, "Refuzznik - Sinusoidal distortion unit");
	return true;
}

bool Refuzznik::getVendorString(char *text) {
	std::strcpy(text, "Transvaal Audio");
	return true;
}

long Refuzznik::getVendorVersion() {return VENDOR_VERSION;}

VstPlugCategory Refuzznik::getPlugCategory() {return kPlugCategEffect;}

long Refuzznik::canDo(char *text) {
	if (!std::strcmp(text, "sendVstEvents") ||
	    !std::strcmp(text, "sendVstMidiEvent") ||
	    !std::strcmp(text, "sendVstTimeInfo"))
		return -1l; // No event send.
	if (!std::strcmp(text, "receiveVstEvents") ||
	    !std::strcmp(text, "receiveVstMidiEvent") ||
	    !std::strcmp(text, "receiveVstTimeInfo"))
		return -1l; // No event receive.
	else if (!std::strcmp(text, "offline") || !std::strcmp(text, "noRealTime"))
		return -1l; // Realtime interface only.
	else if (!std::strcmp(text, "1in1out") || !std::strcmp(text, "2in2out"))
		return 1l; // Supported IO configurations.
	else if (!std::strcmp(text, "1in2out") || !std::strcmp(text, "2in1out") ||
	         !std::strcmp(text, "2in4out") || !std::strcmp(text, "4in2out") ||
		       !std::strcmp(text, "4in4out") || !std::strcmp(text, "4in8out") ||
	         !std::strcmp(text, "8in4out") || !std::strcmp(text, "8in8out"))
		return -1l; // Unsupported IO configurations.
	else if (!std::strcmp(text, "bypass"))
		return 1l; // Soft/listening bypass supported.
	return 0l; // Dunno.
}

void Refuzznik::setProgram(long program) {} // No program changes allowed.

void Refuzznik::setProgramName(char *name) {std::strcpy(programName, name);}

void Refuzznik::getProgramName(char *name) {std::strcpy(name, programName);}

bool Refuzznik::getProgramNameIndexed(long category, long index, char *text) {
	if ((category == 0 || category == -1) && index == 0) {
		std::strcpy(text, programName);
		return true;
	}
	return false;
}

void Refuzznik::setParameter(long index, float value) {
	(this->*paramSetters[index])(value);
	paramValues[index] = value;
	
#ifndef NO_GUI
	if (editor != NULL)
		((AEffGUIEditor *) editor)->setParameter(index, value);
#endif
}

void Refuzznik::getParameterName(long index, char *label) {
	std::strcpy(label, paramNames[index]);
}

void Refuzznik::getParameterDisplay(long index, char *text) {
	paramDisplayers[index](paramValues[index], text);
}

void Refuzznik::getParameterLabel(long index, char *label) {
	std::strcpy(label, paramLabels[index]);
}

bool Refuzznik::setBypass(bool onOff) {
	bypassed = onOff;
	setProcHandlers();
	return true;
}

void Refuzznik::resume() {
	stereoMode = isInputConnected(1l) && isOutputConnected(1l);
	
	setProcHandlers();
}

void Refuzznik::process(float **inputs, float **outputs, long sampleFrames) {
	(this->*procHandler)(inputs[0],
	                     (stereoMode) ? inputs[1] : NULL,
	                     outputs[0],
											 (stereoMode) ? outputs[1] : NULL,
	                     sampleFrames);
}

void Refuzznik::processReplacing(float **inputs, float **outputs, long sampleFrames) {
	(this->*procRHandler)(inputs[0],
	                      (stereoMode) ? inputs[1] : NULL,
	                      outputs[0],
											  (stereoMode) ? outputs[1] : NULL,
	                      sampleFrames);
}


// Setters.
void Refuzznik::zeroXSetter(float value) {zeroX = BOOL_T(value);}

void Refuzznik::funcMixSetter(float value) {funcMix = value;}

void Refuzznik::inGainSetter(float value) {inGain = GAIN_T(value);}

void Refuzznik::ampMinSetter(float value) {
	ampMin = value;
	ampDif = 1.0f - ampMin;
}

void Refuzznik::freqMinSetter(float value) {
	freqMin = F_MIN_T(value) * ((float) M_PI);
}

void Refuzznik::freqDifSetter(float value) {
	float periods = F_DIFF_T(value);
	
	periodsDifLog = std::log(std::fabs(periods) + 2.0f)/LN2;
	freqDif = periods * ((float) M_PI);
}


// Processing handlers.
void Refuzznik::setProcHandlers() {
	int i = 0;
	if (stereoMode)
		i += 2;
	if (bypassed)
		i += 1;
	
	procHandler = procHandlers[i];
	procRHandler = procRHandlers[i];
}

void Refuzznik::proc1In1Out(float *in0, float *in1, float *out0, float *out1, int sampleFrames) {
	if (zeroX)
		while (--sampleFrames >= 0) {
			*out0++ += refuzzZeroX(*in0++);
		}
	else
		while (--sampleFrames >= 0) {
			*out0++ += refuzzNoZeroX(*in0++);
		}
}

void Refuzznik::proc1In1OutB(float *in0, float *in1, float *out0, float *out1, int sampleFrames) {
	while (--sampleFrames >= 0) {
		*out0++ += *in0++;
	}
}

void Refuzznik::proc2In2Out(float *in0, float *in1, float *out0, float *out1, int sampleFrames) {
	if (zeroX)
		while (--sampleFrames >= 0) {
			*out0++ += refuzzZeroX(*in0++);
			*out1++ += refuzzZeroX(*in1++);
		}
	else
		while (--sampleFrames >= 0) {
			*out0++ += refuzzNoZeroX(*in0++);
			*out1++ += refuzzNoZeroX(*in1++);
		}
}

void Refuzznik::proc2In2OutB(float *in0, float *in1, float *out0, float *out1, int sampleFrames) {
	while (--sampleFrames >= 0) {
		*out0++ += *in0++;
		*out1++ += *in1++;
	}
}

void Refuzznik::procR1In1Out(float *in0, float *in1, float *out0, float *out1, int sampleFrames) {
	if (zeroX)
		while (--sampleFrames >= 0) {
			*out0++ = refuzzZeroX(*in0++);
		}
	else
		while (--sampleFrames >= 0) {
			*out0++ = refuzzNoZeroX(*in0++);
		}
}

void Refuzznik::procR1In1OutB(float *in0, float *in1, float *out0, float *out1, int sampleFrames) {
	while (--sampleFrames >= 0) {
		*out0++ = *in0++;
	}
}

void Refuzznik::procR2In2Out(float *in0, float *in1, float *out0, float *out1, int sampleFrames) {
	if (zeroX)
		while (--sampleFrames >= 0) {
			*out0++ = refuzzZeroX(*in0++);
			*out1++ = refuzzZeroX(*in1++);
		}
	else
		while (--sampleFrames >= 0) {
			*out0++ = refuzzNoZeroX(*in0++);
			*out1++ = refuzzNoZeroX(*in1++);
		}
}

void Refuzznik::procR2In2OutB(float *in0, float *in1, float *out0, float *out1, int sampleFrames) {
	while (--sampleFrames >= 0) {
		*out0++ = *in0++;
		*out1++ = *in1++;
	}
}


// DSP routines
inline float Refuzznik::refuzzZeroX(float in) {
	in *= inGain;
	
	float ain = std::fabs(in);
	float a = aMap(ain);
	
	float shelfTerm = copysignf(a, in),
	      sinTerm = (ampMin + ampDif * a) * std::sin(fMap(ain) * in);
	
	return shelfTerm + funcMix * (sinTerm - shelfTerm);
}

inline float Refuzznik::refuzzNoZeroX(float in) {
	in *= inGain;
	
	float ain = std::fabs(in);
	float a = aMap(ain);
	
	float sinTerm = (ampMin + ampDif * a) * 0.5f * (1.0f - std::cos(fMap(ain) * in));
	
	return copysignf(a + funcMix * (sinTerm - a), in);
}

inline float Refuzznik::aMap(float ain) {
	float f1 = 4.0f * ain * ain;
	
	return f1 / (f1 + 1.0f);
}

inline float Refuzznik::fMap(float ain) {
	return freqMin + freqDif * std::pow(ain, periodsDifLog);
}

