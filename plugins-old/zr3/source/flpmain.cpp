#include "AudioEffect.cpp"
#include "audioeffectx.h"
#include "audioeffectx.cpp"

#include "Globals.h"
#include "voice_classes.cpp"
#include "FX.cpp"
#include "flp.cpp"
#include "flpproc.cpp"
#include "flp_frame.cpp"

#include <stddef.h>

AEffect *main_plugin (audioMasterCallback audioMaster) asm("main");
#define main main_plugin

AEffect *main (audioMasterCallback audioMaster)
{
	// get vst version
	if(!audioMaster (0, audioMasterVersion, 0, 0, 0, 0))
		return 0;  // old version

	flp* effect = new flp (audioMaster);
	if (!effect)
		return 0;
	return effect->getAeffect ();
}

