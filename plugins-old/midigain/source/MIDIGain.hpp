/*-----------------------------------------------------------------------------
  MIDI Gain 1.02

  David Viens
  davidv@plogue.com
-----------
rev history

 1.02 jan09 2003 Now overriding aeffectx.h's VstEvent* events[2]
                 By creating my own struct (see comments)
-----------------------------------------------------------------------------*/


#ifndef __PLUGIN_H
#define __PLUGIN_H

#include "audioeffectx.h"

#include <vector>
#include <string>
#include <fstream>


typedef std::vector<VstMidiEvent> VstMidiEventVec;

#define MAX_EVENTS_PER_TIMESLICE 256

/*
please see below why we need to create our OWN struct,

http://lists.steinberg.net:8100/Lists/vst-plugins/Message/1841.html
http://lists.steinberg.net:8100/Lists/vst-plugins/Message/2231.html
http://lists.steinberg.net:8100/Lists/vst-plugins/Message/6999.html

thanx for mrl_ for re-pointing it out!!
*/

struct MyVstEvents
{
    long numEvents;
    long reserved;
    VstEvent* events[MAX_EVENTS_PER_TIMESLICE];
};


enum
{
	kParam01,
	kNUmParams
};

//audio i/o
#define PLUG_AUDIO_INPUTS   0
#define PLUG_AUDIO_OUTPUTS  0


//max 1!!! it cant have more than one midi stream in VST 2.x!!
#define PLUG_MIDI_INPUTS    1 //(only 1 or 0!)
#define PLUG_MIDI_OUTPUTS   1 //(only 1 or 0!)


// you absolutely MUST change all the following!!!!////
#ifdef _DEBUG
#define PLUG_IDENT    'FAK1'
#define PLUG_NAME     "MIDI Gain Debug"
#else
#define PLUG_IDENT    'CAKE'
#define PLUG_NAME     "MIDI Gain"
#endif

#define PLUG_VENDOR   "A company Name"
////////////////////////////////////////////////////////



#ifdef _DEBUG
#define DEBUG_OUT(x) (cout << x << endl)
#else
#define DEBUG_OUT(x)
#endif


class MIDIGain : public AudioEffectX
{
public:
	MIDIGain(audioMasterCallback audioMaster);
	~MIDIGain();

	inline  void   process(float **inputs, float **outputs, long sampleFrames);
	inline  void   processReplacing(float **inputs, float **outputs, long sampleFrames);
    inline  void   innerProcess(float * in, float * out);

	virtual long   processEvents(VstEvents* events);

	virtual void   setParameter(long index, float value);
	virtual float  getParameter(long index);
	virtual void   getParameterLabel(long index, char *label);
	virtual void   getParameterDisplay(long index, char *text);
	virtual void   getParameterName(long index, char *text);
    virtual bool   getVendorString (char* text);	// fill text with a string identifying the vendor (max 64 char)
	virtual bool   getProductString (char* text);	// fill text with a string identifying the product name (max 64 char)					// fills <ptr> with a string with product name (max 64 char)
    virtual long   getVendorVersion ();				// return vendor-specific version

	virtual void   setSampleRate(float sampleRate);

	virtual void   setBlockSize(long blockSize);
	virtual void   resume();

	virtual bool   getInputProperties (long index, VstPinProperties* properties);
	virtual bool   getOutputProperties (long index, VstPinProperties* properties);

	virtual bool   getEffectName (char* name);
	virtual long   canDo (char* text);

protected:


	bool init(void);
	virtual void processMidiEvents(VstMidiEventVec *inputs, VstMidiEventVec *outputs);

	virtual void preProcess(void);
	virtual void postProcess(void);

    // inputs
    VstMidiEventVec *_midiEventsIn;
    void          _cleanMidiInBuffers();

    // outputs
    VstMidiEventVec *_midiEventsOut;
    void          _cleanMidiOutBuffers();

	MyVstEvents  *_vstEventsToHost;
    VstMidiEvent *_vstMidiEventsToHost;

	float fParam01;

    bool _hostCanDoVstTimeInfo;
    bool _hostCanDoTempo;

    float _bpm;
    char  _programName[32];

    long _iter;

#ifdef _DEBUG
    std::streambuf *_outbuf;
    std::streambuf *_errbuf;
    std::ofstream _debugOut;
    bool          _debugActive;
#endif

};

#endif
