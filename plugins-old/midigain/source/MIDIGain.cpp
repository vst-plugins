/*-----------------------------------------------------------------------------
  MIDI Gain 1.02

  David Viens
  davidv@plogue.com
-----------------------------------------------------------------------------*/

#include "MIDIGain.hpp"
#include "AEffEditor.hpp"


#include <iostream>
#include <string>
#include <cmath>
#include <exception>
#include <algorithm>
#include "MIDI.h"


using namespace std;


// VstMidiEvent sorting utility
// say you have a VstMidiEventVec _vec :
// std::sort( _vec.begin(), _vec.end(), midiSort() );
struct midiSort {
    bool operator()(VstMidiEvent &first, VstMidiEvent &second) {
        return first.deltaFrames < second.deltaFrames;
    }
};


//-----------------------------------------------------------------------------
MIDIGain::MIDIGain(audioMasterCallback audioMaster): AudioEffectX(audioMaster, 1, 1)
// 1 program, 1 parameters
{

    // check to see if the host supports sending tempo & time information to VST plugins
    _hostCanDoVstTimeInfo = canHostDo("sendVstTimeInfo")?true:false;
    _hostCanDoTempo       = tempoAt(0)                  ?true:false;

    setNumInputs (PLUG_AUDIO_INPUTS);  // no in
    setNumOutputs(PLUG_AUDIO_OUTPUTS); // mono out
    setUniqueID  ((long)"FAK1");       // identify
    canProcessReplacing();             // supports both accumulating and replacing output
    hasVu (false);
    hasClip (false);

    strcpy(_programName, "Default"); // default program name

    sampleRate = getSampleRate();

    fParam01 = 1.0f;

    //so it can appear in some hosts!
    isSynth();

    init();
    suspend ();
}


//-----------------------------------------------------------------------------------------
MIDIGain::~MIDIGain()
{
#ifdef _DEBUG
    _debugOut.flush();
    _debugOut.close();
    cout.rdbuf(_outbuf);
    cerr.rdbuf(_errbuf);
#endif

    _cleanMidiInBuffers();
    _cleanMidiOutBuffers();

    delete [] _midiEventsIn;
    delete [] _midiEventsOut;

    if (_vstEventsToHost)
        delete    _vstEventsToHost;
    if (_vstMidiEventsToHost)
        delete [] _vstMidiEventsToHost;
}


//-----------------------------------------------------------------------------------------
bool   MIDIGain::getVendorString (char* text)
{
    sprintf(text,PLUG_VENDOR);
    return true;
}

//-----------------------------------------------------------------------------------------
bool   MIDIGain::getProductString (char* text)
{
    sprintf(text,PLUG_NAME);
    return true;
}

//-----------------------------------------------------------------------------------------
bool MIDIGain::getEffectName (char* name)
{
    sprintf(name,PLUG_NAME);
    return true;
}

//-----------------------------------------------------------------------------------------
long MIDIGain::getVendorVersion ()
{
    return 1;
}

//-----------------------------------------------------------------------------------------
void  MIDIGain:: setSampleRate(float sampleRateIn) {

    AudioEffectX::setSampleRate (sampleRate);
    sampleRate = sampleRateIn;
}


//-----------------------------------------------------------------------------------------
void MIDIGain::setBlockSize (long blockSize)
{
    AudioEffectX::setBlockSize (blockSize);
    // you may need to have to do something here...
}

//-----------------------------------------------------------------------------------------
void MIDIGain::resume ()
{
    wantEvents ();
}


//-----------------------------------------------------------------------------------------
void MIDIGain::setParameter(long index, float value)
{
    switch (index)
    {
    case kParam01:
        fParam01 = value;
        break;
    default :
        break;
    }
}

//-----------------------------------------------------------------------------------------
float MIDIGain::getParameter(long index)
{
    float v=0;

    switch (index)
    {
    case kParam01:
        v = fParam01;
        break;
    default :
        break;
    }
    return v;
}

//-----------------------------------------------------------------------------------------
void MIDIGain::getParameterName(long index, char *label)
{
    switch (index)
    {
    case kParam01:
        strcpy(label, "velocity");
        break;
    default :
        break;
    }
}

//-----------------------------------------------------------------------------------------
void MIDIGain::getParameterDisplay(long index, char *text)
{
    switch (index)
    {
    case kParam01:
        sprintf(text, "%d",FLOAT_TO_MIDI(fParam01));
        break;
    default :
        break;
    }

}

//-----------------------------------------------------------------------------------------
void MIDIGain::getParameterLabel(long index, char *label)
{
    switch (index)
    {
    case kParam01:
        strcpy(label, "note");
        break;
    default :
        break;
    }
}

//-----------------------------------------------------------------------------------------
long MIDIGain::canDo (char* text)
{

    if (PLUG_MIDI_OUTPUTS) {
        if (!strcmp (text, "sendVstMidiEvent"))    return 1;
        if (!strcmp (text, "sendVstEvents"))       return 1;
    }

    if (PLUG_MIDI_INPUTS) {
        if (!strcmp (text, "receiveVstEvents"))    return 1;
        if (!strcmp (text, "receiveVstMidiEvent")) return 1;
    }

    // VstTimeInfo
    if (!strcmp (text, "receiveVstTimeInfo"))  return 1;

    //this is for hosts that support receiving VstTimeInfo..
    //(Supported in Plogue Bidule since v0.5600)
//	if (!strcmp (text, "sendVstTimeInfo"))     return 1;

    return -1;	// explicitly can't do; 0 => don't know
}


//-----------------------------------------------------------------------------------------
bool MIDIGain::getInputProperties (long index, VstPinProperties* properties)
{
    //if you want to give more details to the host about your connectors configuration
    return false;
}

//-----------------------------------------------------------------------------------------
bool MIDIGain::getOutputProperties (long index, VstPinProperties* properties)
{
    //if you want to give more details to the host about your connectors configuration
    return false;
}





//-----------------------------------------------------------------------------------------
bool MIDIGain::init(void) {

#ifdef _DEBUG
    // quick and easy log file!
    streambuf *psbuf;
    string log(PLUG_NAME);
    log.append(".log");
    _debugOut.open (log.c_str());
    psbuf = _debugOut.rdbuf();
    _outbuf = cout.rdbuf(psbuf);
    _errbuf = cerr.rdbuf(psbuf);
    _debugActive = true;
    DEBUG_OUT(PLUG_NAME << " log file");
#endif

    _iter =0;

    try
    {
        _vstEventsToHost            = new MyVstEvents;
        _vstEventsToHost->numEvents = 0;
        _vstMidiEventsToHost        = new VstMidiEvent[MAX_EVENTS_PER_TIMESLICE];//might be not enough...!!

        _midiEventsIn = new VstMidiEventVec[PLUG_MIDI_INPUTS];
        _cleanMidiInBuffers();

        _midiEventsOut = new VstMidiEventVec[PLUG_MIDI_OUTPUTS];
        _cleanMidiOutBuffers();
    }
    catch (...) {
        DEBUG_OUT("unknown exception occured trying to acquire ressources");
        return false;
    }


    return true;
}


//DONT MODIFY THIS METHOD EITHER!!!!
void
MIDIGain::_cleanMidiInBuffers()
{
    for ( int i = 0; i < PLUG_MIDI_INPUTS; i++ )
        _midiEventsIn[i].clear();
}

//DONT MODIFY THIS METHOD EITHER!!!!
void
MIDIGain::_cleanMidiOutBuffers()
{
    for ( int i = 0; i < PLUG_MIDI_OUTPUTS; i++ )
        _midiEventsOut[i].clear();
}


void
MIDIGain::preProcess(void) {

    // preparing Proccess
    VstTimeInfo * timeInfo = NULL;
    timeInfo = getTimeInfo(0xffff); //ALL

    if (_hostCanDoVstTimeInfo && timeInfo)
    {
        if (kVstTempoValid & timeInfo->flags)
            _bpm = (float)timeInfo->tempo;
        // you can get all other pertinent info from VstTimeInfo
    }
    else
        if (_hostCanDoTempo) {

            _bpm = tempoAt (1)/(10000.0f);
        }
        else
        {
            _bpm = 120.0f ;
        }

    _cleanMidiOutBuffers();
}



//DONT MODIFY THIS METHOD!!!!
//it sends all midi events to the host from the internal vector
void
MIDIGain::postProcess(void) {

    DEBUG_OUT("_iter: " << _iter << "postProceesing:" << _midiEventsOut[0].size() << " events");

    _vstEventsToHost->numEvents = _midiEventsOut[0].size();

    if (_midiEventsOut[0].size() < MAX_EVENTS_PER_TIMESLICE)
        for (unsigned int i=0;i<_midiEventsOut[0].size();i++)
        {
            _vstMidiEventsToHost[i].type            = kVstMidiType;
            _vstMidiEventsToHost[i].byteSize        = 24;
            _vstMidiEventsToHost[i].deltaFrames     = _midiEventsOut[0][i].deltaFrames;
            _vstMidiEventsToHost[i].flags           = 0;
            _vstMidiEventsToHost[i].noteLength      = 0;
            _vstMidiEventsToHost[i].noteOffset      = 0;
            _vstMidiEventsToHost[i].midiData[0]     = _midiEventsOut[0][i].midiData[0];
            _vstMidiEventsToHost[i].midiData[1]     = _midiEventsOut[0][i].midiData[1];
            _vstMidiEventsToHost[i].midiData[2]     = _midiEventsOut[0][i].midiData[2];
            _vstMidiEventsToHost[i].midiData[3]     = 0;
            _vstMidiEventsToHost[i].detune          = _midiEventsOut[0][i].detune;

            _vstEventsToHost->events[i] = (VstEvent*) &_vstMidiEventsToHost[i];
        }
    else {
        //TODO call sendVstEventsToHost multiple times with MAX_EVENTS_PER_TIMESLICE sized buffers!!)
        //this would mean your plugin outputs MIDI events _much_ faster than the speed of normal MIDI :)
    }

    _vstEventsToHost->reserved  = 0;

    if (_midiEventsOut[0].size() > 0)
        sendVstEventsToHost((VstEvents*)_vstEventsToHost);

    //flushing Midi Input Buffers before they are filled
    _cleanMidiInBuffers();

}

//DONT MODIFY THIS METHOD EITHER!!!!
//it copies all VST events comming from the host to internal vector

long
MIDIGain::processEvents (VstEvents* ev) {

    VstEvents * evts = (VstEvents*)ev;

    DEBUG_OUT("_iter: " << _iter << "processEvents:" << evts->numEvents << " events");

    for (int i = 0; i < evts->numEvents; i++) {

        if ((evts->events[i])->type != kVstMidiType)
            continue;
        VstMidiEvent * event = (VstMidiEvent*)evts->events[i];
        _midiEventsIn[0].push_back(*event);
    }

    //if the host doesnt sort the incoming MIDI events (dumb)
    //std::sort( _midiEventsIn[0].begin(), _midiEventsIn[0].end(), midiSort() );

    return true;
}



//Only modify this if you want to do paralel Audio/Midi
//-----------------------------------------------------------------------------------------
void MIDIGain::process(float **inputs, float **outputs, long sampleFrames)
{
    //takes care of VstTimeInfo and such
    preProcess();

    //host should have called processEvents before process
    processMidiEvents(_midiEventsIn,_midiEventsOut);

    while (--sampleFrames >= 0) {
        // you would do audio processing here as in the rest of the sdk
    }

    //sending out MIDI events to Host to conclude wrapper
    postProcess();

    DEBUG_OUT("-------------------------------------");

    _iter++;
}



//Only modify this if you want to do paralel Audio/Midi
//-----------------------------------------------------------------------------------------
void MIDIGain::processReplacing(float **inputs, float **outputs, long sampleFrames)
{
    //takes care of VstTimeInfo and such
    preProcess();

    //host should have called processEvents before process
    processMidiEvents(_midiEventsIn,_midiEventsOut);

    while (--sampleFrames >= 0) {
        // you would do audio processing here as in the rest of the sdk
    }

    //sending out MIDI events to Host to conclude wrapper
    postProcess();

    DEBUG_OUT("-------------------------------------");

    _iter++;
}



//have fun modifying this one!
void
MIDIGain::processMidiEvents(VstMidiEventVec *inputs, VstMidiEventVec *outputs) {

    DEBUG_OUT("_iter: " << _iter << "processMidiEvents input:" << inputs[0].size() << " events");

    //passing through... do whatever midi effect you want here
    for (unsigned int i=0;i<inputs[0].size();i++)
    {
        //copying event "i" from input (with all its fields)
        VstMidiEvent tomod = inputs[0][i];

        long status     = tomod.midiData[0] & 0xf0;   // scraping  channel
//         long channel    = tomod.midiData[0] & 0x0f;   // isolating channel
//         long note       = tomod.midiData[1] & 0x7f;
        long velocity   = tomod.midiData[2] & 0x7f;

        if (status == MIDI_NOTEON && velocity > 0)    // we only look at notes
        {
            //cout << "note:" << note << "old vel:" << velocity;

            long newVel       = (long)(velocity * fParam01);

            //lower limit for volume (so that the end plugin doesnt receive
            //an unexpected note off
            if      (newVel == 0 )  newVel = (char) 1;
            else if (newVel > 127)  newVel = (char) 127;

            tomod.midiData[2] = (char)(newVel);

            //cout << " new vel=" << newVel << " in char:" << (int)tomod.midiData[2] << endl;

        }

        //pushing back our copied midi event from input, modified or not!
        outputs[0].push_back(tomod);
    }


    DEBUG_OUT("_iter: " << _iter << "processMidiEvents output:" << outputs[0].size() << " events");
}
