/*-----------------------------------------------------------------------------
The Ambisonic Reflector Project
(C) 2004 Dave Malham, Dogma Design

This program is released as free software; You redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program will be distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

dmalham@users.sourceforge.net
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include "BFReflector.hpp"

#include "math.h"
#include "Ambisonics.hpp"
#if MACX
/* The following optimization for floor() works for the default round to nearest FPU rounding mode on PowerPC for
all values except for -0.0, for which it returns 0.0 instead of -0.0:
*/
#include <ppc_intrinsics.h>
inline double fastfloor( double f )
{
	double c = __fsel( f, -0x1.0p+52, 0x1.0p+52 );
	double result = (f - c) + c;
	if( f < result ) result -= 1.0;
	return result;
}

#else
#define fastfloor floor
#endif


//-----------------------------------------------------------------------------

ReflectorProgram::ReflectorProgram ()
{

	dLeftRightMix	= 0.5;
	fLeftRightMix	= 0.5f;

	dAzimuthLeft	= 1.0;		// due front
	dElevationLeft	= 0.5;		// due front
	dZerothLeft 	= 0.707;
	dFirstLeft 		= 1.0;
	dSecondLeft 	= 1.0;
	dThirdtLeft 	= 1.0;

	dDistanceLeft 	= 1.0;
	fDistanceLeft 	= 0.1f;
	dCentreLeft		= 0.15;		// notional centre area

	dDistanceFactorLeft = 0.75;	// -4.5 dB for each doubling of distance (-6 * 0.25)

	dVolumeLeft 	= k6dBMyPoint;// allows for left+right without overload

	dScalingLeft	= 10.0;
	fScalingLeft	= 0.1f;
	dWetDry		= 0.5;	//normal operations
	fWetDry		= 0.5f;

	fRoomWallReflectivity	= 0.0f;	// test only
	dRoomWallReflectivity	= 0.0;
	fRoomWallTransmisivity	= 0.0f;
	dRoomWallTransmisivity	= 0.0;
	fRoomWallReflectionSharpness	= 1.0f;
	dRoomWallReflectionSharpness	= 1.0;

	fRoomWallReflectivityFilterMode = 0.0f;
	lRoomWallReflectivityFilterMode = 0;
	fRoomWallReflectivityFilterFrequency = 0.001f;
	dRoomWallReflectivityFilterFrequency = 1000.0;
	fRoomWallReflectivityFilterQ = 0.5f;
	dRoomWallReflectivityFilterQ = 0.5;
	fRoomWallReflectivityFilterGain	= 0.25f;
	dRoomWallReflectivityFilterGain = 16 * 0.25;

	fRoomWallTransmisivityFilterMode = 0.0f;
	lRoomWallTransmisivityFilterMode = 0;
	fRoomWallTransmisivityFilterFrequency = 0.001f;
	dRoomWallTransmisivityFilterFrequency = 1000.0f;
	fRoomWallTransmisivityFilterQ = 0.5f;
	dRoomWallTransmisivityFilterQ = 0.5;
	fRoomWallTransmisivityFilterGain = 0.25f;
	dRoomWallTransmisivityFilterGain =  16 * 0.0001;	// gainLaw result for .25


	fRoomLength		= 0.5f;
	dRoomLength		= 5.0;
	fRoomWidth		= 0.4f;
	dRoomWidth		= 4.0;
	fRoomHeight		= 0.3f;
	dRoomHeight		= 3.0;

	fListenerX		= 0.5f;
	dListenerX		= 0.5;
	fListenerY		= 0.5f;
	dListenerY		= 0.5;
	fListenerZ		= 0.5f;
	dListenerZ		= 0.5;

	DirectLeft.dAzimuth		= 1.0;		// due front
	DirectLeft.dElevation	= 0.5;		// due front
	DirectLeft.dZeroth 		= 0.707;
	DirectLeft.dFirst 		= 1.0f;
	DirectLeft.dSecond 		= 1.0f;
	DirectLeft.dThird 		= 1.0f;
	DirectLeft.dDistance 	= 1.0f;
	DirectLeft.dCentre		= 0.15;		// notional centre area
	DirectLeft.dDistanceFactor = 0.75;	// -4.5 dB for each doubling of distance (-6 * 0.25)
	DirectLeft.dVolume 		= k6dBMyPoint;// allows for left+right without overload

	DirectLeft.dW		= 0.0;
	DirectLeft.dX		= 0.0;
	DirectLeft.dY		= 0.0;
	DirectLeft.dZ		= 0.0;
	DirectLeft.dWOld	= 0.0;
	DirectLeft.dXOld	= 0.0;
	DirectLeft.dYOld	= 0.0;
	DirectLeft.dZOld	= 0.0;


	strcpy (name,"Reflector");
}

//-----------------------------------------------------------------------------

void Reflector::init ()
{

	strcpy(sComplexityStrings[kNoReflectionsFxx],"Pan only");
	strcpy(sComplexityStrings[kNoReflectionsFHH],"FHH Pan");
	strcpy(sComplexityStrings[kFxxFxx],"Fxx Fxx");
	strcpy(sComplexityStrings[kFHHFxx],"FHH Fxx");
	strcpy(sComplexityStrings[kFHHFHH],"FHH FHH");

	W_channel.elevation = 0;				// rad		-PI/2...PI/2	10.f for LFE channel
	W_channel.radius = 0;					// meter					0.f for LFE channel
	strcpy(W_channel.name, "W channel");	// for new setups, new names should be given (L/R/C... won't do)
	W_channel.type = kSpeakerUndefined;		// speaker type

	X_channel.elevation = 0;				// rad		-PI/2...PI/2	10.f for LFE channel
	X_channel.radius = 0;					// meter					0.f for LFE channel
	strcpy(X_channel.name, "X_channel");	// for new setups, new names should be given (L/R/C... won't do)
	X_channel.type = kSpeakerUndefined;		// speaker type

	Y_channel.elevation = 0;				// rad		-PI/2...PI/2	10.f for LFE channel
	Y_channel.radius = 0;					// meter					0.f for LFE channel
	strcpy(Y_channel.name, "Y_channel");	// for new setups, new names should be given (L/R/C... won't do)
	Y_channel.type = kSpeakerUndefined;		// speaker type

	Z_channel.elevation = 0;				// rad		-PI/2...PI/2	10.f for LFE channel
	Z_channel.radius = 0;					// meter					0.f for LFE channel
	strcpy(Z_channel.name, "Z_channel");	// for new setups, new names should be given (L/R/C... won't do)
	Z_channel.type = kSpeakerUndefined;		// speaker type

	bFormatFirstOrder.type = kSpeakerArrUserDefined;	// (was float lfeGain; // LFE channel gain is adjusted [dB] higher than other channels)
	bFormatFirstOrder.numChannels = lNumOutputs;		// number of channels in this speaker arrangement
	bFormatFirstOrder.speakers[0] = W_channel;
	bFormatFirstOrder.speakers[1] = X_channel;
	bFormatFirstOrder.speakers[2] = Y_channel;
	bFormatFirstOrder.speakers[3] = Z_channel;


	dLeftRightMix	= 0.5;
	fLeftRightMix	= 0.5f;
	dAzimuthLeft	= 1.0;		// due front
	dElevationLeft	= 0.5;		// due front
	dZerothLeft 	= 0.707;
	dFirstLeft 		= 1.0;
	dSecondLeft 	= 1.0;
	dThirdtLeft 	= 1.0;
	fDistanceLeft 	= 0.1f;
	dDistanceLeft 	= 1.0;
	dCentreLeft		= 0.15;		// notional centre area
	dDistanceFactorLeft = 0.75;	// -4.5 dB for each doubling of distance (-6 * 0.25)
	dVolumeLeft 	= k6dBMyPoint;// allows for left+right without overload
	dScalingLeft	= 10.0;
	fScalingLeft	= 0.1f;
	dWetDry			= 0.5;
	fWetDry			= 0.5f;

	fRoomWallReflectivity	= 0.0f;	// test only
	dRoomWallReflectivity	= 0.0;
	fRoomWallTransmisivity	= 0.0f;
	dRoomWallTransmisivity	= 0.0;
	fRoomWallReflectionSharpness	= 1.0f;
	dRoomWallReflectionSharpness	= 1.0;

	fRoomWallReflectivityFilterMode = 0.0f;
	lRoomWallReflectivityFilterMode = 0;
	fRoomWallReflectivityFilterFrequency = 0.001f;
	dRoomWallReflectivityFilterFrequency = 1000.0;
	fRoomWallReflectivityFilterQ = 0.5f;
	dRoomWallReflectivityFilterQ = 0.5;
	fRoomWallReflectivityFilterGain	= 0.25f;
	dRoomWallReflectivityFilterGain =  16 * 0.0001;	// gainLaw result for .25

	fRoomWallTransmisivityFilterMode = 0.0f;
	lRoomWallTransmisivityFilterMode = 0;
	fRoomWallTransmisivityFilterFrequency = 0.001f;
	dRoomWallTransmisivityFilterFrequency = 1000.0;
	fRoomWallTransmisivityFilterQ = 0.5f;
	dRoomWallTransmisivityFilterQ = 0.5;
	fRoomWallTransmisivityFilterGain = 0.25f;
	dRoomWallTransmisivityFilterGain =  16 * 0.0001;	// gainLaw result for .25

	fRoomLength		= 0.5f;
	dRoomLength		= 5.0;
	fRoomWidth		= 0.4f;
	dRoomWidth		= 4.0;
	fRoomHeight		= 0.3f;
	dRoomHeight		= 3.0;

	fListenerX		= 0.5f;
	dListenerX		= 0.5;
	fListenerY		= 0.5f;
	dListenerY		= 0.5;
	fListenerZ		= 0.5f;
	dListenerZ		= 0.5;

	dRoomOffsetLeftY	= 0.5;
	dRoomOffsetRightY	= 0.5;
	dRoomOffsetUpZ		= 0.5;
	dRoomOffsetDownZ	= 0.5;
	dRoomOffsetFrontX	= 0.5;
	dRoomOffsetBackX	= 0.5;

	DirectLeft.dAzimuth		= 1.0f;		// due front
	DirectLeft.dElevation	= 0.5;		// due front
	DirectLeft.dZeroth 		= 0.707;
	DirectLeft.dFirst 		= 1.0f;
	DirectLeft.dSecond 		= 1.0f;
	DirectLeft.dThird 		= 1.0f;
	DirectLeft.dDistance	= 1.0f;
	DirectLeft.dCentre		= 0.15;		// notional centre area
	DirectLeft.dDistanceFactor = 0.75;	// -4.5 dB for each doubling of distance (-6 * 0.25)
	DirectLeft.dVolume 		= k6dBMyPoint;// allows for left+right without overload

	DirectLeft.dW		= 0.0f;
	DirectLeft.dX		= 0.0f;
	DirectLeft.dY		= 0.0f;
	DirectLeft.dZ		= 0.0f;
	DirectLeft.dU		= 0.0f;
	DirectLeft.dV		= 0.0f;
	DirectLeft.dP		= 0.0f;
	DirectLeft.dQ		= 0.0f;
	DirectLeft.dWOld	= 0.0f;
	DirectLeft.dXOld	= 0.0f;
	DirectLeft.dYOld	= 0.0f;
	DirectLeft.dZOld	= 0.0f;
	DirectLeft.dUInc	= 0.0f;
	DirectLeft.dVInc	= 0.0f;
	DirectLeft.dPInc	= 0.0f;
	DirectLeft.dQInc	= 0.0f;

	BufferLeft = NULL;
	ControlBufferLeft = NULL;

SourcePosition initialPosition;
DirectLeft.setCurrentPosition(initialPosition);
DirectLeft.setEndPosition(initialPosition);
/*
cRoom.walls[kleftWall].setReflectivity(0.1);
cRoom.walls[krightWall].setReflectivity(0.2);
cRoom.walls[kbackWall].setReflectivity(0.3);
cRoom.walls[kfrontWall].setReflectivity(0.4);
cRoom.walls[kCeiling].setReflectivity(0.5);
cRoom.walls[kFloor].setReflectivity(0.6);
*/
cRoom.walls[kleftWall].setReflectivity(0.0);
cRoom.walls[krightWall].setReflectivity(0.0);
cRoom.walls[kbackWall].setReflectivity(0.0);
cRoom.walls[kfrontWall].setReflectivity(0.0);
cRoom.walls[kCeiling].setReflectivity(0.0);
cRoom.walls[kFloor].setReflectivity(0.0);
listener.setXYZV(0,0,0,1);	// listener is currently always at the coordinate origin

LeftReflectionRoom.setWall(cRoom.walls[kleftWall]);
RightReflectionRoom.setWall(cRoom.walls[krightWall]);
BackReflectionRoom.setWall(cRoom.walls[kbackWall]);
FrontReflectionRoom.setWall(cRoom.walls[kfrontWall]);
CeilingReflectionRoom.setWall(cRoom.walls[kCeiling]);
FloorReflectionRoom.setWall(cRoom.walls[kFloor]);

LeftReflectionRoom.setCurrentPosition(initialPosition);
RightReflectionRoom.setCurrentPosition(initialPosition);
BackReflectionRoom.setCurrentPosition(initialPosition);
FrontReflectionRoom.setCurrentPosition(initialPosition);
CeilingReflectionRoom.setCurrentPosition(initialPosition);
FloorReflectionRoom.setCurrentPosition(initialPosition);

LeftReflectionRoom.setEndPosition(initialPosition);
RightReflectionRoom.setEndPosition(initialPosition);
BackReflectionRoom.setEndPosition(initialPosition);
FrontReflectionRoom.setEndPosition(initialPosition);
CeilingReflectionRoom.setEndPosition(initialPosition);
FloorReflectionRoom.setEndPosition(initialPosition);



		TempSpectrum.setUp(300.0, 0.25, 0.5, kFilterLowShelf );


		LeftReflectionRoom.wallReflection.setSpectrum(kFrontBackSpectrum, TempSpectrum) ;
		RightReflectionRoom.wallReflection.setSpectrum(kFrontBackSpectrum, TempSpectrum);
		FrontReflectionRoom.wallReflection.setSpectrum(kFrontBackSpectrum, TempSpectrum);
		BackReflectionRoom.wallReflection.setSpectrum(kFrontBackSpectrum, TempSpectrum);
		CeilingReflectionRoom.wallReflection.setSpectrum(kFrontBackSpectrum, TempSpectrum);
		FloorReflectionRoom.wallReflection.setSpectrum(kFrontBackSpectrum, TempSpectrum);

		LeftReflectionRoom.wallReflection.setSpectrum(kLeftRightSpectrum, TempSpectrum);
		RightReflectionRoom.wallReflection.setSpectrum(kLeftRightSpectrum, TempSpectrum);
		FrontReflectionRoom.wallReflection.setSpectrum(kLeftRightSpectrum, TempSpectrum);
		BackReflectionRoom.wallReflection.setSpectrum(kLeftRightSpectrum, TempSpectrum);
		CeilingReflectionRoom.wallReflection.setSpectrum(kLeftRightSpectrum, TempSpectrum);
		FloorReflectionRoom.wallReflection.setSpectrum(kLeftRightSpectrum, TempSpectrum);

		LeftReflectionRoom.wallReflection.setSpectrum(kUpDownSpectrum, TempSpectrum);
		RightReflectionRoom.wallReflection.setSpectrum(kUpDownSpectrum, TempSpectrum);
		FrontReflectionRoom.wallReflection.setSpectrum(kUpDownSpectrum, TempSpectrum);
		BackReflectionRoom.wallReflection.setSpectrum(kUpDownSpectrum, TempSpectrum);
		CeilingReflectionRoom.wallReflection.setSpectrum(kUpDownSpectrum, TempSpectrum);
		FloorReflectionRoom.wallReflection.setSpectrum(kUpDownSpectrum, TempSpectrum);



	BufferLeft 	= NULL;	// don't exist when it starts up!

}

//-----------------------------------------------------------------------------

Reflector::Reflector(audioMasterCallback audioMaster)
	: AudioEffectX(audioMaster, 32,kNumParams)
{

	lHostNo = 0;

	programs = new ReflectorProgram[numPrograms];

	if (programs)
		setProgram (0);

	lNumOutputs = kNumOutputs;		// we can set this up to automatically be eight out for Nuendo v1,
									// 12 out for Nuendo 2 and 16 out for all others if we want to

	setNumInputs(kNumInputs);		// two in
	setNumOutputs(lNumOutputs);

	setUniqueID((long)"d392");	// identify as standalone effect version

	canMono();				// makes sense to feed both inputs with the same signal
	canProcessReplacing();	// supports both accumulating and replacing output
	strcpy(programName, "Default");	// default program name
        init();


//	suspend ();		// flush buffer
	resume();

// test section only
/*ParametricEq testPareq;
testPareq.setSampleRate(dSampleRate);
testPareq.setUp(2000.0, 0.0, 0.5, 1);
LeftReflectionRoom.wallReflection.setReflectivitySpectrum(testPareq);
*/
}

//-----------------------------------------------------------------------------------------

Reflector::~Reflector()
{
	if (programs)
		delete[] programs;


}

//------------------------------------------------------------------------

 void Reflector::setProgram (long program)
{
	ReflectorProgram * ap = &programs[program];

	curProgram = program;

		{
		setParameter (kLeftRightMix, float (ap->fLeftRightMix));
		setParameter (kAzimuthLeft, float (ap->DirectLeft.EndPosition.getAzimuth()));
		setParameter (kElevationLeft, float (ap->DirectLeft.EndPosition.getElevation()));
		setParameter (kVolumeLeft, float (ap->DirectLeft.EndPosition.getVolume()));
		setParameter (kDistanceLeft, float ((ap->DirectLeft.EndPosition.getDistance() / dScalingLeft)));
		setParameter (kCentreLeft, float (ap->DirectLeft.EndPosition.getCentre()));
		setParameter (kDistanceFactorLeft, float (ap->DirectLeft.EndPosition.getDistanceFactor()));
		setParameter (kZerothLeft, float (ap->DirectLeft.EndPosition.getZeroth()));
		setParameter (kFirstLeft, float (ap->DirectLeft.EndPosition.getFirst()));
		setParameter (kSecondLeft, float (ap->DirectLeft.EndPosition.getSecond()));
		setParameter (kThirdLeft, float (ap->DirectLeft.EndPosition.getThird()));
		setParameter (kScalingLeft, float (ap->fScalingLeft));
		setParameter (kWetDry, float (ap->fWetDry));
		setParameter (kRoomWallReflectivity, float (ap->fRoomWallReflectivity));
		setParameter (kRoomWallTransmisivity, float (ap->fRoomWallTransmisivity));
		setParameter (kRoomWallReflectionSharpness, float (ap->fRoomWallReflectionSharpness));
		setParameter (kRoomWallReflectivityFilterMode, float (ap->fRoomWallReflectivityFilterMode));
		setParameter (kRoomWallReflectivityFilterFrequency, float (ap->fRoomWallReflectivityFilterFrequency));
		setParameter (kRoomWallReflectivityFilterQ, float (ap->fRoomWallReflectivityFilterQ));
		setParameter (kRoomWallReflectivityFilterGain, float (ap->fRoomWallReflectivityFilterGain));
		setParameter (kRoomWallReflectionLFCutoff, float (ap->fRoomWallReflectionLFCutoff));
		setParameter (kRoomWallReflectionLFLoss, float (ap->fRoomWallReflectionLFLoss));
		setParameter (kRoomWallTransmisivityFilterMode, float (ap->fRoomWallTransmisivityFilterMode));
		setParameter (kRoomWallTransmisivityFilterFrequency, float (ap->fRoomWallTransmisivityFilterFrequency));
		setParameter (kRoomWallTransmisivityFilterQ, float (ap->fRoomWallTransmisivityFilterQ));
		setParameter (kRoomWallTransmisivityFilterGain, float (ap->fRoomWallTransmisivityFilterGain));
		setParameter (kRoomLength, float (ap->fRoomLength));
		setParameter (kRoomWidth, float (ap->fRoomWidth));
		setParameter (kRoomHeight, float (ap->fRoomHeight));
		setParameter (kListenerX, float (ap->fListenerX));
		setParameter (kListenerY, float (ap->fListenerY));
		setParameter (kListenerZ, float (ap->fListenerZ));


	}

}


//-----------------------------------------------------------------------------------------

 void Reflector::setParameter(long index, float value)
{

double freq;

updateRoom = false;
DirectLeft.lockSection = LeftReflectionRoom.lockSection = RightReflectionRoom.lockSection = BackReflectionRoom.lockSection = FrontReflectionRoom.lockSection = CeilingReflectionRoom.lockSection =	FloorReflectionRoom.lockSection = true;		// audio processing shouldn't pickup new values till this is finished
							// we need to insert checks here to stop multiple assertions
// 	ReflectorProgram * ap = &programs[curProgram];
	switch (index)
		{
		case kLeftRightMix	: 	fLeftRightMix = value;
								dLeftRightMix = MyDouble (fLeftRightMix);
								DirectLeft.setLeftRightMix(dLeftRightMix);
								break;

								// note 1) The Reflector Pluggin originally had stereo inputs but the right channel
								// was removed to easy performance problems during development, so this mix option
								// was added and the right channel removed in the code. Hence it is that we
								// only have, for instance, kAzimuthLeft and no kAzimuthRight at present
								// Do not confuse these with things like LeftReflectionRoom, which refer to the
								// room, not the input channel. In general, names to do with the left input channel
								// finish with Left and those to do with the left wall start with Left
								// note 2) The wall designations will have to change when we move away from cuboid rooms

		case kAzimuthLeft	:	DirectLeft.EndPosition.setAzimuth(1 - value);

								updateRoom = true;
								break;

		case kElevationLeft :	DirectLeft.EndPosition.setElevation(value);

								updateRoom = true;
								break;

		case kVolumeLeft 	:	DirectLeft.EndPosition.setVolume(value);			// note here that all wall parameters are separate
								LeftReflectionRoom.EndPosition.setVolume(value);	// but are controlled together to limit the numbers of sliders on
								RightReflectionRoom.EndPosition.setVolume(value);	// screen at any one time. With the gui this should change
								BackReflectionRoom.EndPosition.setVolume(value);	// to individually programmable
								FrontReflectionRoom.EndPosition.setVolume(value);	// this approach is taken with other parameters as well
								CeilingReflectionRoom.EndPosition.setVolume(value);
								FloorReflectionRoom.EndPosition.setVolume(value);

								break;

		case kDistanceLeft	: 	if (value < 0.00001) {
									value = 0.00001f;
									}
								fDistanceLeft = value;
								DirectLeft.EndPosition.setDistance(value * dScalingLeft);
								updateRoom = true;

								break;
		case kWetDry	: 		fWetDry = value;
								dWetDry = MyDouble (value);
								DirectLeft.setWetDryMix(dWetDry);
								LeftReflectionRoom.setWetDryMix(dWetDry);
								RightReflectionRoom.setWetDryMix(dWetDry);
								BackReflectionRoom.setWetDryMix(dWetDry);
								FrontReflectionRoom.setWetDryMix(dWetDry);
								CeilingReflectionRoom.setWetDryMix(dWetDry);
								FloorReflectionRoom.setWetDryMix(dWetDry);

								break;
		case kScalingLeft	: 	if (value < 0.00001) {
									value = 0.00001f;
									}
								fScalingLeft = value;
								dScalingLeft = 100 * (MyDouble)fScalingLeft;

								updateRoom = true;

								break;

		case	kRoomLength		:	fRoomLength = value;
										updateRoom = true;
										break;
		case	kRoomWidth		:	fRoomWidth = value;
										updateRoom = true;
										break;
		case	kRoomHeight		:	fRoomHeight = value;

										updateRoom = true;
										break;
		case	kListenerX		:	fListenerX = value;
										updateRoom = true;
										break;
		case	kListenerY		:	fListenerY = value;
										updateRoom = true;
										break;
		case	kListenerZ		:	fListenerZ = value;
										updateRoom = true;
										break;


		case kDistanceFactorLeft : 	DirectLeft.EndPosition.setDistanceFactor(value);
									LeftReflectionRoom.EndPosition.setDistanceFactor(value);
									RightReflectionRoom.EndPosition.setDistanceFactor(value);
									BackReflectionRoom.EndPosition.setDistanceFactor(value);
									FrontReflectionRoom.EndPosition.setDistanceFactor(value);
									CeilingReflectionRoom.EndPosition.setDistanceFactor(value);
									FloorReflectionRoom.EndPosition.setDistanceFactor(value);
									break;

		case kCentreLeft	: 	if (value < 0.001)
									{
									value =0.001f;
									}
								DirectLeft.EndPosition.setCentre(value);
								LeftReflectionRoom.EndPosition.setCentre(value);
								RightReflectionRoom.EndPosition.setCentre(value);
								BackReflectionRoom.EndPosition.setCentre(value);
								FrontReflectionRoom.EndPosition.setCentre(value);
								CeilingReflectionRoom.EndPosition.setCentre(value);
								FloorReflectionRoom.EndPosition.setCentre(value);
								break;

		case kZerothLeft	: 	DirectLeft.EndPosition.setZeroth(value);
								LeftReflectionRoom.EndPosition.setZeroth(value);
								RightReflectionRoom.EndPosition.setZeroth(value);
								BackReflectionRoom.EndPosition.setZeroth(value);
								FrontReflectionRoom.EndPosition.setZeroth(value);
								CeilingReflectionRoom.EndPosition.setZeroth(value);
								FloorReflectionRoom.EndPosition.setZeroth(value);
								break;

		case kFirstLeft		: 	DirectLeft.EndPosition.setFirst(value);
								LeftReflectionRoom.EndPosition.setFirst(value);
								RightReflectionRoom.EndPosition.setFirst(value);
								BackReflectionRoom.EndPosition.setFirst(value);
								FrontReflectionRoom.EndPosition.setFirst(value);
								CeilingReflectionRoom.EndPosition.setFirst(value);
								FloorReflectionRoom.EndPosition.setFirst(value);
								break;

		case kSecondLeft		: 	DirectLeft.EndPosition.setSecond(value);
								LeftReflectionRoom.EndPosition.setSecond(value);
								RightReflectionRoom.EndPosition.setSecond(value);
								BackReflectionRoom.EndPosition.setSecond(value);
								FrontReflectionRoom.EndPosition.setSecond(value);
								CeilingReflectionRoom.EndPosition.setSecond(value);
								FloorReflectionRoom.EndPosition.setSecond(value);
								break;

		case kThirdLeft		: 	DirectLeft.EndPosition.setThird(value);
								LeftReflectionRoom.EndPosition.setThird(value);
								RightReflectionRoom.EndPosition.setThird(value);
								BackReflectionRoom.EndPosition.setThird(value);
								FrontReflectionRoom.EndPosition.setThird(value);
								CeilingReflectionRoom.EndPosition.setThird(value);
								FloorReflectionRoom.EndPosition.setThird(value);
								break;



		case kRoomWallReflectivity	:		fRoomWallReflectivity = value;
											dRoomWallReflectivity = (MyDouble) value;

											LeftReflectionRoom.wallReflection.setReflectivity(dRoomWallReflectivity);
											RightReflectionRoom.wallReflection.setReflectivity(dRoomWallReflectivity);
											FrontReflectionRoom.wallReflection.setReflectivity(dRoomWallReflectivity);
											BackReflectionRoom.wallReflection.setReflectivity(dRoomWallReflectivity);
											CeilingReflectionRoom.wallReflection.setReflectivity(dRoomWallReflectivity);
											FloorReflectionRoom.wallReflection.setReflectivity(dRoomWallReflectivity);

											break;

		case kRoomWallReflectivityFilterMode	:
											fRoomWallReflectivityFilterMode = value;
											lRoomWallReflectivityFilterMode = long ((value + 0.16) * 3);

											LeftReflectionRoom.wallReflection.setSpectrumMode(kReflectivitySpectrum, lRoomWallReflectivityFilterMode);
											RightReflectionRoom.wallReflection.setSpectrumMode(kReflectivitySpectrum, lRoomWallReflectivityFilterMode);
											FrontReflectionRoom.wallReflection.setSpectrumMode(kReflectivitySpectrum, lRoomWallReflectivityFilterMode);
											BackReflectionRoom.wallReflection.setSpectrumMode(kReflectivitySpectrum, lRoomWallReflectivityFilterMode);
											CeilingReflectionRoom.wallReflection.setSpectrumMode(kReflectivitySpectrum, lRoomWallReflectivityFilterMode);
											FloorReflectionRoom.wallReflection.setSpectrumMode(kReflectivitySpectrum, lRoomWallReflectivityFilterMode);

											break;
		case kRoomWallReflectivityFilterFrequency	:
											if (value < .001 )		// I'm not at all sure that this is the best way of dealing with varying filter parameters
													value = 0.001f;
											fRoomWallReflectivityFilterFrequency = value;
											dRoomWallReflectivityFilterFrequency = double ( 16 + ((pow(10,value) - 1.0f) * 1776.0));	// this gives a range of 20 Hz to 16kHz, with a nice law

											LeftReflectionRoom.wallReflection.setSpectrumCutOffFreq(kReflectivitySpectrum, dRoomWallReflectivityFilterFrequency);
											RightReflectionRoom.wallReflection.setSpectrumCutOffFreq(kReflectivitySpectrum, dRoomWallReflectivityFilterFrequency);
											FrontReflectionRoom.wallReflection.setSpectrumCutOffFreq(kReflectivitySpectrum, dRoomWallReflectivityFilterFrequency);
											BackReflectionRoom.wallReflection.setSpectrumCutOffFreq(kReflectivitySpectrum, dRoomWallReflectivityFilterFrequency);
											CeilingReflectionRoom.wallReflection.setSpectrumCutOffFreq(kReflectivitySpectrum, dRoomWallReflectivityFilterFrequency);
											FloorReflectionRoom.wallReflection.setSpectrumCutOffFreq(kReflectivitySpectrum, dRoomWallReflectivityFilterFrequency);

											break;
		case kRoomWallReflectivityFilterGain	:
											fRoomWallReflectivityFilterGain = value;
											dRoomWallReflectivityFilterGain = 16 * gainLaw(MyDouble (value));

											LeftReflectionRoom.wallReflection.setSpectrumGain(kReflectivitySpectrum, dRoomWallReflectivityFilterGain);
											RightReflectionRoom.wallReflection.setSpectrumGain(kReflectivitySpectrum, dRoomWallReflectivityFilterGain);
											FrontReflectionRoom.wallReflection.setSpectrumGain(kReflectivitySpectrum, dRoomWallReflectivityFilterGain);
											BackReflectionRoom.wallReflection.setSpectrumGain(kReflectivitySpectrum, dRoomWallReflectivityFilterGain);
											CeilingReflectionRoom.wallReflection.setSpectrumGain(kReflectivitySpectrum, dRoomWallReflectivityFilterGain);
											FloorReflectionRoom.wallReflection.setSpectrumGain(kReflectivitySpectrum, dRoomWallReflectivityFilterGain);

											break;
		case kRoomWallReflectivityFilterQ	:
											if (value < .01 )
												value = 0.01f;
											fRoomWallReflectivityFilterQ = value;
											dRoomWallReflectivityFilterQ = (MyDouble) value;

											LeftReflectionRoom.wallReflection.setSpectrumQ(kReflectivitySpectrum, dRoomWallReflectivityFilterQ);
											RightReflectionRoom.wallReflection.setSpectrumQ(kReflectivitySpectrum, dRoomWallReflectivityFilterQ);
											FrontReflectionRoom.wallReflection.setSpectrumQ(kReflectivitySpectrum, dRoomWallReflectivityFilterQ);
											BackReflectionRoom.wallReflection.setSpectrumQ(kReflectivitySpectrum, dRoomWallReflectivityFilterQ);
											CeilingReflectionRoom.wallReflection.setSpectrumQ(kReflectivitySpectrum, dRoomWallReflectivityFilterQ);
											FloorReflectionRoom.wallReflection.setSpectrumQ(kReflectivitySpectrum, dRoomWallReflectivityFilterQ);

											break;

		case kRoomWallReflectionLFCutoff
										:	fRoomWallReflectionLFCutoff = value;	// controls transition between point and diffuse reflection
											dRoomWallReflectionLFCutoff = double ( (value * 1.5f) + 0.5f);	// this gives a range of 1 octave down to 1 octave up
											updateRoom = true;
											break;

		case kRoomWallReflectionLFLoss
										:	fRoomWallReflectionLFLoss = value;		// which 'models' LF room mode
											dRoomWallReflectionLFLoss = 16 * gainLaw(MyDouble (value));
											updateRoom = true;
											break;

		case kRoomWallTransmisivity	:	fRoomWallTransmisivity = value;
											dRoomWallTransmisivity = (MyDouble) value;
											DirectLeft.setLoss(value); // very temporary

											LeftReflectionRoom.wallReflection.setTransmisivity(dRoomWallTransmisivity);
											RightReflectionRoom.wallReflection.setTransmisivity(dRoomWallTransmisivity);
											FrontReflectionRoom.wallReflection.setTransmisivity(dRoomWallTransmisivity);
											BackReflectionRoom.wallReflection.setTransmisivity(dRoomWallTransmisivity);
											CeilingReflectionRoom.wallReflection.setTransmisivity(dRoomWallTransmisivity);
											FloorReflectionRoom.wallReflection.setTransmisivity(dRoomWallTransmisivity);

											break;


											break;

		case kRoomWallTransmisivityFilterMode	:
											fRoomWallTransmisivityFilterMode = value;
											lRoomWallTransmisivityFilterMode = long ((value + 0.16) * 3);

											LeftReflectionRoom.wallReflection.setSpectrumMode(kTransmisivitySpectrum, lRoomWallTransmisivityFilterMode);
											RightReflectionRoom.wallReflection.setSpectrumMode(kTransmisivitySpectrum, lRoomWallTransmisivityFilterMode);
											FrontReflectionRoom.wallReflection.setSpectrumMode(kTransmisivitySpectrum, lRoomWallTransmisivityFilterMode);
											BackReflectionRoom.wallReflection.setSpectrumMode(kTransmisivitySpectrum, lRoomWallTransmisivityFilterMode);
											CeilingReflectionRoom.wallReflection.setSpectrumMode(kTransmisivitySpectrum, lRoomWallTransmisivityFilterMode);
											FloorReflectionRoom.wallReflection.setSpectrumMode(kTransmisivitySpectrum, lRoomWallTransmisivityFilterMode);

											break;
		case kRoomWallTransmisivityFilterFrequency	:
											if (value < .001 )
													value = 0.001f;
											fRoomWallTransmisivityFilterFrequency = value;
											dRoomWallTransmisivityFilterFrequency = double ( 16 + ((pow(10,value) - 1.0f) * 1776.0));	// this gives a range of 20 Hz to 16kHz, with a nice law

											LeftReflectionRoom.wallReflection.setSpectrumCutOffFreq(kTransmisivitySpectrum, dRoomWallTransmisivityFilterFrequency);
											RightReflectionRoom.wallReflection.setSpectrumCutOffFreq(kTransmisivitySpectrum, dRoomWallTransmisivityFilterFrequency);
											FrontReflectionRoom.wallReflection.setSpectrumCutOffFreq(kTransmisivitySpectrum, dRoomWallTransmisivityFilterFrequency);
											BackReflectionRoom.wallReflection.setSpectrumCutOffFreq(kTransmisivitySpectrum, dRoomWallTransmisivityFilterFrequency);
											CeilingReflectionRoom.wallReflection.setSpectrumCutOffFreq(kTransmisivitySpectrum, dRoomWallTransmisivityFilterFrequency);
											FloorReflectionRoom.wallReflection.setSpectrumCutOffFreq(kTransmisivitySpectrum, dRoomWallTransmisivityFilterFrequency);

											break;
		case kRoomWallTransmisivityFilterGain	:
											fRoomWallTransmisivityFilterGain = value;
											dRoomWallTransmisivityFilterGain = 16 * gainLaw(MyDouble (value));

											LeftReflectionRoom.wallReflection.setSpectrumGain(kTransmisivitySpectrum, dRoomWallTransmisivityFilterGain);
											RightReflectionRoom.wallReflection.setSpectrumGain(kTransmisivitySpectrum, dRoomWallTransmisivityFilterGain);
											FrontReflectionRoom.wallReflection.setSpectrumGain(kTransmisivitySpectrum, dRoomWallTransmisivityFilterGain);
											BackReflectionRoom.wallReflection.setSpectrumGain(kTransmisivitySpectrum, dRoomWallTransmisivityFilterGain);
											CeilingReflectionRoom.wallReflection.setSpectrumGain(kTransmisivitySpectrum, dRoomWallTransmisivityFilterGain);
											FloorReflectionRoom.wallReflection.setSpectrumGain(kTransmisivitySpectrum, dRoomWallTransmisivityFilterGain);

											break;
		case kRoomWallTransmisivityFilterQ	:
											if (value < .01 )
												value = 0.01f;
											fRoomWallTransmisivityFilterQ = value;
											dRoomWallTransmisivityFilterQ = (MyDouble) value;

											LeftReflectionRoom.wallReflection.setSpectrumQ(kTransmisivitySpectrum, dRoomWallTransmisivityFilterQ);
											RightReflectionRoom.wallReflection.setSpectrumQ(kTransmisivitySpectrum, dRoomWallTransmisivityFilterQ);
											FrontReflectionRoom.wallReflection.setSpectrumQ(kTransmisivitySpectrum, dRoomWallTransmisivityFilterQ);
											BackReflectionRoom.wallReflection.setSpectrumQ(kTransmisivitySpectrum, dRoomWallTransmisivityFilterQ);
											CeilingReflectionRoom.wallReflection.setSpectrumQ(kTransmisivitySpectrum, dRoomWallTransmisivityFilterQ);
											FloorReflectionRoom.wallReflection.setSpectrumQ(kTransmisivitySpectrum, dRoomWallTransmisivityFilterQ);

											break;



		case kRoomWallReflectionSharpness	:	fRoomWallReflectionSharpness = value;
													dRoomWallReflectionSharpness = (MyDouble)value;
													LeftReflectionRoom.wallReflection.setReflectionSharpness(dRoomWallReflectionSharpness);
													RightReflectionRoom.wallReflection.setReflectionSharpness(dRoomWallReflectionSharpness);
													FrontReflectionRoom.wallReflection.setReflectionSharpness(dRoomWallReflectionSharpness);
													BackReflectionRoom.wallReflection.setReflectionSharpness(dRoomWallReflectionSharpness);
													CeilingReflectionRoom.wallReflection.setReflectionSharpness(dRoomWallReflectionSharpness);
													FloorReflectionRoom.wallReflection.setReflectionSharpness(dRoomWallReflectionSharpness);
													break;
		case kComplexityLevel				:	fComplexityLevel = value;
												lComplexityLevel = (long ((fComplexityLevel * (kNoOfLevels - 1)) + 0.51f));

											break;


					default 				:	break;


	}





	if (updateRoom)
	{
		dRoomLength = fRoomLength * dScalingLeft;
		dRoomOffsetFrontX = (dRoomLength * 0.5) - dListenerX;
		dRoomOffsetBackX = dListenerX - (dRoomLength * 0.5);
		cRoom.setLength(dRoomLength);

// in this mode, source and listener share the same cooridinate frame, which the room is moved around in
		freq = (kSpeedOfSound / (0.0001 + dRoomLength *2)) * dRoomWallReflectionLFCutoff;

		LeftReflectionRoom.wallReflection.setSpectrumCutOffFreq(kFrontBackSpectrum, freq) ;
		RightReflectionRoom.wallReflection.setSpectrumCutOffFreq(kFrontBackSpectrum, freq);
		FrontReflectionRoom.wallReflection.setSpectrumCutOffFreq(kFrontBackSpectrum, freq);
		BackReflectionRoom.wallReflection.setSpectrumCutOffFreq(kFrontBackSpectrum, freq);
		CeilingReflectionRoom.wallReflection.setSpectrumCutOffFreq(kFrontBackSpectrum, freq);
		FloorReflectionRoom.wallReflection.setSpectrumCutOffFreq(kFrontBackSpectrum, freq);

		LeftReflectionRoom.wallReflection.setSpectrumGain(kFrontBackSpectrum, dRoomWallReflectionLFLoss);
		RightReflectionRoom.wallReflection.setSpectrumGain(kFrontBackSpectrum, dRoomWallReflectionLFLoss);
		FrontReflectionRoom.wallReflection.setSpectrumGain(kFrontBackSpectrum, dRoomWallReflectionLFLoss);
		BackReflectionRoom.wallReflection.setSpectrumGain(kFrontBackSpectrum, dRoomWallReflectionLFLoss);
		CeilingReflectionRoom.wallReflection.setSpectrumGain(kFrontBackSpectrum, dRoomWallReflectionLFLoss);
		FloorReflectionRoom.wallReflection.setSpectrumGain(kFrontBackSpectrum, dRoomWallReflectionLFLoss);

		dRoomWidth = fRoomWidth * dScalingLeft;
		dRoomOffsetLeftY = (dRoomWidth * 0.5) - dListenerY;
		dRoomOffsetRightY = dListenerY - (dRoomWidth * 0.5);
		cRoom.setWidth(dRoomWidth);

		freq = (kSpeedOfSound / (0.0001 + dRoomWidth *2)) * dRoomWallReflectionLFCutoff;

		LeftReflectionRoom.wallReflection.setSpectrumCutOffFreq(kLeftRightSpectrum, freq);
		RightReflectionRoom.wallReflection.setSpectrumCutOffFreq(kLeftRightSpectrum, freq);
		FrontReflectionRoom.wallReflection.setSpectrumCutOffFreq(kLeftRightSpectrum, freq);
		BackReflectionRoom.wallReflection.setSpectrumCutOffFreq(kLeftRightSpectrum, freq);
		CeilingReflectionRoom.wallReflection.setSpectrumCutOffFreq(kLeftRightSpectrum, freq);
		FloorReflectionRoom.wallReflection.setSpectrumCutOffFreq(kLeftRightSpectrum, freq);

		LeftReflectionRoom.wallReflection.setSpectrumGain(kLeftRightSpectrum, dRoomWallReflectionLFLoss);
		RightReflectionRoom.wallReflection.setSpectrumGain(kLeftRightSpectrum, dRoomWallReflectionLFLoss);
		FrontReflectionRoom.wallReflection.setSpectrumGain(kLeftRightSpectrum, dRoomWallReflectionLFLoss);
		BackReflectionRoom.wallReflection.setSpectrumGain(kLeftRightSpectrum, dRoomWallReflectionLFLoss);
		CeilingReflectionRoom.wallReflection.setSpectrumGain(kLeftRightSpectrum, dRoomWallReflectionLFLoss);
		FloorReflectionRoom.wallReflection.setSpectrumGain(kLeftRightSpectrum, dRoomWallReflectionLFLoss);

		dRoomHeight = fRoomHeight * dScalingLeft;
		dRoomOffsetUpZ = (dRoomHeight * 0.5) - dListenerZ;
		dRoomOffsetDownZ = dListenerZ - (dRoomHeight * 0.5);
		cRoom.setHeight(dRoomHeight);

		freq = (kSpeedOfSound / (0.0001 + dRoomHeight *2)) * dRoomWallReflectionLFCutoff;

		LeftReflectionRoom.wallReflection.setSpectrumCutOffFreq(kUpDownSpectrum, freq);
		RightReflectionRoom.wallReflection.setSpectrumCutOffFreq(kUpDownSpectrum, freq);
		FrontReflectionRoom.wallReflection.setSpectrumCutOffFreq(kUpDownSpectrum, freq);
		BackReflectionRoom.wallReflection.setSpectrumCutOffFreq(kUpDownSpectrum, freq);
		CeilingReflectionRoom.wallReflection.setSpectrumCutOffFreq(kUpDownSpectrum, freq);
		FloorReflectionRoom.wallReflection.setSpectrumCutOffFreq(kUpDownSpectrum, freq);

		LeftReflectionRoom.wallReflection.setSpectrumGain(kUpDownSpectrum, dRoomWallReflectionLFLoss);
		RightReflectionRoom.wallReflection.setSpectrumGain(kUpDownSpectrum, dRoomWallReflectionLFLoss);
		FrontReflectionRoom.wallReflection.setSpectrumGain(kUpDownSpectrum, dRoomWallReflectionLFLoss);
		BackReflectionRoom.wallReflection.setSpectrumGain(kUpDownSpectrum, dRoomWallReflectionLFLoss);
		CeilingReflectionRoom.wallReflection.setSpectrumGain(kUpDownSpectrum, dRoomWallReflectionLFLoss);
		FloorReflectionRoom.wallReflection.setSpectrumGain(kUpDownSpectrum, dRoomWallReflectionLFLoss);

		dListenerX = (fListenerX - 0.5) * dScalingLeft;	// centre is 0.0
		dRoomOffsetFrontX = (dRoomLength * 0.5) - dListenerX;
		dRoomOffsetBackX = dListenerX - (dRoomLength * 0.5);

		dListenerY = (fListenerY - 0.5) * dScalingLeft;	// centre is 0.0
		dRoomOffsetLeftY = (dRoomWidth * 0.5) - dListenerY;
		dRoomOffsetRightY = dListenerY - (dRoomWidth * 0.5);

		dListenerZ = (fListenerZ - 0.5) * dScalingLeft;	// centre is 0.0
		dRoomOffsetUpZ = (dRoomHeight * 0.5) - dListenerZ;
		dRoomOffsetDownZ = dListenerZ - (dRoomHeight * 0.5);


		DirectLeft.EndPosition.setDistance(fDistanceLeft * dScalingLeft);


		LeftReflectionRoom.EndPosition.translate(0,dRoomOffsetLeftY,0, DirectLeft.EndPosition);
		RightReflectionRoom.EndPosition.translate(0,dRoomOffsetRightY,0, DirectLeft.EndPosition);
		BackReflectionRoom.EndPosition.translate(dRoomOffsetBackX,0,0, DirectLeft.EndPosition);
		FrontReflectionRoom.EndPosition.translate(dRoomOffsetFrontX,0,0, DirectLeft.EndPosition);
		CeilingReflectionRoom.EndPosition.translate(0,0,dRoomOffsetUpZ, DirectLeft.EndPosition);
		FloorReflectionRoom.EndPosition.translate(0,0,dRoomOffsetDownZ, DirectLeft.EndPosition);

		// after we translate positions (which accomodates any degree of non-centrallity of the listener, we need to assess
		// whether the source is outside the room, and have the direct sound filtered accordingly


		insideSource = cRoom.getOutsideOrInside(DirectLeft.EndPosition);
		if (!insideSource)
		{
//			cRoom.getIntersection(DirectLeft.EndPosition.getPosition(), listener);
			DirectLeft.setInsideSource(insideSource, cRoom.hitPoint);
			LeftReflectionRoom.setInsideSource(insideSource, cRoom.hitPoint);
			RightReflectionRoom.setInsideSource(insideSource, cRoom.hitPoint);
			BackReflectionRoom.setInsideSource(insideSource, cRoom.hitPoint);
			FrontReflectionRoom.setInsideSource(insideSource, cRoom.hitPoint);
			CeilingReflectionRoom.setInsideSource(insideSource, cRoom.hitPoint);
			FloorReflectionRoom.setInsideSource(insideSource, cRoom.hitPoint);
			LeftReflectionRoom.HitPosition.translate(0,dRoomOffsetLeftY,0, DirectLeft.HitPosition);
			RightReflectionRoom.HitPosition.translate(0,dRoomOffsetRightY,0, DirectLeft.HitPosition);
			BackReflectionRoom.HitPosition.translate(dRoomOffsetBackX,0,0, DirectLeft.HitPosition);
			FrontReflectionRoom.HitPosition.translate(dRoomOffsetFrontX,0,0, DirectLeft.HitPosition);
			CeilingReflectionRoom.HitPosition.translate(0,0,dRoomOffsetUpZ, DirectLeft.HitPosition);
			FloorReflectionRoom.HitPosition.translate(0,0,dRoomOffsetDownZ, DirectLeft.HitPosition);
		}else{
			DirectLeft.setInsideSource(insideSource);
			LeftReflectionRoom.setInsideSource(insideSource);
			RightReflectionRoom.setInsideSource(insideSource);
			BackReflectionRoom.setInsideSource(insideSource);
			FrontReflectionRoom.setInsideSource(insideSource);
			CeilingReflectionRoom.setInsideSource(insideSource);
			FloorReflectionRoom.setInsideSource(insideSource);
		}

		updateRoom = false;
//		if (editor)
//		{
//			editor->postUpdate ();
//		}
	}
DirectLeft.lockSection = LeftReflectionRoom.lockSection = RightReflectionRoom.lockSection = BackReflectionRoom.lockSection = FrontReflectionRoom.lockSection = CeilingReflectionRoom.lockSection =	FloorReflectionRoom.lockSection = false;
		// audio processing can get on with things, now
}


//-----------------------------------------------------------------------------------------

 float Reflector::getParameter(long index)
{
MyDouble v = 0.0f;
	switch (index)
		{
			case kLeftRightMix	: v = fLeftRightMix; break;
			case kAzimuthLeft	: v = 1 - DirectLeft.EndPosition.getAzimuth(); break;
			case kElevationLeft : v = DirectLeft.EndPosition.getElevation(); break;
			case kVolumeLeft 	: v = DirectLeft.EndPosition.getVolume(); break;
			case kDistanceLeft	: v = DirectLeft.EndPosition.getDistance()/dScalingLeft; break;
			case kDistanceFactorLeft : v = DirectLeft.EndPosition.getDistanceFactor(); break;
			case kCentreLeft	: v = DirectLeft.EndPosition.getCentre(); break;
			case kZerothLeft	: v = DirectLeft.EndPosition.getZeroth(); break;
			case kFirstLeft		: v = DirectLeft.EndPosition.getFirst(); break;
			case kSecondLeft	: v = DirectLeft.EndPosition.getSecond(); break;
			case kThirdLeft		: v = DirectLeft.EndPosition.getThird(); break;
			case kScalingLeft	: v = fScalingLeft; break;
			case kWetDry	: v = fWetDry;	break;

			case kRoomWallReflectivity				:	v = fRoomWallReflectivity; break;
			case kRoomWallReflectivityFilterMode	:	v = fRoomWallReflectivityFilterMode; break;
			case kRoomWallReflectivityFilterFrequency	:	v = fRoomWallReflectivityFilterFrequency; break;
			case kRoomWallReflectivityFilterQ		:	v = fRoomWallReflectivityFilterQ; break;
			case kRoomWallReflectivityFilterGain	:	v = fRoomWallReflectivityFilterGain; break;

			case kRoomWallReflectionSharpness		:	v = fRoomWallReflectionSharpness; break;

			case kRoomWallReflectionLFCutoff		:	v = fRoomWallReflectionLFCutoff; break;
			case kRoomWallReflectionLFLoss			:	v = fRoomWallReflectionLFLoss; break;

			case kRoomWallTransmisivity				:	v = fRoomWallTransmisivity; break;
			case kRoomWallTransmisivityFilterMode	:	v = fRoomWallTransmisivityFilterMode; break;
			case kRoomWallTransmisivityFilterFrequency	:	v = fRoomWallTransmisivityFilterFrequency; break;
			case kRoomWallTransmisivityFilterQ		:	v = fRoomWallTransmisivityFilterQ; break;
			case kRoomWallTransmisivityFilterGain	:	v = fRoomWallTransmisivityFilterGain; break;

			case kRoomLength	:	v = fRoomLength; break;
			case kRoomWidth		:	v = fRoomWidth; break;
			case kRoomHeight	:	v = fRoomHeight; break;
			case kListenerX		:	v = fListenerX; break;
			case kListenerY		:	v = fListenerY; break;
			case kListenerZ		:	v = fListenerZ; break;
			case kComplexityLevel	:	v = fComplexityLevel; break;

			default 				:	 break;

	}
	return (float) v;
}
//-----------------------------------------------------------------------------------------

 void Reflector::getParameterName(long index, char *label)
{
	switch (index)
		{
			case kLeftRightMix	: strcpy (label, "L-R mix"); break;
			case kAzimuthLeft	: strcpy (label, "Azi"); break;
			case kElevationLeft : strcpy (label, "El"); break;
			case kVolumeLeft 	: strcpy (label, "Vol"); break;
			case kDistanceLeft	: strcpy (label, "Dist"); break;
			case kDistanceFactorLeft : strcpy (label, "DFact"); break;
			case kCentreLeft	: strcpy (label, "Centre"); break;
			case kZerothLeft	: strcpy (label, "Zero"); break;
			case kFirstLeft		: strcpy (label, "First"); break;
			case kSecondLeft	: strcpy (label, "Second"); break;
			case kThirdLeft		: strcpy (label, "Third"); break;
			case kScalingLeft	: strcpy (label, "Scaling"); break;
			case kWetDry	: strcpy (label, "Mix Dry-Wet");	break;

			case kRoomWallReflectivity				: strcpy (label, "Reflectivity"); break;
			case kRoomWallReflectivityFilterMode	: strcpy (label, "Filter Mode"); break;
			case kRoomWallReflectivityFilterFrequency	: strcpy (label, "Filter Frequency"); break;
			case kRoomWallReflectivityFilterQ		: strcpy (label, "Filter Q"); break;
			case kRoomWallReflectivityFilterGain	: strcpy (label, "Filter Gain"); break;
			case kRoomWallReflectionSharpness		: strcpy (label, "Sharpness"); break;
			case kRoomWallReflectionLFCutoff		: strcpy (label, "LFCutoff"); break;
			case kRoomWallReflectionLFLoss			: strcpy (label, "LFLoss"); break;


			case kRoomWallTransmisivity				: strcpy (label, "Transmisivity"); break;
			case kRoomWallTransmisivityFilterMode	: strcpy (label, "Filter Mode"); break;
			case kRoomWallTransmisivityFilterFrequency	: strcpy (label, "Filter Frequency"); break;
			case kRoomWallTransmisivityFilterQ		: strcpy (label, "Filter Q"); break;
			case kRoomWallTransmisivityFilterGain	: strcpy (label, "Filter Gain"); break;


/*
			case kRoomWallTransmisivity				: strcpy (label, "N/A"); break;
			case kRoomWallTransmisivityFilterMode	: strcpy (label, "N/A"); break;
			case kRoomWallTransmisivityFilterFrequency	: strcpy (label, "N/A"); break;
			case kRoomWallTransmisivityFilterQ		: strcpy (label, "N/A"); break;
			case kRoomWallTransmisivityFilterGain	: strcpy (label, "N/A"); break;

*/

			case kRoomLength		: strcpy (label, "Room length"); break;
			case kRoomWidth			: strcpy (label, "Room width"); break;
			case kRoomHeight		: strcpy (label, "Room Height"); break;
			case kListenerX			: strcpy (label, "Listener X"); break;
			case kListenerY			: strcpy (label, "Listener Y"); break;
			case kListenerZ			: strcpy (label, "Listener Z"); break;
			case kComplexityLevel	: strcpy (label, "Complexity"); break;

			default 				: strcpy (label, "Unused");break;
	}
}

//-----------------------------------------------------------------------------------------

 void Reflector::getParameterDisplay (long index, char *text)
{

	switch (index)
		{
			case kLeftRightMix	: MyDouble2string ((100 * dLeftRightMix), text); break;
			case kAzimuthLeft	: MyDouble2string (((1.0 - DirectLeft.EndPosition.getAzimuth()) *360), text); break; // convert to degrees before sending back
			case kElevationLeft : MyDouble2string ((DirectLeft.EndPosition.getElevation() - 0.5) * 180, text); break; // convert to degrees before sending back
			case kVolumeLeft	: dB2string ((float) DirectLeft.EndPosition.getVolume(), text); break;
			case kDistanceLeft	: MyDouble2string (DirectLeft.EndPosition.getDistance(), text); break;
			case kDistanceFactorLeft : MyDouble2string ((DirectLeft.EndPosition.getDistanceFactor() * (-6.0)), text); break;
			case kCentreLeft	: MyDouble2string (DirectLeft.EndPosition.getCentre(), text); break;
			case kZerothLeft	: MyDouble2string (DirectLeft.EndPosition.getZeroth() * 100, text); break;
			case kFirstLeft		: MyDouble2string (DirectLeft.EndPosition.getFirst() * 100, text); break;
			case kSecondLeft	: MyDouble2string (DirectLeft.EndPosition.getSecond() * 100, text); break;
			case kThirdLeft		: MyDouble2string (DirectLeft.EndPosition.getThird() * 100, text); break;
			case kScalingLeft	: MyDouble2string (dScalingLeft, text); break;
			case kWetDry		: MyDouble2string (100 * dWetDry, text); break;


			case kRoomWallReflectivity				: MyDouble2string ((100  * dRoomWallReflectivity), text); break;
			case kRoomWallReflectionSharpness		: MyDouble2string ((100  * dRoomWallReflectionSharpness), text); break;
			case kRoomWallReflectivityFilterMode	: switch (lRoomWallReflectivityFilterMode)
																{
																	case kFilterOff		:	strcpy (text, "Off"); break;
																	case kFilterPeaking	:	strcpy (text, "Peaking"); break;
																	case kFilterLowShelf:	strcpy (text, "Low Shelf"); break;
																	case kFilterHighShelf:	strcpy (text, "High Shelf"); break;
																}
															break;
			case kRoomWallReflectivityFilterFrequency	: MyDouble2string (dRoomWallReflectivityFilterFrequency,text); break;
			case kRoomWallReflectivityFilterQ		: MyDouble2string (dRoomWallReflectivityFilterQ,text); break;
			case kRoomWallReflectivityFilterGain	: dB2string ((float)dRoomWallReflectivityFilterGain,text); break;
			case kRoomWallReflectionLFCutoff		: MyDouble2string (dRoomWallReflectionLFCutoff,text); break;
			case kRoomWallReflectionLFLoss			: dB2string ((float)dRoomWallReflectionLFLoss,text); break;

			case kRoomWallTransmisivity				: MyDouble2string ((100  * dRoomWallTransmisivity), text); break;
			case kRoomWallTransmisivityFilterMode	: switch (lRoomWallTransmisivityFilterMode)
																{
																	case kFilterOff		:	strcpy (text, "Off"); break;
																	case kFilterPeaking	:	strcpy (text, "Peaking"); break;
																	case kFilterLowShelf:	strcpy (text, "Low Shelf"); break;
																	case kFilterHighShelf:	strcpy (text, "High Shelf"); break;
																}
															break;
			case kRoomWallTransmisivityFilterFrequency	: MyDouble2string (dRoomWallTransmisivityFilterFrequency,text); break;
			case kRoomWallTransmisivityFilterQ		: MyDouble2string (dRoomWallTransmisivityFilterQ,text); break;
			case kRoomWallTransmisivityFilterGain	: MyDouble2string (dRoomWallTransmisivityFilterGain,text); break;




			case kRoomLength		: MyDouble2string ( dRoomLength, text); break;
			case kRoomWidth			: MyDouble2string ( dRoomWidth, text); break;
			case kRoomHeight		: MyDouble2string ( dRoomHeight, text); break;
			case kListenerX			: MyDouble2string ( dListenerX, text); break;
			case kListenerY			: MyDouble2string ( dListenerY, text); break;
			case kListenerZ			: MyDouble2string ( dListenerZ, text); break;
			case kComplexityLevel	:
											switch (lComplexityLevel)
											{
											case kNoReflectionsFxx	:	strcpy (text, (sComplexityStrings[kNoReflectionsFxx])); break;
												case kNoReflectionsFHH	:	strcpy (text, (sComplexityStrings[kNoReflectionsFHH])); break;
												case kFxxFxx			:	strcpy (text, (sComplexityStrings[kFxxFxx])); break;
												case kFHHFxx			:	strcpy (text, (sComplexityStrings[kFHHFxx])); break;
												case kFHHFHH			:	strcpy (text, (sComplexityStrings[kFHHFHH])); break;
											}
										break;

			default 				: MyDouble2string (0.0f, text); break;

	}

}

//-----------------------------------------------------------------------------------------

 void Reflector::getParameterLabel(long index, char *label)
{

	switch (index)
	{
			case kLeftRightMix	: strcpy (label, "% left"); break;
			case kAzimuthLeft	: strcpy (label, "degrees"); break;
			case kElevationLeft : strcpy (label, "degrees"); break;
			case kVolumeLeft 	: strcpy (label, " dB "); break;
			case kDistanceLeft	: strcpy (label, "metres"); break;
			case kDistanceFactorLeft	: strcpy (label, " dB "); break;
			case kCentreLeft	: strcpy (label, "metres"); break;
			case kZerothLeft	: strcpy (label, "%"); break;
			case kFirstLeft		: strcpy (label, "%"); break;
			case kSecondLeft		: strcpy (label, "%"); break;
			case kThirdLeft		: strcpy (label, "%"); break;
			case kScalingLeft	: strcpy (label, "metres"); break;
			case kWetDry	: strcpy (label, "%"); break;

			case kRoomWallReflectivity				: strcpy (label, "%"); break;
			case kRoomWallReflectionSharpness		: strcpy (label, " % "); break;
			case kRoomWallReflectivityFilterMode	: strcpy (label, "  "); break;
			case kRoomWallReflectivityFilterFrequency	: strcpy (label, "Hz"); break;
			case kRoomWallReflectivityFilterQ		: strcpy (label, "  "); break;
			case kRoomWallReflectivityFilterGain	: strcpy (label, "dB"); break;
			case kRoomWallReflectionLFCutoff		: strcpy (label, "Oct. "); break;
			case kRoomWallReflectionLFLoss			: strcpy (label, "dB "); break;

			case kRoomWallTransmisivity				: strcpy (label, " % "); break;
			case kRoomWallTransmisivityFilterMode	: strcpy (label, "  "); break;
			case kRoomWallTransmisivityFilterFrequency	: strcpy (label, "Hz"); break;
			case kRoomWallTransmisivityFilterQ		: strcpy (label, "  "); break;
			case kRoomWallTransmisivityFilterGain	: strcpy (label, "dB"); break;

			case kRoomLength		: strcpy (label, "metres" ); break;
			case kRoomWidth			: strcpy (label, "metres" ); break;
			case kRoomHeight		: strcpy (label, "metres" ); break;
			case kListenerX			: strcpy (label, "metres" ); break;
			case kListenerY			: strcpy (label, "metres" ); break;
			case kComplexityLevel	: strcpy (label, " " ); break;

			default 				: strcpy (label, "n/a"); break;

	}
}

//-----------------------------------------------------------------------------------------

 bool Reflector::getInputProperties (long index, VstPinProperties* properties)
{
	if (index < kNumInputs)
		{
		switch (index)
			{

				case 0	: 	sprintf (properties->label, "Left input");
							properties->flags = kVstPinIsActive;
							break;
				case 1	: 	sprintf (properties->label, "Right input");
							properties->flags = kVstPinIsActive;
							break;


			}
			return true;
	}
	return false;
}

//-----------------------------------------------------------------------------------------

 bool Reflector::getOutputProperties (long index, VstPinProperties* properties)
{
	if (index < lNumOutputs)
		{
		switch (index)
			{

				case kW	: 	sprintf (properties->label, "W");
							properties->flags = kVstPinIsActive;
							break;
				case kX	: 	sprintf (properties->label, "X");
							properties->flags = kVstPinIsActive;
							break;
				case kY	: 	sprintf (properties->label, "Y");
							properties->flags = kVstPinIsActive;
							break;
				case kZ	: 	sprintf (properties->label, "Z");
							properties->flags = kVstPinIsActive;
							break;
				case kU	: 	sprintf (properties->label, "U");
							properties->flags = kVstPinIsActive;
							break;
				case kV	: 	sprintf (properties->label, "V");
							properties->flags = kVstPinIsActive;
							break;
				case kP	: 	sprintf (properties->label, "P");
							properties->flags = kVstPinIsActive;
							break;
				case kQ	: 	sprintf (properties->label, "Q");
							properties->flags = kVstPinIsActive;
							break;

			}
			return true;
	}
	return false;
}


//-----------------------------------------------------------------------------------------
void Reflector::process(float **inputs, float **outputs, long sampleFrames)
{
	audioProcessing(inputs, outputs, sampleFrames, false);
}

//-----------------------------------------------------------------------------------------
void Reflector::processReplacing(float **inputs, float **outputs, long sampleFrames)
{
	audioProcessing(inputs, outputs, sampleFrames, true);
}

//-----------------------------------------------------------------------------------------
void Reflector::audioProcessing(float **inputs, float **outputs, long sampleFrames, bool replacing)
{

	switch (lComplexityLevel)
		{
		case kNoReflectionsFxx	:	DirectLeft.moveSource(inputs, outputs, sampleFrames, replacing, 0);
									break;
		case kNoReflectionsFHH	:	DirectLeft.moveSourceFHH(inputs, outputs, sampleFrames, replacing, 0);
									break;
		case kFHHFxx			:	DirectLeft.moveSourceFHH(inputs, outputs, sampleFrames, replacing, 0);
									LeftReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									RightReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									BackReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									FrontReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									CeilingReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									FloorReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									break;
		case kFHHFHH			:	DirectLeft.moveSourceFHH(inputs, outputs, sampleFrames, replacing, 0);
									LeftReflectionRoom.moveSourceFHH(inputs, outputs, sampleFrames, false, 0);
									RightReflectionRoom.moveSourceFHH(inputs, outputs, sampleFrames, false, 0);
									BackReflectionRoom.moveSourceFHH(inputs, outputs, sampleFrames, false, 0);
									FrontReflectionRoom.moveSourceFHH(inputs, outputs, sampleFrames, false, 0);
									CeilingReflectionRoom.moveSourceFHH(inputs, outputs, sampleFrames, false, 0);
									FloorReflectionRoom.moveSourceFHH(inputs, outputs, sampleFrames, false, 0);
									break;

		case kFxxFxx			:
			default				: 	DirectLeft.moveSource(inputs, outputs, sampleFrames, replacing, 0);
									LeftReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									RightReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									BackReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									FrontReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									CeilingReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									FloorReflectionRoom.moveSource(inputs, outputs, sampleFrames, false, 0);
									break;

		}

}


//-----------------------------------------------------------------------------------------

void Reflector::suspend ()
{
//	memset (buffer, 0, size * sizeof (float));
}

//-----------------------------------------------------------------------------------------
// this gets called (several times!) when the plugin is activated
void Reflector::resume()
{
	dSizeInSecondsLeft = 1.0f;	// temporary

	createbuffer_d( BufferLeft,dSizeInSecondsLeft,lOldBufferLengthLeft);
	cRoom.dDistancePerSample = dDistancePerSample;

	LeftReflectionRoom.setAudioBuffer(BufferLeft,lBufferLengthLeft,dDistancePerSample);
	RightReflectionRoom.setAudioBuffer(BufferLeft,lBufferLengthLeft,dDistancePerSample);
	BackReflectionRoom.setAudioBuffer(BufferLeft,lBufferLengthLeft,dDistancePerSample);
	FrontReflectionRoom.setAudioBuffer(BufferLeft,lBufferLengthLeft,dDistancePerSample);
	CeilingReflectionRoom.setAudioBuffer(BufferLeft,lBufferLengthLeft,dDistancePerSample);
	FloorReflectionRoom.setAudioBuffer(BufferLeft,lBufferLengthLeft,dDistancePerSample);

	DirectLeft.setAudioBuffer(BufferLeft,lBufferLengthLeft,dDistancePerSample);
	DirectLeft.setControlBuffer(ControlBufferLeft,lControlBufferLengthLeft,dDistancePerSample);

	AudioEffectX::resume ();	// not sure what this does, but ....
}

//-----------------------------------------------------------------------------
// this one returns true if allocations were successful, false if any allocation failed
bool Reflector::createbuffer_d(MyDouble * buffer,MyDouble size,long oldLength)
{

	// update the sample rate value
	dSampleRate = (MyDouble) getSampleRate();
	dNyquist = dSampleRate/2;
	dDistancePerSample = kSpeedOfSound / (dSampleRate);	// speed of sound = 340 m/s
	// just in case the host responds with something wacky
	if (dSampleRate <= 0.0f)
		dSampleRate = 44100.0f;

	long lOldBufferLengthLeft = lBufferLengthLeft;

	lBufferLengthLeft = (long) (dSampleRate);

	lControlBufferLengthLeft = lBufferLengthLeft;
				// one second's worth is enough for now for most purposes but
				// it really should be set according to the maximum size user selects
	if (lBufferLengthLeft != lOldBufferLengthLeft)
		{
		if (BufferLeft != NULL)
			delete[] BufferLeft;
		BufferLeft = NULL;
		}

	if (BufferLeft == NULL)
		BufferLeft = new MyDouble[lBufferLengthLeft];
	if (BufferLeft == NULL)
		return false;
	memset (BufferLeft,0,lBufferLengthLeft * sizeof (MyDouble));	// clear buffer to stop big clunks
	dDelayLeftOld = kMinDelay;	// reset delay
	lBufferTopLeft = lBufferLengthLeft - 1;

	if (lControlBufferLengthLeft != lOldBufferLengthLeft)
		{
		if (ControlBufferLeft != NULL)
			delete[] ControlBufferLeft;
		ControlBufferLeft = NULL;
		}


	if (ControlBufferLeft == NULL)
		ControlBufferLeft = new MyDouble[lControlBufferLengthLeft * sizeof (SourcePosition)];	// times 4 to contain az, el, dist and vol.
	if (ControlBufferLeft == NULL)
		return false;
	memset (ControlBufferLeft, 0,lControlBufferLengthLeft * sizeof (SourcePosition));	// clear ControlBuffer to stop big clunks
	dDelayLeftOld = 0;	// reset delay
	lControlBufferTopLeft = lControlBufferLengthLeft - 1;

	return true;
}


//-----------------------------------------------------------------------------------------

 MyDouble Reflector::computeDelay(MyDouble distance)
{

	MyDouble delay = distance  / dDistancePerSample;	// samples needed
	if (delay < kMinDelay)
		delay = kMinDelay;// allow space for interpolation so we get no pre-echoes
	if (delay > (lBufferLengthLeft - kMinDelay))
		delay = (lBufferLengthLeft - kMinDelay);	// prevent buffer overruns - temporary - this functionmay not be needed

	return ( delay );

}

//-----------------------------------------------------------------------------------------

 MyDouble Reflector::interpLinear (MyDouble interp_frac,MyDouble sample_point1,MyDouble sample_point2)
{
	return (sample_point1 + (1 - interp_frac) * ( sample_point2 - sample_point1 ));
}

//------------------------------------------------------------------------

 MyDouble Reflector::interpSpline (MyDouble interp_frac, MyDouble sample_point0, MyDouble sample_point1,MyDouble sample_point2, MyDouble sample_point3)
{

MyDouble a0;
MyDouble a1;
MyDouble a2;
MyDouble a3;

	a3=(-sample_point0+3*(sample_point1-sample_point2)+sample_point3) * 0.1666666666666667f;
	a2=(sample_point0-2*sample_point1+sample_point2) * 0.5f;
	a1=(sample_point2-sample_point0) * 0.5f;
	a0=(sample_point0+4*sample_point1+sample_point2) * 0.1666666666666667f;

	return  (((a3*(1-interp_frac) + a2)*(1-interp_frac) + a1)*(1-interp_frac) + a0);

// return sample_point1;
}

//------------------------------------------------------------------------

 MyDouble Reflector::gainLaw (MyDouble gain)
{
MyDouble vol;
	if (gain >= 0.5) {
	vol = pow(10, ((gain-1) * 2.0));	// generate log gain, based on (gain - 1) * 2 * 20/20
										// giving a 20 dB range	to half way down
	} else if (gain >= 0.2) {
	vol = pow(10, ((gain-0.625)*8.0));	// generate log gain, based on (gain - factor based on end  of previous range)* 2 * 40/20))
	} else {							// giving a 40 dB range over the next 0.3 range, total = 60dB
	vol = pow(10, ((gain-0.483333333333)*12.0));
}
	return vol;
}

//------------------------------------------------------------------------

void Reflector::setProgramName(char *name)
{
	strcpy(programs[curProgram].name, name);
}

//-----------------------------------------------------------------------------

long Reflector::getHostNo()
{
long lHostVer;
	char text[64];

		getHostProductString (text);
if (strcmp(text,"Nuendo") == 0)
	{
	lHostVer = getHostVendorVersion ();
	if (lHostVer < 2000)
		{
		lHostNo = 1;
	}else{
		lHostNo = 2;
		}
	}
/*	switch (lHostNo)
		{
			case 0:
			default:	getAeffect()->numParams = kNumParams; break;
			case 1:		getAeffect()->numParams = kN1NumParams; break;
			case 2:		getAeffect()->numParams = kN2NumParams; break;
		}
*/	return lHostNo;

}
//------------------------------------------------------------------------

bool Reflector:: getVendorString (char* text)
{
	strcpy (text,"The Reflector Project");
	return true;
}

//------------------------------------------------------------------------

VstPlugCategory Reflector::getPlugCategory()
{

	return kPlugCategEffect;

}

//------------------------------------------------------------------------

 void Reflector::getProgramName (char *name)
{
	if (!strcmp (programs[curProgram].name, "Init"))
		sprintf (name, "%s %d",programs[curProgram].name,(int)(curProgram + 1));
	else
		strcpy (name,programs[curProgram].name);
}

//-----------------------------------------------------------------------------------------

 MyDouble computePanAngle(MyDouble dLRPan, MyDouble dFBPan)
{
	MyDouble alpha = atan2 (dLRPan - 0.5, 0.5 - dFBPan);
	if (alpha < 0.1)
		alpha += k2PI;
	return ( -alpha);
}

//-------------------------------------------------------------------------------------------------------

 void MyDouble2string (MyDouble v, char *text)
{
	long c = 0, neg = 0;
	char string[32];
	char *s;
	MyDouble integ, i10, mantissa, m10, ten = 10.;

	if (v < 0)
	{
		neg = 1;

		v = -v;
		c++;
		if (v > 9999999.)
		{
			strcpy (string, "Huge!");
			return;
		}
	}
	else if( v > 99999999.)
	{
		strcpy (string, "Huge!");
		return;
	}

	s = string + 31;
	*s-- = 0;
	*s-- = '.';
	c++;

	integ = floor (v);
	i10 = fmod (integ, ten);
	*s-- = (char)((long)i10 + '0');
	integ /= ten;
	c++;
	while (integ >= 1. && c < 8)
	{
		i10 = fmod (integ, ten);
		*s-- = (char)((long)i10 + '0');
		integ /= ten;
		c++;
	}
	if (neg)
		*s-- = '-';
	strcpy (text, s + 1);
	if (c >= 8)
		return;

	s = string + 31;
	*s-- = 0;
	mantissa = fmod (v, 1.);
	mantissa *= pow (ten, (MyDouble)(8 - c));
	while (c < 8)
	{
		if (mantissa <= 0)
			*s-- = '0';
		else
		{
			m10 = fmod (mantissa, ten);
			*s-- = (char)((long)m10 + '0');
			mantissa /= 10.;
		}
		c++;
	}
	strcat (text, s + 1);
}

//-----------------------------------------------------------------------------------------

MyDouble computePanDistance(MyDouble, MyDouble);

MyDouble computePanDistance(MyDouble dLRPan, MyDouble dFBPan)
{

//#if MAC
	MyDouble dist = hypot (dLRPan - 0.5, dFBPan - 0.5);
//#else
//	MyDouble dist = _hypot (dLRPan - 0.5, dFBPan - 0.5);
//#endif
	return (dist);
}



//------------------------------------------------------------------------

 MyPoint::MyPoint(MyDouble XorAzimuth, MyDouble YorElevation, MyDouble ZorDistance, MyDouble volume)
{

	Azimuth = polars[0] = 0.0f;
	Elevation = polars[1] = 0.0f;
	Distance = polars[2] = 1.0f;
	polars[3] = cartesians[3] = Volume = volume;
	X = cartesians[0] = polars[2] * cos(polars[0]) * cos(polars[1]);	// X
	Y = cartesians[1] = polars[2] * sin(polars[0]) * cos(polars[1]);	// Y
	Z = cartesians[2] = polars[2] * sin(polars[1]);						// Z


}

//------------------------------------------------------------------------

 void MyPoint::setXYZV(MyDouble x = 1.0, MyDouble y = 0.0, MyDouble z = 0.0, MyDouble volume = 1.0)
{

	cartesians[0] = X = x;
	cartesians[1] = Y = y;
	cartesians[2] = Z = z;
	Volume = volume;
	polars[3] = Volume;
	cartesians[3] = Volume;


	Distance = polars[2] = sqrt(pow(cartesians[0],2) + pow(cartesians[1],2) + pow(cartesians[2],2))+ kAntiDenorm;	// Distance


	Azimuth  = polars[0] = asin(cartesians[1] / polars[2]);		// Azimuth
	Elevation = polars[1] = asin(cartesians[2] / polars[2]);	// Elevation

}


//------------------------------------------------------------------------

 void MyPoint::setAzimuth(MyDouble azimuth)
{

	polars[0] = Azimuth = azimuth;

	X = cartesians[0] = polars[2] * cos(polars[0]) * cos(polars[1]);	// X
	Y = cartesians[1] = polars[2] * sin(polars[0]) * cos(polars[1]);	// Y
	Z = cartesians[2] = polars[2] * sin(polars[1]);						// Z


}

//------------------------------------------------------------------------

 void MyPoint::setElevation(MyDouble elevation)
{


	polars[1] = Elevation = elevation;
;
	X = cartesians[0] = polars[2] * cos(polars[0]) * cos(polars[1]);	// X
	Y = cartesians[1] = polars[2] * sin(polars[0]) * cos(polars[1]);	// Y
	Z = cartesians[2] = polars[2] * sin(polars[1]);						// Z


}

//------------------------------------------------------------------------

 void MyPoint::setDistance(MyDouble distance)
{

	polars[2] = Distance = distance;
	X = cartesians[0] = polars[2] * cos(polars[0]) * cos(polars[1]);	// X
	Y = cartesians[1] = polars[2] * sin(polars[0]) * cos(polars[1]);	// Y
	Z = cartesians[2] = polars[2] * sin(polars[1]);						// Z

}
//------------------------------------------------------------------------

 void MyPoint::setX(MyDouble x)
{

	cartesians[0] = X = x;

	Distance = polars[2] = sqrt(pow(cartesians[0],2) + pow(cartesians[1],2) + pow(cartesians[2],2));	// Distance
	if (polars[2] == 0)
		polars[2] = kAntiDenorm;

	Azimuth  = polars[0] = asin(cartesians[1] / polars[2]);	// Azimuth
	Elevation = polars[1] = asin(cartesians[2] / polars[2]);	// Elevation

}

//------------------------------------------------------------------------

 void MyPoint::setY(MyDouble y)
{

	cartesians[1] = Y = y;

	Distance = polars[2] = sqrt(pow(cartesians[0],2) + pow(cartesians[1],2) + pow(cartesians[2],2));	// Distance
	if (polars[2] == 0)
		polars[2] = kAntiDenorm;

	Azimuth  = polars[0] = asin(cartesians[1] / polars[2]);	// Azimuth
	Elevation = polars[1] = asin(cartesians[2] / polars[2]);	// Elevation

}

//------------------------------------------------------------------------

 void MyPoint::setZ(MyDouble z)
{

	cartesians[2] = Z = z;

	Distance = polars[2] = sqrt(pow(cartesians[0],2) + pow(cartesians[1],2) + pow(cartesians[2],2));	// Distance
	if (polars[2] == 0)
		polars[2] = kAntiDenorm;

	Azimuth  = polars[0] = asin(cartesians[1] / polars[2]);	// Azimuth
	Elevation = polars[1] = asin(cartesians[2] / polars[2]);	// Elevation

}

//------------------------------------------------------------------------

 void MyPoint::setVolume(MyDouble volume)
{

	cartesians[3] = polars[3] = Volume = volume;

}

//------------------------------------------------------------------------

 MyPoint MyPoint::operator+ (MyPoint param) {
  MyPoint temp ;
  temp.setX(X + param.X);
  temp.setY(Y + param.Y);
  temp.setZ(Z + param.Z);
  temp.setVolume(Volume + param.Volume);
  return (temp);
}
//------------------------------------------------------------------------

 MyPoint MyPoint::operator- (MyPoint param) {
  MyPoint temp ;
  temp.setX(X - param.X);
  temp.setY(Y - param.Y);
  temp.setZ(Z - param.Z);
  temp.setVolume(Volume - param.Volume);
  return (temp);
}
//------------------------------------------------------------------------

 MyPoint MyPoint::operator/ (MyPoint param) {
  MyPoint temp ;
  	if (param.X == 0)
		param.X = kAntiDenorm;
  	if (param.Y == 0)
		param.Y = kAntiDenorm;
  	if (param.Z == 0)
		param.Z = kAntiDenorm;
  	if (param.Volume == 0)
		param.Volume = kAntiDenorm;

  temp.setX(X / param.X);
  temp.setY(Y / param.Y);
  temp.setZ(Z / param.Z);
  temp.setVolume(Volume / param.Volume);
  return (temp);
}

//------------------------------------------------------------------------

 MyPoint MyPoint::operator/ (MyDouble IncrementFraction) {
  MyPoint temp ;
  if (IncrementFraction == 0)
	  IncrementFraction = kAntiDenorm;
  temp.setX(X / IncrementFraction);
  temp.setY(Y / IncrementFraction);
  temp.setZ(Z / IncrementFraction);
  temp.setVolume(Volume / IncrementFraction);
  return (temp);
}
//------------------------------------------------------------------------

 MyPoint MyPoint::operator* (MyDouble multiplier)
 {
  MyPoint temp ;
  if (multiplier == 0)
	  multiplier = kAntiDenorm;
  temp.setX(X * multiplier);
  temp.setY(Y * multiplier);
  temp.setZ(Z * multiplier);
  temp.setVolume(Volume * multiplier);
  return (temp);
}
//------------------------------------------------------------------------

 void MyPoint::incrementMyPoint(MyPoint param)

{

	cartesians[0] = X += param.X;
	cartesians[1] = Y += param.Y;
	cartesians[2] = Z += param.Z;;

	polars[2] = sqrt(pow(cartesians[0],2) + pow(cartesians[1],2) + pow(cartesians[2],2));	// Distance
	if (polars[2] == 0)
		polars[2] = kAntiDenorm;

	polars[0] = asin(cartesians[1] / polars[2]);	// Azimuth
	polars[1] = asin(cartesians[2] / polars[2]);	// Elevation

	polars[3] = cartesians[3] = Volume = (Volume + param.Volume);

}

//------------------------------------------------------------------------

 PolarMyPoint::PolarMyPoint(MyDouble azimuth, MyDouble elevation, MyDouble distance, MyDouble volume)
{

	polars[0] = Azimuth = azimuth;
	polars[1] = Elevation = elevation;
	polars[2] = Distance = distance;
	polars[3] = cartesians[3] = Volume = volume;

	X = cartesians[0] = polars[2] * cos(polars[0]) * cos(polars[1]);	// X
	Y = cartesians[1] = polars[2] * sin(polars[0]) * cos(polars[1]);	// Y
	Z = cartesians[2] = polars[2] * sin(polars[1]);						// Z


}
//------------------------------------------------------------------------

 PolarMyPoint PolarMyPoint::operator+ (PolarMyPoint param) {
  PolarMyPoint temp ;
  temp.setX(X + param.X);
  temp.setY(Y + param.Y);
  temp.setZ(Z + param.Z);
  temp.setVolume(Volume + param.Volume);
  return (temp);
}
//------------------------------------------------------------------------

 PolarMyPoint PolarMyPoint::operator- (PolarMyPoint param) {
  PolarMyPoint temp ;
  temp.setX(X - param.X);
  temp.setY(Y - param.Y);
  temp.setZ(Z - param.Z);
  temp.setVolume(Volume - param.Volume);
  return (temp);
}
//------------------------------------------------------------------------

 PolarMyPoint PolarMyPoint::operator/ (PolarMyPoint param) {
  PolarMyPoint temp ;
  	if (param.X == 0)
		param.X = kAntiDenorm;
  	if (param.Y == 0)
		param.Y = kAntiDenorm;
  	if (param.Z == 0)
		param.Z = kAntiDenorm;

  temp.setX(X / param.X);
  temp.setY(Y / param.Y);
  temp.setZ(Z / param.Z);
  temp.setVolume(Volume / param.Volume);
  return (temp);
}

//------------------------------------------------------------------------

 PolarMyPoint PolarMyPoint::operator/ (MyDouble IncrementFraction) {
  PolarMyPoint temp ;
    	if (IncrementFraction == 0)
		IncrementFraction = kAntiDenorm;

  temp.setX(X / IncrementFraction);
  temp.setY(Y / IncrementFraction);
  temp.setZ(Z / IncrementFraction);
  temp.setVolume(Volume / IncrementFraction);
  return (temp);
}

//------------------------------------------------------------------------

 PolarMyPoint PolarMyPoint::operator* (MyDouble multiplier)
 {
  PolarMyPoint temp ;
  if (multiplier == 0)
	  multiplier = kAntiDenorm;
  temp.setX(X * multiplier);
  temp.setY(Y * multiplier);
  temp.setZ(Z * multiplier);
  temp.setVolume(Volume * multiplier);
  return (temp);
}

//------------------------------------------------------------------------

 void PolarMyPoint::incrementPolarMyPoint(PolarMyPoint param)
{
	polars[0] = Azimuth		+= param.Azimuth;
	polars[1] = Elevation	+= param.Elevation;
	polars[2] = Distance	+= param.Distance;
	polars[3] = Volume		+= param.Volume;
//	return  polars;
}

//------------------------------------------------------------------------

 void PolarMyPoint::updatePolarMyPoint()
{
	setAzimuth(polars[0]);		// forces update of cartesians, omitted  from incrementPolarMyPoint
	cartesians[3] = polars[3];	// volume needs to be treated separately


}

//------------------------------------------------------------------------

 /*PolarMyPoint PolarMyPoint::operator = (CartesianMyPoint param)
{
	// this is essentially a conversion operator
  PolarMyPoint temp ;
  MyPoint temp2 ;
  temp2 = temp;
  temp.setX(param.X);
  temp.setY(param.Y);
  temp.setZ(param.Z);
  temp.setVolume(param.Volume);
  return (temp);
}*/
//------------------------------------------------------------------------

 CartesianMyPoint::CartesianMyPoint(MyDouble x, MyDouble y, MyDouble z, MyDouble Volume)
{
	X = x;
	Y = y;
	Z = z;
	cartesians[0] = X;
	cartesians[1] = Y;
	cartesians[2] = Z;
	cartesians[3] = polars[3] = Volume;
	polars[2] = sqrt(pow(cartesians[0],2) + pow(cartesians[1],2) + pow(cartesians[2],2));	// Distance						// Distance
  	if (polars[2] == 0)
		polars[2] = kAntiDenorm;

	polars[0] = asin(cartesians[1] / polars[2]);	// Azimuth
	polars[1] = asin(cartesians[2] / polars[2]);	// Elevation

}
//------------------------------------------------------------------------

/* CartesianMyPoint CartesianMyPoint::operator= (PolarMyPoint param) {
	// this is essentially a conversion operator
  CartesianMyPoint temp ;
  temp.setX(param.X);
  temp.setY(param.Y);
  temp.setZ(param.Z);
  temp.setVolume(param.Volume);
  return (temp);
}
*/

//------------------------------------------------------------------------

 CartesianMyPoint CartesianMyPoint::operator+ (CartesianMyPoint param) {
  CartesianMyPoint temp ;
  temp.setX(X + param.X);
  temp.setY(Y + param.Y);
  temp.setZ(Z + param.Z);
  temp.setVolume(Volume + param.Volume);
  return (temp);
}
//------------------------------------------------------------------------

 CartesianMyPoint CartesianMyPoint::operator- (CartesianMyPoint param) {
  CartesianMyPoint temp ;
  temp.setX(X - param.X);
  temp.setY(Y - param.Y);
  temp.setZ(Z - param.Z);
  temp.setVolume(Volume - param.Volume);
  return (temp);
}
//------------------------------------------------------------------------

 CartesianMyPoint CartesianMyPoint::operator/ (CartesianMyPoint param) {
  CartesianMyPoint temp ;
   	if (param.X == 0)
		param.X = kAntiDenorm;
  	if (param.Y == 0)
		param.Y = kAntiDenorm;
  	if (param.Z == 0)
		param.Z = kAntiDenorm;

  temp.setX(X / param.X);
  temp.setY(Y / param.Y);
  temp.setZ(Z / param.Z);
  temp.setVolume(Volume / param.Volume);
  return (temp);
}

//------------------------------------------------------------------------

 CartesianMyPoint CartesianMyPoint::operator/ (MyDouble IncrementFraction) {
  CartesianMyPoint temp ;
    	if (IncrementFraction == 0)
		IncrementFraction = kAntiDenorm;

  temp.setX(X / IncrementFraction);
  temp.setY(Y / IncrementFraction);
  temp.setZ(Z / IncrementFraction);
  temp.setVolume(Volume / IncrementFraction);
  return (temp);
}
//------------------------------------------------------------------------

 CartesianMyPoint CartesianMyPoint::operator* (MyDouble multiplier)
 {
  CartesianMyPoint temp ;
  if (multiplier == 0)
	  multiplier = kAntiDenorm;
  temp.setX(X * multiplier);
  temp.setY(Y * multiplier);
  temp.setZ(Z * multiplier);
  temp.setVolume(Volume * multiplier);
  return (temp);
}

//------------------------------------------------------------------------

 void CartesianMyPoint::incrementCartesianMyPoint(CartesianMyPoint param)
{
	cartesians[0] = X += param.X;
	cartesians[1] = Y += param.Y;
	cartesians[2] = Z += param.Z;
	cartesians[3] = Volume += param.Volume;
}

//------------------------------------------------------------------------

 void CartesianMyPoint::updateCartesianMyPoint()
{
	setX(cartesians[0]);	// forces update of cartesians, omitted  from incrementCartesianMyPoint
	polars[3] = cartesians[3];


}

//------------------------------------------------------------------------

ParametricEq::ParametricEq() :
 dFilterCutOffFreq(1000.0), dFilterGain(.25), dFilterQ(0.5), dSampleRate(44100.0),
 dNyquist(22050.0), input(0), output(0), oldIn1(0), oldIn2(0), oldOut1(0), oldOut2(0),
 lFilterMode(0)

{

}

//------------------------------------------------------------------------
 void ParametricEq::setFilterMode(long mode)
 {
	 lFilterMode = mode;
	 setUp();			// update all coefficients
 }
//------------------------------------------------------------------------


 void	ParametricEq::setFilterCutOffFreq(MyDouble Fc)
{
	dFilterCutOffFreq = Fc;
	setUp();			// update all coefficients
}
//------------------------------------------------------------------------

 void	ParametricEq::setFilterGain(MyDouble FGain)
{
	dFilterGain = FGain;
	setUp();			// update all coefficients
}
//------------------------------------------------------------------------

 void	ParametricEq::setFilterQ(MyDouble FQ)
{
	if (FQ < .01 )
		FQ = 0.01;

	dFilterQ = FQ;
	setUp();			// update all coefficients
}
//------------------------------------------------------------------------

 void	ParametricEq::setSampleRate(MyDouble SR)
{
	dSampleRate = SR;
		setUp();			// update all coefficients

}
//------------------------------------------------------------------------

 void	ParametricEq::setNyquist(MyDouble Ny)
{
	dNyquist = Ny;
	setUp();			// update all coefficients
}

//------------------------------------------------------------------------

void ParametricEq::setUp(MyDouble Fc, MyDouble FGain, MyDouble FQ, long Mode)
{

	dFilterCutOffFreq = Fc;
	dFilterGain = FGain;
	if (FQ < .01 )
		FQ = 0.01;
	dFilterQ = FQ;
	lFilterMode = Mode;

	// this code is based on the CSound implementation of Zoelzer's parametric eq, hence the cryptic variable names
	// I'm not at all sure that this is the best way of dealing with varying filter parameters
	switch (lFilterMode)
		{
	  // no filter
		case kFilterOff :
				default	:
								  b0 =  1.0f;
								  b1 =  0.0f;
								  b2 =  0.0f;
								  a0 =  1.0f;
								  a1 =  0.0f;
								  a2 =  0.0f;
								  break;
	   // Low Shelf
		case kFilterLowShelf : 	  sq = sqrt(2.0 * dFilterGain);
								  omega = k2PI * dFilterCutOffFreq / dSampleRate;
								  k =  tan(omega * 0.5);
								  kk = k*k;
								  vkk = dFilterGain * kk;
								  b0 =  1.0 + sq * k + vkk;
								  b1 =  2.0 * (vkk - 1.0);
								  b2 =  1.0 - sq * k + vkk;
								  a0 =  1.0 + k / dFilterQ + kk;
								  a1 =  2.0 * (kk - 1.0);
								  a2 =  1.0 - k / dFilterQ + kk;
								  break;
		// High Shelf
		case kFilterHighShelf :   sq = sqrt(2.0 * dFilterGain);
								  omega = k2PI*dFilterCutOffFreq/dSampleRate;
								  k =  tan((kPI - omega) * 0.5);
								  kk = k*k;
								  vkk = dFilterGain * kk;
								  b0 =  1.0 + sq*k + vkk;
								  b1 = -2.0 * (vkk - 1.0);
								  b2 =  1.0 - sq*k + vkk;
								  a0 =  1.0 + k / dFilterQ +kk;
								  a1 = -2.0 * (kk - 1.0);
								  a2 =  1.0 - k/ dFilterQ + kk;
								  break;
		// Peaking EQ
	   case kFilterPeaking	:	  omega = k2PI * dFilterCutOffFreq / dSampleRate;
								  k =  tan(omega * 0.5);
								  kk = k*k;
								  vk = dFilterGain * k;
								  vkdq = vk / dFilterQ;
								  b0 =  1.0 + vkdq + kk;
								  b1 =  2.0 * (kk - 1.0);
								  b2 =  1.0 - vkdq + kk;
								  a0 =  1.0 + k / dFilterQ +kk;
								  a1 =  2.0 * (kk - 1.0);
								  a2 =  1.0 - k / dFilterQ + kk;
								  break;
		}
}

//------------------------------------------------------------------------

void ParametricEq::setUp()
	{


	  // this just updates all filter coefficients
	  // I'm not at all sure that this is the best way of dealing with varying filter parameters
	switch (lFilterMode)
		{
	  // no filter
		case kFilterOff :
				default	:
								  b0 =  1.0f;
								  b1 =  0.0f;
								  b2 =  0.0f;
								  a0 =  1.0f;
								  a1 =  0.0f;
								  a2 =  0.0f;
								  break;
	   // Low Shelf
		case kFilterLowShelf : 	  sq = sqrt(2.0 * dFilterGain);
								  omega = k2PI * dFilterCutOffFreq / dSampleRate;
								  k =  tan(omega * 0.5);
								  kk = k*k;
								  vkk = dFilterGain * kk;
								  b0 =  1.0 + sq * k + vkk;
								  b1 =  2.0 * (vkk - 1.0);
								  b2 =  1.0 - sq * k + vkk;
								  a0 =  1.0 + k / dFilterQ + kk;
								  a1 =  2.0 * (kk - 1.0);
								  a2 =  1.0 - k / dFilterQ + kk;
								  break;
		// High Shelf
		case kFilterHighShelf :   sq = sqrt(2.0 * dFilterGain);
								  omega = k2PI*dFilterCutOffFreq/dSampleRate;
								  k =  tan((kPI - omega) * 0.5);
								  kk = k*k;
								  vkk = dFilterGain * kk;
								  b0 =  1.0 + sq*k + vkk;
								  b1 = -2.0 * (vkk - 1.0);
								  b2 =  1.0 - sq*k + vkk;
								  a0 =  1.0 + k / dFilterQ +kk;
								  a1 = -2.0 * (kk - 1.0);
								  a2 =  1.0 - k/ dFilterQ + kk;
								  break;
		// Peaking EQ
	   case kFilterPeaking	:	  omega = k2PI * dFilterCutOffFreq / dSampleRate;
								  k =  tan(omega * 0.5);
								  kk = k*k;
								  vk = dFilterGain * k;
								  vkdq = vk / dFilterQ;
								  b0 =  1.0 + vkdq + kk;
								  b1 =  2.0 * (kk - 1.0);
								  b2 =  1.0 - vkdq + kk;
								  a0 =  1.0 + k / dFilterQ +kk;
								  a1 =  2.0 * (kk - 1.0);
								  a2 =  1.0 - k / dFilterQ + kk;
								  break;
		}
}

//------------------------------------------------------------------------
 Wall::Wall()
{
	topLeft = CartesianMyPoint(1,1,1) ;
	topRight = CartesianMyPoint(1,-1,1) ;
	bottomRight = CartesianMyPoint(1,-1,-1) ;
	bottomLeft = CartesianMyPoint(1,1,-1) ;
	dReflectivity = 0.0f;			// perfect absorber ... test only
	dTransmisivity = 0.0f;			// no transmission
	dReflectionSharpness = 1.0f;		// again, perfect


}

//------------------------------------------------------------------------

 void Wall::setTopLeft(CartesianMyPoint & cp )
{
	topLeft = cp;
}

//------------------------------------------------------------------------

 void Wall::setTopRight(CartesianMyPoint & cp )
{
	topRight = cp;
}
//------------------------------------------------------------------------

 void Wall::setNormal()
{

}

//------------------------------------------------------------------------

 void Wall::setBottomRight(CartesianMyPoint & cp )
{
	bottomRight = cp;
}

//------------------------------------------------------------------------

 void Wall::setBottomLeft(CartesianMyPoint & cp )
{
	bottomLeft = cp;
}

//------------------------------------------------------------------------
 bool Wall::checkIntersecting(CartesianMyPoint source, CartesianMyPoint listener)

 {
	MyDouble az,el;
	az = listener.getAzimuth();
	el = listener.getElevation();
	if(az < bottomLeft.getAzimuth() && az > bottomRight.getAzimuth() && az < topLeft.getElevation() && az > bottomLeft.getElevation() )
	{
		return true;
	}

	 return false;	// ray doesn't hit this wall
	 				// however, I'm not sure how robust this is
 }
//------------------------------------------------------------------------
 CartesianMyPoint Wall::intersection(CartesianMyPoint source, CartesianMyPoint listener)
 {

	 CartesianMyPoint effectiveSource;
	 CartesianMyPoint normal;
	 normal = normalToPlane ( bottomLeft, bottomRight, topRight );
	 // assumption is made that listener is inside the room!!  Should be changed.
	 effectiveSource = IntersectLinePlane(listener, source, bottomLeft, normal);

	 return effectiveSource;	// return point ray from source to listener strikes the wall

 }

//------------------------------------------------------------------------

 void Wall::setReflectivity(MyDouble r)
{
	dReflectivity = r;
}


//------------------------------------------------------------------------

 void Wall::setReflectivitySpectrum(ParametricEq & rs )
{
	copySpectrum(kReflectivitySpectrum, rs);
}
//------------------------------------------------------------------------

 void Wall::setSpectrum(long NoOfSpectrum, ParametricEq & rs )
{
	switch  (NoOfSpectrum)
		{
		case kReflectivitySpectrum	:
		default						:	ReflectivitySpectrum = rs; break;
		case kTransmisivitySpectrum	:	TransmisivitySpectrum = rs; break;
		case kOmniSpectrum			:	OmniSpectrum = rs; break;
		case kFrontBackSpectrum		:	FrontBackSpectrum = rs; break;
		case kLeftRightSpectrum		:	LeftRightSpectrum = rs; break;
		case kUpDownSpectrum		:	UpDownSpectrum = rs; break;
		}

}

//------------------------------------------------------------------------
 void Wall::copySpectrum(long NoOfSpectrum, ParametricEq & rs )
{
	switch  (NoOfSpectrum)
		{
		case kReflectivitySpectrum	:
		default						:	ReflectivitySpectrum = rs; break;
		case kTransmisivitySpectrum	:	TransmisivitySpectrum = rs; break;
		case kOmniSpectrum			:	OmniSpectrum = rs; break;
		case kFrontBackSpectrum		:	FrontBackSpectrum = rs; break;
		case kLeftRightSpectrum		:	LeftRightSpectrum = rs; break;
		case kUpDownSpectrum		:	UpDownSpectrum = rs; break;
		}

}

//------------------------------------------------------------------------

 void Wall::setReflectionSharpness(MyDouble rs )
{
	dReflectionSharpness = rs;	//sin(kHalfPI * rs);
}

//------------------------------------------------------------------------

 void Wall::setTransmisivity(MyDouble t )
{
	dTransmisivity = t;
}

//------------------------------------------------------------------------

 void Wall::setTransmisivitySpectrum(ParametricEq & ts )
{
	copySpectrum(kTransmisivitySpectrum, ts);
}
//------------------------------------------------------------------------

 void Wall::setSpectrumMode(long NoOfSpectrum, long mode)
{
	switch  (NoOfSpectrum)
		{
		case kReflectivitySpectrum	:
		default						:	ReflectivitySpectrum.setFilterMode(mode); break;
		case kTransmisivitySpectrum	:	TransmisivitySpectrum.setFilterMode(mode); break;
		case kOmniSpectrum			:	OmniSpectrum.setFilterMode(mode); break;
		case kFrontBackSpectrum		:	FrontBackSpectrum.setFilterMode(mode); break;
		case kLeftRightSpectrum		:	LeftRightSpectrum.setFilterMode(mode); break;
		case kUpDownSpectrum		:	UpDownSpectrum.setFilterMode(mode);
		}

}
//------------------------------------------------------------------------

 void Wall::setSpectrumCutOffFreq(long NoOfSpectrum, double freq)
{
	switch  (NoOfSpectrum)
		{
		case kReflectivitySpectrum	:
		default						:	ReflectivitySpectrum.setFilterCutOffFreq(freq); break;
		case kTransmisivitySpectrum	:	TransmisivitySpectrum.setFilterCutOffFreq(freq); break;
		case kOmniSpectrum			:	OmniSpectrum.setFilterCutOffFreq(freq); break;
		case kFrontBackSpectrum		:	FrontBackSpectrum.setFilterCutOffFreq(freq); break;
		case kLeftRightSpectrum		:	LeftRightSpectrum.setFilterCutOffFreq(freq); break;
		case kUpDownSpectrum		:	UpDownSpectrum.setFilterCutOffFreq(freq);
		}

}
//------------------------------------------------------------------------

 void Wall::setSpectrumGain(long NoOfSpectrum, double gain)
{
	switch  (NoOfSpectrum)
		{
		case kReflectivitySpectrum	:
		default						:	ReflectivitySpectrum.setFilterGain(gain); break;
		case kTransmisivitySpectrum	:	TransmisivitySpectrum.setFilterGain(gain); break;
		case kOmniSpectrum			:	OmniSpectrum.setFilterGain(gain); break;
		case kFrontBackSpectrum		:	FrontBackSpectrum.setFilterGain(gain); break;
		case kLeftRightSpectrum		:	LeftRightSpectrum.setFilterGain(gain); break;
		case kUpDownSpectrum		:	UpDownSpectrum.setFilterGain(gain);
		}

}
//------------------------------------------------------------------------

 void Wall::setSpectrumQ(long NoOfSpectrum, double Q)
{
	switch  (NoOfSpectrum)
		{
		case kReflectivitySpectrum	:
		default						:	ReflectivitySpectrum.setFilterQ(Q); break;
		case kTransmisivitySpectrum	:	TransmisivitySpectrum.setFilterQ(Q); break;
		case kOmniSpectrum			:	OmniSpectrum.setFilterQ(Q); break;
		case kFrontBackSpectrum		:	FrontBackSpectrum.setFilterQ(Q); break;
		case kLeftRightSpectrum		:	LeftRightSpectrum.setFilterQ(Q); break;
		case kUpDownSpectrum		:	UpDownSpectrum.setFilterQ(Q);
		}

}

//------------------------------------------------------------------------
ParametricEq &Wall::getSpectrum(long NoOfSpectrum)
{
	switch  (NoOfSpectrum)
		{
		case kReflectivitySpectrum	:
		default						:	return ReflectivitySpectrum; break;
		case kTransmisivitySpectrum	:	return TransmisivitySpectrum; break;
		case kOmniSpectrum			:	return OmniSpectrum; break;
		case kFrontBackSpectrum		:	return FrontBackSpectrum; break;
		case kLeftRightSpectrum		:	return LeftRightSpectrum; break;
		case kUpDownSpectrum		:	return UpDownSpectrum; break;
		}
}




//------------------------------------------------------------------------
MyDouble Wall::FilterIt(MyDouble input, ParametricEq &spectrum)
{
  // this code is based on the CSound implementation of Zoelzer's parametric eq
if (spectrum.lFilterMode != kFilterOff)
	{
 		spectrum.output 	= (spectrum.b0*input + spectrum.b1*spectrum.oldIn1 + spectrum.b2*spectrum.oldIn2 - spectrum.a1*spectrum.oldOut1 - spectrum.a2*spectrum.oldOut2)/spectrum.a0;
		spectrum.oldIn2		= spectrum.oldIn1;
		spectrum.oldIn1		= input;
		spectrum.oldOut2	= spectrum.oldOut1;
		spectrum.oldOut1	= spectrum.output;
	return spectrum.output;
	}else{
		return input;
	}
}

//------------------------------------------------------------------------

 void ReflectedDSoundSource::setWall(Wall & mw)
{
	wallReflection = mw;
}

//------------------------------------------------------------------------

 Wall ReflectedDSoundSource::getWall()
{
return wallReflection;
}

//------------------------------------------------------------------------

 Room::Room()
{
CartesianMyPoint tp(1,0,0);
	dTiltAngle = 0.0f;
	dTumbleAngle = 0.0f;
	dRotateAngle = 0.0f;
	dTurbulence = 0.0f;
	dAirAbsorbtion = 0.0f;

// default wall settings - each wall is viewed as if facing it from the centre of the room

	tp.setXYZV(1,1,1);		// corner 0
	frontWall.setTopLeft(tp) ;
	leftWall.setTopRight(tp) ;
	Ceiling.setBottomLeft(tp) ;

	tp.setXYZV(1,1,-1);		// corner 1
	leftWall.setBottomRight(tp) ;
	frontWall.setBottomLeft(tp) ;
	Floor.setTopLeft(tp) ;

	tp.setXYZV(1,-1,-1);	// corner 2
	rightWall.setBottomLeft(tp) ;
	frontWall.setBottomRight(tp) ;
	Floor.setTopRight(tp) ;

	tp.setXYZV(1,-1,1);		// corner 3
	frontWall.setTopRight(tp) ;
	rightWall.setTopLeft(tp) ;
	Ceiling.setBottomRight(tp) ;

	tp.setXYZV(-1,-1,1);		// corner 4
	backWall.setTopLeft(tp) ;
	rightWall.setTopRight(tp) ;
	Ceiling.setTopRight(tp) ;

	tp.setXYZV(-1,-1,-1);	// corner 5
	backWall.setBottomLeft(tp) ;
	rightWall.setBottomRight(tp) ;
	Floor.setBottomRight(tp) ;

	tp.setXYZV(-1,1,-1);	// corner 6
	leftWall.setBottomLeft(tp) ;
	backWall.setBottomRight(tp) ;
	Floor.setBottomLeft(tp) ;

	tp.setXYZV(-1,1,1);	// corner 7
	leftWall.setTopLeft(tp) ;
	backWall.setTopRight(tp) ;
	Ceiling.setTopLeft(tp) ;



	walls[0] = frontWall;
	walls[1] = backWall;
	walls[2] = leftWall;
	walls[3] = rightWall;
	walls[4] = Floor;
	walls[5] = Ceiling;


	walls[kfrontWall].setTransmisivity(0.0) ;
	walls[kbackWall].setTransmisivity(0.0) ;
	walls[kleftWall].setTransmisivity(0.0) ;
	walls[krightWall].setTransmisivity(0.0) ;
	walls[kFloor].setTransmisivity(0.0) ;
	walls[kCeiling].setTransmisivity(0.0) ;

	walls[kfrontWall].setReflectivity(0.0) ;	// test only
	walls[kbackWall].setReflectivity(0.0) ;
	walls[kleftWall].setReflectivity(0.0) ;
	walls[krightWall].setReflectivity(0.0) ;
	walls[kFloor].setReflectivity(0.0) ;
	walls[kCeiling].setReflectivity(0.0) ;

	walls[kfrontWall].setReflectionSharpness(1.0) ;
	walls[kbackWall].setReflectionSharpness(1.0) ;
	walls[kleftWall].setReflectionSharpness(1.0) ;
	walls[krightWall].setReflectionSharpness(1.0) ;
	walls[kFloor].setReflectionSharpness(1.0) ;
	walls[kCeiling].setReflectionSharpness(1.0) ;

}
//------------------------------------------------------------------------

 void Room::audioProcess(float **inputs, float **outputs, long sampleFrames, bool replacing, SourcePosition listener, SourcePosition source)
{	// placeholder
}
//------------------------------------------------------------------------

 void Room::setFrontWall(Wall w)
{
	frontWall = w;
}

//------------------------------------------------------------------------

 void Room::setBackWall(Wall w)
{
	backWall = w;

}

//------------------------------------------------------------------------

 void Room::setRightWall(Wall w)
{
	rightWall = w;
}

//------------------------------------------------------------------------

 void Room::setLeftWall(Wall w)
{
	leftWall = w;
}

//------------------------------------------------------------------------

 void Room::setFloor(Wall w)
{
	Floor = w;	// note that we break the rules and capitalise first letter
				// to avoid conflict with reserved word "floor"
}

//------------------------------------------------------------------------
 void Room::setCeiling(Wall w)
{
	Ceiling = w;	// note that we break the rules and capitalise first letter
					// to avoid conflict with reserved word
}

//------------------------------------------------------------------------

 void Room::setLength(MyDouble length)
{
	 double l = length/2;	 // currently, we assume symmetricity

	 Floor.bottomLeft.setX(-l);
	 Floor.bottomRight.setX(-l);
	 Floor.topLeft.setX(l);
	 Floor.topRight.setX(l);

	 Ceiling.bottomLeft.setX(l);
	 Ceiling.bottomRight.setX(l);
	 Ceiling.topLeft.setX(-l);
	 Ceiling.topRight.setX(-l);

	 leftWall.bottomLeft.setX(-l);
	 leftWall.bottomRight.setX(l);
	 leftWall.topLeft.setX(-l);
	 leftWall.topRight.setX(l);

	 rightWall.bottomLeft.setX(l);
	 rightWall.bottomRight.setX(-l);
	 rightWall.topLeft.setX(l);
	 rightWall.topRight.setX(-l);

	 frontWall.bottomLeft.setX(l);
	 frontWall.bottomRight.setX(l);
	 frontWall.topLeft.setX(l);
	 frontWall.topRight.setX(l);

	 backWall.bottomLeft.setX(-l);
	 backWall.bottomRight.setX(-l);
	 backWall.topLeft.setX(-l);
	 backWall.topRight.setX(-l);


}

//------------------------------------------------------------------------

 void Room::setWidth(MyDouble width)
{
	 double w = width/2;	 // currently, we assume symmetricity

	 Floor.bottomLeft.setY(w);
	 Floor.bottomRight.setY(-w);
	 Floor.topLeft.setY(w);
	 Floor.topRight.setY(-w);

	 Ceiling.bottomLeft.setY(w);
	 Ceiling.bottomRight.setY(-w);
	 Ceiling.topLeft.setY(w);
	 Ceiling.topRight.setY(-w);

	 leftWall.bottomLeft.setY(w);
	 leftWall.bottomRight.setY(w);
	 leftWall.topLeft.setY(w);
	 leftWall.topRight.setY(w);

	 rightWall.bottomLeft.setY(-w);
	 rightWall.bottomRight.setY(-w);
	 rightWall.topLeft.setY(-w);
	 rightWall.topRight.setY(-w);

	 frontWall.bottomLeft.setY(w);
	 frontWall.bottomRight.setY(-w);
	 frontWall.topLeft.setY(w);
	 frontWall.topRight.setY(-w);

	 backWall.bottomLeft.setY(-w);
	 backWall.bottomRight.setY(w);
	 backWall.topLeft.setY(-w);
	 backWall.topRight.setY(w);

}

//------------------------------------------------------------------------

 void Room::setHeight(MyDouble height)
{
	 double h = height/2;	 // currently, we assume symmetricity

	 Floor.bottomLeft.setZ(-h);
	 Floor.bottomRight.setZ(-h);
	 Floor.topLeft.setZ(-h);
	 Floor.topRight.setZ(-h);

	 Ceiling.bottomLeft.setZ(h);
	 Ceiling.bottomRight.setZ(h);
	 Ceiling.topLeft.setZ(h);
	 Ceiling.topRight.setZ(h);

	 leftWall.bottomLeft.setZ(-h);
	 leftWall.bottomRight.setZ(-h);
	 leftWall.topLeft.setZ(h);
	 leftWall.topRight.setZ(h);

	 rightWall.bottomLeft.setZ(-h);
	 rightWall.bottomRight.setZ(-h);
	 rightWall.topLeft.setZ(h);
	 rightWall.topRight.setZ(h);

	 frontWall.bottomLeft.setZ(-h);
	 frontWall.bottomRight.setZ(-h);
	 frontWall.topLeft.setZ(h);
	 frontWall.topRight.setZ(h);

	 backWall.bottomLeft.setZ(-h);
	 backWall.bottomRight.setZ(-h);
	 backWall.topLeft.setZ(h);
	 backWall.topRight.setZ(h);

}


//------------------------------------------------------------------------

 MyDouble Room::getLength()
{
	return (Floor.topLeft.getX() - Floor.bottomLeft.getX());

}

//------------------------------------------------------------------------

 MyDouble Room::getWidth()
{
	return (Floor.topLeft.getY() - Floor.topRight.getY());

}

//------------------------------------------------------------------------

 MyDouble Room::getHeight()
{
	return (leftWall.topLeft.getZ() - leftWall.bottomLeft.getZ());


}


//------------------------------------------------------------------------

 void Room::setTiltAngle(MyDouble a)
{
	dTiltAngle = a;	// to be used later
}

//------------------------------------------------------------------------

 void Room::setTumbleAngle(MyDouble a)
{
	dTumbleAngle = a;	// to be used later
}

//------------------------------------------------------------------------

 void Room::setRotateAngle(MyDouble a)
{
	dRotateAngle = a;	// to be used later
}

//------------------------------------------------------------------------

 MyDouble Room::computeDelay(MyDouble distance, MyDouble distancePerSample)
{
MyDouble	delay = fabs(distance / distancePerSample);	// samples needed, distance is now in metres
	return ( delay );
}
//------------------------------------------------------------------------

bool Room::HitBoundingBox(SourcePosition p)
{
//
// 	  based on "Fast Ray-Box Intersection"
// 				by Andrew Woo
// from "Graphics Gems", Academic Press, 1990
//

MyDouble source[kNumDim];	// of ray
MyDouble dir[kNumDim];		// of ray

MyDouble dist = p.getDistance();
MyDouble X = p.getX();
MyDouble Y = p.getY();
MyDouble Z = p.getZ();

	dir[0] = X/dist;
	dir[1] = Y/dist;
	dir[2] = Z/dist;
	source[0] = X;
	source[1] = Y;
	source[2] = Z;


MyDouble coord[kNumDim];				/* hit point */
	bool inside = true;
	int quadrant[kNumDim];
	register int i;
	int whichPlane;
	MyDouble maxT[kNumDim];
	MyDouble candidatePlane[kNumDim];

//	maxB = frontWall.topLeft.getCartesians();
//	minB = backWall.bottomLeft.getCartesians();


	/* Find candidate planes; this loop can be avoided if
   	rays cast all from the eye(assume perpsective view) */
	for (i=0; i<kNumDim; i++)
		if(source[i] < (backWall.bottomLeft.getCartesians())[i]) {
			quadrant[i] = kLeftQuadrant;
			candidatePlane[i] = (backWall.bottomLeft.getCartesians())[i];
			inside = false;
		}else if (source[i] > (frontWall.topLeft.getCartesians())[i]) {
			quadrant[i] = kRightQuadrant;
			candidatePlane[i] = (frontWall.topLeft.getCartesians())[i];
			inside = false;
		}else	{
			quadrant[i] = kMiddleQuadrant;
		}

	/* Ray source inside bounding box */
	if(inside)	{
		for (i = 0; i < kNumDim; i++)
			{
			coord[i] = source[i];
			}
	return (true);
	}


	/* Calculate T distances to candidate planes */
	for (i = 0; i < kNumDim; i++)
		if (quadrant[i] != kMiddleQuadrant && dir[i] !=0.)
			{
				maxT[i] = (candidatePlane[i]-source[i]) / dir[i];
		}else{
				maxT[i] = -1.;
			}

	/* Get largest of the maxT's for final choice of intersection */
	whichPlane = 0;
	for (i = 1; i < kNumDim; i++)
		{
		if (maxT[whichPlane] < maxT[i])
			whichPlane = i;
		}

	/* Check final candidate actually inside box */
	if (maxT[whichPlane] < 0.) return (false);
	for (i = 0; i < kNumDim; i++)
		if (whichPlane != i) {
			coord[i] = source[i] + maxT[whichPlane] *dir[i];
			if ((quadrant[i] == kRightQuadrant && coord[i] < (backWall.bottomRight.getCartesians())[i]) ||
			   (quadrant[i] == kLeftQuadrant && coord[i] > (frontWall.topLeft.getCartesians())[i]))
			   return (false);	/* outside box */
			}else {
				coord[i] = candidatePlane[i];
			}

// MyDouble x = coord[0];	// test only
// MyDouble y = coord[1];	// test only
// MyDouble z = coord[2];	// test only

	return (true);				/* ray hits box */
}



//------------------------------------------------------------------------

 CartesianMyPoint Room::getIntersection(CartesianMyPoint source, CartesianMyPoint listener)
{

if (lWhereOutside > 0)
	{

	switch (lWhereOutside)
		{
			case 	kXPlus 	: hitPoint = frontWall.intersection(source, listener); break;
			case 	kXMinus : hitPoint = backWall.intersection(source, listener); break;
			case 	kYPlus 	: hitPoint = leftWall.intersection(source, listener); break;
			case 	kYMinus : hitPoint = rightWall.intersection(source, listener); break;
			case 	kZPlus 	: hitPoint = Ceiling.intersection(source, listener); break;
			case 	kZMinus : hitPoint = Floor.intersection(source, listener); break;
			default			:	;	// nowt doing at present
		}
	}



	return hitPoint;
}

//------------------------------------------------------------------------
 bool Room::getOutsideOrInside(SourcePosition p)
{
MyDouble source[kNumDim];	// of ray
MyDouble dir[kNumDim];		// of ray

MyDouble dist = p.getDistance();
MyDouble X = p.getX();
MyDouble Y = p.getY();
MyDouble Z = p.getZ();
MyDouble t = 0;
// MyDouble temp = 0;
MyDouble cX = 0;	// candidates for hit point coordinates
MyDouble cY = 0;
MyDouble cZ = 0;
bool foundIt = false;
	dir[0] = X/dist;
	dir[1] = Y/dist;
	dir[2] = Z/dist;
	source[0] = X;
	source[1] = Y;
	source[2] = Z;
	dPreDelay = 0;

//MyDouble coord[kNumDim];
	bool isInside = true;
	lWhereOutside = 0;
	 // these will need re-jigging when room rotations are implemented
	if ((p.getX()) > (cX = (frontWall.getTopLeft()).getX()))
		{
		lWhereOutside = kXPlus;
		t = cX / X;
		cY = t * dir[1];
		if ( cY < (frontWall.getTopLeft()).getY() && cY > (frontWall.getTopRight()).getY())	// might be a candidate
			{
				cZ = t * dir[2];
				if ( cZ < (frontWall.getTopLeft()).getZ() && cZ > (frontWall.getBottomLeft()).getZ())	// it IS a candidate
					{
						foundIt = true;
					}

			}
		}
	if (!foundIt)
	{
	if (p.getX() < (cX = (backWall.getTopLeft()).getX()))
		{
		lWhereOutside = kXMinus;
		t = cX / X;
		cY = t * dir[1];
		// all surfaces are coded left-right according to the orientation of a central viewer facing them
		if ( cY < (backWall.getTopRight()).getY() && cY > (backWall.getTopLeft()).getY())	// might be a candidate
			{
				cZ = t * dir[2];
				if ( cZ < (backWall.getTopLeft()).getZ() && cZ > (backWall.getBottomLeft()).getZ())	// it IS a candidate
					{
						foundIt = true;
					}

			}

		}
	}

	if (!foundIt)
	{
//		if (cY = p.getY() > (cY = (leftWall.getTopLeft()).getY()))
		if ( p.getY() > (cY = (leftWall.getTopLeft()).getY()))
			{
			lWhereOutside += kYPlus;
			t = cY / Y;
			cX = t * dir[0];
			// all surfaces are coded left-right according to the orientation of a central viewer facing them
			if ( cX < (leftWall.getTopLeft()).getX() && cX > (leftWall.getTopRight()).getX())	// might be a candidate
				{
					cZ = t * dir[2];
					if ( cZ < (leftWall.getTopLeft()).getZ() && cZ > (leftWall.getBottomLeft()).getZ())	// it IS a candidate
						{
							foundIt = true;
						}

				}

			}
	}
	if (!foundIt)
	{
	if (p.getY() < (cY = (rightWall.getTopLeft()).getY()))
		{
		lWhereOutside += kYMinus;
		t = cY / Y;
		cX = t * dir[0];
		// all surfaces are coded left-right according to the orientation of a central viewer facing them
		if ( cX < (rightWall.getTopRight()).getX() && cX > (rightWall.getTopLeft()).getX())	// might be a candidate
			{
				cZ = t * dir[2];
				if ( cZ < (rightWall.getTopLeft()).getZ() && cZ > (rightWall.getBottomLeft()).getZ())	// it IS a candidate
					{
						foundIt = true;
					}

			}
		}
	}
	if (!foundIt)
	{
	if (p.getZ() > (cZ = (Ceiling.getTopLeft()).getZ()))
		{
		lWhereOutside += kZPlus;
		t = cZ / Z;
		cX = t * dir[0];
		// all surfaces are coded left-right according to the orientation of a central viewer facing them
		if ( cX < (rightWall.getTopRight()).getX() && cX > (rightWall.getTopLeft()).getX())	// might be a candidate
			{
				cY = t * dir[1];
				if ( cY < (rightWall.getTopLeft()).getY() && cY > (leftWall.getTopRight()).getY())	// it IS a candidate
					{
						foundIt = true;
					}

			}

		}
	}
	if (!foundIt)
	{
	if (p.getZ() < (cZ = (Floor.getTopLeft()).getZ()))
	{
		lWhereOutside += kZMinus;
		t = cZ / Z;
		cX = t * dir[0];
		// all surfaces are coded left-right according to the orientation of a central viewer facing them
		if ( cX < (rightWall.getBottomRight()).getX() && cX > (rightWall.getBottomLeft()).getX())	// might be a candidate
			{
				cY = t * dir[1];
				if ( cY < (rightWall.getBottomLeft()).getY() && cY > (leftWall.getBottomRight()).getY())	// it IS a candidate
					{
						foundIt = true;
					}
			}
		}
	}

// the above detects if its outside ( when lWhereOutside > 0 )

if (lWhereOutside > 0)
	{
		isInside	= false;
		hitPoint.setXYZV(cX,cY,cZ,1.0);
		dPreDelay	= computeDelay(dist - hitPoint.getDistance(), dDistancePerSample);
	}

	return isInside;	// other variant should return point of intersection with walls
}

//------------------------------------------------------------------------

 void Room::setInsideListener(bool in)

 {
	 insideListener = in;
 }

//------------------------------------------------------------------------

 void Room::setTurbulence(MyDouble t)
{
	dTurbulence = t;
}

//------------------------------------------------------------------------

 void Room::setAirAbsorbtion(MyDouble a)
{
	dAirAbsorbtion = a;
}

//------------------------------------------------------------------------

  void	ParametricEq::Process(float **inputs, float **outputs, long sampleFrames, long channel)
{
  // this code is based on the CSound implementation of Zoelzer's parametric eq


	float *out = outputs[channel];
	for (long samplecount = 0; (samplecount < sampleFrames); samplecount++)
		{
		input = MyDouble (inputs[channel][samplecount]);
		output 	 = (b0*input + b1*oldIn1 + b2*oldIn2 - a1*oldOut1 - a2*oldOut2)/a0;
		oldIn2 	 = oldIn1;
		oldIn1 	 = input;
		oldOut2  = oldOut1;
		oldOut1  = output;
		(*out++) = (float) output;	// replacing
		}
}//------------------------------------------------------------------------

  MyDouble	ParametricEq::Filter(MyDouble input)
{
  // this code is based on the CSound implementation of Zoelzer's parametric eq
if (lFilterMode != kFilterOff)
	{
 		output 	 = (b0*input + b1*oldIn1 + b2*oldIn2 - a1*oldOut1 - a2*oldOut2)/a0;
		oldIn2 	 = oldIn1;
		oldIn1 	 = input;
		oldOut2  = oldOut1;
		oldOut1  = output;
	return output;
	}else{
		return input;
	}
}

//------------------------------------------------------------------------

 SourcePosition::SourcePosition()
{
	dAzimuth = 0.0;
	dElevation = 0.5;
	dAzimuthInRads = 0.0;
	dElevationInRads = 0.0;
	dZeroth = 0.707;
	dFirst = 1.0;
	dSecond = 1.0;
	dThird = 1.0;
	dVolume = 1.0;
	dDistance = 1.0;
	dDistanceFactor = 0.0;
	dCentre = 0.15;

	LocalX = 1.0;
	LocalY = 0.0;
	LocalZ = 0.0;
}

//------------------------------------------------------------------------

 void SourcePosition::setAzimuth(MyDouble a)
{
	dAzimuth = a ;
	dAzimuthInRads = (-a * k2PI);

	LocalX = dDistance * cos(dAzimuthInRads) * cos(dElevationInRads);
	LocalY = dDistance * sin(dAzimuthInRads) * cos(dElevationInRads);
	LocalZ = dDistance * sin(dElevationInRads);
}

//------------------------------------------------------------------------

 void SourcePosition::setElevation(MyDouble e)
{
	dElevation = e ;
	dElevationInRads = ((e - 0.5) * kPI);

	LocalX = dDistance * cos(dAzimuthInRads) * cos(dElevationInRads);
	LocalY = dDistance * sin(dAzimuthInRads) * cos(dElevationInRads);
	LocalZ = dDistance * sin(dElevationInRads);
}

//------------------------------------------------------------------------

 void SourcePosition::setAzimuthInRads(MyDouble ar)
{
	dAzimuthInRads = ar ;
	dAzimuth = (ar / k2PI);

	LocalX = dDistance * cos(dAzimuthInRads) * cos(dElevationInRads);
	LocalY = dDistance * sin(dAzimuthInRads) * cos(dElevationInRads);
	LocalZ = dDistance * sin(dElevationInRads);
}

//------------------------------------------------------------------------

 void SourcePosition::setElevationInRads(MyDouble er)
{
	dElevationInRads = er;
	dElevation = ((dElevationInRads / kPI) + 0.5 );

	LocalX = dDistance * cos(dAzimuthInRads) * cos(dElevationInRads);
	LocalY = dDistance * sin(dAzimuthInRads) * cos(dElevationInRads);
	LocalZ = dDistance * sin(dElevationInRads);
}

//------------------------------------------------------------------------

 void SourcePosition::setDistance(MyDouble d)
{
	dDistance = d ;

	LocalX = dDistance * cos(dAzimuthInRads) * cos(dElevationInRads);
	LocalY = dDistance * sin(dAzimuthInRads) * cos(dElevationInRads);
	LocalZ = dDistance * sin(dElevationInRads);
}

//------------------------------------------------------------------------

 void SourcePosition::setDistanceFactor(MyDouble df)
{
dDistanceFactor = df ;
}

//------------------------------------------------------------------------

 void SourcePosition::setCentre(MyDouble c)
{
dCentre = c ;
}

//------------------------------------------------------------------------

 void SourcePosition::setZeroth(MyDouble z)
{
dZeroth = z ;
}

//------------------------------------------------------------------------

 void SourcePosition::setFirst(MyDouble f)
{
dFirst = f ;
}

//------------------------------------------------------------------------

 void SourcePosition::setSecond(MyDouble s)
{
dSecond = s ;
}

//------------------------------------------------------------------------

 void SourcePosition::setThird(MyDouble t)
{
dThird = t ;
}

//------------------------------------------------------------------------

 void SourcePosition::setVolume(MyDouble vol)
{
dVolume = vol ;
}

//------------------------------------------------------------------------

 void SourcePosition::incrementSourcePosition(SourcePosition param)
{
	dAzimuth		+= param.dAzimuth;
	dElevation		+= param.dElevation;
	dAzimuthInRads	+= param.dAzimuthInRads;
	dElevationInRads	+= param.dElevationInRads;
	dDistance		+= param.dDistance;
	dDistanceFactor	+= param.dDistanceFactor;
	dCentre			+= param.dCentre;
	dZeroth			+= param.dZeroth;
	dFirst			+= param.dFirst;
	dSecond			+= param.dSecond;
	dThird			+= param.dThird;
	dVolume			+= param.dVolume;

}

//------------------------------------------------------------------------

 SourcePosition SourcePosition::operator+ (SourcePosition param)
{
SourcePosition temp;

	temp.dAzimuth		+= param.dAzimuth;

	if (temp.dAzimuth > 1.0)
		{
		temp.dAzimuth -= 1.0f;
	}
	if (temp.dAzimuth < 0)
		{
		temp.dAzimuth += 1.0f;
	}

	temp.dElevation		+= param.dElevation;

	if (temp.dElevationInRads > 1.0)
		{
		temp.dElevationInRads -= 1.0f;
	}
	if (temp.dElevationInRads < 0)
		{
		temp.dElevationInRads += 1.0f;
	}

	temp.dAzimuthInRads	+= param.dAzimuthInRads;

	if (temp.dAzimuthInRads > k2PI)
		{
		temp.dAzimuthInRads -= k2PI;
	}
	if (temp.dAzimuthInRads < 0)
		{
		temp.dAzimuthInRads += k2PI;
	}

	temp.dElevationInRads	+= param.dElevationInRads;

	if (temp.dElevationInRads > kPI)
		{
		temp.dElevationInRads -= kPI;
	}
	if (temp.dElevationInRads < -kPI)
		{
		temp.dElevationInRads += kPI;
	}

	temp.dDistance		+= param.dDistance;

	temp.dDistanceFactor	+= param.dDistanceFactor;	// these next 5 may not make sense in this form
	temp.dCentre		+= param.dCentre;
	temp.dZeroth		+= param.dZeroth;
	temp.dFirst			+= param.dFirst;
	temp.dSecond		+= param.dSecond;
	temp.dThird			+= param.dThird;
	temp.dVolume		+= param.dVolume;
	return (temp);
}

//------------------------------------------------------------------------

 SourcePosition SourcePosition::operator- (SourcePosition param)
{
SourcePosition temp ;
	temp.dAzimuth		-= param.dAzimuth;

	if (temp.dAzimuth > 1.0)
		{
		temp.dAzimuth -= 1.0f;
	}
	if (temp.dAzimuth < 0)
		{
		temp.dAzimuth += 1.0f;
	}

	temp.dElevation		-= param.dElevation;

	if (temp.dAzimuth > 1.0)
		{
		temp.dAzimuth -= 1.0f;
	}
	if (temp.dAzimuth < 0)
		{
		temp.dAzimuth += 1.0f;
	}

	temp.dAzimuthInRads	-= param.dAzimuthInRads;

	if (temp.dAzimuthInRads > k2PI)
		{
		temp.dAzimuthInRads -= k2PI;
	}
	if (temp.dAzimuthInRads < 0)
		{
		temp.dAzimuthInRads += k2PI;
	}
	temp.dElevationInRads	-= param.dElevationInRads;

	if (temp.dElevationInRads > kPI)
		{
		temp.dElevationInRads -= kPI;
	}
	if (temp.dElevationInRads < -kPI)
		{
		temp.dElevationInRads += kPI;
	}
	temp.dDistance		-= param.dDistance;
	temp.dDistanceFactor	-= param.dDistanceFactor;
	temp.dCentre		-= param.dCentre;
	temp.dZeroth		-= param.dZeroth;
	temp.dFirst			-= param.dFirst;
	temp.dSecond		-= param.dSecond;
	temp.dThird			-= param.dThird;
	temp.dVolume		-= param.dVolume;
	return (temp);
}

//------------------------------------------------------------------------

 SourcePosition SourcePosition::operator/ (SourcePosition param)
{
  SourcePosition temp ;
  temp.dAzimuth			= temp.dAzimuth / param.dAzimuth;
  temp.dElevation		= temp.dElevation / param.dElevation;
  temp.dAzimuthInRads	= temp.dAzimuthInRads / param.dAzimuthInRads;
  temp.dElevationInRads	= temp.dElevationInRads / param.dElevationInRads;
  temp.dDistance		= temp.dDistance / param.dDistance;
  temp.dDistanceFactor	= temp.dDistanceFactor / param.dDistanceFactor;
  temp.dCentre			= temp.dCentre / param.dCentre;
  temp.dZeroth			= temp.dZeroth / param.dZeroth;
  temp.dFirst			= temp.dFirst / param.dFirst;
  temp.dSecond			= temp.dFirst / param.dFirst;
  temp.dThird			= temp.dThird / param.dThird;
  temp.dVolume			= temp.dVolume / param.dVolume;
  return (temp);
}

//------------------------------------------------------------------------

 SourcePosition SourcePosition::operator/ (MyDouble IncrementFraction)
{
  SourcePosition temp ;
  temp.dAzimuth			= temp.dAzimuth / IncrementFraction;
  temp.dElevation		= temp.dElevation / IncrementFraction;
  temp.dAzimuthInRads	= temp.dAzimuthInRads / IncrementFraction;
  temp.dElevationInRads		= temp.dElevationInRads / IncrementFraction;
  temp.dDistance		= temp.dDistance / IncrementFraction;
  temp.dDistanceFactor	= temp.dDistanceFactor / IncrementFraction;
  temp.dCentre			= temp.dCentre / IncrementFraction;
  temp.dZeroth			= temp.dZeroth / IncrementFraction;
  temp.dSecond			= temp.dSecond / IncrementFraction;
  temp.dThird			= temp.dThird / IncrementFraction;
  temp.dVolume			= temp.dVolume / IncrementFraction;
  return (temp);
}
//------------------------------------------------------------------------

 void SourcePosition::translate (MyDouble X, MyDouble Y, MyDouble Z, SourcePosition position)
{

//	LocalX = direct.getDistance() * cos(direct.getAzimuthInRads()) * cos(direct.getElevationInRads());
//	LocalY = dDistance * sin(direct.getAzimuthInRads()) * cos(direct.getElevationInRads());
//	LocalZ = dDistance * sin(direct.getElevationInRads());
	LocalX = 2 * (X - position.LocalX );	// translate the position
	LocalY = 2 * (Y - position.LocalY );	// of the source
	LocalZ = 2 * (Z - position.LocalZ );	// in 3 dimensions

					// then convert back to polars
					// to consider - merits of concentrating on cartesians, not polars

	dDistance			= sqrt( LocalX*LocalX + LocalY*LocalY + LocalZ*LocalZ);
	if ( dDistance == 0 || LocalY == 0 )
		{
			dAzimuthInRads	= 0;
	}else{
		dAzimuthInRads	= asin(LocalY / dDistance);
	}
	if ( dDistance == 0 || LocalZ == 0 )
		{
			dElevationInRads	= 0;
	}else{
		dElevationInRads	= asin(LocalZ / dDistance);
	}
	dAzimuth		= dAzimuthInRads / k2PI;
	dElevation	= dElevationInRads / k2PI;

}
//------------------------------------------------------------------------

void SourcePosition::setPosition ()
{
	Position.setXYZV(LocalX, LocalY, LocalZ, dVolume);	// in case position does not exist at this point
}
//------------------------------------------------------------------------

void SourcePosition::setPosition (CartesianMyPoint position)
{
	Position = position;
	LocalX = Position.getX();
	LocalY = Position.getY();
	LocalZ = Position.getZ();

					// then convert back to polars
					// to consider - merits of concentrating on cartesians, not polars

	dDistance			= sqrt( LocalX*LocalX + LocalY*LocalY + LocalZ*LocalZ);
	if ( dDistance == 0 || LocalY == 0 )
		{
			dAzimuthInRads	= 0;
	}else{
		dAzimuthInRads	= asin(LocalY / dDistance);
	}
	if ( dDistance == 0 || LocalZ == 0 )
		{
			dElevationInRads	= 0;
	}else{
		dElevationInRads	= asin(LocalZ / dDistance);
	}
	dAzimuth		= dAzimuthInRads / k2PI;
	dElevation	= dElevationInRads / k2PI;

}
//------------------------------------------------------------------------

CartesianMyPoint SourcePosition::getPosition ()
{
	return Position;
}
//------------------------------------------------------------------------

 DSoundSource::DSoundSource()
{
	dWOld = 0.707;
	dXOld = 1.0f;
	dYOld = 0.0f;
	dZOld = 0.0f;
	dDelayOld = 0.0f;
}

//------------------------------------------------------------------------

 void DSoundSource::setIncCount(long IncCount)
{
incCount = IncCount;
}

//------------------------------------------------------------------------

 void DSoundSource::setAudioBuffer(MyDouble * buffer, long bufferLength, MyDouble distancepersample)
{
	AudioBuffer = buffer;
	lAudioBufferLength = bufferLength;
	lAudioBufferTop = lAudioBufferLength - 1;
	dDistancePerSample = distancepersample;
	lWriteOffset = 128;	// allow some space in the buffer - minimise this to minimise
						// delay through plug

	lAudioReadPos	= lAudioReadReference = 0;
	lAudioWritePos	= lAudioReadPos + lWriteOffset;	// a bit of free space, here

}

//------------------------------------------------------------------------

 void DSoundSource::setControlBuffer(MyDouble * buffer, long bufferLength, MyDouble distancepersample)
{
	ControlBuffer = buffer;
	lControlBufferLength = bufferLength;
	lControlBufferTop	= lControlBufferLength - 1;
//	dDistancePerSample = distancepersample;	// logically, this is done in setAudioBuffer - but might need to be done here, too
	lControlReadPos		= 0;
	lWriteOffset = 128;	// allow some space in the buffer - minimise this to minimise
						// delay through plug

	lControlWritePos	= lControlReadPos + lWriteOffset;	// a bit of free space, here

}


//------------------------------------------------------------------------

 void DSoundSource::setLeftRightMix(MyDouble v)
{
dLeftRightMix= v ;
dRightLeftMix= 1 - v ;
}

//------------------------------------------------------------------------

 void DSoundSource::setWetDryMix(MyDouble value)
{
dWetDryMix= value ;
dDryWetMix = 1 - value;
}

//------------------------------------------------------------------------

 void DSoundSource::setVolume(MyDouble vol)
{
dVolume = vol ;
}

//------------------------------------------------------------------------

 void DSoundSource::setInsideSource(bool in, CartesianMyPoint hitPoint)
 {
	insideSource = in;
	HitPoint = hitPoint;
	HitPosition.setPosition(hitPoint);

 }
//------------------------------------------------------------------------
// this is used mostly to say, actually, it's inside
 void DSoundSource::setInsideSource(bool in)
 {
	insideSource = in;
 }
//------------------------------------------------------------------------

 void DSoundSource::setFilterSpectrum(ParametricEq & fs )
{
	filterSpectrum = fs;
}
//------------------------------------------------------------------------
MyDouble DSoundSource::FilterIt(MyDouble input, ParametricEq &spectrum)
{
  // this code is based on the CSound implementation of Zoelzer's parametric eq
if (spectrum.lFilterMode != kFilterOff)
	{
 		spectrum.output 	= (spectrum.b0*input + spectrum.b1*spectrum.oldIn1 + spectrum.b2*spectrum.oldIn2 - spectrum.a1*spectrum.oldOut1 - spectrum.a2*spectrum.oldOut2)/spectrum.a0;
		spectrum.oldIn2		= spectrum.oldIn1;
		spectrum.oldIn1		= input;
		spectrum.oldOut2	= spectrum.oldOut1;
		spectrum.oldOut1	= spectrum.output;
	return spectrum.output;
	}else{
		return input;
	}
}


//------------------------------------------------------------------------

 void DirectDSoundSource::moveSource(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel)

{ // put all the coeff calculations in an 'updateCoefficients' method, call from the user interface thread

//	float *input  =  inputs[channel];
// 	float *inputLeft  =  inputs[0];
// 	float *inputRight  =  inputs[1];

	float *outW = outputs[0];
	float *outX = outputs[1];
	float *outY = outputs[2];
	float *outZ = outputs[3];

	MyDouble dSample;
	MyDouble dWInc;
	MyDouble dXInc;
	MyDouble dYInc;
	MyDouble dZInc;

	MyDouble dInterpolateFraction;
	MyDouble dVol;

	MyDouble dFirstDistance;
	MyDouble dZerothDistance;
 	MyDouble dDistanceMultiplier;
	MyDouble dZeroth;
	MyDouble dFirst;
	MyDouble dCentre;
	MyDouble dDistance;
	MyDouble dDistanceFactor;
	MyDouble dAzimuth;
	MyDouble dElevation;
	MyDouble dDelayOld;
	MyDouble dDelay;
	MyDouble dDelayInc;

	long	lReadPosition0;
	long	lReadPosition2;
	long	lReadPosition3;

if (lockSection == true)
	{

		dWInc		= 0.0;
		dXInc		= 0.0;
		dYInc		= 0.0;
		dZInc		= 0.0;

	}else{	// none of the following is done if setParameter is running somewhere


	if (insideSource == true)
		{
			dVol = (gainLaw(EndPosition.getVolume())) * dDryWetMix;
		}else{
			dVol = (gainLaw(EndPosition.getVolume())) * dDryWetMix * dLoss;
		}
		dFirstDistance = 1.0f;
		dZerothDistance = 1.0f;
		dDistanceMultiplier = 0.0f;

		dZeroth = EndPosition.getZeroth();
		dFirst = EndPosition.getFirst();
		dCentre = EndPosition.getCentre();
		dDistance = EndPosition.getDistance();
		dDistanceFactor = EndPosition.getDistanceFactor();
		dAzimuth = EndPosition.getAzimuthInRads();
		dElevation = EndPosition.getElevationInRads();

		dDelayOld = computeDelay (CurrentPosition.getDistance());
		dDelay	= computeDelay (dDistance);
	//	dDelayInc = (dDelay - dDelayOld)/incCount;	// needs to be sorted, in the meantime ....
		dDelayInc = (dDelay - dDelayOld)/sampleFrames;


		dX = cos(dAzimuth) * cos(dElevation);
		dY = sin(dAzimuth) * cos(dElevation);
		dZ = sin(dElevation);


		if ( dDistance >= dCentre ) {

			dDistanceMultiplier = distancing(kdBFactor, dDistanceFactor,  dDistance,  dCentre);
			// kdBFactor is a constant factor which allows us to set the maxloss
			// the maxloss per doubling of distance

			dZerothDistance = dVol * dZeroth * dDistanceMultiplier;
			dFirstDistance =  dVol * dFirst * dDistanceMultiplier;
		} else {
			dZerothDistance = dVol * dZeroth * (2 - (dDistance / dCentre));
			dFirstDistance =  dVol * dFirst * (dDistance / dCentre) ;
		}

		dW = kAntiDenorm + dZerothDistance;
		dX = kAntiDenorm + dFirstDistance * dX;
		dY = kAntiDenorm + dFirstDistance * dY;
		dZ = kAntiDenorm + dFirstDistance * dZ;


		dWInc	= (dW - dWOld)/sampleFrames;
		dXInc	= (dX - dXOld)/sampleFrames;
		dYInc	= (dY - dYOld)/sampleFrames;
		dZInc	= (dZ - dZOld)/sampleFrames;

		if ( fabs(dWInc) < kAntiDenorm)
			{
				dWInc = 0.0f;
			}
		if ( fabs(dXInc) < kAntiDenorm)
			{
				dXInc = 0.0f;
			}
		if ( fabs(dYInc) < kAntiDenorm)
			{
				dYInc = 0.0f;
			}
		if ( fabs(dZInc) < kAntiDenorm)
			{
				dZInc = 0.0f;
			}
	}
	for (long samplecount = 0; (samplecount < sampleFrames); samplecount++)
     {
		lControlWritePos = lAudioWritePos;
		lControlWritePos = lControlWritePos << 2;	// allows for accessing four MyDoubles

//		AudioBuffer[lAudioWritePos] = MyDouble  ((dRightLeftMix * inputs[0][samplecount]) +  (dLeftRightMix * inputs[1][samplecount]) );
		AudioBuffer[lAudioWritePos] = FilterIt((dRightLeftMix * inputs[0][samplecount]) +  (dLeftRightMix * inputs[1][samplecount]), filterSpectrum);

		lAudioWritePos++;
		if (lAudioWritePos > lAudioBufferTop)
			{
			lAudioWritePos = 0;
			}

		if (dDelayOld <= kMinDelay)
			dDelayOld = kMinDelay;	// catches errors of delay on entry, not just after first pass
									// through dDelayold += dDelayInc

		lAudioReadPos = (long) ( (MyDouble)lAudioReadReference - fastfloor(dDelayOld));

		if (++lAudioReadReference > lAudioBufferTop )
			lAudioReadReference = 0;
		if (lAudioReadPos < 0 )
			{
				lAudioReadPos = lAudioBufferLength + lAudioReadPos;
			}
			//else{
			//	if (lAudioReadPos > lAudioBufferTop )
			//		lAudioReadPos = lAudioReadPos - lAudioBufferLength;
			//	}
		dInterpolateFraction = dDelayOld - fastfloor(dDelayOld);

		lReadPosition0 = lAudioReadPos - 1 ;
		if (lReadPosition0 < 0 ) {
			lReadPosition0 += (lAudioBufferLength);
			}
		lReadPosition2 = lAudioReadPos + 1 ;

		if (lReadPosition2 > lAudioBufferTop ) {
			lReadPosition2 = 0;
			}
		lReadPosition3 = lReadPosition2 + 1 ;
		if (lReadPosition3 > lAudioBufferTop ) {
			lReadPosition3 = 0 ;
			}

	if (dDelayOld != kMinDelay)
		{
		dSample = ( interpSpline (dInterpolateFraction , AudioBuffer[lReadPosition0], AudioBuffer[lAudioReadPos], AudioBuffer[lReadPosition2], AudioBuffer[lReadPosition3]));
	}else{
		dSample = AudioBuffer[lAudioReadPos];	// if at minimum delay, don't need interpolation - can we extend this to check all near to quantised locations?
												// or even use the 'distance to nearest quantised point' to regulate degree of interpolation?
		}

	if (replacing)
		{
				(*outW) = float (kAntiDenorm + dSample * dWOld); 	// replacing
				(*outX++) = float (kAntiDenorm + dSample * dXOld);	// replacing
				(*outY++) = float (kAntiDenorm + dSample * dYOld);	// replacing
				(*outZ++) = float (kAntiDenorm + dSample * dZOld);	// replacing
				*outW++;
			}else{	// NOT processReplacing
				(*outW++) += float (kAntiDenorm + dSample * dWOld);	// accumulating
				(*outX++) += float (kAntiDenorm + dSample * dXOld);	// accumulating
				(*outY++) += float (kAntiDenorm + dSample * dYOld);	// accumulating
				(*outZ++) += float (kAntiDenorm + dSample * dZOld);	// accumulating
			}

	  	dWOld		+= dWInc ;
		dXOld		+= dXInc ;
		dYOld		+= dYInc ;
		dZOld		+= dZInc ;
		dDelayOld += dDelayInc;

//		ControlBuffer[lControlWritePos] = CurrentPosition.getAzimuth;
//		ControlBuffer[lControlWritePos + 1] = CurrentPosition.getElevation;
//		ControlBuffer[lControlWritePos + 2] = CurrentPosition.getDistance;
//		ControlBuffer[lControlWritePos + 3] = CurrentPosition.getVolume;
//
//		CurrentPosition.incrementSourcePosition(increment);


    }
CurrentPosition = EndPosition;

}


//------------------------------------------------------------------------

 void DirectDSoundSource::moveSourceFHH(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel)

{ // put all the coeff calculations in an 'updateCoefficients' method, call from the user interface thread

//	float *input  =  inputs[channel];
// 	float *inputLeft  =  inputs[0];
// 	float *inputRight  =  inputs[1];
	float *outW  =  outputs[0];
	float *outX  =  outputs[1];
	float *outY  =  outputs[2];
	float *outZ  =  outputs[3];
	float *outU  =  outputs[4];
	float *outV  =  outputs[5];
	float *outP  =  outputs[6];
	float *outQ  =  outputs[7];

	MyDouble dSample;
	MyDouble dWInc;
	MyDouble dXInc;
	MyDouble dYInc;
	MyDouble dZInc;
	MyDouble dUInc;
	MyDouble dVInc;
	MyDouble dPInc;
	MyDouble dQInc;

	MyDouble dInterpolateFraction;
	MyDouble dVol;

	MyDouble dFirstDistance;
	MyDouble dZerothDistance;
	MyDouble dSecondDistance;
	MyDouble dThirdDistance;

 	MyDouble dDistanceMultiplier;
	MyDouble dZeroth;
	MyDouble dFirst;
	MyDouble dSecond;
	MyDouble dThird;

	MyDouble dCentre;
	MyDouble dDistance;
	MyDouble dDistanceFactor;
	MyDouble dAzimuth;
	MyDouble dElevation;
	MyDouble dDelayOld;
	MyDouble dDelay;
	MyDouble dDelayInc;

	long	lReadPosition0;
	long	lReadPosition2;
	long	lReadPosition3;

if (lockSection == true)
	{

		dWInc		= 0.0;
		dXInc		= 0.0;
		dYInc		= 0.0;
		dZInc		= 0.0;
		dUInc		= 0.0;
		dVInc		= 0.0;
		dPInc		= 0.0;
		dQInc		= 0.0;

	}else{	// none of the following is done if setParameter is running somewhere


	if (insideSource == true)
		{
			dVol = (gainLaw(EndPosition.getVolume())) * dDryWetMix;
		}else{
			dVol = (gainLaw(EndPosition.getVolume())) * dDryWetMix * dLoss;
		}
		dFirstDistance = 1.0f;
		dZerothDistance = 1.0f;
		dDistanceMultiplier = 0.0f;

		dZeroth = EndPosition.getZeroth();
		dFirst = EndPosition.getFirst();
		dSecond = EndPosition.getSecond();
		dThird = EndPosition.getThird();
		dCentre = EndPosition.getCentre();
		dDistance = EndPosition.getDistance();
		dDistanceFactor = EndPosition.getDistanceFactor();
		dAzimuth = EndPosition.getAzimuthInRads();
		dElevation = EndPosition.getElevationInRads();

		dDelayOld = computeDelay (CurrentPosition.getDistance());
		dDelay	= computeDelay (dDistance);
	//	dDelayInc = (dDelay - dDelayOld)/incCount;	// needs to be sorted, in the meantime ....
		dDelayInc = (dDelay - dDelayOld)/sampleFrames;


		dX = cos(dAzimuth) * cos(dElevation);
		dY = sin(dAzimuth) * cos(dElevation);
		dZ = sin(dElevation);

		dU = cos(2 * dAzimuth) * cos(dElevation);
		dV = sin(2 * dAzimuth) * cos(dElevation);

		dP = cos(3 * dAzimuth) * cos(dElevation);
		dQ = sin(3 * dAzimuth) * cos(dElevation);



		if ( dDistance >= dCentre ) {

			dDistanceMultiplier = distancing(kdBFactor, dDistanceFactor,  dDistance,  dCentre);
			// kdBFactor is a constant factor which allows us to set the maxloss
			// the maxloss per doubling of distance

			dZerothDistance = dVol * dZeroth * dDistanceMultiplier;
			dFirstDistance 	=  dVol * dFirst * dDistanceMultiplier;
 			dSecondDistance = dVol * dSecond * dDistanceMultiplier;
			dThirdDistance 	=  dVol * dThird * dDistanceMultiplier;

		} else {
			dZerothDistance = dVol * dZeroth * (2 - (dDistance / dCentre));
			dFirstDistance 	= dVol * dFirst * (dDistance / dCentre) ;
			dSecondDistance = dVol * dSecond * (dDistance / dCentre) ;
 			dThirdDistance 	= dVol * dThird * (dDistance / dCentre ) ;

		}

		dW = kAntiDenorm + dZerothDistance;
		dX = kAntiDenorm + dFirstDistance * dX;
		dY = kAntiDenorm + dFirstDistance * dY;
		dZ = kAntiDenorm + dFirstDistance * dZ;

		dU = kAntiDenorm + dSecondDistance * dU;
		dV = kAntiDenorm + dSecondDistance * dV;

		dP = kAntiDenorm + dThirdDistance * dP;
		dQ = kAntiDenorm + dThirdDistance * dQ;


		dWInc	= (dW - dWOld)/sampleFrames;
		dXInc	= (dX - dXOld)/sampleFrames;
		dYInc	= (dY - dYOld)/sampleFrames;
		dZInc	= (dZ - dZOld)/sampleFrames;
		dUInc	= (dU - dUOld)/sampleFrames;
		dVInc	= (dV - dVOld)/sampleFrames;
		dPInc	= (dP - dPOld)/sampleFrames;
		dQInc	= (dQ - dQOld)/sampleFrames;

		if ( fabs(dWInc) < kAntiDenorm)
			{
				dWInc = 0.0f;
			}
		if ( fabs(dXInc) < kAntiDenorm)
			{
				dXInc = 0.0f;
			}
		if ( fabs(dYInc) < kAntiDenorm)
			{
				dYInc = 0.0f;
			}
		if ( fabs(dZInc) < kAntiDenorm)
			{
				dZInc = 0.0f;
			}
		if ( fabs(dUInc) < kAntiDenorm)
			{
				dUInc = 0.0f;
			}
		if ( fabs(dVInc) < kAntiDenorm)
			{
				dVInc = 0.0f;
			}
		if ( fabs(dPInc) < kAntiDenorm)
			{
				dPInc = 0.0f;
			}
		if ( fabs(dQInc) < kAntiDenorm)
			{
				dQInc = 0.0f;
			}
	}
	for (long samplecount = 0; (samplecount < sampleFrames); samplecount++)
     {
		lControlWritePos = lAudioWritePos;
		lControlWritePos = lControlWritePos << 2;	// allows for accessing four MyDoubles

//		AudioBuffer[lAudioWritePos] = MyDouble  ((dRightLeftMix * inputs[0][samplecount]) +  (dLeftRightMix * inputs[1][samplecount]) );
		AudioBuffer[lAudioWritePos] = FilterIt((dRightLeftMix * inputs[0][samplecount]) +  (dLeftRightMix * inputs[1][samplecount]), filterSpectrum);

		lAudioWritePos++;
		if (lAudioWritePos > lAudioBufferTop)
			{
			lAudioWritePos = 0;
			}

		if (dDelayOld <= kMinDelay)
			dDelayOld = kMinDelay;	// catches errors of delay on entry, not just after first pass
									// through dDelayold += dDelayInc

		lAudioReadPos = (long) ( (MyDouble)lAudioReadReference - fastfloor(dDelayOld));

		if (++lAudioReadReference > lAudioBufferTop )
			lAudioReadReference = 0;
		if (lAudioReadPos < 0 )
			{
				lAudioReadPos = lAudioBufferLength + lAudioReadPos;
			}
			//else{
			//	if (lAudioReadPos > lAudioBufferTop )
			//		lAudioReadPos = lAudioReadPos - lAudioBufferLength;
			//	}
		dInterpolateFraction = dDelayOld - fastfloor(dDelayOld);

		lReadPosition0 = lAudioReadPos - 1 ;
		if (lReadPosition0 < 0 ) {
			lReadPosition0 += (lAudioBufferLength);
			}
		lReadPosition2 = lAudioReadPos + 1 ;

		if (lReadPosition2 > lAudioBufferTop ) {
			lReadPosition2 = 0;
			}
		lReadPosition3 = lReadPosition2 + 1 ;
		if (lReadPosition3 > lAudioBufferTop ) {
			lReadPosition3 = 0 ;
			}

	if (dDelayOld != kMinDelay)
		{
		dSample = ( interpSpline (dInterpolateFraction , AudioBuffer[lReadPosition0], AudioBuffer[lAudioReadPos], AudioBuffer[lReadPosition2], AudioBuffer[lReadPosition3]));
	}else{
		dSample = AudioBuffer[lAudioReadPos];	// if at minimum delay, don't need interpolation - can we extend this to check all near to quantised locations?
												// or even use the 'distance to nearest quantised point' to regulate degree of interpolation?
		}

	if (replacing)
		{
				(*outW) = float (kAntiDenorm + dSample * dWOld); 	// replacing
				(*outX++) = float (kAntiDenorm + dSample * dXOld);	// replacing
				(*outY++) = float (kAntiDenorm + dSample * dYOld);	// replacing
				(*outZ++) = float (kAntiDenorm + dSample * dZOld);	// replacing
				(*outU++) = float (kAntiDenorm + dSample * dUOld);	// replacing
				(*outV++) = float (kAntiDenorm + dSample * dVOld);	// replacing
				(*outP++) = float (kAntiDenorm + dSample * dPOld);	// replacing
				(*outQ++) = float (kAntiDenorm + dSample * dQOld);	// replacing
				*outW++;
			}else{	// NOT processReplacing
				(*outW++) += float (kAntiDenorm + dSample * dWOld);	// accumulating
				(*outX++) += float (kAntiDenorm + dSample * dXOld);	// accumulating
				(*outY++) += float (kAntiDenorm + dSample * dYOld);	// accumulating
				(*outZ++) += float (kAntiDenorm + dSample * dZOld);	// accumulating
				(*outU++) += float (kAntiDenorm + dSample * dUOld);	// accumulating
				(*outV++) += float (kAntiDenorm + dSample * dVOld);	// accumulating
				(*outP++) += float (kAntiDenorm + dSample * dPOld);	// accumulating
				(*outQ++) += float (kAntiDenorm + dSample * dQOld);	// accumulating

			}

	  	dWOld		+= dWInc ;
		dXOld		+= dXInc ;
		dYOld		+= dYInc ;
		dZOld		+= dZInc ;
		dUOld		+= dUInc ;
		dVOld		+= dVInc ;
		dPOld		+= dPInc ;
		dQOld		+= dQInc ;
		dDelayOld += dDelayInc;

//		ControlBuffer[lControlWritePos] = CurrentPosition.getAzimuth;
//		ControlBuffer[lControlWritePos + 1] = CurrentPosition.getElevation;
//		ControlBuffer[lControlWritePos + 2] = CurrentPosition.getDistance;
//		ControlBuffer[lControlWritePos + 3] = CurrentPosition.getVolume;
//
//		CurrentPosition.incrementSourcePosition(increment);


    }
CurrentPosition = EndPosition;

}

//------------------------------------------------------------------------

 void DirectDSoundSource::positionSource(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel)

{

// 	float *input  =  inputs[channel];

	float *outW = outputs[0];
	float *outX = outputs[1];
	float *outY = outputs[2];
	float *outZ = outputs[3];

	MyDouble	dSample;

	MyDouble 	dInterpolateFraction;
	long	lReadPosition0;
	long	lReadPosition2;
	long	lReadPosition3;


//	MyDouble dVol = gainLaw(EndPosition.getVolume());

	MyDouble dVol = (gainLaw(EndPosition.getVolume())) * dDryWetMix;

	MyDouble dFirstDistance = 1.0f;
	MyDouble dZerothDistance = 1.0f;
 	MyDouble dDistanceMultiplier = 0.0f;


	dX = cos(EndPosition.getAzimuthInRads())* cos(EndPosition.getElevationInRads());
	dY = sin(EndPosition.getAzimuthInRads())* cos(EndPosition.getElevationInRads());
	dZ = sin(EndPosition.getElevation());


 			if ( dDistance >= dCentre ) {

				dDistanceMultiplier = distancing(kdBFactor, dDistanceFactor,  dDistance,  dCentre);
 				// kdBFactor is a constant factor which allows us to set the maxloss
 				// the maxloss per doubling of distance

 				dZerothDistance = dVol * dZeroth * dDistanceMultiplier;
 				dFirstDistance =  dVol * dFirst * dDistanceMultiplier;
 			} else {
 				dZerothDistance = dVol * dZeroth * (2 - (dDistance / dCentre));
 				dFirstDistance =  dVol * dFirst * (dDistance / dCentre) ;
 			}

			dW = kAntiDenorm + dZerothDistance;
			dX = kAntiDenorm + dFirstDistance * dX;
			dY = kAntiDenorm + dFirstDistance * dY;
			dZ = kAntiDenorm + dFirstDistance * dZ;




 	for (long samplecount = 0; (samplecount < sampleFrames); samplecount++)
     {
		AudioBuffer[lAudioWritePos] = MyDouble  ((dRightLeftMix * inputs[0][samplecount]) +  (dLeftRightMix * inputs[1][samplecount]) );
		lAudioWritePos++;
		if (lAudioWritePos > lAudioBufferTop)
			{
			lAudioWritePos = 0;
			}

		lAudioReadPos = (long) ( (MyDouble)lAudioReadReference - fastfloor(dDelayOld));

		if (++lAudioReadReference > lAudioBufferTop )
			lAudioReadReference = 0;
		if (lAudioReadPos < 0 )
			{
				lAudioReadPos = lAudioBufferLength + lAudioReadPos;
			}/*else{
				if (lAudioReadPos > lAudioBufferTop )
					lAudioReadPos = lAudioReadPos - lAudioBufferLength;
				}*/
		dInterpolateFraction = dDelayOld - fastfloor(dDelayOld);

		lReadPosition0 = lAudioReadPos - 1 ;
		if (lReadPosition0 < 0 ) {
			lReadPosition0 += (lAudioBufferLength);
			}
		lReadPosition2 = lAudioReadPos + 1 ;

		if (lReadPosition2 > lAudioBufferTop ) {
			lReadPosition2 = 0;
			}
		lReadPosition3 = lReadPosition2 + 1 ;
		if (lReadPosition3 > lAudioBufferTop ) {
			lReadPosition3 = 0 ;
			}


	dSample = ( interpSpline (dInterpolateFraction , AudioBuffer[lReadPosition0], AudioBuffer[lAudioReadPos], AudioBuffer[lReadPosition2], AudioBuffer[lReadPosition3]));

	if (replacing)
			{
				(*outW++) = float (kAntiDenorm + dSample * dWOld); 	// replacing
				(*outX++) = float (kAntiDenorm + dSample * dXOld);		// replacing
				(*outY++) = float (kAntiDenorm + dSample * dYOld);     // replacing
				(*outZ++) = float (kAntiDenorm + dSample * dZOld);		// replacing
			}else{	// NOT processReplacing
				(*outW++) += float (kAntiDenorm + dSample * dWOld);	// accumulating
				(*outX++) += float (kAntiDenorm + dSample * dXOld);	// accumulating
				(*outY++) += float (kAntiDenorm + dSample * dYOld);	// accumulating
				(*outZ++) += float (kAntiDenorm + dSample * dZOld);	// accumulating
		}

    }
}

//------------------------------------------------------------------------

 void ReflectedDSoundSource::moveSource(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel)

{ // put all the coeff calculations in an 'updateCoefficients' method, call from the user interface thread

// 	 float *input  =  inputs[channel];

	float *outW = outputs[0];
	float *outX = outputs[1];
	float *outY = outputs[2];
	float *outZ = outputs[3];

	MyDouble	dSampleOmni,dSampleFrontBack,dSampleLeftRight,dSampleUpDown;

	MyDouble dWInc;
	MyDouble dXInc;
	MyDouble dYInc;
	MyDouble dZInc;

	MyDouble dInterpolateFraction;
	MyDouble dVol;

	MyDouble dFirstDistance;
	MyDouble dZerothDistance;
 	MyDouble dDistanceMultiplier;
	MyDouble dZeroth;
	MyDouble dFirst;
	MyDouble dCentre;
	MyDouble dDistance;
	MyDouble dDistanceFactor;
	MyDouble dAzimuth;
	MyDouble dElevation;
	MyDouble dDelayOld;
	MyDouble dDelay;
	MyDouble dDelayInc;

	long	lReadPosition0;
	long	lReadPosition2;
	long	lReadPosition3;

if (lockSection == true)
	{

		dWInc		= 0.0;
		dXInc		= 0.0;
		dYInc		= 0.0;
		dZInc		= 0.0;

	}else{

	if (insideSource == true)
		{
			dVol = (gainLaw(EndPosition.getVolume())) * dWetDryMix;
		}else{
			dVol = (gainLaw(EndPosition.getVolume())) * dWetDryMix * wallReflection.getTransmisivity();
		}
		dFirstDistance = 1.0f;
		dZerothDistance = 1.0f;
		dDistanceMultiplier = 0.0f;

		dZeroth = EndPosition.getZeroth();
		dFirst = EndPosition.getFirst();
		dCentre = EndPosition.getCentre();
		dDistanceFactor = EndPosition.getDistanceFactor();
		if (insideSource)
			{
				dAzimuth	= EndPosition.getAzimuthInRads();
				dElevation	= EndPosition.getElevationInRads();
				dDistance	= EndPosition.getDistance();
				dDelay		= computeDelay (dDistance);

			}else{
				dAzimuth	= HitPosition.getAzimuthInRads();
				dElevation	= HitPosition.getAzimuthInRads();
				dDistance	= HitPosition.getDistance();
				dDelay		= computeDelay (dDistance) + dPreDelay;
			}

		dDelayOld = computeDelay (CurrentPosition.getDistance()); //not sure why this is here
	//	dDelayInc = (dDelay - dDelayOld)/incCount;	// needs to be sorted, in the meantime ....
		dDelayInc = (dDelay - dDelayOld)/sampleFrames;


		dFirst *= wallReflection.getReflectionSharpness();

		dVol *= wallReflection.getReflectivity();
		dX = cos(dAzimuth) * cos(dElevation);
		dY = sin(dAzimuth) * cos(dElevation);
		dZ = sin(dElevation);


		if ( dDistance >= dCentre ) {

			dDistanceMultiplier = distancing(kdBFactor, dDistanceFactor,  dDistance,  dCentre);
			// kdBFactor is a constant factor which allows us to set the maxloss
			// the maxloss per doubling of distance

			dZerothDistance = dVol * dZeroth * dDistanceMultiplier;
			dFirstDistance =  dVol * dFirst * dDistanceMultiplier;
		}
		else
		{
			dZerothDistance = dVol * dZeroth * (2 - (dDistance / dCentre));
			dFirstDistance =  dVol * dFirst * (dDistance / dCentre) ;
		 }

		dW = kAntiDenorm + dZerothDistance;
		dX = kAntiDenorm + dFirstDistance * dX;
		dY = kAntiDenorm + dFirstDistance * dY;
		dZ = kAntiDenorm + dFirstDistance * dZ;


		dWInc		= (dW - dWOld)/sampleFrames;
		dXInc		= (dX - dXOld)/sampleFrames;
		dYInc		= (dY - dYOld)/sampleFrames;
		dZInc		= (dZ - dZOld)/sampleFrames;

		if ( fabs(dWInc) < kAntiDenorm)
			{
				dWInc = 0.0f;
			}
		if ( fabs(dXInc) < kAntiDenorm)
			{
				dXInc = 0.0f;
			}
		if ( fabs(dYInc) < kAntiDenorm)
			{
				dYInc = 0.0f;
			}
		if ( fabs(dZInc) < kAntiDenorm)
			{
				dZInc = 0.0f;
			}

//		if (!(dDelayOld > kMinDelay))	// done this way for performance reasons (instead of using <= )
//			dDelayOld = kMinDelay;	// catches errors of delay on entry, not just after first pass
	}  //stuff in the section before this only done if setParameter not running

	for (long samplecount = 0; (samplecount < sampleFrames); samplecount++)
     {


		if (dDelayOld <= kMinDelay)
			dDelayOld = kMinDelay;	// catches errors of delay on entry, not just after first pass
									// through dDelayold += dDelayInc

		lAudioReadPos = (long) ( (MyDouble)lAudioReadReference - fastfloor(dDelayOld));

		if (++lAudioReadReference > lAudioBufferTop )
			lAudioReadReference = 0;
		if (lAudioReadPos < 0 )
			{
				lAudioReadPos = lAudioBufferLength + lAudioReadPos;
			}
			else{
				if (lAudioReadPos > lAudioBufferTop )
					lAudioReadPos = lAudioReadPos - lAudioBufferLength;
				}

		dInterpolateFraction = dDelayOld - fastfloor(dDelayOld);

		lReadPosition0 = lAudioReadPos - 1 ;
		if (lReadPosition0 < 0 ) {
			lReadPosition0 += (lAudioBufferLength);
			}
		lReadPosition2 = lAudioReadPos + 1 ;

		if (lReadPosition2 > lAudioBufferTop ) {
			lReadPosition2 = 0;
			}
		lReadPosition3 = lReadPosition2 + 1 ;
		if (lReadPosition3 > lAudioBufferTop ) {
			lReadPosition3 = 0 ;
			}

	if (dDelayOld != kMinDelay)
		{
		dSample = ( interpSpline (dInterpolateFraction , AudioBuffer[lReadPosition0], AudioBuffer[lAudioReadPos], AudioBuffer[lReadPosition2], AudioBuffer[lReadPosition3]));

	}else{
		dSample = AudioBuffer[lAudioReadPos];	// if at minimum delay, don't need interpolation - can we extend this to check all near to quantised locations?
												// or even use the 'distance to nearest quantised point' to regulate degree of interpolation?
		}

	dSampleOmni 		= wallReflection.FilterIt(dSample, wallReflection.getSpectrum(kReflectivitySpectrum));
	dSampleFrontBack	= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kReflectivitySpectrum));
	dSampleLeftRight	= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kReflectivitySpectrum));
	dSampleUpDown		= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kReflectivitySpectrum));
/*	dSampleFrontBack	= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kFrontBackSpectrum));	// when we start using these, perhaps
	dSampleLeftRight	= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kLeftRightSpectrum));	// dOmniSample should be these 3
	dSampleUpDown		= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kUpDownSpectrum));	// averaged?? Must do simulations.....
*/
		(*outW++) += float (kAntiDenorm + dSampleOmni * dWOld);		// accumulating
    	dWOld		+= dWInc ;
	  	(*outX++) += float (kAntiDenorm + dSampleFrontBack * dXOld);// accumulating
		dXOld		+= dXInc ;
		(*outY++) += float (kAntiDenorm + dSampleLeftRight * dYOld);// accumulating
		dYOld		+= dYInc ;
		(*outZ++) += float (kAntiDenorm + dSampleUpDown * dZOld);	// accumulating
		dZOld		+= dZInc ;

		dDelayOld += dDelayInc;



    }
CurrentPosition = EndPosition;
}

//------------------------------------------------------------------------

 void ReflectedDSoundSource::moveSourceFHH(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel)

{ // put all the coeff calculations in an 'updateCoefficients' method, call from the user interface thread

//	float *input  =  inputs[channel];
// 	float *inputLeft  =  inputs[0];
// 	float *inputRight  =  inputs[1];
	float *outW  =  outputs[0];
	float *outX  =  outputs[1];
	float *outY  =  outputs[2];
	float *outZ  =  outputs[3];
	float *outU  =  outputs[4];
	float *outV  =  outputs[5];
	float *outP  =  outputs[6];
	float *outQ  =  outputs[7];

	MyDouble dSample;
	MyDouble	dSampleOmni,dSampleFrontBack,dSampleLeftRight,dSampleUpDown;

	MyDouble dWInc;
	MyDouble dXInc;
	MyDouble dYInc;
	MyDouble dZInc;
	MyDouble dUInc;
	MyDouble dVInc;
	MyDouble dPInc;
	MyDouble dQInc;

	MyDouble dInterpolateFraction;
	MyDouble dVol;

	MyDouble dFirstDistance;
	MyDouble dZerothDistance;
	MyDouble dSecondDistance;
	MyDouble dThirdDistance;

 	MyDouble dDistanceMultiplier;
	MyDouble dZeroth;
	MyDouble dFirst;
	MyDouble dSecond;
	MyDouble dThird;

	MyDouble dCentre;
	MyDouble dDistance;
	MyDouble dDistanceFactor;
	MyDouble dAzimuth;
	MyDouble dElevation;
	MyDouble dDelayOld;
	MyDouble dDelay;
	MyDouble dDelayInc;

	long	lReadPosition0;
	long	lReadPosition2;
	long	lReadPosition3;

if (lockSection == true)
	{

		dWInc		= 0.0;
		dXInc		= 0.0;
		dYInc		= 0.0;
		dZInc		= 0.0;
		dUInc		= 0.0;
		dVInc		= 0.0;
		dPInc		= 0.0;
		dQInc		= 0.0;

	}else{	// none of the following is done if setParameter is running somewhere


	if (insideSource == true)
		{
			dVol = (gainLaw(EndPosition.getVolume())) * dDryWetMix;
		}else{
			dVol = (gainLaw(EndPosition.getVolume())) * dDryWetMix * wallReflection.getTransmisivity();
		}
		dFirstDistance = 1.0f;
		dZerothDistance = 1.0f;
		dDistanceMultiplier = 0.0f;

		dZeroth = EndPosition.getZeroth();
		dFirst = EndPosition.getFirst();
		dSecond = EndPosition.getSecond();
		dThird = EndPosition.getThird();
		dCentre = EndPosition.getCentre();
		dDistance = EndPosition.getDistance();
		dDistanceFactor = EndPosition.getDistanceFactor();
		dAzimuth = EndPosition.getAzimuthInRads();
		dElevation = EndPosition.getElevationInRads();

		dDelayOld = computeDelay (CurrentPosition.getDistance());
		dDelay	= computeDelay (dDistance);
	//	dDelayInc = (dDelay - dDelayOld)/incCount;	// needs to be sorted, in the meantime ....
		dDelayInc = (dDelay - dDelayOld)/sampleFrames;

		dFirst	*= wallReflection.getReflectionSharpness();
		dSecond *= wallReflection.getReflectionSharpness();	// need to get an appropriate weighting for 2nd and third...
		dThird	*= wallReflection.getReflectionSharpness();

		dVol *= wallReflection.getReflectivity();

		dX = cos(dAzimuth) * cos(dElevation);
		dY = sin(dAzimuth) * cos(dElevation);
		dZ = sin(dElevation);

		dU = cos(2 * dAzimuth) * cos(dElevation);
		dV = sin(2 * dAzimuth) * cos(dElevation);

		dP = cos(3 * dAzimuth) * cos(dElevation);
		dQ = sin(3 * dAzimuth) * cos(dElevation);



		if ( dDistance >= dCentre ) {

			dDistanceMultiplier = distancing(kdBFactor, dDistanceFactor,  dDistance,  dCentre);
			// kdBFactor is a constant factor which allows us to set the maxloss
			// the maxloss per doubling of distance

			dZerothDistance = dVol * dZeroth * dDistanceMultiplier;
			dFirstDistance 	=  dVol * dFirst * dDistanceMultiplier;
 			dSecondDistance = dVol * dSecond * dDistanceMultiplier;
			dThirdDistance 	=  dVol * dThird * dDistanceMultiplier;

		} else {
			dZerothDistance = dVol * dZeroth * (2 - (dDistance / dCentre));
			dFirstDistance 	= dVol * dFirst * (dDistance / dCentre) ;
			dSecondDistance = dVol * dSecond * (dDistance / dCentre) ;
 			dThirdDistance 	= dVol * dThird * (dDistance / dCentre ) ;

		}

		dW = kAntiDenorm + dZerothDistance;
		dX = kAntiDenorm + dFirstDistance * dX;
		dY = kAntiDenorm + dFirstDistance * dY;
		dZ = kAntiDenorm + dFirstDistance * dZ;

		dU = kAntiDenorm + dSecondDistance * dU;
		dV = kAntiDenorm + dSecondDistance * dV;

		dP = kAntiDenorm + dThirdDistance * dP;
		dQ = kAntiDenorm + dThirdDistance * dQ;


		dWInc	= (dW - dWOld)/sampleFrames;
		dXInc	= (dX - dXOld)/sampleFrames;
		dYInc	= (dY - dYOld)/sampleFrames;
		dZInc	= (dZ - dZOld)/sampleFrames;
		dUInc	= (dU - dUOld)/sampleFrames;
		dVInc	= (dV - dVOld)/sampleFrames;
		dPInc	= (dP - dPOld)/sampleFrames;
		dQInc	= (dQ - dQOld)/sampleFrames;

		if ( fabs(dWInc) < kAntiDenorm)
			{
				dWInc = 0.0f;
			}
		if ( fabs(dXInc) < kAntiDenorm)
			{
				dXInc = 0.0f;
			}
		if ( fabs(dYInc) < kAntiDenorm)
			{
				dYInc = 0.0f;
			}
		if ( fabs(dZInc) < kAntiDenorm)
			{
				dZInc = 0.0f;
			}
		if ( fabs(dUInc) < kAntiDenorm)
			{
				dUInc = 0.0f;
			}
		if ( fabs(dVInc) < kAntiDenorm)
			{
				dVInc = 0.0f;
			}
		if ( fabs(dPInc) < kAntiDenorm)
			{
				dPInc = 0.0f;
			}
		if ( fabs(dQInc) < kAntiDenorm)
			{
				dQInc = 0.0f;
			}
	}
	for (long samplecount = 0; (samplecount < sampleFrames); samplecount++)
     {

		if (dDelayOld <= kMinDelay)
			dDelayOld = kMinDelay;	// catches errors of delay on entry, not just after first pass
									// through dDelayold += dDelayInc

		lAudioReadPos = (long) ( (MyDouble)lAudioReadReference - fastfloor(dDelayOld));

		if (++lAudioReadReference > lAudioBufferTop )
			lAudioReadReference = 0;
		if (lAudioReadPos < 0 )
			{
				lAudioReadPos = lAudioBufferLength + lAudioReadPos;
			}
			//else{
			//	if (lAudioReadPos > lAudioBufferTop )
			//		lAudioReadPos = lAudioReadPos - lAudioBufferLength;
			//	}
		dInterpolateFraction = dDelayOld - fastfloor(dDelayOld);

		lReadPosition0 = lAudioReadPos - 1 ;
		if (lReadPosition0 < 0 ) {
			lReadPosition0 += (lAudioBufferLength);
			}
		lReadPosition2 = lAudioReadPos + 1 ;

		if (lReadPosition2 > lAudioBufferTop ) {
			lReadPosition2 = 0;
			}
		lReadPosition3 = lReadPosition2 + 1 ;
		if (lReadPosition3 > lAudioBufferTop ) {
			lReadPosition3 = 0 ;
			}

//	if (dDelayOld != kMinDelay)
//		{
		dSample = ( interpSpline (dInterpolateFraction , AudioBuffer[lReadPosition0], AudioBuffer[lAudioReadPos], AudioBuffer[lReadPosition2], AudioBuffer[lReadPosition3]));
//	}else{
//		dSample = AudioBuffer[lAudioReadPos];	// if at minimum delay, don't need interpolation - can we extend this to check all near to quantised locations?
//												// or even use the 'distance to nearest quantised point' to regulate degree of interpolation?
//		}
		dSampleOmni 		= wallReflection.FilterIt(dSample, wallReflection.getSpectrum(kReflectivitySpectrum));
		dSampleFrontBack	= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kReflectivitySpectrum));
		dSampleLeftRight	= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kReflectivitySpectrum));
		dSampleUpDown		= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kReflectivitySpectrum));
/*	dSampleFrontBack	= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kFrontBackSpectrum));
	dSampleLeftRight	= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kLeftRightSpectrum));
	dSampleUpDown		= wallReflection.FilterIt(dSampleOmni, wallReflection.getSpectrum(kUpDownSpectrum));
*/
		(*outW++) += float (kAntiDenorm + dSampleOmni * dWOld);		// accumulating
    	dWOld		+= dWInc ;
	  	(*outX++) += float (kAntiDenorm + dSampleFrontBack * dXOld);// accumulating
		dXOld		+= dXInc ;
		(*outY++) += float (kAntiDenorm + dSampleLeftRight * dYOld);// accumulating
		dYOld		+= dYInc ;
		(*outZ++) += float (kAntiDenorm + dSampleUpDown * dZOld);	// accumulating
		dZOld		+= dZInc ;
		(*outU++) += float (kAntiDenorm + dSampleOmni * dUOld);		// the use of dSampleOmni is clearly not really right,
		dUOld		+= dUInc ;
		(*outV++) += float (kAntiDenorm + dSampleOmni * dVOld);		// but I'm not sure what else to do, needs working on
		dVOld		+= dVInc ;
		(*outP++) += float (kAntiDenorm + dSampleOmni * dPOld);		// accumulating
		dPOld		+= dPInc ;
		(*outQ++) += float (kAntiDenorm + dSampleOmni * dQOld);		// accumulating
		dQOld		+= dQInc ;

		dDelayOld += dDelayInc;


    }
CurrentPosition = EndPosition;

}

//------------------------------------------------------------------------

 void ReflectedDSoundSource::positionSource(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel)

{

// 	float *input  =  inputs[channel];

	float *outW = outputs[0];
	float *outX = outputs[1];
	float *outY = outputs[2];
	float *outZ = outputs[3];

	MyDouble	dSample;

	MyDouble 	dInterpolateFraction;
	long	lReadPosition0;
	long	lReadPosition2;
	long	lReadPosition3;


//	MyDouble dVol = gainLaw(EndPosition.getVolume());
	MyDouble dVol = (gainLaw(EndPosition.getVolume())) * dWetDryMix;
	MyDouble dFirstDistance = 1.0f;
	MyDouble dZerothDistance = 1.0f;
 	MyDouble dDistanceMultiplier = 0.0f;


	dX = cos(EndPosition.getAzimuthInRads())* cos(EndPosition.getElevationInRads());
	dY = sin(EndPosition.getAzimuthInRads())* cos(EndPosition.getElevationInRads());
	dZ = sin(EndPosition.getElevation());

	dFirst = 0.707;	// temp trial

 			if ( dDistance >= dCentre ) {

				dDistanceMultiplier = distancing(kdBFactor, dDistanceFactor,  dDistance,  dCentre);
 				// kdBFactor is a constant factor which allows us to set the maxloss
 				// the maxloss per doubling of distance

 				dZerothDistance = dVol * dZeroth * dDistanceMultiplier;
 				dFirstDistance =  dVol * dFirst * dDistanceMultiplier;
 			} else {
 				dZerothDistance = dVol * dZeroth * (2 - (dDistance / dCentre));
 				dFirstDistance =  dVol * dFirst * (dDistance / dCentre) ;
 			}

			dW = kAntiDenorm + dZerothDistance;
			dX = kAntiDenorm + dFirstDistance * dX;
			dY = kAntiDenorm + dFirstDistance * dY;
			dZ = kAntiDenorm + dFirstDistance * dZ;




 	for (long samplecount = 0; (samplecount < sampleFrames); samplecount++)
     {
		lAudioReadPos = (long) ( (MyDouble)lAudioReadReference - fastfloor(dDelayOld));

		if (++lAudioReadReference > lAudioBufferTop )
			lAudioReadReference = 0;
		if (lAudioReadPos < 0 )
			{
				lAudioReadPos = lAudioBufferLength + lAudioReadPos;
			}/*else{
				if (lAudioReadPos > lAudioBufferTop )
					lAudioReadPos = lAudioReadPos - lAudioBufferLength;
				}*/
		dInterpolateFraction = dDelayOld - fastfloor(dDelayOld);

		lReadPosition0 = lAudioReadPos - 1 ;
		if (lReadPosition0 < 0 ) {
			lReadPosition0 += (lAudioBufferLength);
			}
		lReadPosition2 = lAudioReadPos + 1 ;

		if (lReadPosition2 > lAudioBufferTop ) {
			lReadPosition2 = 0;
			}
		lReadPosition3 = lReadPosition2 + 1 ;
		if (lReadPosition3 > lAudioBufferTop ) {
			lReadPosition3 = 0 ;
			}


//	dSample = ( interpSpline (dInterpolateFraction , AudioBuffer[lReadPosition0], AudioBuffer[lAudioReadPos], AudioBuffer[lReadPosition2], AudioBuffer[lReadPosition3]));
	dSample = AudioBuffer[lAudioReadPos];

				(*outW++) += float (kAntiDenorm + dSample * dWOld);	// accumulating
				(*outX++) += float (kAntiDenorm + dSample * dXOld);	// accumulating
				(*outY++) += float (kAntiDenorm + dSample * dYOld);	// accumulating
				(*outZ++) += float (kAntiDenorm + dSample * dZOld);	// accumulating


    }
}


//------------------------------------------------------------------------

 SourcePosition DSoundSource::getCurrentPosition()
{

		return CurrentPosition;
}

//------------------------------------------------------------------------

 SourcePosition DSoundSource::getEndPosition()
{

		return EndPosition;
}


//------------------------------------------------------------------------

CartesianMyPoint DSoundSource::getHitPoint()
{
		return HitPoint;
}

//------------------------------------------------------------------------

 SourcePosition DSoundSource::getIncrement()
{

		return increment;
}


//------------------------------------------------------------------------

 void DSoundSource::setCurrentPosition( SourcePosition a)
{
	CurrentPosition = a;
}

//------------------------------------------------------------------------

 void DSoundSource::setEndPosition(SourcePosition a)
{
	EndPosition = a;
}

//------------------------------------------------------------------------

 void DSoundSource::setHitPoint(CartesianMyPoint a)
{
	HitPoint = a;
}

//------------------------------------------------------------------------

 void DSoundSource::setIncrement(SourcePosition a)
{
	increment = a;
}
//------------------------------------------------------------------------

 void DSoundSource::setLoss(MyDouble loss)
{
	dLoss = loss;
}
//------------------------------------------------------------------------

 void DSoundSource::setTurbulence(MyDouble t)
{
	dTurbulence = t;
}


//------------------------------------------------------------------------

 MyDouble DSoundSource::distancing(MyDouble dBFactor, MyDouble dDistanceFactor,  MyDouble dDistance, MyDouble dCentre)
{

	MyDouble temp =  pow(10, (dDistanceFactor * dBFactor * log(dDistance/ dCentre)) / kLog2);
	return temp;
}

//------------------------------------------------------------------------

 MyDouble DSoundSource::gainLaw (MyDouble gain)
{
MyDouble vol;
	if (gain >= 0.5) {
	vol = pow(10, ((gain-1) * 2.0));	// generate log gain, based on (gain - 1) * 2 * 20/20
										// giving a 20 dB range	to half way down
	} else if (gain >= 0.2) {
	vol = pow(10, ((gain-0.625)*8.0));	// generate log gain, based on (gain - factor based on end  of previous range)* 2 * 40/20))
	} else {							// giving a 40 dB range over the next 0.3 range, total = 60dB
	vol = pow(10, ((gain-0.483333333333)*12.0));
}
	return vol;
}

//------------------------------------------------------------------------

 MyDouble DSoundSource::computeDelay(MyDouble distance)
{
MyDouble	delay = fabs(distance / dDistancePerSample);	// samples needed, distance is now in metres
	if (delay < kMinDelay)
		delay = kMinDelay;// allow space for interpolation so we get no pre-echoes
	if (delay > (lAudioBufferLength - kMinDelay))
		delay = (lAudioBufferLength - kMinDelay);	// prevent buffer overruns

	return ( delay );
}


//------------------------------------------------------------------------

 MyDouble DSoundSource::interpLinear (MyDouble interp_frac, MyDouble sample_point1, MyDouble sample_point2)
{
	return (sample_point1 + (1 - interp_frac) * ( sample_point2 - sample_point1 ));
}

//------------------------------------------------------------------------

 MyDouble DSoundSource::interpSpline (MyDouble interp_frac, MyDouble sample_point0, MyDouble sample_point1, MyDouble sample_point2, MyDouble sample_point3)
{

MyDouble a0;
MyDouble a1;
MyDouble a2;
MyDouble a3;

	a3=(-sample_point0+3*(sample_point1-sample_point2)+sample_point3) * 0.1666666666666667;
	a2=(sample_point0-2*sample_point1+sample_point2) * 0.5;
	a1=(sample_point2-sample_point0) * 0.5;
	a0=(sample_point0+4*sample_point1+sample_point2) * 0.1666666666666667;

	return  (((a3*(1-interp_frac) + a2)*(1-interp_frac) + a1)*(1-interp_frac) + a0);

// return sample_point1;
}


/*------------------------------------------------------------------------

This section contains general geomtry routines, many of which are adapted from John Burardt's
Geometry source code available at http://www.csit.fsu.edu/~burkardt/cpp_src/geometry/geometry.html
Basically, I need to call with things like CartesianPoints instead of sets of x,y,z coordinates and
I prefer a different naming convention, so I changed much to meet these needs.
However, I fully and gratefully acknowledge that it would
have been a heck of a lot more work without his code. Thanks, Johm!
-----------------------------------------------------------------------------*/

//*********************************************************************
//
//  Purpose:
//
//    CrossProduct computes the cross product of two vectors (rays) in 3D.
//
//  Discussion:
//
//    The vectors are specified with respect to the origin, P0.
//    It computes the normal to the triangle (P0,P1,P2).
//
//  Modified:
//
//    19 April 1999
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, P0,P1,P2, the three points.  The basis point is P0.
//
//    Returns, P3, the cross product (P1-P0) x (P2-P0), or
//    (X1-X0,Y1-Y0,Z1-Z0) x (X2-X0,Y2-Y0,Z2-Z0).
//

CartesianMyPoint CrossProduct(CartesianMyPoint P0,CartesianMyPoint P1,CartesianMyPoint P2)
{
 CartesianMyPoint temp;
double X,Y,Z;
	X = (( P1.getY() - P0.getY() ) * ( P2.getZ() - P0.getZ() )	- ( P1.getZ() - P0.getZ() ) * ( P2.getY ()- P0.getY() ));

	Y = (( P1.getZ() - P0.getZ() ) * ( P2.getX() - P0.getX() ) - ( P1.getX() - P0.getX() ) * ( P2.getZ() - P0.getZ() ));

	Z = (( P1.getX() - P0.getX() ) * ( P2.getY() - P0.getY() ) - ( P1.getY() - P0.getY() ) * ( P2.getX() - P0.getX() ));
	temp.setXYZV(X,Y,Z,1);
	return temp;
}


//********************************************************************
//
//	normalToPlane(P0,P1,P2)
//
//  Purpose:
//	  Generates a normal to a plane, this code is based on John's
//    PLANE_EXP2NORM_3D converts an explicit plane to normal form in 3D.
// John's notes:
//  Definition:
//
//    The explicit form of a plane in 3D is
//
//      (X1,Y1,Z1), (X2,Y2,Z2), (X3,Y3,Z3).
//
//    The normal form of a plane in 3D is
//
//      (Xp,Yp,Zp), a point on the plane, and
//      (Xn,Yn,Zn), the unit normal to the plane.
//
//  Modified:
//
//    22 June 1999
//
//  Author:
//
//    John Burkardt
//  DGM's notes:
//  Parameters:
//
//    Input, P0,P1,P2, are three points
//    on the plane, which must be distinct, and not collinear.
//
//    RETURNS the unit normal vector to the plane.
//
//********************************************************************

CartesianMyPoint normalToPlane ( CartesianMyPoint P0, CartesianMyPoint P1, CartesianMyPoint P2 )

{
  double norm;
  CartesianMyPoint temp;
	temp = CrossProduct( P0,P1,P2 );

  	norm = temp.getDistance();

	if ( norm == 0.0E+00 )
		{
/*			cout << "\n"; // need to change this to access an error handler which I must write
			cout << "  normalToPlane	Fatal error!\n";
			cout << "  The normal vector is null.\n";
			cout << "  Two points coincide, or nearly so.\n";
			exit ( 1 );*/
		}else{
			temp = temp / norm;
		}
  return temp;
}

//********************************************************************

double DotProduct(CartesianMyPoint P0,CartesianMyPoint P1,CartesianMyPoint P2)

//********************************************************************
//
//  Purpose:
//
//    DotProduct(P0, P1, P2) computes the dot product of (P1-P0) and (P2-P0) in 3D.
//
//  Modified:
//
//    20 April 1999
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, P0,P1,P2, the three points.  The basis point is P0.
//
//    Returns, P3, the dot product of (P1-P0) and (P2-P0).
//
{
  double value;

  value =
    ( P1.getX() - P0.getX() ) * ( P2.getX() - P0.getX() ) +
    ( P1.getY() - P0.getY() ) * ( P2.getY() - P0.getY() ) +
    ( P1.getZ() - P0.getZ() ) * ( P2.getZ() - P0.getZ() );


  return value;
}
//******************************************************************************

// Intersection Between a Line and a Plane
//
// This occurs at the point which satisfies both the line and the plane equations.
//
// Line equation: p = org + u * dir                             (1)
//
// Plane equation: p * normal - k = 0.                          (2)
//
// Substituting (1) into (2) and rearranging we get:
//
// (org + u * dir) * normal - k = 0
//
// ie  u * dir * normal = k - org * normal
//
// ie  u = (k - org * normal) / (dir * normal)
//
// If (d * normal) = 0 then the line runs parrallel to the plane and no intersection occurs.
// The exact point at which intersection does occur can be found by plugging u back into the
// line equation in (1). From http://www.geocities.com/pcgpe/math3d.html

CartesianMyPoint IntersectLinePlane(CartesianMyPoint org, CartesianMyPoint dir, CartesianMyPoint p, CartesianMyPoint normal)	//p is a point on the plane
{
 CartesianMyPoint temp;
 double u,k;
 	k =  DotProduct(org,p,normal);
 	u = (k - DotProduct(org,normal,org)) / (DotProduct(org,dir,normal));
	temp = org + (dir * u);
	return temp;
}
