/*-----------------------------------------------------------------------------
The Ambisonic Reflector Project
(C) 2004 Dave Malham, Dogma Design

This program is released as free software; You redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.


This program will be distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

dmalham@users.sourceforge.net
-----------------------------------------------------------------------------*/
// #ifndef __BFReflector_H
// #define __BFReflector_H

#define MyDouble double
#include "audioeffectx.h"
#include "string.h"

#include "math.h"



/*enum bFormChannel
{
	kW = 0,
	kX = 1,
	kY = 2,
	kZ = 3,
	kU = 4,
	kV = 5,
	kP = 6,
	kQ = 7


};
*/

	enum
	{
		kRightQuadrant,
		kLeftQuadrant,
		kMiddleQuadrant,
		kNumDim	// 3 dimensions
	};

enum outsiders
{
	kInside = 0,
	kXPlus = 1,
	kXMinus = 2,
	kYPlus = 4,
	kYMinus = 8,
	kZPlus = 16,
	kZMinus = 32,

	kFrontEndZone1 = kXPlus + kZPlus + kYMinus,	// check frontWall, rightWall and Ceiling
	kFrontEndZone2 = kXPlus + kZPlus,			// check frontWall and Ceiling
	kFrontEndZone3 = kXPlus + kZPlus + kYPlus,	// check frontWall, leftWall and Ceiling
	kFrontEndZone4 = kXPlus + kYPlus,			// check frontWall and leftWall
	kFrontEndZone5 = kXPlus + kZMinus + kYPlus,	// check frontWall, leftWall and Floor
	kFrontEndZone6 = kXPlus + kZMinus,			// check frontWall and Floor
	kFrontEndZone7 = kXPlus + kZMinus + kYMinus,// check frontWall, rightWall and Floor
	kFrontEndZone8 = kXPlus + kYMinus,			// check frontWall and rightWall

	kBackEndZone1 = kXMinus + kZPlus + kYMinus,	// check backWall, rightWall and Ceiling
	kBackEndZone2 = kXMinus + kZPlus,			// check backWall and Ceiling
	kBackEndZone3 = kXMinus + kZPlus + kYPlus,	// check backWall, lefttWall and Ceiling
	kBackEndZone4 = kXMinus + kYPlus,			// check backWall and leftWall
	kBackEndZone5 = kXMinus + kZMinus + kYPlus,	// check backWall, leftWall and Floor
	kBackEndZone6 = kXMinus + kZMinus,			// check backWall and Floor
	kBackEndZone7 = kXMinus + kZMinus + kYMinus,// check backWall, rightWall and Floor
	kBackEndZone8 = kXMinus + kYMinus,			// check backWall and rightWall

	kMiddleZone1 = kZPlus + kYMinus,			// check rightWall and Ceiling
	kMiddleZone3 = kZPlus + kYPlus,				// check leftWall and Ceiling
	kMiddleZone5 = kZMinus + kYPlus,			// check leftWall and Floor
	kMiddleZone7 = kZMinus + kYMinus			// check rightWall and Floor

};

enum spectra
{
	kReflectivitySpectrum,
	kTransmisivitySpectrum,
	kOmniSpectrum,
	kFrontBackSpectrum,
	kLeftRightSpectrum,
	kUpDownSpectrum
};

enum stereoChannel
{
	kLeftChannel,
	kRightChannel
};

enum wallnnumber
{
kfrontWall,
kleftWall,
kbackWall,
krightWall,
kCeiling,
kFloor
};

enum filterMode
{
	kFilterOff,
	kFilterPeaking,
	kFilterLowShelf,
	kFilterHighShelf
};


enum	controlParameters
{

	kLeftRightMix,
	kAzimuthLeft,
	kElevationLeft,
	kVolumeLeft,
	kDistanceLeft,
	kDistanceFactorLeft,
	kCentreLeft,

	kScalingLeft,	 // note 1) The Reflector Pluggin originally had stereo inputs but the right channel
					 // was removed to easy performance problems during development, so this mix option
					 // was added and the right channel removed in the code. Hence it is that we
					 // only have, for instance, kAzimuthLeft and no kAzimuthRight at present
					 // Do not confuse these with things like LeftReflectionRoom, which refer to the
					 // room, not the input channel. In general, names to do with the left input channel
					 // finish with Left and those to do with the left wall start with Left
					 // note 2) The wall designations will have to change when we move away from cuboid rooms


	kRoomLength,
	kRoomWidth,
	kRoomHeight,

	kListenerX,
	kListenerY,
	kListenerZ,

	kWetDry,

	kRoomWallReflectivity,
	kRoomWallReflectivityFilterMode,
	kRoomWallReflectivityFilterFrequency,
	kRoomWallReflectivityFilterQ,
	kRoomWallReflectivityFilterGain,
	kRoomWallReflectionSharpness,

	kRoomWallReflectionLFCutoff,	// controls transition between point and diffuse reflection
	kRoomWallReflectionLFLoss,		// which 'models' LF room mode

	kRoomWallTransmisivity,
	kRoomWallTransmisivityFilterMode,
	kRoomWallTransmisivityFilterFrequency,
	kRoomWallTransmisivityFilterQ,
	kRoomWallTransmisivityFilterGain,


	kZerothLeft,
	kFirstLeft,
	kSecondLeft,
	kThirdLeft,
	kComplexityLevel,

	kNumParams
};
enum complexityLevel
{
	kNoReflectionsFxx,
	kNoReflectionsFHH,
	kFxxFxx,
	kFHHFxx,
	kFHHFHH,			// currently we are using the mixed order approach for higher orders
	kNoOfLevels			// we should have full 2nd and 3rd order as well
};


const int	kNumInputs = 2;
const int	kNumOutputs = 8;



const MyDouble	kAntiDenorm = 0.3e-20f;
#ifndef kHalfPI
#define kHalfPI    1.5707963268
#endif
#ifndef kPI
#define kPI    3.14159265358979323846
#endif
#ifndef k2PI
#define k2PI    6.283185
#endif
const MyDouble	kDtoR = kPI/180;
const MyDouble	kRtoD = 180/kPI;

const MyDouble	kLog2 = 0.6931471805599453;

const MyDouble	kMinDelay  = 4;	// allow space for interpolation so we get no pre-echoes

void MyDouble2string (MyDouble v, char *text);

const MyDouble	kdBFactor = ((-6.0)/20.0);
						// saves later maths operations in converting
						// fDistanceFactorLeft into dB's in range 0 to -maxloss
const MyDouble	k6dBMyPoint = 1 - (0.5/(20/6));
						// allows for left+right without overload
						// this is a -6dB amount if gainlaw is set for
						// 20dB drop over first part of scale

const MyDouble	kAvCoef1 = 0.999;			// coefficients for averaging
const MyDouble	kAvCoef2 = 1 - kAvCoef1;	// used to smooth controls

const MyDouble	kSpeedOfSound = 340.0;

//-------------------------------------------------------------

class MyPoint
{
public:
    MyPoint (MyDouble = 0.0, MyDouble = 0.0, MyDouble = 0.0, MyDouble = 1.0);
    	// x or azimuth, y or elevation, z or distance, volume
    	// note volume is stored in both polars[] and cartesians for convenience
    MyPoint operator + (MyPoint);
    MyPoint operator - (MyPoint);
    MyPoint operator / (MyPoint param);
    MyPoint operator / (MyDouble IncrementFraction);
    				// this version of the division operator generates increments
    MyPoint operator * (MyDouble multiplier);


 	MyDouble	getX() const { return cartesians[0]; }
 	MyDouble	getY() const { return cartesians[1]; }
 	MyDouble	getZ() const { return cartesians[2]; }

 	MyDouble& getX()  { return cartesians[0]; }
 	MyDouble& getY()  { return cartesians[1]; }
 	MyDouble& getZ()  { return cartesians[2]; }


 	MyDouble	getAzimuth() const { return polars[0]; }
 	MyDouble	getElevation() const { return polars[1]; }
 	MyDouble	getDistance() const { return polars[2]; }

 	MyDouble 	getVolume() const { return polars[3]; }

 	MyDouble& getAzimuth()  { return polars[0]; }
 	MyDouble& getElevation()  { return polars[1]; }
 	MyDouble& getDistance()  { return polars[2]; }

 	MyDouble& getVolume()  { return polars[3]; }

 	const MyDouble * getpolars() const { return polars; }
 	MyDouble * getpolars() { return polars; }

 	const MyDouble * getCartesians() const { return cartesians; }
 	MyDouble * getCartesians() { return cartesians; }

	virtual void setXYZV(MyDouble x, MyDouble y, MyDouble z, MyDouble volume);
	virtual void setX(MyDouble X);
	virtual void setY(MyDouble Y);
	virtual void setZ(MyDouble Z);

	virtual void setAzimuth(MyDouble a);
	virtual void setElevation(MyDouble e);
	virtual void setDistance(MyDouble d);

	virtual void setVolume(MyDouble Volume);

	virtual void incrementMyPoint(MyPoint);

protected:
	MyDouble	cartesians[4];
	MyDouble	polars[4];
	MyDouble	Azimuth,Elevation,Distance,X,Y,Z,Volume;

};
//-------------------------------------------------------------
class CartesianMyPoint;	// forward declaration

class PolarMyPoint : public MyPoint
{
	friend  class CartesianMyPoint;

public:
    PolarMyPoint (MyDouble = 0.0, MyDouble = 0.0, MyDouble = 1.0, MyDouble = 1.0);
//    PolarMyPoint operator = (CartesianMyPoint);
    PolarMyPoint operator + (PolarMyPoint);
    PolarMyPoint operator - (PolarMyPoint);
    PolarMyPoint operator / (PolarMyPoint param);
    PolarMyPoint operator / (MyDouble IncrementFraction);
    PolarMyPoint operator * (MyDouble multiplier);

	virtual void incrementPolarMyPoint(PolarMyPoint);	// only increments polars - for speed
	virtual void updatePolarMyPoint();		// so call this after done to update rest



};

//-------------------------------------------------------------

class CartesianMyPoint : public MyPoint
{
	friend class PolarMyPoint;
	friend class MyPoint;
public:
    CartesianMyPoint (MyDouble = 1.0, MyDouble = 0.0, MyDouble = 0.0, MyDouble = 1.0);
//	CartesianMyPoint operator = (PolarMyPoint);
    CartesianMyPoint operator + (CartesianMyPoint);
    CartesianMyPoint operator - (CartesianMyPoint);
    CartesianMyPoint operator / (CartesianMyPoint);
    CartesianMyPoint operator / (MyDouble IncrementFraction);
    CartesianMyPoint operator * (MyDouble multiplier);

	virtual void incrementCartesianMyPoint(CartesianMyPoint);	// only increments cartesians - for speed
	virtual void updateCartesianMyPoint();		// so call this after done to update rest


};

//-------------------------------------------------------------

class SourcePosition
	// the base class for all sound sources and movements
	// designed to  be stored on the time line in DirectDSoundSource and retrieved in ReflectedDSoundSource
	// note that ReflectedDSoundSource will have to interpolate between SourcePositions to recover
	// start and end points

{

public:
	SourcePosition();
	virtual ~SourcePosition() {}

    SourcePosition operator + (SourcePosition);
    SourcePosition operator - (SourcePosition);
    SourcePosition operator / (SourcePosition);
    SourcePosition operator / (MyDouble IncrementFraction);

	 	MyDouble	getAzimuth()		const { return dAzimuth; }
	 	MyDouble	getElevation() 		const { return dElevation; }
	 	MyDouble	getAzimuthInRads()	const { return dAzimuthInRads; }
	 	MyDouble	getElevationInRads() 	const { return dElevationInRads; }
	 	MyDouble	getDistance() 		const { return dDistance; }
	 	MyDouble	getDistanceFactor() const { return dDistanceFactor; }
	 	MyDouble	getCentre() 		const { return dCentre; }
	 	MyDouble	getZeroth() 		const { return dZeroth; }
	 	MyDouble	getFirst() 			const { return dFirst; }
	 	MyDouble	getSecond() 		const { return dSecond; }
	 	MyDouble	getThird() 			const { return dThird; }
	 	MyDouble 	getVolume() 		const { return dVolume; }
	 	MyDouble 	getX() 				const { return LocalX; }
	 	MyDouble 	getY() 				const { return LocalY; }
	 	MyDouble 	getZ() 				const { return LocalZ; }

	 	MyDouble& 	getAzimuth()  		{ return dAzimuth; }
	 	MyDouble& 	getElevation()  	{ return dElevation; }
	 	MyDouble&	getAzimuthInRads()	{ return dAzimuthInRads; }
	 	MyDouble&	getElevationInRads()	{ return dElevationInRads; }
	 	MyDouble& 	getDistance()  		{ return dDistance; }
	 	MyDouble&	getDistanceFactor() { return dDistanceFactor; }
	 	MyDouble&	getCentre() 		{ return dCentre; }
	 	MyDouble&	getZeroth() 		{ return dZeroth; }
	 	MyDouble&	getFirst() 			{ return dFirst; }
	 	MyDouble&	getSecond() 		{ return dSecond; }
	 	MyDouble&	getThird() 			{ return dThird; }
	 	MyDouble& 	getVolume()  		{ return dVolume; }

	virtual void setAzimuth(MyDouble a);
	virtual void setElevation(MyDouble e);
	virtual void setAzimuthInRads(MyDouble ar);
	virtual void setElevationInRads(MyDouble er);
	virtual void setDistance(MyDouble d);
	virtual void setDistanceFactor(MyDouble df);
	virtual void setCentre(MyDouble c);
	virtual void setZeroth(MyDouble z);
	virtual void setFirst(MyDouble f);
	virtual void setSecond(MyDouble s);
	virtual void setThird(MyDouble t);
	virtual void setVolume(MyDouble v);
	virtual void incrementSourcePosition(SourcePosition param);
	virtual void translate (MyDouble X, MyDouble Y, MyDouble Z, SourcePosition position);
	CartesianMyPoint 	getPosition();
	void				setPosition();	// position constructed from Local X,Y,Z
	void				setPosition (CartesianMyPoint position); 	// position constructs the Local X,Y,Z etc.
protected:

	MyDouble	dAzimuth;
	MyDouble	dElevation;
	MyDouble	dAzimuthInRads;
	MyDouble	dElevationInRads;

	MyDouble	dDistance;
	MyDouble	dDistanceFactor;
	MyDouble	dCentre;
	MyDouble	dZeroth;
	MyDouble	dFirst;
	MyDouble	dSecond;
	MyDouble	dThird;
	MyDouble	dVolume;

	MyDouble	LocalX;
	MyDouble	LocalY;
	MyDouble	LocalZ;
	CartesianMyPoint 	Position;

};

//------------------------------------------------------------------------
class Wall;	// forward declaration
class DSoundSource;	// forward declaration
class ParametricEq
{
	friend class Wall;
	friend class DSoundSource;
public:
	ParametricEq();
	~ParametricEq() {}

		long		getFilterMode()			const { return lFilterMode; }
	 	MyDouble	getFilterCutOffFreq() 	const { return dFilterCutOffFreq; }
	 	MyDouble	getFilterGain() 		const { return dFilterGain; }
	 	MyDouble	getFilterQ() 			const { return dFilterQ; }
	 	MyDouble	getSampleRate() 		const { return dSampleRate; }
	 	MyDouble 	getNyquist() 			const { return dNyquist; }

		long&		getFilterMode()			{ return lFilterMode; }
	 	MyDouble&	getFilterCutOffFreq() 	{ return dFilterCutOffFreq; }
	 	MyDouble&	getFilterGain() 		{ return dFilterGain; }
	 	MyDouble&	getFilterQ() 			{ return dFilterQ; }
	 	MyDouble&	getSampleRate() 		{ return dSampleRate; }
	 	MyDouble& 	getNyquist() 			{ return dNyquist; }


	void	setFilterCutOffFreq(MyDouble Fc);
	void	setFilterGain(MyDouble FGain);
	void	setFilterQ(MyDouble FQ);
	void	setSampleRate(MyDouble SR);
	void 	setNyquist(MyDouble Ny);
	void	setFilterMode(long mode);


	void setUp(MyDouble dFilterCutOffFreq, MyDouble dFilterGain, MyDouble dFilterQ, long lFilterMode );
	void setUp();	// just updates everything
	void Process(float **inputs, float **outputs, long sampleFrames, long channel);
	MyDouble Filter(MyDouble input);
private:
    MyDouble a0, a1, a2, b0, b1, b2;
    MyDouble dFilterCutOffFreq, dFilterGain, dFilterQ, dSampleRate, dNyquist;
    MyDouble omega, k, kk, vkk, vk, vkdq;

    MyDouble input, output, oldIn1, oldIn2, oldOut1, oldOut2, sq;

	long  lFilterMode;

};
//------------------------------------------------------------------------

class ReflectorProgram;	// forward declarations
class DSoundSource //: public SourcePosition	// the base class for all movements
{
friend class Reflector;
friend class ReflectorProgram;
friend class ParametricEq;
public:
	DSoundSource();
	virtual ~DSoundSource() {}
	long	getIncCount() const { return incCount; }
	long	getIncCount()  { return incCount; }
	virtual void setIncCount(long incCount);

	virtual void setAudioBuffer(MyDouble *buffer, long bufferlength, MyDouble distancepersample);
	virtual void setControlBuffer(MyDouble *buffer, long bufferlength, MyDouble distancepersample);


	virtual void setCurrentPosition(SourcePosition);
	virtual void setEndPosition(SourcePosition);
	virtual void setHitPoint(CartesianMyPoint);
	virtual void setIncrement(SourcePosition increment);
	virtual void setTurbulence(MyDouble);
	virtual void setInsideSource(bool yes_no, CartesianMyPoint hitPoint);
	virtual void setInsideSource(bool yes_no);

	virtual void setFilterSpectrum(ParametricEq &);

	virtual SourcePosition getCurrentPosition();
	virtual SourcePosition getEndPosition();
	virtual CartesianMyPoint getHitPoint();
	virtual SourcePosition getIncrement();
	virtual MyDouble	getTurbulence() const { return dTurbulence; };
	ParametricEq& getFilterSpectrum() {return filterSpectrum; }

	virtual	MyDouble 	getVolume() const { return dVolume; }
	virtual MyDouble& 	getVolume() { return dVolume; }
	virtual	MyDouble 	getLeftRightMix() const { return dLeftRightMix; }
	virtual MyDouble&	getLeftRightMix() { return dLeftRightMix; }
	virtual	MyDouble 	getWetDryMix() const { return dWetDryMix; }
	virtual MyDouble&	getWetDryMix() { return dWetDryMix; }
	virtual bool&		getInsideOrOut() { return insideSource; }

	virtual void setVolume(MyDouble v);
	virtual void setLeftRightMix(MyDouble v);
	virtual void setWetDryMix(MyDouble v);
	virtual void setLoss(MyDouble v);

	virtual MyDouble	FilterIt(MyDouble sample,ParametricEq &spectrum);
	virtual MyDouble	interpLinear (MyDouble interp_frac, MyDouble sample_point1, MyDouble sample_point2);
	virtual MyDouble	interpSpline (MyDouble interp_frac, MyDouble sample_point0, MyDouble sample_point1, MyDouble sample_point2, MyDouble sample_point3);
	virtual	MyDouble	computeDelay(MyDouble distance);
	virtual MyDouble	distancing(MyDouble dBFactor, MyDouble dDistanceFactor, MyDouble dDistance, MyDouble dCentre);
	virtual MyDouble	gainLaw (MyDouble	gain);

protected:
	long lControlWritePos;
	long lWriteOffset;

	MyDouble	*AudioBuffer;
	MyDouble	*ControlBuffer;

	MyDouble	dWetDryMix;
	MyDouble	dDryWetMix;
	MyDouble	dLeftRightMix;
	MyDouble	dRightLeftMix;

	long	lOffsetAudioWritePos;
	long	lAudioReadReference;

	long 	lAudioWritePos;			// the current sample input position
	long 	lAudioReadPos;			// the current sample output position
	MyDouble	dSampleRate;
	MyDouble	dNyquist;
	long 	lAudioBufferLength;
	long 	lPositionWritePos;
	MyDouble	*PositionBuffer;
 	MyDouble	dDistancePerSample;
	long 	lControlReadPos;
	long 	lControlBufferLength;
	long	lAudioBufferTop;
	long	lControlBufferTop;
	long	incCount;
	bool	lockSection;
	bool	insideSource;

//private:
	SourcePosition 	CurrentPosition;
	SourcePosition 	EndPosition;
	SourcePosition  HitPosition;	// used if the sound source is outside the room to simulate the re-radiation of the

	SourcePosition 	increment; // use the SourcePosition vector format to contain the incrments on the three axies
	CartesianMyPoint HitPoint;	// used if the sound source is outside the room to simulate the re-radiation of the
								// external source by the wall

	MyDouble	dDuration;
	MyDouble	dTurbulence;
	MyDouble	dA;
	MyDouble	dE;
	MyDouble	dW;
	MyDouble	dX;
	MyDouble	dY;
	MyDouble	dZ;
	MyDouble	dU;
	MyDouble	dV;
	MyDouble	dP;
	MyDouble	dQ;
	MyDouble	dWOld;
	MyDouble	dXOld;
	MyDouble	dYOld;
	MyDouble	dZOld;
	MyDouble	dUOld;
	MyDouble	dVOld;
	MyDouble	dPOld;
	MyDouble	dQOld;

	MyDouble 	dDelay;
	MyDouble	dDelayOld;
	MyDouble	dDelayInc;
	MyDouble	dSample;

	MyDouble	dWInc;
	MyDouble	dXInc;
	MyDouble	dYInc;
	MyDouble	dZInc;
	MyDouble	dUInc;
	MyDouble	dVInc;
	MyDouble	dPInc;
	MyDouble	dQInc;

	MyDouble 	dInterpolateFraction;
	long	lReadPosition0;
	long	lReadPosition2;


	long lNumOutputs;

	long	lReadPosition3;

	MyDouble dVol;

	MyDouble dFirstDistance;
	MyDouble dZerothDistance;
 	MyDouble dDistanceMultiplier;
	MyDouble dVolume;
	MyDouble dZeroth;
	MyDouble dFirst;
	MyDouble dSecond;
	MyDouble dThird;
	MyDouble dCentre;
	MyDouble dDistance;
	MyDouble dDistanceFactor;
	MyDouble dAzimuth;
	MyDouble dElevation;
	MyDouble dLoss;
	CartesianMyPoint PositionOfSource;
	ParametricEq filterSpectrum;
};
//------------------------------------------------------------------------

class DirectDSoundSource : public DSoundSource	// the derived class for movements of the direct path
{

public:

	virtual void moveSource(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel);
	virtual void moveSourceFHH(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel);
	virtual void positionSource(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel);

};


//------------------------------------------------------------------------

class Wall  //: public ParametricEq
{
	friend class Room;
public:
	Wall();
	virtual ~Wall() {}
	void setTopLeft(CartesianMyPoint &);
	void setTopRight(CartesianMyPoint &);
	void setBottomRight(CartesianMyPoint &);
	void setBottomLeft(CartesianMyPoint &);
	void setNormal();
	void setReflectivity(MyDouble);
	void setReflectivitySpectrum(ParametricEq &);
	void setReflectionSharpness(MyDouble);
	void setTransmisivity(MyDouble);
	void setTransmisivitySpectrum(ParametricEq &);
	void setSpectrum(long NoOfSpectrum, ParametricEq &);
	void copySpectrum(long NoOfSpectrum, ParametricEq & rs );

	void setSpectrumMode(long NoOfSpectrum, long mode);
	void setSpectrumCutOffFreq(long NoOfSpectrum, double freq);
	void setSpectrumGain(long NoOfSpectrum, double gain);
	void setSpectrumQ(long NoOfSpectrum, double Q);

 	bool checkIntersecting(CartesianMyPoint source, CartesianMyPoint listener);
 	CartesianMyPoint intersection(CartesianMyPoint source, CartesianMyPoint listener);

	CartesianMyPoint& getTopLeft() { return topLeft; }
	CartesianMyPoint& getTopRight() { return topRight; }
	CartesianMyPoint& getBottomLeft() {return bottomLeft; }
	CartesianMyPoint& getBottomRight() {return bottomRight; }
	CartesianMyPoint& getNormal() {return normal; }
	MyDouble&		getReflectivity() {return dReflectivity; }
	ParametricEq&	getReflectivitySpectrum() {return ReflectivitySpectrum; }
	MyDouble&		getReflectionSharpness() {return dReflectionSharpness; }
	MyDouble&		getTransmisivity() {return dTransmisivity; }
	ParametricEq& 	getTransmisivitySpectrum() {return TransmisivitySpectrum; }

	MyDouble	getReflectionSharpness() const {return dReflectionSharpness; }
	MyDouble	FilterIt(MyDouble sample,ParametricEq &spectrum);
	virtual 	ParametricEq &getSpectrum(long NoOfSpectrum);

protected:
	CartesianMyPoint topLeft;
	CartesianMyPoint topRight;
	CartesianMyPoint bottomRight;
	CartesianMyPoint bottomLeft;
	CartesianMyPoint normal;
	MyDouble	dReflectivity;			// may want to make these
	MyDouble	dTransmisivity;			// (psuedo) points so that they
	MyDouble	dReflectionSharpness;	// can be directionally biased
	MyDouble	dRoomModeSlope;			// set rapidity of roll off of
										// directional components - controls
										// mimicry of bass in real rooms




	ParametricEq ReflectivitySpectrum;
	ParametricEq TransmisivitySpectrum;
	ParametricEq OmniSpectrum;
	ParametricEq FrontBackSpectrum;
	ParametricEq LeftRightSpectrum;
	ParametricEq UpDownSpectrum;

};
//------------------------------------------------------------------------

class Room
{

friend class Reflector;
public:

	Room();
	virtual ~Room() {}
	virtual void setFrontWall(Wall);
	virtual void setBackWall(Wall);
	virtual void setRightWall(Wall);
	virtual void setLeftWall(Wall);
	virtual void setFloor(Wall);
	virtual void setCeiling(Wall);
	virtual void setLength(MyDouble);
	virtual void setWidth(MyDouble);
	virtual void setHeight(MyDouble);
	virtual void setTiltAngle(MyDouble);
	virtual void setTumbleAngle(MyDouble);
	virtual void setRotateAngle(MyDouble);
	virtual void setTurbulence(MyDouble);
	virtual void setAirAbsorbtion(MyDouble);
	virtual void setInsideListener(bool);
	virtual Wall& getFrontWall() { return frontWall ;}
	virtual Wall& getBackWall() { return backWall;}
	virtual Wall& getRightWall() { return rightWall;}
	virtual Wall& getLeftWall() { return leftWall;}
	virtual Wall& getFloor() { return Floor;}
	virtual Wall& getCeiling() { return Ceiling;}
	virtual MyDouble 	getLength();
	virtual MyDouble	getWidth();
	virtual MyDouble 	getHeight();
	virtual MyDouble&	getTiltAngle() { return dTiltAngle;}
	virtual MyDouble&	getTumbleAngle() { return dTumbleAngle;}
	virtual MyDouble&	getRotateAngle() { return dRotateAngle;}
	virtual MyDouble&	getTurbulence() { return dTurbulence;}
	bool& 				getInOrOut() {return insideSource;}

	bool			 	getOutsideOrInside(SourcePosition p);	// this checks if it is outside
	CartesianMyPoint 	getIntersection(CartesianMyPoint source, CartesianMyPoint listener);	// and this tells you where it hits the room walls
	bool 				HitBoundingBox(SourcePosition p);
	virtual void audioProcess(float **inputs, float **outputs, long sampleFrames, bool replacing, SourcePosition listener, SourcePosition source);
	virtual	MyDouble	computeDelay(MyDouble distance, MyDouble distancePerSample);
protected:
	ParametricEq	defaultSpectrumForReflectivity;
	ParametricEq	defaultSpectrumForTransmisivity;
	Wall 	frontWall;
	Wall 	backWall;
	Wall 	leftWall;
	Wall 	rightWall;
	Wall 	Floor;
	Wall 	Ceiling;
	Wall walls[6];


	MyDouble	dTiltAngle;	// These modify the field around the listener, not the room itself
	MyDouble	dTumbleAngle;
	MyDouble	dRotateAngle;
	MyDouble	dTurbulence;
	MyDouble	dAirAbsorbtion;
	MyDouble	dPreDelay;
	MyDouble	dDistancePerSample;

	bool		insideSource;
	bool		insideListener;
	long		lWhereOutside;			// holder for case statement to deal with whereabouts the source is when it is outside the room
										// used to select correct filter, amongst other possibilities
	CartesianMyPoint hitPoint;			// location where line from source to listener crosses a wall of the room

};
//------------------------------------------------------------------------

class ReflectedDSoundSource : public DSoundSource	// the derived class for movements of reflections
{
friend class Reflector;
public:
	virtual void moveSource(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel);
	virtual void moveSourceFHH(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel);
	virtual void positionSource(float **inputs, float **outputs, long sampleFrames, bool replacing, long channel);
	virtual void setWall(Wall &);
	virtual Wall getWall();
protected:
Wall wallReflection;
MyDouble dPreDelay;

};

//------------------------------------------------------------------------

class ReflectorProgram
{
friend class Reflector;
public:
	ReflectorProgram();
	~ReflectorProgram() {}


private:
	MyDouble	dLeftRightMix;
	float		fLeftRightMix;
	MyDouble	dALeft;
	MyDouble	dELeft;
	MyDouble	dWLeft;
	MyDouble	dXLeft;
	MyDouble	dYLeft;
	MyDouble	dZLeft;
	MyDouble	dWLeftOld;
	MyDouble	dXLeftOld;
	MyDouble	dYLeftOld;
	MyDouble	dZLeftOld;

	MyDouble	dOldPanAngle;

	MyDouble	dAzimuthLeft;
	MyDouble	dElevationLeft;
	MyDouble	dZerothLeft;
	MyDouble	dFirstLeft;
	MyDouble	dSecondLeft;
	MyDouble	dThirdtLeft;

	MyDouble	dVolumeLeft;
	MyDouble	dDistanceLeft;
	float		fDistanceLeft;
	MyDouble	dDistanceFactorLeft;
	MyDouble	dCentreLeft;
	MyDouble	dScalingLeft;
	float		fScalingLeft;

	MyDouble	dWetDry;
	float		fWetDry;
	MyDouble	dDryWet;
	float	fRoomWallNo;
	long	lRoomWallNo;

	float		fRoomWallReflectivity;
	MyDouble	dRoomWallReflectivity;
	float		fRoomWallTransmisivity;
	MyDouble	dRoomWallTransmisivity;
	float		fRoomWallReflectionSharpness;
	MyDouble	dRoomWallReflectionSharpness;

	float		fRoomWallReflectionLFCutoff;	// controls transition between point and diffuse reflection
	MyDouble	dRoomWallReflectionLFCutoff;
	float		fRoomWallReflectionLFLoss;		// which 'models' LF room mode
	MyDouble	dRoomWallReflectionLFLoss;

	float	fRoomWallReflectivityFilterMode;
	long	lRoomWallReflectivityFilterMode;
	float	fRoomWallReflectivityFilterFrequency;
	MyDouble	dRoomWallReflectivityFilterFrequency;
	float		fRoomWallReflectivityFilterQ;
	MyDouble	dRoomWallReflectivityFilterQ;
	float		fRoomWallReflectivityFilterGain;
	MyDouble	dRoomWallReflectivityFilterGain;


	float	fRoomWallTransmisivityFilterMode;
	long	lRoomWallTransmisivityFilterMode;
	float	fRoomWallTransmisivityFilterFrequency;
	MyDouble	dRoomWallTransmisivityFilterFrequency;
	float		fRoomWallTransmisivityFilterQ;
	MyDouble	dRoomWallTransmisivityFilterQ;
	float		fRoomWallTransmisivityFilterGain;
	MyDouble	dRoomWallTransmisivityFilterGain;

	float		fRoomLength;
	MyDouble	dRoomLength;
	float		fRoomWidth;
	MyDouble	dRoomWidth;

	float		fRoomHeight;
	MyDouble	dRoomHeight;
	float		fListenerX;
	MyDouble	dListenerX;
	float		fListenerY;
	MyDouble	dListenerY;
	float		fListenerZ;
	MyDouble	dListenerZ;

	char name[24];

	DSoundSource LeftSource;
	DirectDSoundSource DirectLeft;

};

//------------------------------------------------------------------------
class Reflector : public AudioEffectX
{
//	friend class Reflector;
public:
	Reflector(audioMasterCallback audioMaster);
	~Reflector();

	virtual void process(float **inputs, float **outputs, long sampleFrames);
	virtual void processReplacing(float **inputs, float **outputs, long sampleFrames);
	virtual void audioProcessing(float **inputs, float **outputs, long sampleFrames, bool replacing);
	virtual void setProgram(long program);
	virtual void setProgramName(char *name);
	virtual void getProgramName(char *name);
	virtual void setParameter(long index, float value);
	virtual float getParameter(long index);
	virtual void getParameterLabel(long index, char *label);
	virtual void getParameterDisplay(long index, char *text);
	virtual void getParameterName(long index, char *text);
	virtual bool getVendorString (char* text);
	virtual VstPlugCategory getPlugCategory();
	virtual MyDouble	gainLaw (MyDouble	gain);
	virtual bool getOutputProperties (long index, VstPinProperties* properties);
	virtual bool getInputProperties (long index, VstPinProperties* properties);
    virtual void init ();
    virtual void suspend ();
	virtual void resume();
	virtual long getHostNo();
	virtual MyDouble	interpLinear (MyDouble interp_frac, MyDouble sample_point1, MyDouble	sample_point2);
	virtual MyDouble	interpSpline (MyDouble interp_frac, MyDouble sample_point0, MyDouble	sample_point1, MyDouble	sample_point2, MyDouble	sample_point3);
	virtual	MyDouble	computeDelay(MyDouble distance);

  	long  lHostNo;
protected:
	bool	createbuffer_d(MyDouble *buffer, MyDouble size, long oldLength);
	bool	updateRoom;
	MyDouble	*ControlBufferLeft;
	long	lControWriteOffset;
	long 	lControlWritePosLeft;			// the current sample input position - same for both ControlBuffers
	long 	lControlReadPosLeft;			// the current sample output position for the left ControlBuffer
	long	lControlReadReferenceLeft;
	long 	lControlBufferLengthLeft;


	long 	lControlBufferTopLeft;

	MyDouble 	**buffer;
	MyDouble	*BufferLeft;
	Room		cRoom;
	DSoundSource LeftSource;
	DirectDSoundSource DirectLeft;

	ReflectedDSoundSource LeftReflectionRoom;
	ReflectedDSoundSource RightReflectionRoom;
	ReflectedDSoundSource BackReflectionRoom;
	ReflectedDSoundSource FrontReflectionRoom;
	ReflectedDSoundSource CeilingReflectionRoom;
	ReflectedDSoundSource FloorReflectionRoom;

	long 	lBufferLengthLeft;

	long 	lBufferTopLeft;
	long 	lOldBufferLengthLeft;
	MyDouble 	dSizeInSecondsLeft;


	MyDouble	dSampleRate;
	MyDouble	dNyquist;
	MyDouble	dDistancePerSample;
	MyDouble	dDelayLeftOld;

	MyDouble	dWetDry;

private:

	ReflectorProgram *programs;
	MyDouble	dLeftRightMix;
	float		fLeftRightMix;

	MyDouble	dOldPanAngle;
	MyDouble	dAzimuthLeft;
	MyDouble	dElevationLeft;
	MyDouble	dZerothLeft;
	MyDouble	dFirstLeft;
	MyDouble	dSecondLeft;
	MyDouble	dThirdtLeft;

	MyDouble	dVolumeLeft;
	MyDouble	dDistanceLeft;
	float		fDistanceLeft;
	MyDouble	dDistanceFactorLeft;
	MyDouble	dCentreLeft;
	MyDouble	dScalingLeft;
	float		fScalingLeft;
	float		fWetDry;
	MyDouble	dDryWet;

	float		fRoomWallNo;
	long		lRoomWallNo;
	float		fRoomWallReflectivity;
	MyDouble	dRoomWallReflectivity;
	float		fRoomWallTransmisivity;
	MyDouble	dRoomWallTransmisivity;
	float		fRoomWallReflectionSharpness;
	MyDouble	dRoomWallReflectionSharpness;
	float		fRoomWallReflectivityFilterMode;
	long		lRoomWallReflectivityFilterMode;
	float		fRoomWallReflectivityFilterFrequency;
	MyDouble	dRoomWallReflectivityFilterFrequency;
	float		fRoomWallReflectivityFilterQ;
	MyDouble	dRoomWallReflectivityFilterQ;
	float		fRoomWallReflectivityFilterGain;
	MyDouble	dRoomWallReflectivityFilterGain;

	float		fRoomWallReflectionLFCutoff;	// controls transition between point and diffuse reflection
	MyDouble	dRoomWallReflectionLFCutoff;
	float		fRoomWallReflectionLFLoss;		// which 'models' LF room mode
	MyDouble	dRoomWallReflectionLFLoss;



	float		fRoomWallTransmisivityFilterMode;
	long		lRoomWallTransmisivityFilterMode;
	float		fRoomWallTransmisivityFilterFrequency;
	MyDouble	dRoomWallTransmisivityFilterFrequency;
	float		fRoomWallTransmisivityFilterQ;
	MyDouble	dRoomWallTransmisivityFilterQ;
	float		fRoomWallTransmisivityFilterGain;
	MyDouble	dRoomWallTransmisivityFilterGain;
	float		fComplexityLevel;
	long		lComplexityLevel;
	MyDouble	dRoomOffsetLeftY;
	MyDouble	dRoomOffsetRightY;
	MyDouble	dRoomOffsetUpZ;
	MyDouble	dRoomOffsetDownZ;
	MyDouble	dRoomOffsetFrontX;
	MyDouble	dRoomOffsetBackX;

	CartesianMyPoint listener;			// location where line from source to listener crosses a wall of the room

	float		fRoomLength;
	MyDouble	dRoomLength;
	float		fRoomWidth;
	MyDouble	dRoomWidth;
	float		fRoomHeight;
	MyDouble	dRoomHeight;
	float		fListenerX;
	MyDouble	dListenerX;
	float		fListenerY;
	MyDouble	dListenerY;
	float		fListenerZ;
	MyDouble	dListenerZ;

	long lNumOutputs;

	ParametricEq	TempSpectrum;

	bool		insideSource;
	bool		insideListener;

	char sComplexityStrings[kNoOfLevels][10] ;
	char programName[24];
};
// Utility routines
MyDouble	computePanAngle(MyDouble	LR, MyDouble	FB);
CartesianMyPoint  CrossProduct(CartesianMyPoint P0, CartesianMyPoint P1, CartesianMyPoint P2);
CartesianMyPoint  normalToPlane(CartesianMyPoint P0, CartesianMyPoint P1, CartesianMyPoint P2 );
CartesianMyPoint IntersectLinePlane(CartesianMyPoint org, CartesianMyPoint dir, CartesianMyPoint p, CartesianMyPoint normal);	//p is a point on the plane
double DotProduct(CartesianMyPoint P0,CartesianMyPoint P1,CartesianMyPoint P2);

// #endif
