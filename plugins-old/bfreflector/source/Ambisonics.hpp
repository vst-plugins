/*-----------------------------------------------------------------------------

� 2005 Dogma Design , All Rights Reserved
	standard Ambisonic defines
-----------------------------------------------------------------------------*/
#ifndef __Ambisonics_H
#define __Ambisonics_H
#endif

#ifndef __AudioEffect__
#include "AudioEffect.hpp"	 // Version 1.0 base class AudioEffect
#endif

#ifndef __aeffectx__
#include "aeffectx.h"		 // Version 2.0 'C' extensions and structures
#endif

enum bFormChannelFirst
{
	kW = 0,
	kX,
	kY,
	kZ

};
enum bFormChannelSecond
{
	kR = 4,
	kS,
	kT,
	kU,
	kV
};
enum bFormChannelThird
{
	kK = 9,
	kL,
	kM,
	kN,
	kO,
	kP,
	kQ

};
enum bFormChannelSecondHorizontalOnly
{
	kUh = 4,
	kVh
};
enum bFormChannelThirdHorizontalOnly
{
	kPh = 6,
	kQh

};

/*
#ifndef kPI
#define kPI    3.14159265358979323846
#endif

#ifndef k2PI
#define k2PI    6.283185
#endif

#ifndef kDtoR
#define const double kDtoR = kPI/180;
#endif

#ifndef kRtoD
#define const double kRtoD = 180/kPI;
#endif

#ifndef kLog2
#define const double kLog2 = 0.6931471805599453;
#endif

#ifndef kdBFactor
#define const double kdBFactor = ((-6.0)/20.0);	// saves later maths operations in converting fDistanceFactorLeft into
										// dB's in range 0 to -maxloss
#endif

#ifndef k6dBPoint
#define const float k6dBPoint = 1 - (0.5/(20/6));	// allows for left+right without overload
										// this is a -6dB amount if gainlaw is set for
										// 20dB drop over first part of scale
#endif
*/
VstSpeakerProperties W_channel;
VstSpeakerProperties X_channel;
VstSpeakerProperties Y_channel;
VstSpeakerProperties Z_channel;

VstSpeakerProperties R_channel;
VstSpeakerProperties S_channel;
VstSpeakerProperties T_channel;
VstSpeakerProperties U_channel;
VstSpeakerProperties V_channel;

VstSpeakerProperties K_channel;
VstSpeakerProperties L_channel;
VstSpeakerProperties M_channel;
VstSpeakerProperties N_channel;
VstSpeakerProperties O_channel;
VstSpeakerProperties P_channel;
VstSpeakerProperties Q_channel; 

VstSpeakerArrangement bFormatFirstOrder;