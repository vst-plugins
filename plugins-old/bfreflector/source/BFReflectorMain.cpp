/*-----------------------------------------------------------------------------
The Ambisonic Reflector Project
� 2004 Dave Malham, Dogma Design

This program is released as free software; You redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program will be distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

dmalham@users.sourceforge.net
-----------------------------------------------------------------------------*/

#include <AudioEffect.cpp>
#include <audioeffectx.h>
#include <audioeffectx.cpp>


#include "BFReflector.hpp"

static AudioEffect *effect = 0;
bool oome = false;

#if MAC
#pragma export on
#endif

// prototype of the export function main
#if BEOS
	#define main main_plugin
	extern "C" __declspec(dllexport) AEffect *main  (audioMasterCallback audioMaster);
#elif MACX
	#define main main_macho
	extern "C" AEffect *main_macho (audioMasterCallback audioMaster);
#elif __GNUC__ 
	#define main main_plugin
	AEffect* main_plugin (audioMasterCallback audioMaster) asm ("main");
#else
	#define main main_plugin
	AEffect *main_plugin (audioMasterCallback audioMaster);
#endif

AEffect *main(audioMasterCallback audioMaster)
{
	// get vst version
	//if (!audioMaster (0, audioMasterVersion, 0, 0, 0, 0)) return 0;  // old version
	effect = new Reflector (audioMaster);
	if (!effect)
		return 0;
	if (oome)
	{
		delete effect;
		return 0;
	}
	return effect->getAeffect ();
}

#if MAC
#pragma export off
#endif


#if WIN32
#include <windows.h>
void* hInstance;
BOOL WINAPI DllMain (HINSTANCE hInst, DWORD dwReason, LPVOID lpvReserved)
{
	hInstance = hInst;
	return 1;
}
#endif
