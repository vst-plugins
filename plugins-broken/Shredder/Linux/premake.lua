
project.name = "Shredder"
project.bindir = "../../../bin/"
project.libdir = "../../../bin/"

project.configs = { "Release", "Debug" }

package = newpackage()
package.name = "Shredder"
package.kind = "dll"
package.language = "c++"
package.linkflags = { "static-runtime" }

package.config["Release"].objdir = "intermediate/Release"
package.config["Debug"].objdir   = "intermediate/Debug"

package.config["Release"].target = "Shredder"
package.config["Debug"].target   = "Shredder_debug"

package.config["Release"].defines      = { "LINUX=1", "NDEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1", "JucePlugin_Build_VST=1" };
package.config["Release"].buildoptions = { "-O2 -s -fvisibility=hidden -msse -ffast-math -static" }
package.config["Release"].links        = { "freetype", "pthread", "rt", "X11", "GL", "juce" }

package.config["Debug"].defines        = { "LINUX=1", "DEBUG=1", "_DEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1", "JucePlugin_Build_VST=1" };
package.config["Debug"].buildoptions   = { "-O0 -ggdb -static" }
package.config["Debug"].links          = { "freetype", "pthread", "rt", "X11", "GL", "juce_debug" }

package.includepaths = {
    "../src",
    "../src/engine",
    "/usr/include",
    "/usr/include/freetype2",
    "../../../libs/juce/source",
    "..",
    "../About",
    "../Source",
    "../Source/Hydrogen",
    "../Source/Plugin",
    "../Source/UI"
}

package.libpaths = {
    "/usr/X11R6/lib/",
    "/usr/lib/",
    "../../../libs/"
}

package.files = {
    matchfiles (
        "../About/*.cpp",
        "../Source/Hydrogen/*.cpp",
        "../Source/Plugin/*.cpp",
        "../Source/UI/*.cpp",
        "../../../libs/juce/source/extras/audio plugins/wrapper/VST/juce_VST_Wrapper.cpp"
    )
}
