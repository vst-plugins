#!/bin/bash

for i in `find src -name *.cpp` `find src -name *.h`
do
  echo "Processing file: $i"
  sed 's/\t/    /g' $i > no_tabs.txt
  mv no_tabs.txt $i
done