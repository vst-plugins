
project.name = "HybridReverb2"
project.bindir = "../../../bin/"
project.libdir = "../../../bin/"

project.configs = { "Release", "Debug" }

package = newpackage()
package.name = "HybridReverb2"
package.kind = "dll"
package.language = "c++"
package.linkflags = { "static-runtime" }

package.config["Release"].objdir = "intermediate/Release"
package.config["Debug"].objdir   = "intermediate/Debug"

package.config["Release"].target = "HybridReverb2"
package.config["Debug"].target   = "HybridReverb2_debug"

package.config["Release"].defines      = { "LINUX=1", "NDEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1", "HYBRIDREVERB2_VST_PLUGIN=1" };
package.config["Release"].buildoptions = { "-O2 -s -msse -fvisibility=hidden -fomit-frame-pointer -funroll-loops -fopenmp -static `pkg-config fftw3f --cflags`" }
package.config["Release"].links        = { "freetype", "pthread", "rt", "X11", "GL", "gomp", "juce" }

package.config["Debug"].defines        = { "LINUX=1", "DEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1", "HYBRIDREVERB2_VST_PLUGIN=1" };
package.config["Debug"].buildoptions   = { "-O0 -ggdb -fopenmp -static `pkg-config fftw3f --cflags`" }
package.config["Debug"].links          = { "freetype", "pthread", "rt", "X11", "GL", "gomp", "juce_debug" }

package.includepaths = {
    "../src",
    "/usr/include",
    "/usr/include/freetype2",
    "../../../libs/juce/source",
    "../../../libs/juce/source/extras/audio plugins/",
}

package.libpaths = {
    "/usr/X11R6/lib/",
    "/usr/lib/",
    "../../../libs/"
}

package.linkoptions = {
"`pkg-config fftw3f --libs`"
}

package.files = {
    matchfiles (
        "../src/*.cpp",
        "../src/gui/*.cpp",
        "../src/libHybridConv/*.c"
    )
}
